insert into jtv_saison values(1,"2015-01-01","2015-06-30",1,1);
insert into jtv_saison values(2,"2016-01-01","2016-06-30",1,1);
insert into jtv_saison values(3,"2015-01-01","2015-06-30",2,1);
insert into jtv_saison values(4,"2016-01-01","2016-06-30",2,1);

update jtv_user set usr_sai_last_edit = 1 where usr_id = 1;

insert into jtv_formation values(1,"Moniteur",5,1);
insert into jtv_formation values(2,"Directeur",5,1);
insert into jtv_formation values(3,"Moniteur remise à niveau",5,1);

insert into jtv_groupe values(1,"Groupe 1",1);
insert into jtv_groupe values(2,"Groupe 2",1);
insert into jtv_groupe values(3,"Groupe 3",1);
insert into jtv_groupe values(4,"Groupe 4",1);

/* Moniteur */
insert into jtv_tireur values(1,"Ane","Arbre",1,"1991-01-01","-","-","-","nom@prenom.ch","-","-",1,1);
insert into jtv_tireur values(2,"Aigle","Gui",1,"1993-01-30","Rue de la Myrte","1212","Carotte","Aigle@gmail.ch","0224562314","0769523652",1,1);
insert into jtv_tireur values(3,"Lori","Erable",0,"1996-06-15","Rue du Citron","1213","Oignon","Lori@gmail.ch","0221112233","0761234567",0,1);
insert into jtv_tireur values(4,"Marouette","Choux",1,"1995-05-03","Rue du Marron","1213","Patate","Marouette@gmail.ch","0225566998","0769856211",1,1);
insert into jtv_tireur values(5,"Noddi","Aralie",1,"1994-10-10","Rue du melon","1214","Endive","Noddi@gmail.ch","0228545454","0767458745",1,1);


insert into jtv_user values(2,"Aigle","123","123",0,1,2,1,2);
insert into jtv_user values(3,"Lori","123","234",0,2,2,1,3);
insert into jtv_user values(4,"Marouette","123","456",0,2,2,1,4);
insert into jtv_user values(5,"Noddi","123","789",0,3,2,1,5);

insert into jtv_moniteur values(1);
insert into jtv_moniteur values(2);
insert into jtv_moniteur values(3);
insert into jtv_moniteur values(4);
insert into jtv_moniteur values(5);

/* Jeune */
insert into jtv_tireur values(6,"Barbu","Cactus",1,"2001-01-03","Rue de l'Avocat","1214","Haricot","Barbu@hotmail.com","0229658745","0766523698",0,1);
insert into jtv_tireur values(7,"Batara","Aneth",0,"2001-02-09","Rue de la Cerise","1215","Laitue","Batara@hotmail.ch","0221414123","0765236874",0,1);
insert into jtv_tireur values(8,"Chouette","Iris",1,"2000-03-10","Rue de l'Ananas","1215","Navet","Chouette@hotmail.ch","0229159523","0763652147",0,1);
insert into jtv_tireur values(9,"Colibri","Frêne",0,"2000-04-22","Rue de la Fraise","1216","Basilic","Colibri@outlook.ch","0226325874","0766541965",0,1);
insert into jtv_tireur values(10,"Colin","Figuier",1,"1999-05-15","Rue du Kiwi","1216","Poireau","Colin@outlook.ch","0229562736","0769563175",1,1);
insert into jtv_tireur values(11,"Coq","Clivie",1,"1999-07-06","Rue de la Pêche","1217","Poivron","Coq@outlook.ch","0226677889","0786655448",1,1);
insert into jtv_tireur values(12,"Fauvette","Célosie",0,"1998-08-17","Rue de l'Amande","1217","Taro","Fauvette@gmail.ch","0223758965","079236547",1,1);
insert into jtv_tireur values(13,"Poule","Ortie",1,"1998-10-20","Rue du Coing","1218","Courge","Poule@gmail.ch","0221213795","07956841036",1,1);
insert into jtv_tireur values(14,"Onoré","Lotus",0,"1997-11-30","Rue de la Pomme","1218","Chou","Onoré@gmail.ch","0220123658","0760412589",1,1);
insert into jtv_tireur values(15,"Sibia","Oxalide",1,"1997-12-03","Rue de l'Olive","1219","Aubergine","Sibia@gmail.ch","0220457896","079021456",1,1);

insert into jtv_user values(6,"user","123","tmp1",0,null,1,1,null);
insert into jtv_user values(7,"moniteur","123","tmp2",0,null,2,1,null);
insert into jtv_user values(8,"directeur","123","tmp3", 0,null,3,1,null);
insert into jtv_user values(9,"barbu","123","tmp4",0, null,1,1,6);
insert into jtv_user values(10,"batara","123","tmp5",0, null,1,1,7);
insert into jtv_user values(11,"chouette","123","tmp6",0,null,1,1,8);
insert into jtv_user values(12,"colibri","123","tmp7",0,null,1,1,9);
insert into jtv_user values(13,"colin","123","tmp8",0,null,1,1,10);
insert into jtv_user values(14,"coq","123","tmp9",0,null,1,1,11);
insert into jtv_user values(15,"fauvette","123","tmp10",0,null,1,1,12);
insert into jtv_user values(16,"poule","123","tmp11",0,null,1,1,13);
insert into jtv_user values(17,"onore","123","tmp12",0,null,1,1,14);
insert into jtv_user values(18,"sibia","123","tmp13",0,null,1,1,15);


insert into jtv_jeune values(6,"Barbu","Myrte","","","","0781245789");
insert into jtv_jeune values(7,"Batara","Muget","","","","0780236547");
insert into jtv_jeune values(8,"Chouette","Glycine","","","","0780123912");
insert into jtv_jeune values(9,"Colibri","Crocus","","","","0789995555");
insert into jtv_jeune values(10,"Colin","Bouleau","","","","0786633220");
insert into jtv_jeune values(11,"Coq","Lupin","Rue de la Lime","1219","Gingembre","0790022335");
insert into jtv_jeune values(12,"Fauvette","Pavot","Rue de la Framboise","1220","Fenouil","0780739156");
insert into jtv_jeune values(13,"Poule","Pensée","Rue de du Raisin","1220","Courgette","0760123456");
insert into jtv_jeune values(14,"Onoré","Persil","Rue du Litichi","1221","Brocoli","0760021458");
insert into jtv_jeune values(15,"Sibia","Tulipe","Rue du Yuzu","1221","Manioc","0780258651");

insert into jtv_arme values(10101010,"Fusil",1,1);
insert into jtv_arme values(10101011,"Fusil",1,1);
insert into jtv_arme values(10101012,"Fusil",1,1);
insert into jtv_arme values(10101013,"Fusil",1,1);
insert into jtv_arme values(20202020,"Fusil",1,1);
insert into jtv_arme values(20202021,"Fusil",1,1);
insert into jtv_arme values(20202022,"Fusil",1,1);
insert into jtv_arme values(20202023,"Fusil",1,1);
insert into jtv_arme values(30303030,"Fusil",1,1);
insert into jtv_arme values(30303031,"Fusil",1,1);
insert into jtv_arme values(30303032,"Fusil",1,1);
insert into jtv_arme values(30303033,"Fusil",1,1);
insert into jtv_arme values(40404040,"Fusil",1,1);

insert into jtv_arme values(90909090,"Pistolet",2,1);
insert into jtv_arme values(90909091,"Pistolet",2,1);
insert into jtv_arme values(90909092,"Pistolet",2,1);
insert into jtv_arme values(90909093,"Pistolet",2,1);
insert into jtv_arme values(80808080,"Pistolet",2,1);
insert into jtv_arme values(80808081,"Pistolet",2,1);
insert into jtv_arme values(80808082,"Pistolet",2,1);
insert into jtv_arme values(80808083,"Pistolet",2,1);
insert into jtv_arme values(70707070,"Pistolet",2,1);
insert into jtv_arme values(70707071,"Pistolet",2,1);

/* 
tot coups = nb coup x séquence 
tot point = nb coup x séquence x valeur cible
*/

/* Saison 1 */
insert into jtv_programme values(1,"Jour 1","Tire initiale",10,50,1,1);
insert into jtv_programme values(2,"Jour 2","Pratique",10,45,1,1);
insert into jtv_programme values(3,"Jour 3","",20,150,1,1);
insert into jtv_programme values(4,"Jour 4","Préparation concours",15,125,1,1);
insert into jtv_programme values(5,"Jour 5","Entrainement",15,150,1,1);

/* Saison 2 */
insert into jtv_programme values(6,"Jour 1","Tire initiale",20,200,2,1);
insert into jtv_programme values(7,"Jour 2","Pratique",10,70,2,1);
insert into jtv_programme values(8,"Jour 3","Préparation concours",15,125,2,1);
insert into jtv_programme values(9,"Jour 4","Entrainement",15,65,2,1);
insert into jtv_programme values(10,"Jour 5","",20,100,2,1);

/* Saison 3 */
insert into jtv_programme values(11,"Jour 1","",10,45,3,1);
insert into jtv_programme values(12,"Jour 2","Tire initiale",20,200,3,1);
insert into jtv_programme values(13,"Jour 3","Pratique",10,100,3,1);
insert into jtv_programme values(14,"Jour 4","Préparation concours",20,90,3,1);
insert into jtv_programme values(15,"Jour 5","Entrainement",10,45,3,1);

/* Saison 4 */
insert into jtv_programme values(16,"Jour 1","Tire initiale",15,125,4,1);
insert into jtv_programme values(17,"Jour 2","Pratique",20,80,4,1);
insert into jtv_programme values(18,"Jour 3","Préparation concours",15,75,4,1);
insert into jtv_programme values(19,"Jour 4","",10,10,4,1);
insert into jtv_programme values(20,"Jour 5","Entrainement",10,10,4,1);

/* Saison 1 */
insert into jtv_cours values(1,"Cours JT 1","2015-01-12","08:00","14:00",1,1,"00:15","00:30");
insert into jtv_cours values(2,"Cours JT 2","2015-01-19","08:00","14:00",1,1,"00:15","00:30");
insert into jtv_cours values(3,"Cours JT 3","2015-01-26","08:00","14:00",1,1,"00:15","00:30");
insert into jtv_cours values(4,"Cours JT 4","2015-02-05","08:00","14:00",1,1,"00:15","00:30");
insert into jtv_cours values(5,"Cours JT 5","2015-02-12","08:00","14:00",1,1,"00:15","00:30");
/* Saison 2 */
insert into jtv_cours values(6,"Cours JT 1","2016-01-12","08:00","14:00",2,1,"00:15","00:30");
insert into jtv_cours values(7,"Cours JT 2","2016-01-19","08:00","14:00",2,1,"00:15","00:30");
insert into jtv_cours values(8,"Cours JT 3","2016-01-19","08:00","14:00",2,1,"00:15","00:30");
insert into jtv_cours values(9,"Cours JT 4","2016-01-26","08:00","14:00",2,1,"00:15","00:30");
insert into jtv_cours values(10,"Cours JT 5","2016-02-12","08:00","14:00",2,1,"00:15","00:30");
/* Saison 3 */
insert into jtv_cours values(11,"Cours JT 1","2015-01-12","08:00","14:00",3,1,"00:15","00:30");
insert into jtv_cours values(12,"Cours JT 2","2015-01-19","08:00","14:00",3,1,"00:15","00:30");
insert into jtv_cours values(13,"Cours JT 3","2015-01-26","08:00","14:00",3,1,"00:15","00:30");
insert into jtv_cours values(14,"Cours JT 4","2015-02-05","08:00","14:00",3,1,"00:15","00:30");
insert into jtv_cours values(15,"Cours JT 5","2015-02-12","08:00","14:00",3,1,"00:15","00:30");
/* Saison 4 */
insert into jtv_cours values(16,"Cours JT 1","2016-01-12","08:00","14:00",4,1,"00:15","00:30");
insert into jtv_cours values(17,"Cours JT 2","2016-01-19","08:00","14:00",4,1,"00:15","00:30");
insert into jtv_cours values(18,"Cours JT 3","2016-01-26","08:00","14:00",4,1,"00:15","00:30");
insert into jtv_cours values(19,"Cours JT 4","2016-02-05","08:00","14:00",4,1,"00:15","00:30");
insert into jtv_cours values(20,"Cours JT 5","2016-02-12","08:00","14:00",4,1,"00:15","00:30");

insert into jtv_stock values(1,1000,"2015-01-01",1);
insert into jtv_stock values(2,1000,"2016-01-01",2);
insert into jtv_stock values(3,1000,"2015-01-01",3);
insert into jtv_stock values(4,1000,"2016-01-01",4);

/* Saison 1 Fusil */
insert into jtv_inscription values(1,10101010,1,6,"",1, null);
insert into jtv_inscription values(1,10101011,1,7,"",1, null);
insert into jtv_inscription values(1,null,1,8,"",1, null);
insert into jtv_inscription values(2,10101012,1,9,"",1, null);
insert into jtv_inscription values(2,20202020,1,10,"",1, null);
insert into jtv_inscription values(2,20202021,1,11,"",1, null);
insert into jtv_inscription values(2,20202022,1,12,"",1, null);
insert into jtv_inscription values(3,20202023,1,13,"",1, null);
insert into jtv_inscription values(3,30303030,1,14,"",1, null);
/* Saison 2 Fusil */
insert into jtv_inscription values(1,10101010,2,6,"",1, null);
insert into jtv_inscription values(1,10101011,2,7,"",1, null);
insert into jtv_inscription values(1,null,2,8,"",1, null);
insert into jtv_inscription values(2,10101012,2,9,"",1, null);
insert into jtv_inscription values(2,20202020,2,10,"",1, null);
insert into jtv_inscription values(2,20202021,2,11,"",1, null);
insert into jtv_inscription values(2,20202022,2,12,"",1, null);
insert into jtv_inscription values(3,20202023,2,13,"",1, null);
insert into jtv_inscription values(3,30303030,2,14,"",1, null);
/* Saison 3 Pistolet */
insert into jtv_inscription values(1,90909090,3,6,"",1, null);
insert into jtv_inscription values(1,90909091,3,7,"",1, null);
insert into jtv_inscription values(1,null,3,8,"",1, null);
insert into jtv_inscription values(2,90909092,3,9,"",1, null);
insert into jtv_inscription values(2,80808080,3,10,"",1, null);
insert into jtv_inscription values(2,80808081,3,11,"",1, null);
insert into jtv_inscription values(2,80808082,3,12,"",1, null);
insert into jtv_inscription values(3,80808083,3,13,"",1, null);
insert into jtv_inscription values(3,null,3,14,"",1, null);
/* Saison 4 Pistolet */
insert into jtv_inscription values(null,null,4,2,"moniteur",1, null);
insert into jtv_inscription values(null,null,4,5,"",1, null);
insert into jtv_inscription values(1,90909090,4,6,"",1, null);
insert into jtv_inscription values(1,90909091,4,7,"",1, null);
insert into jtv_inscription values(1,null,4,8,"",1, null);
insert into jtv_inscription values(2,90909092,4,9,"",1, null);
insert into jtv_inscription values(2,80808080,4,10,"",1, null);
insert into jtv_inscription values(2,80808081,4,11,"",1, null);
insert into jtv_inscription values(2,80808082,4,12,"",1, null);
insert into jtv_inscription values(3,80808083,4,13,"",1, null);
insert into jtv_inscription values(3,null,4,14,"",1, null);

/* 
tot coups = nb coup x séquence 
tot point = nb coup x séquence x valeur cible
*/

insert into jtv_passe values(1,1,5,1,1,1);
insert into jtv_passe values(2,1,5,1,1,1);

insert into jtv_passe values(1,1,5,1,2,1);
insert into jtv_passe values(2,1,5,1,2,4);

insert into jtv_passe values(1,2,5,1,3,1);
insert into jtv_passe values(2,2,5,1,3,2);

insert into jtv_passe values(1,1,5,1,4,1);
insert into jtv_passe values(2,2,5,1,4,2);

insert into jtv_passe values(1,1,5,1,5,2);
insert into jtv_passe values(2,2,5,1,5,2);

insert into jtv_passe values(1,2,5,1,6,2);
insert into jtv_passe values(2,2,5,1,6,2);

insert into jtv_passe values(1,1,5,1,7,2);
insert into jtv_passe values(2,1,5,1,7,4);

insert into jtv_passe values(1,1,5,1,8,1);
insert into jtv_passe values(2,2,5,1,8,2);

insert into jtv_passe values(1,1,5,1,9,1);
insert into jtv_passe values(2,2,5,1,9,4);

insert into jtv_passe values(1,2,5,1,10,1);
insert into jtv_passe values(2,2,5,1,10,1);

insert into jtv_passe values(1,1,5,1,11,1);
insert into jtv_passe values(2,1,5,1,11,4);

insert into jtv_passe values(1,2,5,1,12,2);
insert into jtv_passe values(2,2,5,1,12,2);

insert into jtv_passe values(1,1,5,1,13,2);
insert into jtv_passe values(2,1,5,1,13,2);

insert into jtv_passe values(1,2,5,1,14,1);
insert into jtv_passe values(2,2,5,1,14,4);

insert into jtv_passe values(1,1,5,1,15,1);
insert into jtv_passe values(2,1,5,1,15,4);

insert into jtv_passe values(1,1,5,1,16,1);
insert into jtv_passe values(2,2,5,1,16,2);

insert into jtv_passe values(1,2,5,1,17,4);
insert into jtv_passe values(2,2,5,1,17,4);

insert into jtv_passe values(1,1,5,1,18,1);
insert into jtv_passe values(2,1,5,1,18,2);

insert into jtv_participe values(5,0,0,"","",1,4,6,16,1);
insert into jtv_participe values(6,0,0,"","",1,4,7,16,1);
insert into jtv_participe values(7,0,0,"","",2,4,8,16,1);
insert into jtv_participe values(8,0,0,"","",2,4,9,16,1);

insert into jtv_participe values(10,0,1,"","",1,4,6,17,2);
insert into jtv_participe values(5,0,0,"","",1,4,7,17,2);
insert into jtv_participe values(2,0,2,"","",null,4,8,17,2);
insert into jtv_participe values(1,0,0,"","",2,4,9,17,2);

insert into jtv_participe values(6,0,3,"","",null,4,6,18,3);
insert into jtv_participe values(4,0,0,"","",2,4,7,18,3);
insert into jtv_participe values(2,0,1,"","",1,4,8,18,3);
insert into jtv_participe values(3,0,0,"","",1,4,9,18,3);
                  
insert into jtv_participe values(8,5,1,"","",1,4,6,19,4);
insert into jtv_participe values(9,0,0,"","",1,4,7,19,4);
insert into jtv_participe values(1,0,0,"","",2,4,8,19,4);
insert into jtv_participe values(0,0,2,"","",null,4,9,19,4);
                                                             


insert into jtv_coursProgrammeAnnee values(16,16,1);
insert into jtv_coursProgrammeAnnee values(16,16,2);
insert into jtv_coursProgrammeAnnee values(16,17,3);

insert into jtv_coursProgrammeAnnee values(17,16,1);
insert into jtv_coursProgrammeAnnee values(17,16,2);
insert into jtv_coursProgrammeAnnee values(17,17,3);
insert into jtv_coursProgrammeAnnee values(17,17,4);

insert into jtv_coursProgrammeAnnee values(18,18,1);
insert into jtv_coursProgrammeAnnee values(18,18,2);
insert into jtv_coursProgrammeAnnee values(18,19,3);
insert into jtv_coursProgrammeAnnee values(18,19,4);

insert into jtv_coursProgrammeAnnee values(19,20,1);
insert into jtv_coursProgrammeAnnee values(19,20,2);
insert into jtv_coursProgrammeAnnee values(19,20,3);

insert into jtv_coursProgrammeAnnee values(20,20,1);
insert into jtv_coursProgrammeAnnee values(20,20,2);
insert into jtv_coursProgrammeAnnee values(20,20,3);

insert into jtv_coursPostes values(16,1);
insert into jtv_coursPostes values(16,2);
insert into jtv_coursPostes values(17,1);
insert into jtv_coursPostes values(17,2);
insert into jtv_coursPostes values(18,1);
insert into jtv_coursPostes values(18,2);
insert into jtv_coursPostes values(18,3);
insert into jtv_coursPostes values(19,1);
insert into jtv_coursPostes values(19,2);
insert into jtv_coursPostes values(20,1);
insert into jtv_coursPostes values(20,2);
insert into jtv_coursPostes values(20,3);

insert into jtv_horaire values("08:00","09:55",1,1,16);
insert into jtv_horaire values("10:00","12:00",1,2,16);
insert into jtv_horaire values("08:00","09:55",2,2,16);
insert into jtv_horaire values("10:00","12:00",2,1,16);

insert into jtv_horaire values("08:00","09:55",1,1,17);
insert into jtv_horaire values("10:00","12:00",1,2,17);
insert into jtv_horaire values("08:00","09:55",2,2,17);
insert into jtv_horaire values("10:00","12:00",2,1,17);

insert into jtv_horaire values("08:00","09:55",1,1,18);
insert into jtv_horaire values("10:00","10:55",1,2,18);
insert into jtv_horaire values("11:00","12:00",1,3,18);
insert into jtv_horaire values("08:00","09:55",2,2,18);
insert into jtv_horaire values("10:00","10:55",2,3,18);
insert into jtv_horaire values("11:00","12:00",2,1,18);
insert into jtv_horaire values("08:00","09:55",3,3,18);
insert into jtv_horaire values("10:00","10:55",3,1,18);
insert into jtv_horaire values("11:00","12:00",3,2,18);

insert into jtv_horaire values("08:00","09:55",1,1,19);
insert into jtv_horaire values("10:00","12:00",1,2,19);
insert into jtv_horaire values("08:00","09:55",2,2,19);
insert into jtv_horaire values("10:00","12:00",2,1,19);

insert into jtv_horaire values("08:00","09:55",1,1,20);
insert into jtv_horaire values("10:00","10:55",1,2,20);
insert into jtv_horaire values("11:00","12:00",1,3,20);
insert into jtv_horaire values("08:00","09:55",2,2,20);
insert into jtv_horaire values("10:00","10:55",2,3,20);
insert into jtv_horaire values("11:00","12:00",2,1,20);
insert into jtv_horaire values("08:00","09:55",3,3,20);
insert into jtv_horaire values("10:00","10:55",3,1,20);
insert into jtv_horaire values("11:00","12:00",3,2,20);

insert into jtv_concours values(1,"Tir des effeuilles","2016-04-05",5.0,0,1,null);
insert into jtv_concours values(2,"Tir du diable","2016-03-10",10.0,1,1,null);
insert into jtv_concours values(3,"Tir des amoureux","2016-05-15",25.0,1,1,null);

insert into jtv_concours values(4,"Tir du printemps","2015-01-10",0,0,1,null);
insert into jtv_concours values(5,"Tir du salon","2015-02-11",45,1,1,null);
insert into jtv_concours values(6,"Tir des amoureux","2015-03-01",2.5,1,1,null);

insert into jtv_concours values(7,"Tir du gros","2016-04-25",35.2,1,1,null);
insert into jtv_concours values(8,"Tir du 1er aout","2016-03-17",0,1,1,null);
insert into jtv_concours values(9,"Tir du diable","2016-05-09",8.5,1,1,null);

insert into jtv_concours_saison values(1,4);
insert into jtv_concours_saison values(2,4);
insert into jtv_concours_saison values(3,4);

insert into jtv_concours_saison values(4,3);
insert into jtv_concours_saison values(5,3);
insert into jtv_concours_saison values(6,3);

insert into jtv_concours_saison values(7,2);
insert into jtv_concours_saison values(8,2);
insert into jtv_concours_saison values(9,2);
