-- MySQL dump 10.16  Distrib 10.1.30-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: jtv
-- ------------------------------------------------------
-- Server version	10.1.30-MariaDB

--
-- Table structure for table `jtv_type_arme`
--

DROP TABLE IF EXISTS `jtv_type_arme`;
CREATE TABLE `jtv_type_arme` (
  `typ_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `typ_actif` bit(1) DEFAULT NULL,
  `typ_nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`typ_id`)
);

--
-- Table structure for table `jtv_arme`
--

DROP TABLE IF EXISTS `jtv_arme`;
CREATE TABLE `jtv_arme` (
  `arm_id` bigint(20) NOT NULL,
  `arm_actif` bit(1) DEFAULT NULL,
  `arm_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arm_typ_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`arm_id`),
  FOREIGN KEY (`arm_typ_id`) REFERENCES `jtv_type_arme` (`typ_id`)
);

--
-- Table structure for table `jtv_cible`
--

DROP TABLE IF EXISTS `jtv_cible`;
CREATE TABLE `jtv_cible` (
  `cib_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cib_actif` bit(1) NOT NULL,
  `cib_nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cib_valeur_max` int(11) NOT NULL,
  PRIMARY KEY (`cib_id`)
);

--
-- Table structure for table `jtv_groupe`
--

DROP TABLE IF EXISTS `jtv_groupe`;
CREATE TABLE `jtv_groupe` (
  `gro_id` int(11) NOT NULL AUTO_INCREMENT,
  `gro_nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`gro_id`)
);

--
-- Table structure for table `jtv_annee`
--

DROP TABLE IF EXISTS `jtv_annee`;
CREATE TABLE `jtv_annee` (
  `ann_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ann_actif` bit(1) DEFAULT NULL,
  `ann_nom` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ann_id`)
);

--
-- Table structure for table `jtv_poste`
--

DROP TABLE IF EXISTS `jtv_poste`;
CREATE TABLE `jtv_poste` (
  `pos_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pos_actif` bit(1) DEFAULT NULL,
  `pos_nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`pos_id`)
);

--
-- Table structure for table `jtv_saison`
--

DROP TABLE IF EXISTS `jtv_saison`;
CREATE TABLE `jtv_saison` (
  `sai_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sai_actif` bit(1) DEFAULT NULL,
  `sai_date_debut` datetime NOT NULL,
  `sai_date_fin` datetime NOT NULL,
  `sai_typ_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`sai_id`),
  FOREIGN KEY (`sai_typ_id`) REFERENCES `jtv_type_arme` (`typ_id`)
);

--
-- Table structure for table `jtv_programme`
--

DROP TABLE IF EXISTS `jtv_programme`;
CREATE TABLE `jtv_programme` (
  `pro_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pro_actif` bit(1) DEFAULT NULL,
  `pro_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_sai_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`pro_id`),
  FOREIGN KEY (`pro_sai_id`) REFERENCES `jtv_saison` (`sai_id`)
);

--
-- Table structure for table `jtv_tireur`
--

DROP TABLE IF EXISTS `jtv_tireur`;
CREATE TABLE `jtv_tireur` (
  `tir_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tir_actif` bit(1) DEFAULT NULL,
  `tir_adresse` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tir_date_naissance` datetime NOT NULL,
  `tir_email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tir_level` int(11) DEFAULT NULL,
  `tir_localite` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tir_nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tir_npa` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tir_num_natel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tir_numtelephone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tir_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tir_prenom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tir_titre` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tir_username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tir_sai_last_edit_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`tir_id`),
  FOREIGN KEY (`tir_sai_last_edit_id`) REFERENCES `jtv_saison` (`sai_id`)
);

--
-- Table structure for table `jtv_jeune_tireur`
--

DROP TABLE IF EXISTS `jtv_jeune_tireur`;
CREATE TABLE `jtv_jeune_tireur` (
  `jeu_adresse_parent` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jeu_localite_parent` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jeu_nom_parent` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jeu_npa` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jeu_num_natel_parent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jeu_prenom_parent` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tir_id` bigint(20) NOT NULL,
  PRIMARY KEY (`tir_id`),
  FOREIGN KEY (`tir_id`) REFERENCES `jtv_tireur` (`tir_id`)
);

--
-- Table structure for table `jtv_moniteur`
--

DROP TABLE IF EXISTS `jtv_moniteur`;
CREATE TABLE `jtv_moniteur` (
  `tir_id` bigint(20) NOT NULL,
  PRIMARY KEY (`tir_id`),
  FOREIGN KEY (`tir_id`) REFERENCES `jtv_tireur` (`tir_id`)
);

--
-- Table structure for table `jtv_cours`
--

DROP TABLE IF EXISTS `jtv_cours`;
CREATE TABLE `jtv_cours` (
  `cou_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cou_actif` bit(1) DEFAULT NULL,
  `cou_date` datetime NOT NULL,
  `cou_heure_debut` datetime NOT NULL,
  `cou_heure_fin` datetime NOT NULL,
  `cou_lieu` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cou_nom` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cou_temps_nettoyage` int(11) DEFAULT NULL,
  `cou_temps_pause` int(11) DEFAULT NULL,
  `cou_sai_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`cou_id`),
  FOREIGN KEY (`cou_sai_id`) REFERENCES `jtv_saison` (`sai_id`)
);

--
-- Table structure for table `jtv_absence`
--

DROP TABLE IF EXISTS `jtv_absence`;

CREATE TABLE `jtv_absence` (
  `abs_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `abs_date` tinyblob,
  `abs_motif` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `abs_cou_id` bigint(20) DEFAULT NULL,
  `abs_tir_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`abs_id`),
  FOREIGN KEY (`abs_tir_id`) REFERENCES `jtv_tireur` (`tir_id`),
  FOREIGN KEY (`abs_cou_id`) REFERENCES `jtv_cours` (`cou_id`)
);


--
-- Table structure for table `jtv_concours`
--

DROP TABLE IF EXISTS `jtv_concours`;
CREATE TABLE `jtv_concours` (
  `con_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `con_actif` bit(1) DEFAULT NULL,
  `con_date` datetime NOT NULL,
  `con_licence` bit(1) DEFAULT NULL,
  `con_lieu` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `con_nom` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `con_prix` double DEFAULT NULL,
  `con_sai_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`con_id`),
  FOREIGN KEY (`con_sai_id`) REFERENCES `jtv_saison` (`sai_id`)
);

--
-- Table structure for table `jtv_cours_groupes`
--

DROP TABLE IF EXISTS `jtv_cours_groupes`;
CREATE TABLE `jtv_cours_groupes` (
  `cou_id` bigint(20) NOT NULL,
  `gro_id` int(11) NOT NULL,
  PRIMARY KEY (`cou_id`,`gro_id`),
  FOREIGN KEY (`gro_id`) REFERENCES `jtv_groupe` (`gro_id`),
  FOREIGN KEY (`cou_id`) REFERENCES `jtv_cours` (`cou_id`)
);

--
-- Table structure for table `jtv_cours_postes`
--

DROP TABLE IF EXISTS `jtv_cours_postes`;
CREATE TABLE `jtv_cours_postes` (
  `cou_id` bigint(20) NOT NULL,
  `pos_id` bigint(20) NOT NULL,
  PRIMARY KEY (`cou_id`,`pos_id`),
  FOREIGN KEY (`cou_id`) REFERENCES `jtv_cours` (`cou_id`),
  FOREIGN KEY (`pos_id`) REFERENCES `jtv_poste` (`pos_id`)
);

--
-- Table structure for table `jtv_passe`
--

DROP TABLE IF EXISTS `jtv_passe`;
CREATE TABLE `jtv_passe` (
  `pas_id` int(11) NOT NULL AUTO_INCREMENT,
  `pas_nbcoups` int(11) DEFAULT NULL,
  `pas_ordre` int(11) DEFAULT NULL,
  `pas_sequence` int(11) DEFAULT NULL,
  `pas_type_coup` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pas_cib_id` bigint(20) DEFAULT NULL,
  `pas_pro_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`pas_id`),
  FOREIGN KEY (`pas_pro_id`) REFERENCES `jtv_programme` (`pro_id`),
  FOREIGN KEY (`pas_cib_id`) REFERENCES `jtv_cible` (`cib_id`)
);

--
-- Table structure for table `jtv_cours_programme_annee`
--

DROP TABLE IF EXISTS `jtv_cours_programme_annee`;
CREATE TABLE `jtv_cours_programme_annee` (
  `cpa_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cpa_ann_id` bigint(20) DEFAULT NULL,
  `cpa_cou_id` bigint(20) DEFAULT NULL,
  `cpa_pro_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`cpa_id`),
  FOREIGN KEY (`cpa_pro_id`) REFERENCES `jtv_programme` (`pro_id`),
  FOREIGN KEY (`cpa_ann_id`) REFERENCES `jtv_annee` (`ann_id`),
  FOREIGN KEY (`cpa_cou_id`) REFERENCES `jtv_cours` (`cou_id`)
);

--
-- Table structure for table `jtv_horaire`
--

DROP TABLE IF EXISTS `jtv_horaire`;
CREATE TABLE `jtv_horaire` (
  `hor_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hor_heure_debut` datetime DEFAULT NULL,
  `hor_heure_fin` datetime DEFAULT NULL,
  `hor_pos_id` bigint(20) NOT NULL,
  `hor_gro_id` int(11) NOT NULL,
  `hor_cou_id` bigint(20) NOT NULL,
  PRIMARY KEY (`hor_id`),
  FOREIGN KEY (`hor_pos_id`) REFERENCES `jtv_poste` (`pos_id`),
  FOREIGN KEY (`hor_cou_id`) REFERENCES `jtv_cours` (`cou_id`),
  FOREIGN KEY (`hor_gro_id`) REFERENCES `jtv_groupe` (`gro_id`)
);

--
-- Table structure for table `jtv_inscription`
--

DROP TABLE IF EXISTS `jtv_inscription`;
CREATE TABLE `jtv_inscription` (
  `ins_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ins_actif` bit(1) DEFAULT NULL,
  `ins_date_desinscrit` datetime DEFAULT NULL,
  `ins_ann_id` bigint(20) DEFAULT NULL,
  `ins_arm_id` bigint(20) DEFAULT NULL,
  `ins_sai_id` bigint(20) DEFAULT NULL,
  `ins_tir_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ins_id`),
  FOREIGN KEY (`ins_sai_id`) REFERENCES `jtv_saison` (`sai_id`),
  FOREIGN KEY (`ins_ann_id`) REFERENCES `jtv_annee` (`ann_id`),
  FOREIGN KEY (`ins_arm_id`) REFERENCES `jtv_arme` (`arm_id`),
  FOREIGN KEY (`ins_tir_id`) REFERENCES `jtv_tireur` (`tir_id`)
);

--
-- Table structure for table `jtv_participe`
--

DROP TABLE IF EXISTS `jtv_participe`;
CREATE TABLE `jtv_participe` (
  `par_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `par_etat` int(11) DEFAULT NULL,
  `par_excuse` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `par_nb_munitions_supplementaires` int(11) DEFAULT NULL,
  `par_remarque` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `par_total_obtenu` int(11) DEFAULT NULL,
  `par_cou_id` bigint(20) DEFAULT NULL,
  `par_gro_id` int(11) DEFAULT NULL,
  `par_ins_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`par_id`),
  FOREIGN KEY (`par_cou_id`) REFERENCES `jtv_cours` (`cou_id`),
  FOREIGN KEY (`par_gro_id`) REFERENCES `jtv_groupe` (`gro_id`),
  FOREIGN KEY (`par_ins_id`) REFERENCES `jtv_inscription` (`ins_id`)
);

INSERT INTO `jtv_cible` (`cib_id`, `cib_actif`, `cib_nom`, `cib_valeur_max`) VALUES (1,'','A5',5),(2,'','A10',10),(3,'','A100',100),(4,'','B4',4);
INSERT INTO `jtv_groupe` (`gro_id`, `gro_nom`) VALUES (1,'Groupe 1'),(2,'Groupe 2'),(3,'Groupe 3'),(4,'Groupe 4'),(5,'Groupe 5'),(6,'Groupe 6'),(7,'Groupe 7'),(8,'Groupe 8');
INSERT INTO `jtv_tireur` (`tir_id`, `tir_actif`, `tir_adresse`, `tir_date_naissance`, `tir_email`, `tir_level`, `tir_localite`, `tir_nom`, `tir_npa`, `tir_num_natel`, `tir_numtelephone`, `tir_password`, `tir_prenom`, `tir_titre`, `tir_username`, `tir_sai_last_edit_id`) VALUES (1,'','In the cloud','1900-01-01 00:00:00','admin@example.com',2,'heaven','Admin','1234','0791234567','0221234567','$2a$10$/Tya9jKwGMfty6kxA91Cs.GeJM37sRi84sqOReHYTguXo3aM1H8FW','Super','Indefini','admin',NULL);
INSERT INTO `jtv_moniteur` (`tir_id`) VALUES (1);


