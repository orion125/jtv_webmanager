
function Inputcontrol (classstr, type) {
    // DEFINIR VALEUR PAR DEFAUT EN FONCTION DU CHAMP
    init();
    
    function init() {
        var validator = getValidatator();
        var defaultval;
        switch (type) {
            case "email" :
                defaultval = "xxx@gmail.com";
                break;
            case "numbers" :
                defaultval = "0";
                break;
            default :
                return;
        }
        if (!validator.isNotEmpty($('.' + classstr).val())) {
            $('.' + classstr).val(defaultval);
        }
    }
    // EVENT LISTENER
    $('.'+classstr).change(function () {
        var content = $(this).val();
        var validator = getValidatator();
        // DEFINIR VALEUR PAR DEFAUT EN FONCTION DU CHAMP
        var defaultval;
        switch (type){
            case "email" : defaultval = "xxx@gmail.com"; break;
            case "numbers" : defaultval = "0"; break;
            default : return;
        }
        // SI LE CHAMPS EST VIDE -> VALEUR PAR DEFAUT
        if (!validator.isNotEmpty(content)){
            $(this).val(defaultval);
            return;
        }
        // SI LE CONTENU N'EST PAS COMPATIBLE AVEC LE TYPE DE CHAMPS -> VALEUR PAR DEFAUT
        switch (type){
            case "email" : if (!validator.isEmailAddress(content)) $(this).val(defaultval); break;
            case "numbers" : if (!validator.isNumber(content)) $(this).val(defaultval); break;
            default : return;
        }
    });
    // VALIDATIOR GENERATION
    function getValidatator () {
        var validator = {
            isEmailAddress: function (str) {
                var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                return pattern.test(str);
            },
            isNotEmpty: function (str) {
                var pattern = /\S+/;
                return pattern.test(str);
            },
            isNumber: function (str) {
                var pattern = /^\d+$/;
                return pattern.test(str);
            }
        };
        return validator;
    }


}