function rowAdded(rowElement) {
    $(rowElement).find("input").val("0");
    //saveNeeded();
}
function rowRemoved(rowElement) {
    //saveNeeded();
}

function saveNeeded() {
    $('#submit').css('color','red');
    $('#submit').css('font-weight','bold');
    if( $('#submit').val().indexOf('!') != 0 ) {
        $('#submit').val( '!' + $('#submit').val() );
    }
}

function beforeSubmit() {
    return true;
}

$(document).ready( function() {
    var config = {
        rowClass : 'passetoadd',
        addRowId : 'addpasse',
        removeRowClass : 'btn-danger',
        formId : 'programmeForm',
        rowContainerId : 'passesContainer',
        indexedPropertyName : 'pas',
        indexedPropertyMemberNames : 'cible,typeCoup,sequence,nbcoups',
        rowAddedListener : rowAdded,
        rowRemovedListener : rowRemoved,
        beforeSubmit : beforeSubmit
    };
    new DynamicListHelper(config);

    new Inputcontrol("form-control.numeric-data","numbers");
 });
