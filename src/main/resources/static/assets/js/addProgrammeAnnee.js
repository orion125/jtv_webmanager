function rowAdded(rowElement) {
    $(rowElement).find("input").val("");
    //saveNeeded();
}
function rowRemoved(rowElement) {
    //saveNeeded();
}

function saveNeeded() {
    $('#submit').css('color','red');
    $('#submit').css('font-weight','bold');
    if( $('#submit').val().indexOf('!') != 0 ) {
        $('#submit').val( '!' + $('#submit').val() );
    }
}

function beforeSubmit() {
    return true;
}

$(document).ready( function() {
    var config = {
        rowClass : 'progtoadd',
        addRowId : 'addProgAnn',
        removeRowClass : 'btn-danger',
        formId : 'coursForm',
        rowContainerId : 'programmesContainer',
        indexedPropertyName : 'progannee',
        indexedPropertyMemberNames : 'annee,programme',
        rowAddedListener : rowAdded,
        rowRemovedListener : rowRemoved,
        beforeSubmit : beforeSubmit
    };
    new DynamicListHelper(config);
 });
