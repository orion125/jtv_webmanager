package ch.steversoix.jtv.desktop.saison.cours.model;

import ch.steversoix.jtv.desktop.saison.model.Saison;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CoursRepository extends JpaRepository<Cours, Long> {

    @Query(value="select cou from Cours cou WHERE cou.actif = ?1")
    List<Cours> findCoursByActif(boolean actif);

    @Query(value="select cou from Cours cou WHERE cou.saison.id = ?1 AND cou.actif = ?2")
    List<Cours> findCoursBySaisonAndActif(Long id, boolean actif);

    Optional<Cours> findById(long id);

    List<Cours> findBySaison(Saison saison);
}
