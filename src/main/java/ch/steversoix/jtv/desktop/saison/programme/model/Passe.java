package ch.steversoix.jtv.desktop.saison.programme.model;


import ch.steversoix.jtv.desktop.saison.programme.services.TypeCoupEnumeration;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.Optional;

@Entity
@Table(name = "jtv_passe")
public class Passe {
    public static final int NB_COUPS_MAX = 50;
    public static final int NB_SEQUENCES_MAX = 50;

    @Id
    @GeneratedValue
    @Column(name = "pas_id")
    private int id;

    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "pas_pro_id", referencedColumnName="pro_id")
    private Programme programme;

    @Column(name = "pas_ordre")
    private int ordre;

    @ManyToOne
    @JoinColumn(name = "pas_cib_id", referencedColumnName="cib_id")
    private Cible cible;


    @Column(name = "pas_sequence")
    private int sequence;

    @Column(name = "pas_nbcoups")
    private int nbcoups;

    @Column(name = "pas_typeCoup")
    @NotEmpty(message = "*Please provide a type of hit for the passe*")
    private String typeCoup;


    /* Constructeur */
    public Passe(){}
    public Passe(Optional<Integer> ordre, Optional<Integer> sequence, Optional<Integer> nbcoups, String typeCoup, Cible c) {
        this(-1,
                ordre.orElse(0),  sequence.orElse(0),nbcoups.orElse(0),typeCoup,c,new Programme());
    }
    public Passe(Optional<Integer> ordre, Optional<Integer> sequence, Optional<Integer> nbcoups, String typeCoup, Cible c, Programme p) {
        this(-1, ordre.orElse(0), sequence.orElse(0), nbcoups.orElse(0),typeCoup,c,p);
    }

    public Passe(Optional<Integer> id, Optional<Integer> ordre, Optional<Integer> sequence, Optional<Integer> nbcoups, String typeCoup, Cible c, Programme p) {
        this(id.orElse(-1), ordre.orElse(0), sequence.orElse(0), nbcoups.orElse(0),typeCoup,c,p);
    }
    public Passe(int id, int ordre, int sequence, int nbcoups, String typeCoup, Cible c, Programme p) {
        this.id = id;
        this.sequence = sequence;
        this.nbcoups = nbcoups;
        this.typeCoup = typeCoup;
        this.ordre = ordre;
        this.cible=c;
        this.setProgramme(p);
    }


    /* Getter & Setter */
    // Clé primaire composée

    public int getId() {return id;}

    public void setId(int id) {this.id = id;}

    public Cible getCible() {return this.cible;}
    public void setCible(Cible cible) {this.cible = cible;}

    public Programme getProgramme() {return this.programme;}
    public void setProgramme(Programme prog) {this.programme = prog;}

    // Autre champ
    public int getOrdre() {return ordre;}
    public void setOrdre(int ordre) {this.ordre = ordre;}
    public int getSequence() {return sequence;}
    public void setSequence(int sequence) {this.sequence = sequence;}
    public int getNbcoups() {return nbcoups;}
    public void setNbcoups(int nbcoups) {this.nbcoups = nbcoups;}
    public String getTypeCoup() {return typeCoup;}
    public void setTypeCoup(String typeCoup) {this.typeCoup = typeCoup;}


    public int calculeTotalPointsPasse(){
        int ponderation =0;
        for (TypeCoupEnumeration t:TypeCoupEnumeration.values())
            if (t.getType().equals(typeCoup))
                ponderation = t.getPonderation();
        return (calculeNombreTirsPasse()*cible.getValeurMax())*ponderation;
    }
    public int calculeNombreTirsPasse(){
        return sequence*nbcoups;
    }

    @Override
    public String toString() {
        String ordre = "Ordre : "+ getOrdre()==null?"null":String.valueOf(this.ordre);
        String seq ="Sequence : "+getSequence()==null?"null":String.valueOf(this.sequence);
        String typeC= "Type de coup : "+getTypeCoup()==null?"null":typeCoup;
        String nbc="nb coups : "+getNbcoups()==null?"null":String.valueOf(this.nbcoups);
        String cible="Cible : "+ (this.getCible().equals(null)?"null":this.getCible().toString());
        return "Passe " + ordre+" ,"+seq+" ,"+typeC+" ,"+nbc+" ,"+cible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Passe passe = (Passe) o;
        if (passe.getProgramme() == null || passe.getOrdre() == 0) return false;
        if (getProgramme() == null || getOrdre() == 0) return false;
        if (!getProgramme().equals(passe.getProgramme())) return false;
        if (!(getOrdre() == passe.getOrdre())) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
