package ch.steversoix.jtv.desktop.saison.programme.model;

/**
* @author jonathan.capitao
* @version 0.1
**/

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "jtv_cible")
public class Cible {
    public static final int MIN_VALUE = 1;
    public static final int MAX_VALUE = 1000;

    @Id
    @GeneratedValue
    @Column(name = "cib_id")
    private Long id;
    @Column(name = "cib_nom", nullable = false)
    @Size(min=2, max=50)
    private String nom;
    @Column(name = "cib_valeurMax", nullable = false)
    @Range(min=MIN_VALUE,max=MAX_VALUE, message = "La valeur maximal doit être entre "+MIN_VALUE+" et "+MAX_VALUE) // RG 12
    private int valeurMax;
    @Column(name = "cib_actif", nullable = false)
    private boolean actif;

    /* Constructeur */
    public Cible(Long id, String nom, int valeurMax, boolean actif) {
        this.id = id;
        this.nom = nom;
        this.valeurMax = valeurMax;
        this.actif = actif;
    }
    public Cible(Long id, String nom, int valeurMax) {this(id,nom,valeurMax,true);}
    public Cible(String nom, int valeurMax) {this(new Long(-1),nom,valeurMax,true);}
    public Cible() {}

    /* Getter & Setter */
    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}
    public String getNom() {return nom;}
    public void setNom(String nom) {this.nom = nom;}
    public int getValeurMax() {return valeurMax;}
    public void setValeurMax(int valeurMax) {this.valeurMax = valeurMax;}
    public boolean isActif() {return actif;}
    public void setActif(boolean actif) {this.actif = actif;}

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cible other = (Cible) obj;
        return this.getId() == other.getId();
    }

    @Override
    public String toString() {
        return (this!=null)?this.getNom()+" "+this.getValeurMax():"null";
    }


}
