package ch.steversoix.jtv.desktop.saison.programme.controller;

import ch.steversoix.jtv.desktop.outils.FlashMessage;
import ch.steversoix.jtv.desktop.outils.FlashMessageGenerator;
import ch.steversoix.jtv.desktop.saison.programme.exceptions.CibleException;
import ch.steversoix.jtv.desktop.saison.programme.model.CibleRepository;
import ch.steversoix.jtv.desktop.saison.programme.model.Passe;
import ch.steversoix.jtv.desktop.saison.programme.services.CibleEnumeration;
import ch.steversoix.jtv.desktop.saison.programme.services.CibleService;
import ch.steversoix.jtv.desktop.saison.programme.model.Cible;
import ch.steversoix.jtv.desktop.saison.programme.services.ProgrammeService;
import ch.steversoix.jtv.desktop.tireur.services.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;


@Controller
@RequestMapping(path="/cibles")
public class CibleController {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Autowired
    private CibleService cibleService;
    @Autowired
    private ProgrammeService programmeService;


    @ModelAttribute("user")
    public SecurityUser getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (SecurityUser) auth.getPrincipal();
    }

    /* Affichage du formulaire qui liste les cibles */
    @GetMapping("")
    public String getCibles(Model model){
        List<Cible> lst = cibleService.findCiblesByActif(true);
        model.addAttribute("cibles",lst);
        model.addAttribute("actif",true);
        return "gestionCibles";
    }

    @GetMapping("/inactifs")
    public String getCiblesinactives(Model model){
        List<Cible> lst = cibleService.findCiblesByActif(false);
        model.addAttribute("cibles",lst);
        model.addAttribute("actif",false);
        return "gestionCibles";
    }

    /* Affichage du formulaire qui ajoute un cible */
    @GetMapping("/add")
    public String addCible(Model model){
        model.addAttribute("cible",new Cible());
        return "ajoutCible";
    }

    /* Affichage du formulaire qui modifie un cible */
    @GetMapping("/{id}/edit")
    public String editCible(@PathVariable("id") long id, Model model, RedirectAttributes redirAttrs){
        Cible cible = cibleService.findCibleById(id);
        List<Passe> lstPasse = programmeService.findPassesByCible(cible);
        /* RM 10 - on ne peut modifier une cible attribuée à un programme */
        if(lstPasse.size()!=0){
            LOGGER.log(Level.SEVERE, CibleEnumeration.DEJA_ATTRIBUEE.getErreurforLog());
            redirAttrs.addFlashAttribute("message",FlashMessageGenerator.generateSingleMessage(
                    FlashMessage.Level.DANGER,CibleEnumeration.DEJA_ATTRIBUEE.getErreurException()));
            return "redirect:/cibles";
        }
        model.addAttribute("cible",cible);
        return "ajoutCible";
    }

    /*
    * Les requêtes qui suivent intéragissent avec la BDD
    * */

    /* Ajout un cible dans la BDD */
    @PostMapping("")
    public String addCibleDB(@Valid @ModelAttribute("cible") Cible newCible, BindingResult bindingResult, Model model, RedirectAttributes redirAttrs){
        if (bindingResult.hasErrors()) {
            return "ajoutCible";
        }
        try{
            cibleService.save(newCible);
        } catch(CibleException ex){
            model.addAttribute("messages", FlashMessageGenerator.generateMultipleMessages(
                    FlashMessage.Level.DANGER, ex.getErreurs())
            );
            model.addAttribute("cible",newCible);
            return "ajoutCible";
        }
        LOGGER.log(Level.FINE,"La cible n°"+newCible.getId()+" a été ajoutée.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "La cible "+newCible.getNom()+" a été ajoutée."));
        return "redirect:/cibles";
    }

    /* Modifie un type arme dans la BDD */
    @PostMapping("/{id}/update")
    public String updateCibleDB(@Valid @ModelAttribute("cible") Cible cible,@PathVariable("id") long id,
                                BindingResult bindingResult, Model model, RedirectAttributes redirAttrs){
        Cible cib = cibleService.findCibleById(id);
        if (bindingResult.hasErrors()) {
            model.addAttribute("cible",cib);
            return "ajoutCible";
        }
        try{
            cibleService.update(cible);
        }catch(CibleException ex){
            model.addAttribute("messages", FlashMessageGenerator.generateMultipleMessages(
                    FlashMessage.Level.DANGER, ex.getErreurs())
            );
            model.addAttribute("cible",cib);
            return "ajoutCible";
        }
        LOGGER.log(Level.FINE,"La cible n°"+cible.getId()+" a été modifiée.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "La cible "+cible.getNom()+" a été modifiée."));
        return "redirect:/cibles";
    }

    @GetMapping("/{id}/desactivate")
    public String desactivateCibleDB(@PathVariable("id") long id, RedirectAttributes redirAttrs){
        Cible cible = cibleService.findCibleById(id);
        cibleService.desactivate(cible);

        LOGGER.log(Level.FINE,"La cible "+cible.getNom()+" a été désactivée.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "La cible "+cible.getNom()+" a été désactivée."));
        return "redirect:/cibles";
    }

    @GetMapping("/{id}/activate")
    public String activateCibleDB(@PathVariable("id") long id, RedirectAttributes redirAttrs){
        Cible cible = cibleService.findCibleById(id);
        cibleService.activate(cible);

        LOGGER.log(Level.FINE,"La cible "+cible.getNom()+" a été activée.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "La cible "+cible.getNom()+" a été activée."));
        return "redirect:/cibles/inactifs";
    }
}
