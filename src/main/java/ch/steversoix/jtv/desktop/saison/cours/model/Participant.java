package ch.steversoix.jtv.desktop.saison.cours.model;

import ch.steversoix.jtv.desktop.saison.model.Inscription;

import javax.persistence.*;

@Entity
@Table(name = "jtv_participe")
public class Participant implements Comparable<Participant> {
    public final static int PRESENT = 0;
    public final static int EXCUSE = 1;
    public final static int RETARD = 2;
    public final static int ABSENT = 3;
    public final static int UNDEFINED = 4;

    @Id
    @GeneratedValue
    @Column(name = "par_id")
    private long id;
    @ManyToOne
    @JoinColumn(name = "par_ins_id")
    private Inscription inscrit;
    @ManyToOne
    @JoinColumn(name = "par_cou_id")
    private Cours cours;
    @ManyToOne
    @JoinColumn(name = "par_gro_id")
    private Groupe groupe;
    @Column(name = "par_totalObtenu")
    private int totalObtenu;
    @Column(name = "par_nbMunitionsSupplementaires")
    private int nbMunitionsSupplementaires;
    @Column(name = "par_etat")
    private int etat;
    @Column(name = "par_remarque")
    private String remarque;
    @Column(name = "par_excuse")
    private String excuse;

    public Participant() {
    }

    public Participant(Cours cours, Inscription inscrit) {
        this.cours = cours;
        this.inscrit = inscrit;
    }

    /* Getter & Setter */
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public Inscription getInscrit() {
        return inscrit;
    }
    public void setInscrit(Inscription inscrit) {
        this.inscrit = inscrit;
    }
    public Cours getCours() {
        return cours;
    }
    public void setCours(Cours cours) {
        this.cours = cours;
    }
    public Groupe getGroupe() { return groupe; }
    public void setGroupe(Groupe groupe) { this.groupe = groupe; }
    public int getTotalObtenu() {
        return totalObtenu;
    }
    public void setTotalObtenu(int totalObtenu) {
        this.totalObtenu = totalObtenu;
    }
    public int getNbMunitionsSupplementaires() {
        return nbMunitionsSupplementaires;
    }
    public void setNbMunitionsSupplementaires(int nbMunitionsSupplementaires) { this.nbMunitionsSupplementaires = nbMunitionsSupplementaires; }
    public int getEtat() {
        return etat;
    }
    public void setEtat(int etat) {
        this.etat = etat;
    }
    public String getRemarque() {
        return remarque;
    }
    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }
    public String getExcuse() {
        return excuse;
    }
    public void setExcuse(String excuse) {
        this.excuse = excuse;
    }

    @Override
    public int compareTo(Participant o) {
        return this.getInscrit().compareTo(o.getInscrit());
    }
}
