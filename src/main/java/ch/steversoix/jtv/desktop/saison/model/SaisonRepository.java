package ch.steversoix.jtv.desktop.saison.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface SaisonRepository extends JpaRepository<Saison, Long> {

    @Query(value="select sai from Saison sai WHERE sai.actif = ?1")
    List<Saison> findSaisonsByActif(boolean actif);

    Optional<Saison> findById(long id);

}
