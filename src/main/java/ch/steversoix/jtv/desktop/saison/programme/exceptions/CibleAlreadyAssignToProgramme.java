package ch.steversoix.jtv.desktop.saison.programme.exceptions;

public class CibleAlreadyAssignToProgramme extends IllegalArgumentException {
    public CibleAlreadyAssignToProgramme() {
    }

    public CibleAlreadyAssignToProgramme(String s) {
        super(s);
    }
}
