package ch.steversoix.jtv.desktop.saison.annee.controller;

import ch.steversoix.jtv.desktop.outils.FlashMessage;
import ch.steversoix.jtv.desktop.outils.FlashMessageGenerator;
import ch.steversoix.jtv.desktop.saison.annee.exception.AnneeException;
import ch.steversoix.jtv.desktop.saison.annee.model.Annee;
import ch.steversoix.jtv.desktop.saison.annee.service.AnneeService;
import ch.steversoix.jtv.desktop.tireur.services.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
@Order(2)
@RequestMapping(path = "/annees")
public class AnneeController implements ApplicationRunner {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Value("${jtv.testdata}")
    private Boolean testdata;

    @Autowired
    private AnneeService anneeService;

    /*
    * Les requêtes qui suivent concernent la navigation
    * */

    @ModelAttribute("user")
    public SecurityUser getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (SecurityUser) auth.getPrincipal();
    }

    /* Affichage du formulaire qui liste les années */
    @GetMapping("")
    public String getAnnees(Model model) {
        List<Annee> lst = anneeService.findAnneesByActif(true);
        model.addAttribute("annees", lst);
        model.addAttribute("actif", true);
        return "gestionAnnees";
    }

    @GetMapping("/inactifs")
    public String getAnneesInactifs(Model model) {
        List<Annee> lst = anneeService.findAnneesByActif(false);
        model.addAttribute("annees", lst);
        model.addAttribute("actif", false);
        return "gestionAnnees";
    }

    /* Affichage du formulaire qui ajoute une année */
    @GetMapping("/add")
    public String addAnnee(Model model) {
        model.addAttribute("annee", new Annee());
        return "ajoutAnnee";
    }

    /* Affichage du formulaire qui modifie une année */
    @GetMapping("/{id}/edit")
    public String editAnnee(@PathVariable("id") long id, Model model) {
        Annee annee = anneeService.findAnneeById(id);
        model.addAttribute("annee", annee);
        model.addAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.WARNING,
                "Attention ! Les modifications apportées à cette année vont se répercuter sur toutes les saisons qui l'utilisent."));
        return "ajoutAnnee";
    }

    /*
    * Les requêtes qui suivent intéragissent avec la BDD
    * */

    /* Ajout un poste dans la BDD */
    @PostMapping("")
    public String addAnneeDB(@Valid @ModelAttribute("annee") Annee newAnnee, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "ajoutAnnee";
        }

        try {
            anneeService.save(newAnnee);
        } catch (AnneeException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            model.addAttribute("erreurs", e.getMessage());
            return "ajoutAnnee";
        }

        return "redirect:/annees";
    }

    /* Modifie un type arme dans la BDD */
    @PostMapping("/{id}/update")
    public String updateAnneeDB(@Valid @ModelAttribute("annee") Annee annee, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "ajoutAnnee";
        }

        try {
            anneeService.update(annee);
        } catch (AnneeException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            model.addAttribute("erreurs", e.getMessage());
            return "ajoutAnnee";
        }


        return "redirect:/annees";
    }

    @GetMapping("/{id}/desactivate")
    public String desactivateAnneeDB(@PathVariable("id") long id) {
        Annee annee = anneeService.findAnneeById(id);
        anneeService.desactivate(annee);
        return "redirect:/annees";
    }

    @GetMapping("/{id}/activate")
    public String activateAnneeDB(@PathVariable("id") long id) {
        Annee annee = anneeService.findAnneeById(id);
        anneeService.activate(annee);
        return "redirect:/annees/inactifs";
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        if (applicationArguments.containsOption("jtv.testdata") || testdata) {
            anneeService.save(new Annee("JT 1"));
            anneeService.save(new Annee("JT 2"));
            anneeService.save(new Annee("JT 3"));
            anneeService.save(new Annee("JT 4"));
            anneeService.save(new Annee("JT 5"));
            anneeService.save(new Annee("JT 6"));
            anneeService.save(new Annee("JT 7"));
            anneeService.save(new Annee("JT 8"));
        }
    }
}
