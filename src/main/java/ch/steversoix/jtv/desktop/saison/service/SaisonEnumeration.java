package ch.steversoix.jtv.desktop.saison.service;

public enum SaisonEnumeration {
    DATEDEB_ANT_DATEFIN("La date de début doit être antérieure à la date de fin.","dateDeb < dateFin"),
    DATEDEB_BEF_TODAY("Une saison ne peut commencer avant aujourd'hui.","dateDeb ; dateJ"),
    DATE_NULL("Les dates de débuts et de fin ne peuvent être vides.","dateDeb = null | dateFin = null"),
    DUREE_AN("Une saison ne peut pas duré plus d'un an.","saison.length < 1 an"),
    EXISTE_DEJA("Une saison de même type éxiste déjà dans le même intervalle.","saison de ce type existe déjà"),
    ARME_DEJA_ATTRIBUEE("Cette arme a déjà été attribuée pour cette saison.","arme déjà attribuée"),
    COURS_AVANT_APRES("Il existe des cours avant ou après la date de début ou de fin.",
            "il y a des cours de créer avant/après la date début/fin de la saison."),
    AUCUN_TYPE_ARME("Veuillez créer des types d'armes.", "Aucun type d'armes"),
    SAISON_TERMINER("Cette saison est terminée, vous ne pouvez plus l'éditer.", "TODAY > saisonDateFin"),
    AUCUNE_SAISON_SELEC("Vous n'avez sélectionné aucune saison.", "No Saison Select"),
    SAISON_INEXISTANTE("La saison à laquelle vous tentez d'accéder n'existe pas.", "Saison Not Found");

    private String erreurException;
    private String erreurForLog;

    SaisonEnumeration(String erreurException, String erreurForLog) {
        this.erreurException = erreurException;
        this.erreurForLog = erreurForLog;
    }

    public String getErreurException() {
        return erreurException;
    }
    public String getErreurforLog() {
        return erreurForLog;
    }
}
