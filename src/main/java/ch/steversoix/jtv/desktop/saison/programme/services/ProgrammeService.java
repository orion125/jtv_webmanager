package ch.steversoix.jtv.desktop.saison.programme.services;

import ch.steversoix.jtv.desktop.saison.model.Saison;
import ch.steversoix.jtv.desktop.saison.programme.model.Cible;
import ch.steversoix.jtv.desktop.saison.programme.model.Passe;
import ch.steversoix.jtv.desktop.saison.programme.model.Programme;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface ProgrammeService {
    List<Programme> findProgrammesByActif(boolean actif);
    List<Programme> findProgrammesBySaison(Saison s);
    List<Programme> findProgrammesBySaisonAndByActif(Saison s, boolean actif);
    List<Passe> findPassesByCible(Cible cible);
    Programme findProgrammeById(long id);
    Optional<Programme> create(String nom, String description, Saison s, Optional<ArrayList<Passe>> passes);
    Optional<Programme> create(Programme pro);
    Programme update(Programme pro);
    Programme save(Programme pro);
    void desactivate(Programme pro);
    void activate(Programme pro);

    Optional<Passe> create(Programme p, Cible c, int ordre,
                           int sequence, int nbcoups, String typeCoup);
    public Programme getProgrammeIfExistOrThrow404(long idProgramme);
}
