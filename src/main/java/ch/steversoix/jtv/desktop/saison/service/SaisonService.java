package ch.steversoix.jtv.desktop.saison.service;

import ch.steversoix.jtv.desktop.arme.model.Arme;
import ch.steversoix.jtv.desktop.arme.model.TypeArme;
import ch.steversoix.jtv.desktop.core.exception.ResourceNotFoundException;
import ch.steversoix.jtv.desktop.saison.annee.model.Annee;
import ch.steversoix.jtv.desktop.saison.cours.model.Cours;
import ch.steversoix.jtv.desktop.saison.cours.model.Participant;
import ch.steversoix.jtv.desktop.saison.cours.service.CoursService;
import ch.steversoix.jtv.desktop.saison.exception.AttributionException;
import ch.steversoix.jtv.desktop.saison.exception.InscriptionException;
import ch.steversoix.jtv.desktop.saison.exception.SaisonException;
import ch.steversoix.jtv.desktop.saison.model.Inscription;
import ch.steversoix.jtv.desktop.saison.model.InscritRepository;
import ch.steversoix.jtv.desktop.saison.model.Saison;
import ch.steversoix.jtv.desktop.saison.model.SaisonRepository;
import ch.steversoix.jtv.desktop.tireur.model.JeuneTireur;
import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import ch.steversoix.jtv.desktop.tireur.services.TireurService;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class SaisonService {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private SaisonRepository saisonRepository;
    private InscritRepository inscritRepository;
    private TireurService tireurService;
    private CoursService coursService;

    /* Constructeur */
    public SaisonService(SaisonRepository saisonRepository, InscritRepository inscritRepository, CoursService coursService) {
        this.saisonRepository = saisonRepository;
        this.inscritRepository = inscritRepository;
        this.tireurService = tireurService;
        this.coursService = coursService;
    }

    public Optional<Saison> findById(long id) {
        return saisonRepository.findById(id);
    }

    public Saison findSaisonById(long id) {
        return saisonRepository.findOne(id);
    }

    public List<Saison> findSaisonsByActif(boolean actif) {
        return saisonRepository.findSaisonsByActif(actif);
    }

    public List<Saison> findAll() {
        return saisonRepository.findAll();
    }

    /* Méthodes appelées par les controlleurs */

    public void save(Saison newSaison) throws SaisonException {
        SaisonException exception = new SaisonException();

        // Mettre les dates à 0 pour les heures et les minutes

        /* Traitement des erreurs AJOUT */
        exception.addAll(rulesAdd(newSaison));

        /* Traitement des erreurs AJOUT et MODIFICATION */
        exception.addAll(rulesAddUpdate(newSaison));

        /* Si aucune erreur alors on ajoute */
        if (exception.isEmpty()) {
            saisonRepository.save(newSaison);
        } else {
            throw exception;
        }
    }

    public void update(Saison saison) throws SaisonException {
        SaisonException exception = new SaisonException();

        /* Traitement des erreurs AJOUT et MODIFICATION */
        exception.addAll(rulesAddUpdate(saison));
        exception.addAll(rulesUpdate(saison));

        /* Si aucune erreur alors on ajoute */
        if (exception.isEmpty()) {
            saisonRepository.save(saison);
        } else {
            throw exception;
        }
    }

    public void desactivate(Saison saison) {
        saison.setActif(false);
        saisonRepository.save(saison);
    }

    public void activate(Saison saison) {
        saison.setActif(true);
        saisonRepository.save(saison);
    }

    /* Méthodes internes */

    /* Controles qui doivent être éffectué en cas d'ajout ET de modification !
    *  Si un tableau est renvoyé, c est qu il y a une erreur */
    private ArrayList<String> rulesAddUpdate(Saison saison) {

        ArrayList<String> erreurs = new ArrayList<String>();

        /* Début des controles */

        /* si les dates sont null */
        if (saison.getDateDebut() == null || saison.getDateFin() == null) {
            erreurs.add(SaisonEnumeration.DATE_NULL.getErreurException());
        } else {

        /* RG 18.A : Création d une saison (date début / fin) - date début < date fin */
            if (saison.getDateFin().before(saison.getDateDebut())) {
                // Message erreur dates incorrectes
                erreurs.add(SaisonEnumeration.DATEDEB_ANT_DATEFIN.getErreurException());
            }

        /* RG 18.B : Création d une saison (date début / fin) -  durée max saison = 1 an */
            Calendar c = Calendar.getInstance();
            c.setTime(saison.getDateDebut());
            c.add(Calendar.YEAR, 1);
            Date finMax = c.getTime();
            if (saison.getDateFin().after(finMax)) {
                // Message erreur la saison ne peut durer qu un an
                erreurs.add(SaisonEnumeration.DUREE_AN.getErreurException());
            }

            boolean existe = false;
            List<Saison> al = saisonRepository.findAll(); // Il faut aussi vérifier les saisons supprimées

            for (Saison s : al) {
                if (s.getType().equals(saison.getType()) && s.getId() != saison.getId()) {
                /* RG 08 : Intervalles de saisons du même type */
                    if (saison.getDateDebut().after(s.getDateDebut()) && saison.getDateDebut().before(s.getDateFin())) {
                        existe = true;
                    }
                    if (saison.getDateFin().after(s.getDateDebut()) && saison.getDateFin().before(s.getDateFin())) {
                        existe = true;
                    }
                }
            }

            if (existe) {
                // Message erreur une saison existe déjà dans cet intervalle
                erreurs.add(SaisonEnumeration.EXISTE_DEJA.getErreurException());
            }
        }
        /* Fin des controles */
        return erreurs;
    }

    /* Controles effectué UNIQUEMENT en cas d ajout
    *  Si un tableau est renvoyé, c est qu il y a une erreur*/
    private ArrayList<String> rulesAdd(Saison newSaison) {

        ArrayList<String> erreurs = new ArrayList<>();

        /* Début des contrôles */
        if (newSaison.getDateDebut() == null || newSaison.getDateFin() == null) {
            erreurs.add(SaisonEnumeration.DATE_NULL.getErreurException());
        } else {

            /* RA 06 : Saison - date du jour > date début */
            Date d = new Date();
            if (d.after(newSaison.getDateDebut())) {
                // Message erreur la date de début doit être ultérieur à aujourd hui
                erreurs.add(SaisonEnumeration.DATEDEB_BEF_TODAY.getErreurException());
            }
        }
        /* Fin des controles */
        return erreurs;
    }

    /* Contrôles effectué UNIQUEMENT en cas de modification */
    public ArrayList<String> rulesUpdate(Saison saison) {
        ArrayList<String> erreurs = new ArrayList<>();

        List<Cours> lstCours = coursService.findCoursBySaison(saison);
        /* RM 05 – Si on raccourcit la Saison mais que des cours existe après la date de fin ou avant celle de début */
        for (Cours c : lstCours) {
            if (c.getDateCours().before(saison.getDateDebut()) || c.getDateCours().after(saison.getDateFin())) {
                erreurs.add(SaisonEnumeration.COURS_AVANT_APRES.getErreurException());
            }
        }
        return erreurs;
    }

    public void createForInit(TypeArme tya, Date deb, Date fin) {
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        try {
            saisonRepository.save(new Saison(tya, deb, fin));
            LOGGER.log(Level.INFO, "Create Saison (init) : " +
                    dateFormat.format(deb) + " " + dateFormat.format(fin) + " " + tya.getNom());
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, "Create Saison (init) : " + ex.getMessage());
        }
    }


    /**
     * Inscrits
     **/

    public List<Inscription> findInscritsActifs(Saison s) {
        LOGGER.log(Level.FINE, String.format("Récupération de la liste des inscrits actifs à la saison %d", s.getId()));
        return inscritRepository.findBySaisonAndActifTrue(s);
    }

    public List<Inscription> findInscritsInactifs(Saison s) {
        LOGGER.log(Level.FINE, String.format("Récupération de la liste des inscrits inactifs à la saison %d", s.getId()));
        return inscritRepository.findBySaisonAndActifFalse(s);
    }

    public Optional<Inscription> findFirstInscriptionByArme(Arme arme) {
        return inscritRepository.findFirstByArme(arme);
    }

    public List<Inscription> findInscriptionsByTireur(Tireur t) {
        return inscritRepository.findByTireur(t);
    }

    public Inscription inscrit(Saison saison, Tireur tireur) {
        return inscrit(saison, tireur, Optional.empty());
    }

    public Inscription inscrit(Saison saison, Tireur tireur, Annee annee) {
        return inscrit(saison, tireur, Optional.of(annee));
    }

    public Inscription inscrit(Saison saison, Tireur tireur, Optional<Annee> annee) {
        Inscription ins;
        if (annee.isPresent())
            ins = new Inscription(saison, tireur, annee.get());
        else
            ins = new Inscription(saison, tireur);

        // ** RG 11 : Un tireur ne peut être inscrit qu’une seule fois pour une saison. **
        if (!inscritRepository.findByTireurAndSaison(tireur, saison).isPresent()) {
            LOGGER.log(Level.INFO, "Inscription de " + tireur.getUsername() + " à la saison " + saison.getId());

            inscritRepository.save(ins);
        }
        return ins;
    }

    public Inscription inscrit(Inscription inscription, Optional<Annee> annee) {
        LOGGER.log(Level.INFO, "Réinscription de " + inscription.getTireur().getUsername() + " à la saison " + inscription.getSaison().getId());
        inscription.setActif(true);
        if(annee.isPresent()) inscription.setAnnee(annee.get());
        inscritRepository.save(inscription);
        return inscription;
    }

    public void desinscrit(Inscription ins) throws InscriptionException {
        InscriptionException ex = new InscriptionException();
        if (ins.getSaison().isSaisonFinished()) {
            LOGGER.log(Level.WARNING, "On ne peut pas désinscrire des tireurs d'une saison terminée.");
            ex.add("On ne peut pas désinscrire des tireurs d'une saison terminée.");
            throw ex;
        } else if (ins.getSaison().isSaisonOpen()) {
            LOGGER.log(Level.INFO, String.format("L'inscription de %s à la saison %d a été désactivée.",
                    ins.getTireur().getFullName(),
                    ins.getSaison().getId()
            ));
            ins.setActif(false);
            ins.setDateDesinscription(new Date());
            // on supprime les potentiels présences futur
            List<Participant> alParticipe = coursService.getParticipantsByInscrit(ins);
            for(Participant p : alParticipe){
                if(p.getCours().getDateCours().after(ins.getDateDesinscription().get())){
                    coursService.removeParticpant(p);
                }
            }
            inscritRepository.save(ins);
        } else {
            LOGGER.log(Level.INFO, String.format("%s est désinscrit de la saison %d.",
                    ins.getTireur().getFullName(),
                    ins.getSaison().getId()
            ));
            inscritRepository.delete(ins);
        }
    }

    public void desinscrit(long id) throws InscriptionException {
        Optional<Inscription> ins = inscritRepository.findById(id);
        if (ins.isPresent()) {
            desinscrit(ins.get());
        }
    }

    public List<Tireur> findTireursNonInscrits(Saison s) {
        LOGGER.log(Level.FINE, "Récupération de la liste des non-inscrits à la saison {0}", s.getId());
        List<Tireur> noninscrits = inscritRepository.findTireurNonInscrits(s);
        noninscrits=noninscrits.stream()
                .filter(t -> t.isActif())
                .collect(Collectors.toList());
        return noninscrits;
    }
    public Optional<Inscription> findInscription(long id) {
        return inscritRepository.findById(id);
    }

    public Optional<Inscription> findFirstBySaison(Saison saison) {
        return inscritRepository.findFirstBySaison(saison);
    }

    public List<Arme> findArmesAttribues(Saison s) {
        return inscritRepository.findArmesAssignee(s);
    }

    public void attribuerArme(Inscription inscription, Arme arme) throws AttributionException {
        Optional<Inscription> opInsc = inscritRepository.findBySaisonAndArme(inscription.getSaison(), arme);

        /* RG 10 - attribution d'une arme */
        if (opInsc.isPresent()) {
            throw new AttributionException(SaisonEnumeration.ARME_DEJA_ATTRIBUEE.getErreurException());
        } else {
            inscription.setArme(arme);
            inscritRepository.save(inscription);
        }
    }

    public void enleverArme(Inscription inscription) {
        inscription.setArme(null);
        inscritRepository.save(inscription);
    }

    public Saison getSaisonIfExistOrThrow404(long idSaison) {
        Optional<Saison> optSaison = findById(idSaison);
        if (!optSaison.isPresent()) {
            LOGGER.log(Level.SEVERE, SaisonEnumeration.SAISON_INEXISTANTE.getErreurforLog());
            throw new ResourceNotFoundException(SaisonEnumeration.SAISON_INEXISTANTE.getErreurException());
        } else
            return optSaison.get();
    }
}
