package ch.steversoix.jtv.desktop.saison.programme.services;

public enum ProgrammeEnumeration {
    NOM_NULL("Le nom du programme ne doit pas être vide.", "nom = null"),
    SAISON_NULL("La saison d'un programme doit être renseignée.", "saison = null"),
    NOPASS("Le nombre de passe ne peut pas être de zéro.", "passes.size <= 0"),
    ORDER_OUTOFBOUND("L'ordre d'une passe ne doit ni être vide ni supérieur au nombre de passes.", "passe.ordre < 1 or > passes.size"),
    PASSE_SERIES_OUTOFBOUND("Le nombre de séquences doit être entre 1 et 50.", "sequence < 1 or > 50"),
    PASSE_HITCOUNT_OUTOFBOUND("Le nombre de coups par séquence doit être entre 1 et 50.", "nbcoups < 1 or > 50"),
    PROGUPDATE_WITH_COURSUSING("Il n'est pas possible de modifier un programme si celui-ci est déjà attribué à un cours.", "programme déjà utiliser"),
    SAISON_TERMINE("Saison terminée on ne peut ni ajouter ni modifier de programme.","Today > SaisonFin"),
    PROGRAMME_USED("Des cours utilisent ce programme, il ne peut donc pas être modifier.","Programme Utilised By Courses"),
    PROGRAMME_INEXISTANT("Le programme auquel vous tentez d'accéder n'existe pas.", "Programme Not Found");

    private String erreurException;
    private String erreurForLog;

    ProgrammeEnumeration(String erreurException, String erreurForLog) {
        this.erreurException = erreurException;
        this.erreurForLog = erreurForLog;
    }

    public String getErreurException() {
        return erreurException;
    }
    public String getErreurforLog() {
        return erreurForLog;
    }
}
