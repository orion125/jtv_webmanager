package ch.steversoix.jtv.desktop.saison.model;

import ch.steversoix.jtv.desktop.arme.model.Arme;
import ch.steversoix.jtv.desktop.saison.annee.model.Annee;
import ch.steversoix.jtv.desktop.saison.cours.model.Participant;
import ch.steversoix.jtv.desktop.tireur.model.Tireur;

import javax.persistence.*;
import java.util.Date;
import java.util.Optional;
import java.util.Set;

@Entity
@Table(name = "jtv_inscription")
public class Inscription implements Comparable<Inscription> {
    @Id
    @GeneratedValue
    @Column(name = "ins_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ins_sai_id")
    private Saison saison;

    @ManyToOne
    @JoinColumn(name = "ins_tir_id")
    private Tireur tireur;

    @ManyToOne
    @JoinColumn(name = "ins_ann_id")
    private Annee annee;

    @ManyToOne
    @JoinColumn(name = "ins_arm_id")
    private Arme arme;

    @Column(name = "ins_actif")
    private boolean actif;

    @Column(name = "ins_date_desinscrit")
    private Date dateDesinscription;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "inscrit", cascade = CascadeType.ALL)
    private Set<Participant> presences;

    public Inscription() {

    }

    public Inscription(Saison saison, Tireur tireur) {
        this(saison, tireur, null);
    }

    public Inscription(Saison saison, Tireur tireur, Annee annee) {
        this(saison, tireur, annee, null, true);
    }

    public Inscription(Saison saison, Tireur tireur, Annee annee, Arme arme, boolean actif) {
        this.saison = saison;
        this.tireur = tireur;
        this.annee = annee;
        this.arme = arme;
        this.actif = actif;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Saison getSaison() {
        return saison;
    }

    public void setSaison(Saison saison) {
        this.saison = saison;
    }

    public Tireur getTireur() {
        return tireur;
    }

    public void setTireur(Tireur tireur) {
        this.tireur = tireur;
    }

    public Annee getAnnee() {
        return annee;
    }

    public void setAnnee(Annee annee) {
        this.annee = annee;
    }

    public Arme getArme() {
        return arme;
    }

    public void setArme(Arme arme) {
        this.arme = arme;
    }

    public Optional<Date> getDateDesinscription() {
        return Optional.ofNullable(dateDesinscription);
    }

    public void setDateDesinscription(Date dateDesinscription) {
        this.dateDesinscription = dateDesinscription;
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Inscription that = (Inscription) o;

        if (!saison.equals(that.saison)) return false;
        return tireur.equals(that.tireur);
    }

    @Override
    public int hashCode() {
        int result = saison.hashCode();
        result = 31 * result + tireur.hashCode();
        return result;
    }

    @Override
    public int compareTo(Inscription o) {
        return this.getTireur().compareTo(o.getTireur());
    }
}
