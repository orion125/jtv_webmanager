package ch.steversoix.jtv.desktop.saison.cours.model;

import ch.steversoix.jtv.desktop.saison.model.Inscription;
import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Logger;

@Entity
@Table(name = "jtv_absence")
public class Absence {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Id
    @GeneratedValue
    @Column(name = "abs_id")
    private Long id;
    @ManyToOne
    @JoinColumn(name="abs_cou_id")
    private Cours cours;
    @ManyToOne
    @JoinColumn(name="abs_tir_id")
    private Tireur tireur;
    @Column(name = "abs_motif")
    private String motif;
    @Column(name = "abs_date")
    @DateTimeFormat(pattern = "jj-MM-aaaa HH'h'mm")
    private LocalDateTime dateAbsence = LocalDateTime.now();

    // Constructeurs
    public Absence() {}
    public Absence(Cours cours, Tireur tireur, String motif ) {
        this.cours = cours;
        this.tireur = tireur;
        this.motif = motif;
    }
    public Absence(Long id, Cours cours, Tireur tireur, String motif) {
        this(id,cours,tireur,motif,LocalDateTime.now());
    }
    public Absence(Long id, Cours cours, Tireur tireur, String motif, LocalDateTime dateAbsence) {
        this.id = id;
        this.cours = cours;
        this.tireur = tireur;
        this.motif = motif;
        this.dateAbsence = dateAbsence;
    }

    // RG 35 - Ne peut pas prévenir d'une absence qu'avant la veille du cours à midi.
    public boolean controlDate(){
        ZoneId defaultZoneId = ZoneId.systemDefault();
        Instant instant = this.getCours().getDateCours().toInstant();
        LocalDateTime coursDate = instant.atZone(defaultZoneId).toLocalDateTime();
        LocalDateTime veilleMidi = coursDate.minusDays(new Long(1)).plusHours(new Long(12));
        return this.dateAbsence.isBefore(veilleMidi);
    }

    // Getter et Setter
    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}
    public Cours getCours() {return cours;}
    public void setCours(Cours cours) {this.cours = cours;}
    public Tireur getTireur() {return tireur;}
    public void setTireur(Tireur tireur) {this.tireur = tireur;}
    public String getMotif() {return motif;}
    public void setMotif(String motif) {this.motif = motif;}

    public LocalDateTime getDateAbsence() {return dateAbsence;}
    public void setDateAbsence(LocalDateTime dateAbsence) {this.dateAbsence = dateAbsence;}
    public String printDateAbsence(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH'h'mm");
        return this.dateAbsence.format(formatter);
    }

    /* Comparaison */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Absence absence = (Absence) o;

        if (getId() != absence.getId()) return false;
        if (!getCours().equals(absence.getCours())) return false;
        return getTireur().equals(absence.getTireur());
    }

}
