package ch.steversoix.jtv.desktop.saison.annee.service;

public enum AnneeEnumeration {
    NOM_VIDE("nom != null"),
    EXISTE_DEJA("année existe déjà");

    private String erreurException;

    AnneeEnumeration(String erreurException) {
        this.erreurException = erreurException;
    }

    public String getErreurException() {
        return erreurException;
    }
}
