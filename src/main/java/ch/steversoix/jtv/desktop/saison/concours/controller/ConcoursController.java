package ch.steversoix.jtv.desktop.saison.concours.controller;

import ch.steversoix.jtv.desktop.outils.FlashMessage;
import ch.steversoix.jtv.desktop.outils.FlashMessageGenerator;
import ch.steversoix.jtv.desktop.saison.concours.exception.ConcoursException;
import ch.steversoix.jtv.desktop.saison.concours.model.Concours;
import ch.steversoix.jtv.desktop.saison.concours.service.ConcoursService;
import ch.steversoix.jtv.desktop.saison.model.Saison;
import ch.steversoix.jtv.desktop.saison.service.SaisonService;
import ch.steversoix.jtv.desktop.tireur.services.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Controller
@RequestMapping(path = "/saisons/{idSaison}/concours")
public class ConcoursController {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Autowired
    SaisonService saisonService;
    @Autowired
    ConcoursService concoursService;

    @ModelAttribute("user")
    public SecurityUser getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (SecurityUser) auth.getPrincipal();
    }

    /*
    * Les requêtes qui suivent, concernent la navigation
    * */

    /* Affichage du formulaire qui liste les cours */
    @GetMapping("")
    public String getConcours(@PathVariable("idSaison") long idSaison, Model model) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        List<Concours> lst = concoursService.findConcoursBySaisonAndByActif(saison, true);
        model.addAttribute("concours", lst);
        model.addAttribute("saison", saison);
        model.addAttribute("actif",true);
        return "gestionConcours";
    }

    /* Affichage du formulaire qui liste les cours */
    @GetMapping("/inactifs")
    public String getCoursInactifs(@PathVariable("idSaison") long idSaison, Model model) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        List<Concours> lst = concoursService.findConcoursBySaisonAndByActif(saison, false);
        model.addAttribute("concours", lst);
        model.addAttribute("saison", saison);
        model.addAttribute("actif",false);
        return "gestionConcours";
    }

    /* Affichage du formulaire qui ajoute un cours */
    @GetMapping("/add")
    public String addCours(@PathVariable("idSaison") long idSaison, Model model) {
        /* RA 03 : saison terminée - on ne peut plus ajouter de concours */
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        Date d = new Date();
        if (saison.getDateFin().before(d)) {
            List<Concours> lst = concoursService.findConcoursBySaisonAndByActif(saison, true);
            model.addAttribute("cours", lst);
            model.addAttribute("saison",saison);
            model.addAttribute("actif",true);
            model.addAttribute("erreurs", "saison terminée on peut pas ajouter de concours");
            return "redirect:/saisons/"+idSaison+"/concours";
        }

        model.addAttribute("concours", new Concours());
        model.addAttribute("saison", saison);
        return "ajoutConcours";
    }

    /* Affichage du formulaire qui modifie un cours */
    @GetMapping("/{idConcours}/edit")
    public String editConcours(@PathVariable("idSaison") long idSaison, @PathVariable("idConcours") long idConcours, Model model) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        Concours concours = concoursService.getConcoursIfExistOrThrow404(idConcours);

        model.addAttribute("concours", concours);
        model.addAttribute("saison", saison);
        return "ajoutConcours";
    }

    /*
    * Les requêtes qui suivent, intéragissent avec la BDD
    * */

    /* Ajout du cours dans la BDD */
    @PostMapping("")
    public String addCoursDB(@Valid @ModelAttribute("concours") Concours newConcours, BindingResult bindingResult, @PathVariable("idSaison") long idSaison, Model model) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        if (bindingResult.hasErrors()) {
            model.addAttribute("saison", saison);
            return "ajoutConcours";
        }

        newConcours.setSaison(saisonService.findSaisonById(idSaison));
        try {
            concoursService.save(newConcours);
        } catch (ConcoursException e) {
            model.addAttribute("concours", newConcours);
            model.addAttribute("saison", saison);
            model.addAttribute("messages", FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER, e.getErreurs()));
            return "ajoutConcours";
        }
        return "redirect:/saisons/" + idSaison + "/concours";
    }

    /* Modifie un cours dans la BDD */
    @PostMapping("/{idConcours}/update")
    public String updateCoursDB(@Valid @ModelAttribute("concours") Concours concours, BindingResult bindingResult, @PathVariable("idSaison") long idSaison, @PathVariable("idConcours") long idConcours, Model model) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        concours.setId(idConcours);
        concours.setSaison(saisonService.findSaisonById(idSaison));

        if (bindingResult.hasErrors()) {
            model.addAttribute("saison", saison);
            model.addAttribute("concours",concours);
            return "ajoutConcours";
        }

        try {
            concoursService.update(concours);
        } catch (ConcoursException e) {
            model.addAttribute("concours", concours);
            model.addAttribute("saison", saison);
            model.addAttribute("messages", FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER, e.getErreurs()));
            return "ajoutConcours";
        }
        return "redirect:/saisons/" + idSaison + "/concours";
    }

    /* Désactive un cours dans la BDD */
    @GetMapping("/{idConcours}/desactivate")
    public String desactivateConcoursDB(@PathVariable("idConcours") long idConcours, @PathVariable("idSaison") long idSaison, Model model,
                                        RedirectAttributes redirectAttr) {
        Concours co = concoursService.getConcoursIfExistOrThrow404(idConcours);
        try {
            concoursService.desactivate(co);
        } catch (ConcoursException ex) {
            redirectAttr.addFlashAttribute("messages", FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER,ex.getErreurs()));
            return "redirect:/saisons/"+idSaison+"/concours";
        }
        return "redirect:/saisons/" + idSaison + "/concours";
    }

    /* Active un cours dans la BDD */
    @GetMapping("/{idConcours}/activate")
    public String activateCoursDB(@PathVariable("idConcours") long idConcours, @PathVariable("idSaison") long idSaison, Model model,
                                  RedirectAttributes redirectAttr) {
        Concours co = concoursService.getConcoursIfExistOrThrow404(idConcours);
        try {
            concoursService.activate(co);
        } catch (ConcoursException ex) {
            redirectAttr.addFlashAttribute("messages", FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER,ex.getErreurs()));
            return "redirect:/saisons/"+idSaison+"/concours";
        }
        return "redirect:/saisons/" + idSaison + "/concours/inactifs";
    }

    /*
    * Méthodes internes
    * */

}
