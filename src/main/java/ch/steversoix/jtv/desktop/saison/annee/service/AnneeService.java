package ch.steversoix.jtv.desktop.saison.annee.service;

import ch.steversoix.jtv.desktop.outils.FilterTools;
import ch.steversoix.jtv.desktop.saison.annee.exception.AnneeException;
import ch.steversoix.jtv.desktop.saison.annee.model.Annee;
import ch.steversoix.jtv.desktop.saison.annee.model.AnneeRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class AnneeService {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    AnneeRepository anneeRepository;

    /*
    *  Constructeur
    * */
    public AnneeService(AnneeRepository anneeRepository) {
        this.anneeRepository = anneeRepository;
    }

    public Annee findAnneeById(long id){
        return anneeRepository.findOne(id);
    }

    public List<Annee> findAnneesByActif(boolean actif){
        return anneeRepository.findAnneesByActif(actif);
    }

    public void save(Annee newAnnee) throws AnneeException {
        AnneeException exception = new AnneeException();
        exception.addAll(rulesAddUpdate(newAnnee));
        if(exception.isEmpty()){
            anneeRepository.save(newAnnee);
            LOGGER.log(Level.INFO, "Create Annee : " + newAnnee.getNom());
        }else{
            throw exception;
        }

    }

    public void update(Annee annee) throws AnneeException {
        AnneeException exception = new AnneeException();
        exception.addAll(rulesAddUpdate(annee));
        if(exception.isEmpty()) {
            anneeRepository.save(annee);
            LOGGER.log(Level.INFO, "Update Annee : " + annee.getNom());
        }else{
            throw exception;
        }
    }

    public void desactivate(Annee annee){
        annee.setActif(false);
        anneeRepository.save(annee);
        LOGGER.log(Level.INFO, "Desactivate Annee : " + annee.getNom());
    }

    public void activate(Annee annee){
        annee.setActif(true);
        anneeRepository.save(annee);
        LOGGER.log(Level.INFO, "Activate Annee : " + annee.getNom());
    }

    private ArrayList<String> rulesAddUpdate(Annee annee){
        ArrayList<String> erreurs = new ArrayList<>();

        if(annee.getNom().trim()==""){
            erreurs.add(AnneeEnumeration.NOM_VIDE.getErreurException());
        }

        /* RG 34 : L'année (nom) doit être unique */
        boolean existe = false;
        List<Annee> al = anneeRepository.findAll(); // Il faut aussi vérifier les saisons supprimées
        for(Annee a: al){
            if(FilterTools.replaceAccents(a.getNom().toLowerCase()).equals(FilterTools.replaceAccents(annee.getNom().toLowerCase()))){
                existe = true;
            }
        }

        if(existe){
            erreurs.add(AnneeEnumeration.EXISTE_DEJA.getErreurException());
        }

        return erreurs;
    }
}
