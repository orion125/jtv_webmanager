package ch.steversoix.jtv.desktop.saison.concours.service;

import ch.steversoix.jtv.desktop.core.exception.ResourceNotFoundException;
import ch.steversoix.jtv.desktop.saison.concours.exception.ConcoursException;
import ch.steversoix.jtv.desktop.saison.concours.model.Concours;
import ch.steversoix.jtv.desktop.saison.concours.model.ConcoursRepository;
import ch.steversoix.jtv.desktop.saison.model.Saison;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class ConcoursService {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private ConcoursRepository concoursRepository;

    public ConcoursService(ConcoursRepository concoursRepository) {
        this.concoursRepository = concoursRepository;
    }

    public List<Concours> findConcoursBySaisonAndByActif(Saison saison, boolean actif){
        return concoursRepository.findBySaisonAndActif(saison,actif);
    }

    public Concours findConcoursById(Long id){
        return concoursRepository.findOne(id);
    }

    public void create(Concours newConcours){
        concoursRepository.save(newConcours);
    }

    public void save(Concours newConcours)throws ConcoursException{
        ConcoursException exception = new ConcoursException();
        exception.addAll(rulesAddUpdate(newConcours));
        exception.addAll(rulesAdd(newConcours));
        if(exception.isEmpty()) {
            newConcours.setActif(true);
            concoursRepository.save(newConcours);
        }else{
            throw exception;
        }
    }

    public void update(Concours concours)throws ConcoursException{
        ConcoursException exception = new ConcoursException();
        exception.addAll(rulesAddUpdate(concours));
        if(exception.isEmpty()) {
            concours.setActif(true);
            concoursRepository.save(concours);
        }else{
            throw exception;
        }
    }

    public void desactivate(Concours concours)throws ConcoursException{
        concours.setActif(false);
        concoursRepository.save(concours);
    }

    public void activate(Concours concours)throws ConcoursException{
        concours.setActif(true);
        concoursRepository.save(concours);
    }

    private ArrayList<String> rulesAddUpdate(Concours concours){
        ArrayList<String> erreurs = new ArrayList<>();

        if (concours.getNom() == null || concours.getNom().equals(""))
            erreurs.add(ConcoursEnumeration.NO_NAME.getErreurException());


        if (concours.getLieu() == null || concours.getLieu().equals(""))
            erreurs.add(ConcoursEnumeration.NO_LOCATION.getErreurException());

        /* RG 25 - Prix */
        if(concours.getPrix() < Concours.PRIX_MIN || concours.getPrix() > Concours.PRIX_MAX){
            erreurs.add(ConcoursEnumeration.PRIX_HORS_BORNE.getErreurException());
        }

        if(concours.getDateConcours() != null) {
            /* RG 29 - Date concours */
            if (concours.getSaison().getDateDebut().after(concours.getDateConcours()) || concours.getSaison().getDateFin().before(concours.getDateConcours())) {
                erreurs.add(ConcoursEnumeration.HORS_SAISON.getErreurException());
            }
        }else{
            erreurs.add(ConcoursEnumeration.CONCOURS_DATE_NULL.getErreurException());
        }

        return erreurs;
    }

    private ArrayList<String> rulesAdd(Concours concours){
        ArrayList<String> erreurs = new ArrayList<>();

        /* RA 03 - Concours */
        if(concours.getSaison().isSaisonFinished()){
            erreurs.add(ConcoursEnumeration.SAISON_FINISHED.getErreurException());
        }


        return erreurs;
    }

    public Concours getConcoursIfExistOrThrow404(long idConcours) {
        Optional<Concours> optConcours = concoursRepository.findById(idConcours);
        if (!optConcours.isPresent()) {
            LOGGER.log(Level.SEVERE, ConcoursEnumeration.CONCOURS_INEXISTANT.getErreurforLog());
            throw new ResourceNotFoundException(ConcoursEnumeration.CONCOURS_INEXISTANT.getErreurException());
        } else
            return optConcours.get();
    }
}
