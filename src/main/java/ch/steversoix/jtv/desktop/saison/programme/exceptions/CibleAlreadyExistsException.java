package ch.steversoix.jtv.desktop.saison.programme.exceptions;

public class CibleAlreadyExistsException extends IllegalArgumentException {
    public CibleAlreadyExistsException(String s) {
        super(s);
    }

    public CibleAlreadyExistsException() {
    }
}
