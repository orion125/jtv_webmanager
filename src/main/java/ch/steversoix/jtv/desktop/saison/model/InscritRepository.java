package ch.steversoix.jtv.desktop.saison.model;

import ch.steversoix.jtv.desktop.arme.model.Arme;
import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InscritRepository extends JpaRepository<Inscription, Long> {

    public List<Inscription> findBySaison(Saison s);

    public List<Inscription> findBySaisonAndActifTrue(Saison s);
    public List<Inscription> findBySaisonAndActifFalse(Saison s);

    public List<Inscription> findByTireur(Tireur t);

    public Optional <Inscription> findById(long id);

    public Optional<Inscription> findByTireurAndSaison(Tireur t, Saison s);

    @Query("select t from Tireur t where not exists(select 1 from Inscription i where i.tireur = t and i.saison = ?1)")
    public List<Tireur> findTireurNonInscrits(Saison s);

    public Optional<Inscription> findBySaisonAndArme(Saison s, Arme a);

    @Query("select i.arme from Inscription i where i.saison = ?1")
    public List<Arme> findArmesAssignee(Saison s);

    public Optional<Inscription> findFirstByArme(Arme arme);

    public Optional<Inscription> findFirstBySaison(Saison saison);

}
