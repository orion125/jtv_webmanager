package ch.steversoix.jtv.desktop.saison.programme.model;

import ch.steversoix.jtv.desktop.saison.cours.model.CoursProgrammeAnnee;
import ch.steversoix.jtv.desktop.saison.model.Saison;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "jtv_programme")
public class Programme {

    @Id
    @GeneratedValue
    @Column(name = "pro_id")
    private Long id;
    @Column(name = "pro_nom")
    private String nom;
    @Column(name = "pro_description", nullable = true)
    private String desc;
    @Column(name = "pro_actif", nullable = true)
    private boolean actif = true;

    public Programme(){}
    public Programme(String nom, String desc) {this(nom,desc,true);}

    public Programme(String nom, String desc, Saison saison) {
        this(nom,desc,true, saison);
    }
    public Programme(String nom, String desc, boolean actif) {
        this(nom,desc,actif, new Saison());
    }
    public Programme(String nom, String desc, boolean actif, Saison saison) {this(nom,desc,actif,saison,new ArrayList<Passe>());}
    public Programme(String nom, String desc, Saison saison, List<Passe> passes) {this(nom,desc,true,saison,passes); }
    public Programme(String nom, String desc, boolean actif, Saison saison, List<Passe> passes) {
        this.nom = nom;
        this.desc = desc;
        this.actif = actif;
        this.saison = saison;
        this.passes = passes;
    }

    @ManyToOne
    @JoinColumn(name="pro_sai_id", referencedColumnName = "sai_id")
    private Saison saison;


    @OneToMany(mappedBy = "programme", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    List<Passe> passes;


    @OneToMany(mappedBy="programme", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<CoursProgrammeAnnee> coursConcerner;

    public List<Passe> getPasses() {return passes;}
    public void setPasses(List<Passe> passes) {this.passes = passes;}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}
    public String getNom() {return nom; }
    public void setNom(String nom) {this.nom = nom;}
    public String getDesc() {return desc;}
    public void setDesc(String desc) {this.desc = desc;}
    public boolean isActif() {return actif;}
    public void setActif(boolean actif) {this.actif = actif;}

    public Saison getSaison() {return saison;}
    public void setSaison(Saison saison) {this.saison = saison;}

    public List<CoursProgrammeAnnee> getCoursConcerner() {  return coursConcerner;  }
    public void setCoursConcerner(List<CoursProgrammeAnnee> coursConcerner) {this.coursConcerner = coursConcerner; }

    public boolean hasCoursConcerner(){
        boolean test = true;
        if (coursConcerner == null) test = false;
        else if (coursConcerner.size() <=0) test = false;
        return test;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Programme programme = (Programme) o;

        return getId() == programme.getId();
    }

    public int calculTotalPoints(){
        int totalPoints = 0;
        for (Passe p : getPasses()) totalPoints += p.calculeTotalPointsPasse();
        return totalPoints;
    }

    public int calculeTotalMunitionsRequis(){
        int totalMunitions = 0;
        for (Passe p : getPasses()) totalMunitions += p.calculeNombreTirsPasse();
        return totalMunitions;

    }

    @Override
    public String toString() {
        return "Programme n°" + id +
                " lià à la saison " +saison.toString()+
                " "+ nom +
                " Description " + desc ;
    }
}
