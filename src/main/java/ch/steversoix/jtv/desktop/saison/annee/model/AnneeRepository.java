package ch.steversoix.jtv.desktop.saison.annee.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AnneeRepository extends JpaRepository<Annee, Long> {

    @Query(value="select an from Annee an where an.actif= ?1")
    List<Annee> findAnneesByActif(boolean actif);
}
