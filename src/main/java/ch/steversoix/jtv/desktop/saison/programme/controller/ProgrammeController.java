package ch.steversoix.jtv.desktop.saison.programme.controller;


import ch.steversoix.jtv.desktop.outils.FlashMessage;
import ch.steversoix.jtv.desktop.outils.FlashMessageGenerator;
import ch.steversoix.jtv.desktop.saison.model.Saison;
import ch.steversoix.jtv.desktop.saison.model.SaisonRepository;
import ch.steversoix.jtv.desktop.saison.programme.exceptions.ProgrammeException;
import ch.steversoix.jtv.desktop.saison.programme.model.*;
import ch.steversoix.jtv.desktop.saison.programme.services.ProgrammeEnumeration;
import ch.steversoix.jtv.desktop.saison.programme.services.ProgrammeService;
import ch.steversoix.jtv.desktop.saison.programme.services.TypeCoupEnumeration;
import ch.steversoix.jtv.desktop.saison.service.SaisonEnumeration;
import ch.steversoix.jtv.desktop.saison.service.SaisonService;
import ch.steversoix.jtv.desktop.tireur.services.SecurityUser;
import org.springframework.beans.factory.annotation.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

@Controller
@RequestMapping(path="/saisons/{idSaison}/programmes")
public class ProgrammeController {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Autowired
    private SaisonService saisonService;
    @Autowired
    private CibleRepository cibleRepository;
    @Autowired
    private ProgrammeService programmeService;

    @ModelAttribute("user")
    public SecurityUser getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (SecurityUser) auth.getPrincipal();
    }

    /* Affichage du formulaire qui liste les programmes */
    @GetMapping("")
    public String getProgrammes(@PathVariable("idSaison") long idSaison ,Model model){
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        List<Programme> lst = programmeService.findProgrammesBySaisonAndByActif(saison,true);
        model.addAttribute("saison",saison);
        model.addAttribute("programmes",lst);
        model.addAttribute("actif",true);
        return "gestionProgrammes";
    }

    /* Affichage du formulaire qui liste les programmes inactif */
    @GetMapping("/inactifs")
    public String getProgrammesInactifs(@PathVariable("idSaison") long idSaison ,Model model){
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        List<Programme> lst = programmeService.findProgrammesBySaisonAndByActif(saison,false);
        model.addAttribute("saison",saison);
        model.addAttribute("programmes",lst);
        model.addAttribute("actif",false);
        return "gestionProgrammes";
    }

    /* Affichage du formulaire qui ajoute un programme */
    @GetMapping("/add")
    public String addProgramme(@PathVariable("idSaison") long idSaison, Model model, RedirectAttributes redirAttrs){
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        Optional<String> opSaisonFinish = checkSaisonFinished(saison);
        if(opSaisonFinish.isPresent()){
            LOGGER.log(Level.SEVERE, ProgrammeEnumeration.SAISON_TERMINE.getErreurforLog());
            redirAttrs.addFlashAttribute("message",FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.DANGER,
                    ProgrammeEnumeration.SAISON_TERMINE.getErreurException()));
            return opSaisonFinish.get();
        }
        List<Cible> cibles = cibleRepository.findCibleByActif(true);
        Programme pro = new Programme();
        model.addAttribute("saison",saison);
        model.addAttribute("programme",pro);
        model.addAttribute("enumType", TypeCoupEnumeration.values());
        model.addAttribute("cibles",cibles);
        return "ajoutProgramme";
    }

    /* Affichage du formulaire qui modifie un programme */
    @GetMapping("/{id}/edit")
    public String editProgramme(@PathVariable("idSaison") long idSaison,@PathVariable("id") Long id, Model model, RedirectAttributes redirAttrs){
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        Programme po = programmeService.getProgrammeIfExistOrThrow404(id);
        Optional<String> opSaisonFinish = checkSaisonFinished(saison);
        if(opSaisonFinish.isPresent()){
            LOGGER.log(Level.SEVERE, ProgrammeEnumeration.SAISON_TERMINE.getErreurforLog());
            redirAttrs.addFlashAttribute("message",FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.DANGER,
                    ProgrammeEnumeration.SAISON_TERMINE.getErreurException()));
            return opSaisonFinish.get();
        }
        Optional<String> opUsedInCourses = checkProgrammesUsed(po,saison);
        if(opUsedInCourses.isPresent()){
            LOGGER.log(Level.SEVERE, ProgrammeEnumeration.PROGRAMME_USED.getErreurforLog());
            redirAttrs.addFlashAttribute("message",FlashMessageGenerator.generateSingleMessage(
                    FlashMessage.Level.DANGER,ProgrammeEnumeration.PROGRAMME_USED.getErreurException()));
            return opUsedInCourses.get();
        }
        List<Cible> cibles = cibleRepository.findCibleByActif(true);
        model.addAttribute("saison",saison);
        model.addAttribute("enumType", TypeCoupEnumeration.values());
        model.addAttribute("cibles",cibles);
        model.addAttribute("programme",po);
        return "ajoutProgramme";
    }

    @GetMapping("/{id}/detail")
    public String detailProgramme(@PathVariable("idSaison") long idSaison,@PathVariable("id") Long id, Model model){
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        Programme po = programmeService.getProgrammeIfExistOrThrow404(id);
        model.addAttribute("saison",saison);
        model.addAttribute("programme",po);
        return "programmeDetail";
    }

    /*
    * Les requêtes qui suivent intéragissent avec la BDD
    * */

    /* Ajout un programme dans la BDD */
    @PostMapping("")
    public String addProgrammeDB(@Valid @ModelAttribute("programme") Programme newProgramme,@PathVariable("idSaison") long idSaison,
                                 BindingResult bindingResult, Model model, RedirectAttributes redirAttrs){
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        if (bindingResult.hasErrors()) return reloadAjoutEditIfBindingResultError(model, saison, newProgramme);
        newProgramme.setSaison(saison);
        try{programmeService.create(newProgramme);}
        catch(ProgrammeException ex){return reloadAjoutEditIfException(model, saison, newProgramme, ex);}
        LOGGER.log(Level.FINE, "Le programme "+newProgramme.getNom()+" a été ajouté.");
        redirAttrs.addFlashAttribute("message",FlashMessageGenerator.generateSingleMessage(
                FlashMessage.Level.SUCCESS,"Le programme "+newProgramme.getNom()+" a été ajouté."));
        return "redirect:/saisons/"+idSaison+"/programmes";
    }

    /* Modifie un programme dans la BDD */
    @PostMapping("/{id}/update")
    public String updateProgrammeDB(@Valid @ModelAttribute("programme") Programme programme,@PathVariable("id") long id,
                                    @PathVariable("idSaison") long idSaison, BindingResult bindingResult, Model model
            , RedirectAttributes redirAttrs){
        Programme pro = programmeService.getProgrammeIfExistOrThrow404(id);
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);

        if (bindingResult.hasErrors()) return reloadAjoutEditIfBindingResultError(model, saison, programme);
        if(!pro.equals(null)){
            programme.setId(pro.getId());
            programme.setSaison(saison);
            try{programmeService.update(programme);}
            catch(ProgrammeException ex){return reloadAjoutEditIfException(model, saison, programme, ex);}
        }
        LOGGER.log(Level.FINE, "Le programme "+programme.getNom()+" a été modifié.");
        redirAttrs.addFlashAttribute("message",FlashMessageGenerator.generateSingleMessage(
                FlashMessage.Level.SUCCESS,"Le programme "+programme.getNom()+" a été modifié."));
        return "redirect:/saisons/"+idSaison+"/programmes";
    }
    @GetMapping("/{id}/desactivate")
    public String desactivateProgrammeDB(@PathVariable("id") Long id,  @PathVariable("idSaison") long idSaison, Model model, RedirectAttributes redirAttrs){
        Programme pro = programmeService.getProgrammeIfExistOrThrow404(id);
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        programmeService.desactivate(pro);
        LOGGER.log(Level.FINE, "Le programme "+pro.getNom()+" a été désactivé.");
        redirAttrs.addFlashAttribute("message",FlashMessageGenerator.generateSingleMessage(
                FlashMessage.Level.SUCCESS,"Le programme "+pro.getNom()+" a été désactivé."));
        return "redirect:/saisons/"+idSaison+"/programmes";
    }

    @GetMapping("/{id}/activate")
    public String activateProgrammeDB(@PathVariable("id") Long id, @PathVariable("idSaison") long idSaison, Model model, RedirectAttributes redirAttrs){
        Programme pro = programmeService.getProgrammeIfExistOrThrow404(id);
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        programmeService.activate(pro);
        LOGGER.log(Level.FINE, "Le programme "+pro.getNom()+" a été activé.");
        redirAttrs.addFlashAttribute("message",FlashMessageGenerator.generateSingleMessage(
                FlashMessage.Level.SUCCESS,"Le programme "+pro.getNom()+" a été activé."));
        return "redirect:/saisons/"+idSaison+"/programmes/inactifs";
    }


    /*
    * Méthodes internes
    * */

    public String reloadAjoutEditIfBindingResultError(Model model,Saison saison,Programme programme){
        List<Cible> cibles = cibleRepository.findCibleByActif(true);
        model.addAttribute("saison",saison);
        model.addAttribute("programme",programme);
        model.addAttribute("enumType", TypeCoupEnumeration.values());
        model.addAttribute("cibles",cibles);
        return "ajoutProgramme";
    }

    public String reloadAjoutEditIfException(Model model, Saison saison, Programme programme, ProgrammeException ex){
        List<Cible> cibles = cibleRepository.findCibleByActif(true);
        model.addAttribute("saison",saison);
        model.addAttribute("programme",programme);
        model.addAttribute("messages",FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER,ex.getErreurs()));
        model.addAttribute("enumType", TypeCoupEnumeration.values());
        model.addAttribute("cibles",cibles);
        return "ajoutProgramme";
    }

    /* Méthode qui vérifie que la saison est fini */
    // RA02
    private Optional<String> checkSaisonFinished(Saison saison){
        if(saison.isSaisonFinished()){
            return Optional.of("redirect:/saisons/"+saison.getId()+"/programmes");
        }
        return Optional.empty();
    }


    /* Méthode qui vérifie si le programme est déjà utiliser dans des cours */
    // RM02
    private Optional<String> checkProgrammesUsed(Programme pro, Saison saison){
        if(pro.hasCoursConcerner()){
            return Optional.of("redirect:/saisons/"+saison.getId()+"/programmes");
        }
        return Optional.empty();
    }
}
