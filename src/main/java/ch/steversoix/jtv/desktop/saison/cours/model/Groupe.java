package ch.steversoix.jtv.desktop.saison.cours.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "jtv_groupe")
public class Groupe implements Comparable<Groupe> {
    @Id
    @GeneratedValue
    @Column(name = "gro_id")
    private int id;
    @NotNull
    @Size(min=2, max=50)
    @Column(name = "gro_nom")
    private String nom;
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "groupes")
    private Set<Cours> cours = new HashSet<Cours>(0);
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "groupe", cascade = CascadeType.ALL)
    private List<Participant> participants;

    /* Constructeur */
    public Groupe(int id, String nom){
        this.id = id;
        this.nom = nom;
    }

    public Groupe(String nom) {
        this(-1,nom);
    }

    public Groupe(){
        this("");
    }

    /* Getter & Setter */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    /* Comparaison */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Groupe groupe = (Groupe) o;

        return id == groupe.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public int compareTo(Groupe o) {
        return id-o.getId();
    }

    public Set<Cours> getCours() {
        return cours;
    }

    public void setCours(Set<Cours> cours) {
        this.cours = cours;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }
}
