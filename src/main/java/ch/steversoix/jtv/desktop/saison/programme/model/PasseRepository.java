package ch.steversoix.jtv.desktop.saison.programme.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface PasseRepository extends JpaRepository<Passe, Long> {
    @Query(value="select pas from Passe pas WHERE pas.programme = ?1")
    List<Passe> findPassesByProgramme(Programme pro);

    @Transactional
    List<Passe> removePasseByProgramme(Programme programme);

    Optional<Cible> findCibleById(int id);

    List<Passe> findByCible(Cible cible);
}
