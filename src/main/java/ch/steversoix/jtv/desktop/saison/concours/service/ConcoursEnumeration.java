package ch.steversoix.jtv.desktop.saison.concours.service;

import ch.steversoix.jtv.desktop.saison.concours.model.Concours;

public enum ConcoursEnumeration {
    CONCOURS_DEJA_PRODUIT("La date d'un concours doit être postérieur à la date du jour.","dateCours ; dateJ"),
    HORS_SAISON("La date d'un concours doit se situer dans l'intervalle de la saison.","saionDeb ; saisonFin ; dateConcours"),
    PRIX_HORS_BORNE("Le prix d'un concours doit être entre "+Concours.PRIX_MIN+" et "+Concours.PRIX_MAX+".",
            "prix < "+Concours.PRIX_MIN+" || prix > "+Concours.PRIX_MAX),
    SAISON_FINISHED("On ne peut insérer un concours pour une saison terminée.","saison terminée"),
    NO_NAME("Le nom ne peut pas être vide.","Name = null"),
    NO_LOCATION("Un lieu doit être attribué pour un concours.", "Lieu = null"),
    CONCOURS_INEXISTANT("Le concours auquel vous tentez d'accéder n'existe pas.", "Concours NOT FOUND"),
    CONCOURS_DATE_NULL("La date d'un concours doit être renseignée.","dateConcours == null");


    private String erreurException;
    private String erreurForLog;

    ConcoursEnumeration(String erreurException, String erreurForLog) {
        this.erreurException = erreurException;
        this.erreurForLog = erreurForLog;
    }

    public String getErreurException() {
        return erreurException;
    }
    public String getErreurforLog() {
        return erreurForLog;
    }
}
