package ch.steversoix.jtv.desktop.saison.cours.model;

import ch.steversoix.jtv.desktop.poste.model.Poste;
import ch.steversoix.jtv.desktop.saison.annee.model.Annee;
import ch.steversoix.jtv.desktop.saison.model.Saison;
import ch.steversoix.jtv.desktop.saison.programme.model.Programme;
import org.hibernate.annotations.SortComparator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Entity
@Table(name = "jtv_cours")
public class Cours {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public final static int MIN_POSTE = 2;

    @Id
    @GeneratedValue
    @Column(name = "cou_id")
    private Long id;
    @Size(min=2, max=50)
    @Column(name = "cou_nom")
    private String nom;
    @Size(min=2, max=50)
    @Column(name = "cou_lieu")
    private String lieu;
    @NotNull(message="La date du cours ne peut pas être vide.")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "cou_date")
    private Date dateCours;
    @NotNull(message="L'''heure de début du cours ne peut pas être vide.")
    @DateTimeFormat(pattern = "HH:mm")
    @Column(name = "cou_heureDebut")
    private Date heureDebut;
    @NotNull(message="L'''heure de fin du cours ne peut pas être vide.")
    @DateTimeFormat(pattern = "HH:mm")
    @Column(name = "cou_heureFin")
    private Date heureFin;
    @Min(0) @Max(60)
    @Column(name = "cou_tempsNettoyage")
    private int tempsNettoyage;
    @Min(0) @Max(60)
    @Column(name = "cou_tempsPause")
    private int tempsPause;
    @Column(name = "cou_actif")
    private boolean actif;
    @ManyToOne
    @JoinColumn(name="cou_sai_id")
    private Saison saison;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "jtv_cours_postes",
            joinColumns = { @JoinColumn(name = "cou_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = { @JoinColumn(name = "pos_id", nullable = false, updatable = false)
            })
    private Set<Poste> postes = new HashSet<Poste>(0);
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "jtv_cours_groupes",
            joinColumns = {
                @JoinColumn(name = "cou_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "gro_id", nullable = false, updatable = false)
            })
    private Set<Groupe> groupes = new HashSet<Groupe>(0);
    @OneToMany(mappedBy = "cours", fetch = FetchType.LAZY)
    @SortComparator(HoraireComparator.class)
    private SortedSet<Horaire> horaires;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cours", cascade = CascadeType.ALL)
    @OrderBy("inscrit ASC")
    private SortedSet<Participant> participants;

    @OneToMany(mappedBy="cours", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<CoursProgrammeAnnee> programmesAnnee;

    @OneToMany(mappedBy="cours", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Absence> absences;

/* Constructeur */

    public Cours(){
        this(null,"","",null,null,null,0,0,null);
    }

    public Cours(String nom, String lieu, Date dateCours, Date heureDebut, Date heureFin, int tempsPause, int tempsNettoyage, Saison saison) {
        this(null,nom,lieu,dateCours,heureDebut,heureFin,tempsPause,tempsNettoyage,saison);
    }

    public Cours(Long id,String nom, String lieu, Date dateCours, Date heureDebut, Date heureFin, int tempsPause, int tempsNettoyage, Saison saison) {
        this.id = id;
        this.nom = nom;
        this.lieu = lieu;
        this.dateCours = dateCours;
        this.heureDebut = heureDebut;
        this.heureFin = heureFin;
        this.tempsNettoyage = tempsNettoyage;
        this.tempsPause = tempsPause;
        this.saison = saison;
        this.actif = true;
    }

    public Cours(Long id,String nom, String lieu, Date dateCours, Date heureDebut, Date heureFin, int tempsPause, int tempsNettoyage, Saison saison, boolean actif) {
        this.id = id;
        this.nom = nom;
        this.lieu = lieu;
        this.dateCours = dateCours;
        this.heureDebut = heureDebut;
        this.heureFin = heureFin;
        this.tempsNettoyage = tempsNettoyage;
        this.tempsPause = tempsPause;
        this.saison = saison;
        this.actif = actif;
    }


    /* Getter & Setter */

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}
    public String getNom() {return nom;}
    public void setNom(String nom) {this.nom = nom;}
    public String getLieu() {return lieu;}
    public void setLieu(String lieu) {this.lieu = lieu;}
    public Date getDateCours() {return dateCours;}
    public void setDateCours(Date dateCours) {this.dateCours = dateCours;}
    public Date getHeureDebut() {return heureDebut;}
    public void setHeureDebut(Date heureDebut) {this.heureDebut = heureDebut;}
    public Date getHeureFin() {return heureFin;}
    public void setHeureFin(Date heureFin) {this.heureFin = heureFin;}
    public int getTempsNettoyage() {return tempsNettoyage;}
    public void setTempsNettoyage(int tempsNettoyage) {this.tempsNettoyage = tempsNettoyage;}
    public int getTempsPause() {return tempsPause;}
    public void setTempsPause(int tempsPause) {this.tempsPause = tempsPause;}
    public boolean isActif() {return actif;}
    public void setActif(boolean actif) {this.actif = actif;}
    public Saison getSaison() {return saison;}
    public void setSaison(Saison saison) {this.saison = saison;}
    public Set<Poste> getPostes() {return postes;}
    public void setPostes(Set<Poste> postes) {this.postes = postes;}
    public Set<Groupe> getGroupes() {return groupes;}
    public void setGroupes(Set<Groupe> groupes) {this.groupes = groupes;}
    public SortedSet<Horaire> getHoraires() {return horaires;}
    public void setHoraires(SortedSet<Horaire> horaires) {this.horaires = horaires;}
    public SortedSet<Participant> getParticipants() {return participants;}
    public void setParticipants(SortedSet<Participant> participants) {
        this.participants = participants;
    }
    public List<CoursProgrammeAnnee> getProgrammesAnnee() {return programmesAnnee; }
    public void setProgrammesAnnee(List<CoursProgrammeAnnee> programmesAnnee) { this.programmesAnnee = programmesAnnee;}
    public List<Absence> getAbsences() {return absences;}
    public void setAbsences(List<Absence> absences) {this.absences = absences;}


    public Optional<Programme> getProgramme(Annee annee) {
        List<CoursProgrammeAnnee> programmeAnnee = getProgrammesAnnee().stream()
                .filter(pa -> pa.annee == annee)
                .limit(2)
                .collect(Collectors.toList());
        if(programmeAnnee.size() > 0)
            return Optional.of(programmeAnnee.get(0).getProgramme());
        else
            return Optional.empty();
    }
    /* Comparaison */

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cours other = (Cours) obj;
        return this.id.longValue() == other.id.longValue();
    }

    public boolean isCoursEditable() {
        return !isCoursFinished() && isActif();
    }

    public boolean isCoursFinished() {
        Date today = new java.util.Date();
        ZoneId defaultZoneId = ZoneId.systemDefault();
        Instant instant = getDateCours().toInstant();
        LocalDateTime coursDate = instant.atZone(defaultZoneId).toLocalDateTime();
        return coursDate.plusDays(1).isBefore(today.toInstant().atZone(defaultZoneId).toLocalDateTime());
    }
}
