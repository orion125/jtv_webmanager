package ch.steversoix.jtv.desktop.saison.cours.model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupeRepository extends JpaRepository<Groupe, Long> {

}
