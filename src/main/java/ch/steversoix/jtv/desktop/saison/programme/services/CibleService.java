package ch.steversoix.jtv.desktop.saison.programme.services;

import ch.steversoix.jtv.desktop.saison.programme.exceptions.CibleException;
import ch.steversoix.jtv.desktop.saison.programme.model.Cible;

import java.util.List;
import java.util.Optional;

public interface CibleService {
    List<Cible> findCiblesByActif(boolean actif);
    Cible findCibleById(long id);
    Optional<Cible> create(String nom, int valuemax);
    void update(Cible cible) throws CibleException;
    void save(Cible c) throws CibleException;
    void desactivate(Cible cible);
    void activate(Cible cible);

}
