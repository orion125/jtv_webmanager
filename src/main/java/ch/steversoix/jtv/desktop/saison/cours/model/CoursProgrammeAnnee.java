package ch.steversoix.jtv.desktop.saison.cours.model;

import ch.steversoix.jtv.desktop.saison.annee.model.Annee;
import ch.steversoix.jtv.desktop.saison.programme.model.Programme;

import javax.persistence.*;

@Entity
@Table(name="jtv_coursProgrammeAnnee")
public class CoursProgrammeAnnee {

    @Id
    @GeneratedValue
    @Column(name = "cpa_id")
    Long id;
    @ManyToOne
    @JoinColumn(name = "cpa_cou_id",referencedColumnName="cou_id")
    Cours cours;
    @ManyToOne
    @JoinColumn(name = "cpa_pro_id",referencedColumnName="pro_id")
    Programme programme;
    @ManyToOne
    @JoinColumn(name = "cpa_ann_id",referencedColumnName="ann_id")
    Annee annee;

    public CoursProgrammeAnnee() {}

    public CoursProgrammeAnnee(Cours cours, Annee annee) {
        this(cours,null,annee);
    }

    public CoursProgrammeAnnee(Cours cours, Programme programme, Annee annee) {
        this(new Long(-1),cours,programme,annee);
    }

    public CoursProgrammeAnnee(Long id, Cours cours, Programme programme, Annee annee) {
        this.id = id;
        this.cours = cours;
        this.programme = programme;
        this.annee = annee;
    }

    public Long getId() { return id;}
    public void setId(Long id) { this.id = id; }
    public Cours getCours() { return cours; }
    public void setCours(Cours cours) { this.cours = cours; }
    public Programme getProgramme() { return programme; }
    public void setProgramme(Programme programme) { this.programme = programme; }
    public Annee getAnnee() { return annee; }
    public void setAnnee(Annee annee) { this.annee = annee; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CoursProgrammeAnnee that = (CoursProgrammeAnnee) o;
        if (that.getAnnee() == null || that.getCours() == null) return false;
        if (getAnnee() == null || getCours() == null) return false;
        if (!getCours().equals(that.getCours())) return false;
        return getAnnee().equals(that.getAnnee());
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }

    @Override
    public String toString() {
        return this.annee.getNom() + " - " + this.programme.getNom();
    }
}
