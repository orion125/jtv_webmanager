package ch.steversoix.jtv.desktop.saison.cours.model;

import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AbsenceRepository extends JpaRepository<Absence, Long> {

    List<Absence> findAbsenceByCours(Cours c);
    Optional<Absence> findAbsenceByCoursAndTireur(Cours c, Tireur t);
    Long deleteAbsenceById(Long id);

}
