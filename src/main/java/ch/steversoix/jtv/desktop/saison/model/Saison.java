package ch.steversoix.jtv.desktop.saison.model;

//import ch.steversoix.jtv.desktop.saison.concours.model.Concours;
//import Cours;
//import ch.steversoix.jtv.desktop.saison.cours.model.Programme;
//import ch.steversoix.jtv.desktop.saison.armes.model.Stock;

import ch.steversoix.jtv.desktop.arme.model.TypeArme;
import ch.steversoix.jtv.desktop.saison.programme.model.Programme;
import ch.steversoix.jtv.desktop.saison.cours.model.Cours;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author Adrien Debal
 * @version 0.2
 */

@Entity
@Table(name = "jtv_saison")
public class Saison {
    @Id
    @GeneratedValue
    @Column(name = "sai_id")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "sai_typ_id")
    private TypeArme type;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "sai_dateDebut", nullable = false)
    private Date dateDebut;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "sai_dateFin", nullable = false)
    private Date dateFin;
    //    private ArrayList<Inscription> alInscription = new ArrayList<>();
//    private ArrayList<Programme> alProgramme = new ArrayList<>();
    //    private ArrayList<Stock> alStock = new ArrayList<>();
//    private ArrayList<Concours> alConcours = new ArrayList<>();
    @Column(name = "sai_actif")
    private boolean actif;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "saison")
    private Set<Cours> cours;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "saison", cascade = CascadeType.ALL)
    private Set<Programme> programmes;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "saison", cascade = CascadeType.ALL)
    private List<Inscription> inscriptions;
    /* Constructeur */

    public Saison() {
        this(null, null, null);
    }

    public Saison(TypeArme type, Date dateDebut, Date dateFin) {
        this.type = type;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.actif = true;
    }

    /* Getter & Setter */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeArme getType() {
        return type;
    }

    public void setType(TypeArme type) {
        this.type = type;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    public Set<Cours> getCours() {
        return cours;
    }

    public void setCours(Set<Cours> cours) {
        this.cours = cours;
    }

    public Set<Programme> getProgrammes() {
        return programmes;
    }

    public void setProgrammes(Set<Programme> programmes) {
        this.programmes = programmes;
    }

    public List<Inscription> getInscriptions() {
        return inscriptions;
    }

    public void setInscriptions(List<Inscription> inscriptions) {
        this.inscriptions = inscriptions;
    }

    public boolean isSaisonEditable() {
        return !isSaisonFinished() && isActif();
    }

    public boolean isSaisonFinished() {
        Date today = new java.util.Date();
        ZoneId defaultZoneId = ZoneId.systemDefault();
        Instant instant = getDateFin().toInstant();
        LocalDateTime saisonDateFin = instant.atZone(defaultZoneId).toLocalDateTime();
        return saisonDateFin.plusDays(1).isBefore(today.toInstant().atZone(defaultZoneId).toLocalDateTime());
    }

    public boolean isSaisonOpen() {
        Date today = new java.util.Date();

        ZoneId defaultZoneId = ZoneId.systemDefault();
        Instant instantDeb = getDateDebut().toInstant();
        LocalDateTime saisonDateDeb = instantDeb.atZone(defaultZoneId).toLocalDateTime();

        Instant instantFin = getDateFin().toInstant();
        LocalDateTime saisonDateFin = instantFin.atZone(defaultZoneId).toLocalDateTime().plusDays(1);
        return saisonDateDeb.isBefore(today.toInstant().atZone(defaultZoneId).toLocalDateTime()) &&
               saisonDateFin.isAfter(today.toInstant().atZone(defaultZoneId).toLocalDateTime());
    }

    /* Comparaison */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Saison other = (Saison) obj;
        return this.id == other.id;
    }

    /* Methodes diverses */

    /* Affichage */
    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat("yyyy");
        java.sql.Date date = new java.sql.Date(this.getDateDebut().getTime());
        return df.format(dateDebut) + " " + df.format(dateFin);
//        return "Saison de "+getType().getNom()+" du "+df.format(date);
    }

    /* RG 20 : Nom d’une saison */
    public String enTete() {
        DateFormat df = new SimpleDateFormat("MM.yyyy");
        return "Saison " + df.format(dateDebut) +  " - " + df.format(dateFin) + " : cours " + type.getNom();
    }
}
