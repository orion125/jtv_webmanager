package ch.steversoix.jtv.desktop.saison.cours.model;

import ch.steversoix.jtv.desktop.saison.model.Inscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface ParticipantRepository extends JpaRepository<Participant, Long> {

    Optional<Participant> findByCoursAndInscrit(Cours c, Inscription i);

    Optional<Participant> findById(long id);

    List<Participant> findByCoursAndGroupe(Cours cours, Groupe groupe);

    List<Participant> findByInscrit(Inscription inscription);

    @Transactional
    List<Participant> removeByInscritAndCours(Inscription inscription, Cours cours);

}
