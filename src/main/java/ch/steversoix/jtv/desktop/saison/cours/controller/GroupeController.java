package ch.steversoix.jtv.desktop.saison.cours.controller;

import ch.steversoix.jtv.desktop.saison.cours.model.Groupe;
import ch.steversoix.jtv.desktop.saison.cours.service.GroupeService;
import ch.steversoix.jtv.desktop.tireur.services.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
@Order(2)
public class GroupeController implements ApplicationRunner {

    @Value("${jtv.testdata}")
    private Boolean testdata;

    @Autowired
    GroupeService groupeService;

    @ModelAttribute("user")
    public SecurityUser getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (SecurityUser) auth.getPrincipal();
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {

    }
}
