package ch.steversoix.jtv.desktop.saison.programme.services;

import ch.steversoix.jtv.desktop.saison.programme.model.Cible;

public enum CibleEnumeration {
    NOM_VIDE("Le nom ne peut pas être vide.","nom = ''"),
    VALEUR_MIN_MAX("La valeur minimale d'une cible doit être "+ Cible.MIN_VALUE+" et la valeur maximale "+Cible.MAX_VALUE+".",""),
    NOM_UNIQUE("Ce nom de cible existe déjà.","nom existe déjà"),
    DEJA_ATTRIBUEE("Cette cible est déjà attribuée à un programme, il est impossible de la modifier.","cible attribué à au moins un programme -> pas de modif");

    private String erreurException;
    private String erreurForLog;

    CibleEnumeration(String erreurException, String erreurForLog) {
        this.erreurException = erreurException;
        this.erreurForLog = erreurForLog;
    }

    public String getErreurException() {
        return erreurException;
    }
    public String getErreurforLog() {
        return erreurForLog;
    }
}
