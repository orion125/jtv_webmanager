package ch.steversoix.jtv.desktop.saison.controller;

import ch.steversoix.jtv.desktop.arme.model.Arme;
import ch.steversoix.jtv.desktop.arme.model.TypeArme;
import ch.steversoix.jtv.desktop.arme.service.ArmeService;
import ch.steversoix.jtv.desktop.arme.service.TypeArmeService;
import ch.steversoix.jtv.desktop.core.exception.ResourceNotFoundException;
import ch.steversoix.jtv.desktop.outils.FlashMessage;
import ch.steversoix.jtv.desktop.outils.FlashMessageGenerator;
import ch.steversoix.jtv.desktop.poste.model.Poste;
import ch.steversoix.jtv.desktop.poste.service.PosteService;
import ch.steversoix.jtv.desktop.saison.annee.model.Annee;
import ch.steversoix.jtv.desktop.saison.annee.service.AnneeService;
import ch.steversoix.jtv.desktop.saison.concours.model.Concours;
import ch.steversoix.jtv.desktop.saison.concours.service.ConcoursService;
import ch.steversoix.jtv.desktop.saison.cours.model.Cours;
import ch.steversoix.jtv.desktop.saison.cours.model.Participant;
import ch.steversoix.jtv.desktop.saison.cours.service.CoursService;
import ch.steversoix.jtv.desktop.saison.exception.AttributionException;
import ch.steversoix.jtv.desktop.saison.exception.InscriptionException;
import ch.steversoix.jtv.desktop.saison.model.Inscription;
import ch.steversoix.jtv.desktop.saison.programme.exceptions.CibleAlreadyExistsException;
import ch.steversoix.jtv.desktop.saison.programme.model.*;
import ch.steversoix.jtv.desktop.saison.programme.services.*;
import ch.steversoix.jtv.desktop.saison.exception.SaisonException;
import ch.steversoix.jtv.desktop.saison.model.Saison;

import ch.steversoix.jtv.desktop.saison.service.SaisonEnumeration;
import ch.steversoix.jtv.desktop.saison.service.SaisonService;
import ch.steversoix.jtv.desktop.tireur.model.JeuneTireur;
import ch.steversoix.jtv.desktop.tireur.model.Moniteur;
import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import ch.steversoix.jtv.desktop.tireur.services.JeuneTireurService;
import ch.steversoix.jtv.desktop.tireur.services.SecurityUser;
import ch.steversoix.jtv.desktop.tireur.services.TireurEnumeration;
import ch.steversoix.jtv.desktop.tireur.services.TireurService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.swing.text.html.Option;
import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = "/saisons")
@Order(3)
public class SaisonController implements ApplicationRunner {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Value("${jtv.testdata}")
    private Boolean testdata;

    @Autowired
    private TypeArmeService typeArmeService;
    @Autowired
    private SaisonService saisonService;
    @Autowired
    private CoursService coursService;
    @Autowired
    private ConcoursService concoursService;
    @Autowired
    private JeuneTireurService jeuneTireurService;
    @Autowired
    private AnneeService anneeService;
    @Autowired
    private TireurService tireurService;
    @Autowired
    private ArmeService armeService;
    @Autowired
    private PosteService posteService;
    @Autowired
    private CibleService cibleService;
    @Autowired
    private ProgrammeService programmeService;


    /*
    @ModelAttribute("saison")
    public Optional<Saison> getSaison(@PathVariable("idSaison") Optional<Long> idSaison) {
        if (idSaison.isPresent()) {
            Optional<Saison> optSaison = saisonService.findCoursById(idSaison.get());
            return optSaison;
        } else return Optional.empty();
    }
    */

    @ModelAttribute("user")
    public SecurityUser getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (SecurityUser) auth.getPrincipal();
    }

    /* Les requêtes qui suivent ne concernent que la navigation */

    /* Affichage du formulaire qui liste les saisons */
    @GetMapping("")
    public String getSaisons(Model model) {
        /* Récupérer les saisons ACTIVES */
        List<Saison> lst = saisonService.findSaisonsByActif(true);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        SecurityUser su = (SecurityUser) auth.getPrincipal();
        if (su.getLevel() == Tireur.LEVEL_USER) {
            JeuneTireur jt = jeuneTireurService.getTypeJeuneTireurIfExistOrThrow404(su.getId());
            List<Inscription> lstInsc = saisonService.findInscriptionsByTireur(jt);
            lst.clear();
            for (Inscription ins : lstInsc) {
                if (ins.getSaison().isActif()) {
                    lst.add(ins.getSaison());
                }
            }
        }
        model.addAttribute("saisons", lst);
        model.addAttribute("actif", true);
        return "gestionSaisons";
    }

    @GetMapping("/inactifs")
    public String getSaisonsInactifs(Model model) {
        /* Récupérer les saisons ACTIVES */
        List<Saison> lst = saisonService.findSaisonsByActif(false);
        model.addAttribute("saisons", lst);
        model.addAttribute("actif", false);
        return "gestionSaisons";
    }

    /* Affichage du formulaire qui ajoute une saison */
    @GetMapping("/add")
    public String addSaison(Model model, RedirectAttributes redirAttrs) {
        List<TypeArme> lst = typeArmeService.findTypesArmesByActif(true);
        if (lst.isEmpty()) {
            LOGGER.log(Level.SEVERE, SaisonEnumeration.AUCUN_TYPE_ARME.getErreurforLog());
            redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.DANGER,
                    SaisonEnumeration.AUCUN_TYPE_ARME.getErreurException()));
            return "redirect:/saisons";
        }

        model.addAttribute("edit_type_arme", false);
        model.addAttribute("dateDebutActif", false);
        model.addAttribute("saison", new Saison());
        model.addAttribute("typesArmes", lst);
        return "ajoutSaison";
    }

    /* Affichage du formulaire qui modifie une saison */
    @GetMapping("/{id}/edit")
    public String editSaison(Model model, @PathVariable("id") long id, RedirectAttributes redirAttrs) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(id);
        List<TypeArme> lst = typeArmeService.findTypesArmesByActif(true);

        if (!lst.contains(saison.getType())) {
            lst.add(typeArmeService.findTypeArmeById(saison.getType().getId()));
        }

        boolean dateDebutActif = false;
        /* Si la date du jour dépasse la date de début, on ne peut plus la modifier */
        Date d = new Date();
        if (saison.getDateDebut().before(d)) {
            dateDebutActif = true;
        }

        /* Si la date du jour est après la date de fin on ne peut plus modifier la saison */
        if (saison.getDateFin().before(d)) {
            List<Saison> saisons = saisonService.findSaisonsByActif(true);
            model.addAttribute("saisons", saisons);
            LOGGER.log(Level.SEVERE, SaisonEnumeration.SAISON_TERMINER.getErreurforLog());
            redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.DANGER,
                    SaisonEnumeration.SAISON_TERMINER.getErreurException()));
            return "redirect:/saisons";
        }
        boolean edit_type_arme = false;
        // RM 05 – On ne peut pas modifier le type d'arme si il y a des inscrit à la saison.
        if (saison.getInscriptions() != null && saison.getInscriptions().size() > 0) {
            edit_type_arme = true;
        }

        model.addAttribute("edit_type_arme", edit_type_arme);
        model.addAttribute("dateDebutActif", dateDebutActif);
        model.addAttribute("saison", saison);
        model.addAttribute("typesArmes", lst);
        return "ajoutSaison";
    }

    @GetMapping("/{id}")
    public String dashboard(Model model, @PathVariable("id") long id, RedirectAttributes redirAttrs) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        SecurityUser su = (SecurityUser) auth.getPrincipal();

        Saison saison = saisonService.getSaisonIfExistOrThrow404(id);

        // Attribut commun aux jt et moniteurs
        model.addAttribute("saison", saison);

        List<Cours> cours = coursService.findCoursBySaisonAndByActif(saison.getId(), true);
        model.addAttribute("cours", cours);

        List<Concours> concours = concoursService.findConcoursBySaisonAndByActif(saison, true);
        model.addAttribute("concours", concours);

        if (su.getLevel() >= Tireur.LEVEL_MANAGER) {
            List<Inscription> lstInscrit = saisonService.findInscritsActifs(saison);

            List<Inscription> lstJT = lstInscrit
                    .stream()
                    .filter(i -> i.getTireur() instanceof JeuneTireur)
                    .collect(Collectors.toList());
            model.addAttribute("jeunes", lstJT);

            List<Inscription> lstMonit = lstInscrit
                    .stream()
                    .filter(i -> i.getTireur() instanceof Moniteur)
                    .collect(Collectors.toList());
            model.addAttribute("moniteurs", lstMonit);

            return "saisons/dashboardMD";
        } else {
            // si c'est un jt on affiche un autre menu
            JeuneTireur jtco = jeuneTireurService.getTypeJeuneTireurIfExistOrThrow404(su.getId());
            model.addAttribute("jtco", jtco);
            return "saisons/dashboardJT";
        }
    }

    /* Les requêtes qui suivent intéragissent avec la BDD */

    /* Ajout de la saison dans la BDD */
    @PostMapping("")
    public String addSaisonDB(Model model, @Valid @ModelAttribute("saison") Saison newSaison, BindingResult bindingResult, RedirectAttributes redirAttrs) {
        List<TypeArme> lst = typeArmeService.findTypesArmesByActif(true);
        if (lst.isEmpty()) {
            return "Veuillez créer un type d'arme préalablement.";
        }
        if (bindingResult.hasErrors()) {
            model.addAttribute("typesArmes", lst);
            return "ajoutSaison";
        }
        try {
            saisonService.save(newSaison);
        } catch (SaisonException e) {
            model.addAttribute("messages", FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER,
                    e.getErreurs()));
            model.addAttribute("typesArmes", lst);
            model.addAttribute("saison", newSaison);
            return "ajoutSaison";
        }
        LOGGER.log(Level.FINE, "La saison a été ajoutée.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "La saison a été ajoutée."));
        return "redirect:/saisons";
    }

    /* Modifie une saison dans la BDD*/
    @PostMapping("/{id}/update")
    public String updateSaisonDB(@PathVariable("id") long id, @Valid @ModelAttribute("saison") Saison saison, BindingResult bindingResult, Model model, RedirectAttributes redirAttrs) {
        Saison saisonBeforeUpdate = saisonService.getSaisonIfExistOrThrow404(id);
        // RM 05 – On ne peut pas modifier le type d'arme si il y a des inscrit à la saison.
        if (saison.getType() == null) {
            saison.setType(saisonBeforeUpdate.getType());
        }

        List<TypeArme> lst = typeArmeService.findTypesArmesByActif(true);
        if (bindingResult.hasErrors()) {
            model.addAttribute("typesArmes", lst);
            return "ajoutSaison";
        }
        if (lst.isEmpty()) {
            return "Veuillez créer un type d'arme préalablement.";
        }
        try {
            System.out.println(saison);
            saisonService.update(saison);
        } catch (SaisonException e) {
            boolean dateDebutActif = false;
            /* RM 05 - Si la date du jour dépasse la date de début, on ne peut plus la modifier */
            Date d = new Date();
            if (saisonBeforeUpdate.getDateDebut().before(d)) {
                dateDebutActif = true;
            }
            boolean edit_type_arme = false;
            // RM 05 – On ne peut pas modifier le type d'arme si il y a des inscrit à la saison.
            if (saison.getInscriptions() != null && saison.getInscriptions().size() > 0) {
                edit_type_arme = true;
            }
            model.addAttribute("messages", FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER,
                    e.getErreurs()));
            model.addAttribute("edit_type_arme", edit_type_arme);
            model.addAttribute("dateDebutActif", dateDebutActif);
            model.addAttribute("typesArmes", lst);
            model.addAttribute("saison", saison);
            return "ajoutSaison";
        }
        LOGGER.log(Level.FINE, "La saison n°" + saison.getId() + " a été modifiée.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "La saison de " + saison.getType() + " du " + saison.getDateFin() + " a été modifiée."));
        return "redirect:/saisons";
    }

    /* Change l'edition de la saison */
    @GetMapping("/{id}/change")
    public String changeSaison(@PathVariable("id") long id, RedirectAttributes redirAttrs) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(id);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        SecurityUser su = (SecurityUser) auth.getPrincipal();
        Optional<Tireur> tireur = tireurService.findTireurByUsername(su.getUsername());
        if (tireur.isPresent()) {
            tireur.get().setLastEditedSaison(saison);
            tireurService.selectSaison(saison, tireur.get());
            su.setLastEditedSaison(saison);
            LOGGER.log(Level.FINE, "Vous avez sélectionné la saison n°" + saison.getId() + ".");
            redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                    "Vous avez sélectionné la saison de n°" + saison.getId() + "."));
            return "redirect:/saisons/" + saison.getId() + "/cours";
        } else {
            LOGGER.log(Level.SEVERE, TireurEnumeration.TIREUR_NOT_FOUND.getErreurforLog());
            throw new ResourceNotFoundException(TireurEnumeration.TIREUR_NOT_FOUND.getErreurException());
        }
    }

    /* Désactive une saison de la DB */
    @GetMapping("/{id}/desactivate")
    public String desactivateSaisonDB(@PathVariable("id") long id, RedirectAttributes redirAttrs) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(id);

        saisonService.desactivate(saison);
        LOGGER.log(Level.FINE, "La saison n°" + saison.getId() + " a été désactivée.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "La saison de " + saison.getType().toString() + " du " + saison.getDateFin() + " a été désactivée."));
        return "redirect:/saisons";
    }

    @GetMapping("/{id}/activate")
    public String activateSaisonDB(@PathVariable("id") long id, RedirectAttributes redirAttrs) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(id);

        saisonService.activate(saison);
        LOGGER.log(Level.FINE, "La saison n°" + saison.getId() + " a été activée.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "La saison de " + saison.getType().toString() + " du " + saison.getDateFin() + " a été activée."));
        return "redirect:/saisons/inactifs";
    }

    @GetMapping("/{id}/attributionarme")
    public String attributionarmeSaison(@PathVariable("id") long id, Model model) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(id);

        List<Inscription> lstInscrit = saisonService.findInscritsActifs(saison);

        List<Inscription> lstJT = lstInscrit
                .stream()
                .filter(i -> i.getTireur() instanceof JeuneTireur)
                .collect(Collectors.toList());
        model.addAttribute("JTinscrits", lstJT);

        List<Inscription> lstMonit = lstInscrit
                .stream()
                .filter(i -> i.getTireur() instanceof Moniteur)
                .collect(Collectors.toList());
        model.addAttribute("MONinscrits", lstMonit);

        model.addAttribute("saison", saison);

        List<Arme> lstArmes = armeService.findArmeByTypeAndByActif(saison.getType(),true);
        List<Arme> lstAttribue = saisonService.findArmesAttribues(saison);
        /* On enlève toutes les armes déjà attribuées */
        lstArmes.removeAll(lstAttribue);

        model.addAttribute("lstArmes", lstArmes);

        return "attributionArme";
    }

    /* Désactive une saison de la DB */
    @GetMapping("/{id}/statistique")
    public String statistiqueSaison(@PathVariable("id") long id, Model model, RedirectAttributes redirAttrs) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(id);
        ArrayList<InscriptionStatistique> lstStat=new ArrayList<InscriptionStatistique>();
        for (Inscription i : saison.getInscriptions()){
            InscriptionStatistique stat =new InscriptionStatistique(i);
            List<Participant> lstPar = coursService.getParticipantsByInscrit(i);
            for(Participant p : lstPar){
                switch (p.getEtat()) {
                    case Participant.PRESENT:
                        stat.incNbPresent();
                        break;
                    case Participant.RETARD:
                        stat.incNbRetard();
                        break;
                    case Participant.ABSENT:
                        stat.incNbAbsent();
                        break;
                    case Participant.EXCUSE:
                        stat.incNbExcuse();
                        break;
                }
            }
            lstStat.add(stat);
        }
        List<InscriptionStatistique> lstJt = lstStat
                .stream()
                .filter(i -> i.getInscription().getTireur() instanceof JeuneTireur)
                .collect(Collectors.toList());
        List<InscriptionStatistique> lstMonit = lstStat
                .stream()
                .filter(i -> i.getInscription().getTireur() instanceof Moniteur)
                .collect(Collectors.toList());

        model.addAttribute("saison",saison);
        model.addAttribute("statJT",lstJt);
        model.addAttribute("statMON",lstMonit);
        return "saisons/saisonStatistique";
    }

    @GetMapping("/{idSaison}/attributionarme/enlever/{idInscrit}")
    public String enleverArme(@PathVariable("idSaison") long idSaison, @PathVariable("idInscrit") long idInscrit, Model model, RedirectAttributes redirAttrs) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);

        Optional<Inscription> insc = saisonService.findInscription(idInscrit);
        // TODO : if !insc.isPresent()
        saisonService.enleverArme(insc.get());
        LOGGER.log(Level.FINE, "L'arme du tireur " + insc.get().getTireur().getFullName() + " n'est plus attribuée.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "L'arme du tireur " + insc.get().getTireur().getFullName() + " n'est plus attribuée."));
        return "redirect:/saisons/" + idSaison + "/attributionarme";

    }

    @GetMapping("/{idSaison}/attributionarme/attribue")
    public String attribueArme(@PathVariable("idSaison") long idSaison,
                               @RequestParam(value = "ins", required = true) long idInscrit,
                               @RequestParam(value = "arm", required = true) long idArme,
                               Model model, RedirectAttributes redirAttrs) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);

        Optional<Inscription> insc = saisonService.findInscription(idInscrit);
        // TODO : if !insc.isPresent()

        Optional<Arme> arme = armeService.findArmeById(idArme);

        try {
            saisonService.attribuerArme(insc.get(), arme.get());
        } catch (AttributionException e) {
            redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS, e.getMessage()));
            return "redirect:/saisons/" + idSaison + "/attributionarme";
        }
        LOGGER.log(Level.FINE, "L'arme n°" + idArme + " a été attribuée au tireur " + insc.get().getTireur().getFullName() + ".");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "L'arme n°" + idArme + " a été attribuée au tireur " + insc.get().getTireur().getFullName() + "."));
        return "redirect:/saisons/" + idSaison + "/attributionarme";
    }

    // @GetMapping("/{id}/inscrits")
    public String showInscrits(@PathVariable("id") long id, Model model) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(id);

        model.addAttribute("saison", saison);

        List<Inscription> lstInscrit = saisonService.findInscritsActifs(saison);

        List<Inscription> lstJT = lstInscrit
                .stream()
                .filter(i -> i.getTireur() instanceof JeuneTireur)
                .collect(Collectors.toList());
        model.addAttribute("JTinscrits", lstJT);

        List<Inscription> lstMonit = lstInscrit
                .stream()
                .filter(i -> i.getTireur() instanceof Moniteur)
                .collect(Collectors.toList());
        model.addAttribute("MONinscrits", lstMonit);

        return "cours/gestionInscrits";
    }

    @GetMapping("/{id}/inscrits")
    public String addInscrits(@PathVariable("id") long id, Model model) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(id);

        /** Inscrits **/
        model.addAttribute("saison", saison);
        List<Inscription> inscrits = saisonService.findInscritsActifs(saison);
        List<Inscription> insJT = inscrits
                .stream()
                .filter(i -> i.getTireur() instanceof JeuneTireur)
                .collect(Collectors.toList());
        model.addAttribute("inscritsJT", insJT);

        List<Inscription> insMon = inscrits
                .stream()
                .filter(i -> i.getTireur() instanceof Moniteur)
                .collect(Collectors.toList());
        model.addAttribute("inscritsMON", insMon);

        /** Non-inscrits **/
        List<Tireur> noninscrits = saisonService.findTireursNonInscrits(saison);
        List<Tireur> noninsJT = noninscrits
                .stream()
                .filter(i -> i instanceof JeuneTireur)
                .collect(Collectors.toList());
        model.addAttribute("noninscritsJT", noninsJT);

        List<Tireur> noninsMon = noninscrits
                .stream()
                .filter(i -> i instanceof Moniteur)
                .collect(Collectors.toList());
        model.addAttribute("noninscritsMON", noninsMon);

        /** Désactivés **/
        List<Inscription> inscritsInactifs = saisonService.findInscritsInactifs(saison);
        model.addAttribute("inactifs", inscritsInactifs);


        model.addAttribute("noninscrits", noninscrits);
        ArrayList<String> toregister = new ArrayList<>();

        List<Annee> anneesLst = anneeService.findAnneesByActif(true);
        model.addAttribute("anneeslst", anneesLst);
        model.addAttribute("toregister", new ArrayList<String>());
        return "saisons/ajoutInscrits";
    }


    @PostMapping("/{id}/inscrits/add")
    public String addInscrits(@PathVariable("id") long id,
                              @RequestParam("toregister") Optional<List<String>> toregister,
                              @RequestParam("toreactivate") Optional<List<String>> toreactivate,
                              @RequestParam("anneetoregister") Optional<String> idAnneeStr) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(id);

        Optional<Annee> annee;
        if (idAnneeStr != null && idAnneeStr.isPresent()) {
            int idAnnee = Integer.parseInt(idAnneeStr.get());
            annee = Optional.of(anneeService.findAnneeById(idAnnee));
        } else annee = Optional.empty();

        if (toregister != null && toregister.isPresent()) {
            for (String idTireurStr : toregister.get()) {
                int idTireur = Integer.parseInt(idTireurStr);
                Optional<Tireur> tireur = tireurService.findTireurById(idTireur);
                if (tireur.isPresent()) {
                    saisonService.inscrit(saison, tireur.get(), annee);
                }
            }
        }

        if (toreactivate != null && toreactivate.isPresent()) {
            for (String idInsStr : toreactivate.get()) {
                int idIns = Integer.parseInt(idInsStr);
                Optional<Inscription> inscription = saisonService.findInscription(idIns);
                if (inscription.isPresent()) {
                    if (inscription.get().getTireur() instanceof Moniteur)
                        saisonService.inscrit(inscription.get(), Optional.empty());
                    else
                        saisonService.inscrit(inscription.get(), annee);
                }
            }
        }
        return "redirect:/saisons/" + id + "/inscrits";
    }

    @PostMapping("/{id}/inscrits/delete")
    public String removeInscrits(@PathVariable("id") long id,
                                 @RequestParam("todelete") Optional<List<String>> todelete,
                                 RedirectAttributes redirAttributes) {
        saisonService.getSaisonIfExistOrThrow404(id);

        if (todelete != null && todelete.isPresent()) {
            try {
                for (String idInscriptionStr : todelete.get()) {
                    int idInscription = Integer.parseInt(idInscriptionStr);
                    saisonService.desinscrit(idInscription);
                }
            } catch (InscriptionException ex) {
                redirAttributes.addFlashAttribute("messages", FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER,
                        ex.getErreurs()));
            }
        }
        return "redirect:/saisons/" + id + "/inscrits";
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        if (applicationArguments.containsOption("jtv.testdata") || testdata) {

            // save a couple of Targets
            try {
                cibleService.create("A5", 5);
                cibleService.create("A10", 10);
                cibleService.create("A100", 100);
                cibleService.create("B4", 4);
            } catch (CibleAlreadyExistsException ex) {
                LOGGER.log(Level.FINEST, "Defaults c    ibles already created.");
            }

            LOGGER.log(Level.INFO, "Runner 3: SaisonController ");

            // save a couple of Postes
            posteService.save(new Poste("tir"));
            posteService.save(new Poste("secrétaire"));
            // save a couple of Typearmes
            typeArmeService.save(new TypeArme("Pistolet"));
            typeArmeService.save(new TypeArme("Fusil"));
            ArrayList<TypeArme> tyas = (ArrayList) typeArmeService.findAll();
            // save a couple of Saisons
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date deb13 = dateFormat.parse("01/01/2015");
            Date fin13 = dateFormat.parse("30/06/2015");
            Date deb2 = dateFormat.parse("01/01/2016");
            Date fin2 = dateFormat.parse("30/06/2016");
            Date deb4 = dateFormat.parse("01/06/2017");
            Date fin4 = dateFormat.parse("30/02/2018");
            Date deb5 = dateFormat.parse("01/03/2018");
            Date fin5 = dateFormat.parse("30/12/2018");
            armeService.save(new Arme(new Long(1010), tyas.get(0), "", true));
            armeService.save(new Arme(new Long(1011), tyas.get(0), "", true));
            armeService.save(new Arme(new Long(1012), tyas.get(0), "", true));
            armeService.save(new Arme(new Long(1013), tyas.get(0), "", true));
            armeService.save(new Arme(new Long(1014), tyas.get(0), "", true));
            armeService.save(new Arme(new Long(1015), tyas.get(0), "", true));
            armeService.save(new Arme(new Long(1016), tyas.get(0), "", true));
            armeService.save(new Arme(new Long(1017), tyas.get(0), "", true));
            armeService.save(new Arme(new Long(1018), tyas.get(0), "", true));
            armeService.save(new Arme(new Long(1019), tyas.get(0), "", true));
            armeService.save(new Arme(new Long(1020), tyas.get(0), "", true));

            armeService.save(new Arme(new Long(9090), tyas.get(1), "", true));
            armeService.save(new Arme(new Long(9091), tyas.get(1), "", true));
            armeService.save(new Arme(new Long(9092), tyas.get(1), "", true));
            armeService.save(new Arme(new Long(9093), tyas.get(1), "", true));
            armeService.save(new Arme(new Long(9094), tyas.get(1), "", true));
            armeService.save(new Arme(new Long(9095), tyas.get(1), "", true));
            armeService.save(new Arme(new Long(9096), tyas.get(1), "", true));
            armeService.save(new Arme(new Long(9097), tyas.get(1), "", true));
            armeService.save(new Arme(new Long(9098), tyas.get(1), "", true));
            armeService.save(new Arme(new Long(9099), tyas.get(1), "", true));
            armeService.save(new Arme(new Long(9100), tyas.get(1), "", true));
            armeService.save(new Arme(new Long(9101), tyas.get(1), "", true));
            armeService.save(new Arme(new Long(9102), tyas.get(1), "", true));
            armeService.save(new Arme(new Long(9103), tyas.get(1), "", true));
            armeService.save(new Arme(new Long(9104), tyas.get(1), "", true));
            armeService.save(new Arme(new Long(9105), tyas.get(1), "", true));
            armeService.save(new Arme(new Long(9106), tyas.get(1), "", true));
            saisonService.createForInit(tyas.get(0), deb13, fin13);
            saisonService.createForInit(tyas.get(0), deb2, fin2);
            saisonService.createForInit(tyas.get(1), deb13, fin13);
            saisonService.createForInit(tyas.get(1), deb4, fin4);
            saisonService.createForInit(tyas.get(1), deb5, fin5);

            // save many Programmes
            ArrayList<Saison> saisons = (ArrayList) saisonService.findAll();
            programmeService.create("Jour 1", "Tire initiale", saisons.get(0), Optional.empty());
            programmeService.create("Jour 2", "Pratique", saisons.get(0), Optional.empty());
            programmeService.create("Jour 3", "", saisons.get(0), Optional.empty());
            programmeService.create("Jour 4", "Préparation concours", saisons.get(0), Optional.empty());
            programmeService.create("Jour 5", "Entrainement", saisons.get(0), Optional.empty());
            programmeService.create("Jour 1", "Tire initiale", saisons.get(1), Optional.empty());
            programmeService.create("Jour 2", "Pratique", saisons.get(1), Optional.empty());
            programmeService.create("Jour 3", "Préparation concours", saisons.get(1), Optional.empty());
            programmeService.create("Jour 4", "Entrainement", saisons.get(1), Optional.empty());
            programmeService.create("Jour 5", "", saisons.get(1), Optional.empty());
            programmeService.create("Jour 1", "", saisons.get(2), Optional.empty());
            programmeService.create("Jour 2", "Tire initiale", saisons.get(2), Optional.empty());
            programmeService.create("Jour 3", "Pratique", saisons.get(2), Optional.empty());
            programmeService.create("Jour 4", "Préparation concours", saisons.get(2), Optional.empty());
            programmeService.create("Jour 5", "Entrainement", saisons.get(2), Optional.empty());
            programmeService.create("Jour 1", "Tire initiale", saisons.get(3), Optional.empty());
            programmeService.create("Jour 2", "Pratique", saisons.get(3), Optional.empty());
            programmeService.create("Jour 3", "Préparation concours", saisons.get(3), Optional.empty());
            programmeService.create("Jour 4", "", saisons.get(3), Optional.empty());
            programmeService.create("Jour 5", "Entrainement", saisons.get(3), Optional.empty());
            // save Passes for every programmes
            ArrayList<Programme> programmes = (ArrayList) programmeService.findProgrammesByActif(true);
            ArrayList<Cible> cibles = (ArrayList) cibleService.findCiblesByActif(true);
            programmeService.create(programmes.get(0), cibles.get(0), 1, 1, 1, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(0), cibles.get(1), 2, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(1), cibles.get(0), 1, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(1), cibles.get(3), 2, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(2), cibles.get(0), 1, 2, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(2), cibles.get(1), 2, 2, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(3), cibles.get(0), 1, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(3), cibles.get(1), 2, 2, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(4), cibles.get(1), 1, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(4), cibles.get(1), 2, 2, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(5), cibles.get(1), 1, 2, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(5), cibles.get(1), 2, 2, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(6), cibles.get(1), 1, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(6), cibles.get(3), 2, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(7), cibles.get(0), 1, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(7), cibles.get(1), 2, 2, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(8), cibles.get(0), 1, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(8), cibles.get(3), 2, 2, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(9), cibles.get(0), 1, 2, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(9), cibles.get(0), 2, 2, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(10), cibles.get(0), 1, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(10), cibles.get(3), 2, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(11), cibles.get(1), 1, 2, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(11), cibles.get(1), 2, 2, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(12), cibles.get(1), 1, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(12), cibles.get(1), 2, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(13), cibles.get(0), 1, 2, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(13), cibles.get(3), 2, 2, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(14), cibles.get(0), 1, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(14), cibles.get(3), 2, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(15), cibles.get(0), 1, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(15), cibles.get(1), 2, 2, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(16), cibles.get(3), 1, 2, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(16), cibles.get(3), 2, 2, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(17), cibles.get(0), 1, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());
            programmeService.create(programmes.get(17), cibles.get(1), 2, 1, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType());

            Optional<Tireur> t1 = tireurService.findTireurByUsername("hp");
            Optional<Tireur> t2 = tireurService.findTireurByUsername("rw");
            Optional<Tireur> t3 = tireurService.findTireurByUsername("fw");
            Optional<Tireur> m1 = tireurService.findTireurByUsername("hg");

            Annee a1 = anneeService.findAnneeById(1);
            Annee a2 = anneeService.findAnneeById(2);
            Annee a3 = anneeService.findAnneeById(3);
            Annee a4 = anneeService.findAnneeById(4);
            Annee a5 = anneeService.findAnneeById(5);
            Annee a6 = anneeService.findAnneeById(6);
            Annee a7 = anneeService.findAnneeById(7);
            Annee a8 = anneeService.findAnneeById(8);

            // Saison précédente
            Optional<Saison> saisonPrecedente = saisonService.findById(3);
            if (saisonPrecedente.isPresent()) {
                // inscriptions
                if (t1.isPresent()) saisonService.inscrit(saisonPrecedente.get(), t1.get(), a1);
                if (t2.isPresent()) saisonService.inscrit(saisonPrecedente.get(), t2.get(), a1);

                DateFormat heureFormat = new SimpleDateFormat("HH:mm");
                Date heureDebut = heureFormat.parse("08:00");
                Date heureFin = heureFormat.parse("12:00");

                Date datec1 = dateFormat.parse("27/01/2016");
                Date datec2 = dateFormat.parse("25/02/2016");
                Date datec3 = dateFormat.parse("25/03/2016");

                Cours c1 = new Cours("Cours 3.1", "ST Versoix", datec1, heureDebut, heureFin, 30, 30, saisonPrecedente.get());
                Cours c2 = new Cours("Cours 3.2", "ST Versoix", datec2, heureDebut, heureFin, 30, 30, saisonPrecedente.get());
                Cours c3 = new Cours("Cours 3.3", "ST Versoix", datec3, heureDebut, heureFin, 30, 30, saisonPrecedente.get());

                coursService.create(c1);
                coursService.create(c2);
                coursService.create(c3);
            }

            // Saison courante
            Optional<Saison> saisonOuverte = saisonService.findById(4);
            if (saisonOuverte.isPresent()) {

                // Cours

                Date datec1 = dateFormat.parse("27/08/2017");
                Date datec2 = dateFormat.parse("25/08/2017");
                Date datec3 = dateFormat.parse("25/09/2017");
                Date datec4 = dateFormat.parse("29/09/2017");
                Date datec5 = dateFormat.parse("27/10/2017");
                Date datec6 = dateFormat.parse("24/11/2017");
                Date datec7 = dateFormat.parse("29/11/2017");
                Date datec8 = dateFormat.parse("26/12/2017");
                Date datec9 = dateFormat.parse("30/12/2017");
                Date datec10 = dateFormat.parse("28/01/2018");
                Date datec11 = dateFormat.parse("28/02/2018");
                Date datec12 = dateFormat.parse("30/02/2018");

                DateFormat heureFormat = new SimpleDateFormat("HH:mm");
                Date heureDebut = heureFormat.parse("08:00");
                Date heureFin = heureFormat.parse("12:00");

                Cours c1 = new Cours("Cours 4.1", "ST Versoix", datec1, heureDebut, heureFin, 30, 30, saisonOuverte.get());
                Cours c2 = new Cours("Cours 4.2", "ST Versoix", datec2, heureDebut, heureFin, 30, 30, saisonOuverte.get());
                Cours c3 = new Cours("Cours 4.3", "ST Versoix", datec3, heureDebut, heureFin, 30, 30, saisonOuverte.get());
                Cours c4 = new Cours("Cours 4.4", "ST Versoix", datec4, heureDebut, heureFin, 30, 30, saisonOuverte.get());
                Cours c5 = new Cours("Cours 4.5", "ST Versoix", datec5, heureDebut, heureFin, 30, 30, saisonOuverte.get());
                Cours c6 = new Cours("Cours 4.6", "ST Versoix", datec6, heureDebut, heureFin, 30, 30, saisonOuverte.get());
                Cours c7 = new Cours("Cours 4.7", "ST Versoix", datec7, heureDebut, heureFin, 30, 30, saisonOuverte.get());
                Cours c8 = new Cours("Cours 4.8", "ST Versoix", datec8, heureDebut, heureFin, 30, 30, saisonOuverte.get());
                Cours c9 = new Cours("Cours 4.9", "ST Versoix", datec9, heureDebut, heureFin, 30, 30, saisonOuverte.get());
                Cours c10 = new Cours("Cours 4.10", "ST Versoix", datec10, heureDebut, heureFin, 30, 30, saisonOuverte.get());
                Cours c11 = new Cours("Cours 4.11", "ST Versoix", datec11, heureDebut, heureFin, 30, 30, saisonOuverte.get());
                Cours c12 = new Cours("Cours 4.12", "ST Versoix", datec12, heureDebut, heureFin, 30, 30, saisonOuverte.get());
                Cours c13 = new Cours("Cours 4.13", "ST Versoix", datec10, heureDebut, heureFin, 30, 30, saisonOuverte.get());
                c13.setActif(false);

                coursService.create(c1);
                coursService.create(c2);
                coursService.create(c3);
                coursService.create(c4);
                coursService.create(c5);
                coursService.create(c6);
                coursService.create(c7);
                coursService.create(c8);
                coursService.create(c9);
                coursService.create(c10);
                coursService.create(c11);
                coursService.create(c12);
                coursService.create(c13);

                Date datecon1 = dateFormat.parse("10/05/2017");
                Date datecon2 = dateFormat.parse("25/06/2017");
                Date datecon3 = dateFormat.parse("18/12/2017");

                Concours concours1 = new Concours("Tir des effeuiles", "Vaud", datecon1, 0, true, true, saisonOuverte.get());
                Concours concours2 = new Concours("Tir du diable", "Valais", datecon2, 10, true, true, saisonOuverte.get());
                Concours concours3 = new Concours("Tir du salon", "Genève", datecon3, 50, false, true, saisonOuverte.get());

                concoursService.create(concours1);
                concoursService.create(concours2);
                concoursService.create(concours3);

                // inscriptions
                if (m1.isPresent()) {
                    Inscription ins0 = saisonService.inscrit(saisonOuverte.get(), m1.get());
                }
                if (t1.isPresent()) {
                    Inscription ins1 = saisonService.inscrit(saisonOuverte.get(), t1.get(), a2);
                    coursService.setPresence(c1, ins1, Participant.PRESENT);
                    coursService.setPresence(c2, ins1, Participant.PRESENT);
                    coursService.setPresence(c3, ins1, Participant.PRESENT);
                    coursService.setPresence(c4, ins1, Participant.EXCUSE);
                    coursService.setPresence(c5, ins1, Participant.PRESENT);
                    coursService.setPresence(c6, ins1, Participant.PRESENT);
                    coursService.setPresence(c7, ins1, Participant.PRESENT);
                    coursService.setPresence(c8, ins1, Participant.PRESENT);
                    coursService.setPresence(c9, ins1, Participant.RETARD);
                    coursService.setPresence(c10, ins1, Participant.RETARD);
                    coursService.setPresence(c11, ins1, Participant.UNDEFINED);
                }
                if (t2.isPresent()) {
                    Inscription ins2 = saisonService.inscrit(saisonOuverte.get(), t2.get(), a2);
                    coursService.setPresence(c1, ins2, Participant.RETARD);
                    coursService.setPresence(c2, ins2, Participant.RETARD);
                    coursService.setPresence(c3, ins2, Participant.RETARD);
                    coursService.setPresence(c4, ins2, Participant.RETARD);
                    coursService.setPresence(c5, ins2, Participant.RETARD);
                    coursService.setPresence(c6, ins2, Participant.RETARD);
                    coursService.setPresence(c7, ins2, Participant.RETARD);
                    coursService.setPresence(c8, ins2, Participant.RETARD);
                    coursService.setPresence(c9, ins2, Participant.RETARD);
                    coursService.setPresence(c10, ins2, Participant.RETARD);
                }
                if (t3.isPresent()) {
                    Inscription ins3 = saisonService.inscrit(saisonOuverte.get(), t3.get(), a3);
                    coursService.setPresence(c1, ins3, Participant.PRESENT);
                    coursService.setPresence(c2, ins3, Participant.PRESENT);
                    coursService.setPresence(c3, ins3, Participant.PRESENT);
                    coursService.setPresence(c4, ins3, Participant.PRESENT);
                    coursService.setPresence(c5, ins3, Participant.ABSENT);
                    coursService.setPresence(c6, ins3, Participant.PRESENT);
                    coursService.setPresence(c7, ins3, Participant.PRESENT);
                    coursService.setPresence(c8, ins3, Participant.PRESENT);
                    coursService.setPresence(c9, ins3, Participant.PRESENT);
                    coursService.setPresence(c10, ins3, Participant.PRESENT);
                }
            }
        }
    }
}


