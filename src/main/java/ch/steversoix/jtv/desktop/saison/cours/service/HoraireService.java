package ch.steversoix.jtv.desktop.saison.cours.service;

import ch.steversoix.jtv.desktop.saison.cours.model.Cours;
import ch.steversoix.jtv.desktop.saison.cours.model.Horaire;
import ch.steversoix.jtv.desktop.saison.cours.model.HoraireRepository;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class HoraireService {
    private HoraireRepository horaireRepository;

    public HoraireService(HoraireRepository horaireRepository){
        this.horaireRepository = horaireRepository;
    }

    public void saveAll(Set<Horaire> horaires){
        for(Horaire h : horaires){
            horaireRepository.save(h);
        }
    }

    public void removeHoraireByCours(Cours cours){
        horaireRepository.removeHoraireByCours(cours);
    }
}
