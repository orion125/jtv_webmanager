package ch.steversoix.jtv.desktop.saison.programme.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CibleRepository extends JpaRepository<Cible, Long> {

    @Query(value="select cib from Cible cib WHERE cib.actif = ?1")
    List<Cible> findCibleByActif(boolean actif);


    Optional<Cible> findCibleByNom(String nom);
    Optional<Cible> findCibleById(Long id);
}
