package ch.steversoix.jtv.desktop.saison.annee.model;

import ch.steversoix.jtv.desktop.saison.model.Inscription;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "jtv_annee")
public class Annee {
    @Id
    @GeneratedValue
    @Column(name = "ann_id")
    private Long id;
    @Column(name = "ann_nom")
    @Size(min=2, max=50)
    private String nom;
    @Column(name = "ann_actif")
    private boolean actif;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "annee", cascade = CascadeType.ALL)
    private Set<Inscription> inscriptions;

    /* Constructeur */
    public Annee(){  this(null,null,true); }

    public Annee(Long id, String nom, boolean actif) {
        this.id = id;
        this.nom = nom;
        this.actif = actif;
    }

    public Annee(String nom) {
        this(null,nom,true);
    }

    /* Getter & Setter */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) { this.nom = nom; }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Annee annee = (Annee) o;

        return getId().equals(annee.getId());
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }

    public Set<Inscription> getInscriptions() {
        return inscriptions;
    }

    public void setInscriptions(Set<Inscription> inscriptions) {
        this.inscriptions = inscriptions;
    }

    @Override
    public String toString() {
        return "Annee{" +
                "nom='" + nom + '\'' +
                '}';
    }
}
