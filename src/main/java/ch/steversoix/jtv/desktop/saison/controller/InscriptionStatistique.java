package ch.steversoix.jtv.desktop.saison.controller;

import ch.steversoix.jtv.desktop.saison.model.Inscription;

public class InscriptionStatistique {
    private Inscription inscription;
    private int nbPresence;
    private int nbExcuse;
    private int nbRetard;
    private int nbAbsent;

    public InscriptionStatistique(Inscription inscription) {
        this(inscription,0,0,0,0);
    }

    public InscriptionStatistique(Inscription inscription, int nbPresence, int nbExcuse, int nbRetard, int nbAbsent) {
        this.inscription = inscription;
        this.nbPresence = nbPresence;
        this.nbExcuse = nbExcuse;
        this.nbRetard = nbRetard;
        this.nbAbsent = nbAbsent;
    }

    /* Getter & Setter */
    public Inscription getInscription() {
        return inscription;
    }

    public void setInscription(Inscription inscription) {
        this.inscription = inscription;
    }

    public int getNbPresence() {
        return nbPresence;
    }

    public void setNbPresence(int nbPresence) {
        this.nbPresence = nbPresence;
    }

    public int getNbExcuse() {
        return nbExcuse;
    }

    public void setNbExcuse(int nbExcuse) {
        this.nbExcuse = nbExcuse;
    }

    public int getNbRetard() {
        return nbRetard;
    }

    public void setNbRetard(int nbRetard) {
        this.nbRetard = nbRetard;
    }

    public int getNbAbsent() {
        return nbAbsent;
    }

    public void setNbAbsent(int nbAbsent) {
        this.nbAbsent = nbAbsent;
    }

    /* Divers */
    public void incNbPresent(){ this.nbPresence++; }

    public void incNbRetard(){
        this.nbRetard++;
    }

    public void incNbExcuse(){
        this.nbExcuse++;
    }

    public void incNbAbsent(){ this.nbAbsent++; }
}
