package ch.steversoix.jtv.desktop.saison.programme.model;

import ch.steversoix.jtv.desktop.saison.model.Saison;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProgrammeRepository extends JpaRepository<Programme, Long> {

    @Query(value="select pro from Programme pro WHERE pro.actif = ?1")
    List<Programme> findProgrammeByActif(boolean actif);
    List<Programme> findProgrammeBySaison(Saison s);
    List<Programme> findProgrammeBySaisonAndActif(Saison s, boolean actif);
    Optional<Programme> findById(long id);

}
