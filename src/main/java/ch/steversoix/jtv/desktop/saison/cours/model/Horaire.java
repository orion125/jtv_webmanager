package ch.steversoix.jtv.desktop.saison.cours.model;

import ch.steversoix.jtv.desktop.poste.model.Poste;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "jtv_horaire")
public class Horaire implements Comparable<Horaire> {
    @Id
    @GeneratedValue
    @Column(name = "hor_id")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "hor_cou_id",referencedColumnName="cou_id")
    private Cours cours;
    @ManyToOne
    @JoinColumn(name = "hor_pos_id",referencedColumnName="pos_id")
    private Poste poste;
    @ManyToOne
    @JoinColumn(name = "hor_gro_id",referencedColumnName="gro_id")
    private Groupe groupe;
    @Column(name = "hor_heure_debut")
    private Date heureDebut;
    @Column(name = "hor_heure_fin")
    private Date heureFin;

    /* Constructeur */
    public Horaire(Long id,Cours c, Poste p, Groupe g, Date heureDebut, Date heureFin) {
        this.id = id;
        this.cours =c;
        this.poste =p;
        this.groupe =g;
        this.heureDebut = heureDebut;
        this.heureFin = heureFin;
    }

    public Horaire(Cours c, Poste p, Groupe g, Date heureDebut, Date heureFin) {
        this.cours =c;
        this.poste =p;
        this.groupe =g;
        this.heureDebut = heureDebut;
        this.heureFin = heureFin;
    }

    public Horaire(){
        this(null,null,null,null,null);
    }



    /* Getter & Setter */
    public Long getId() {
        return this.id;
    }
    public Cours getCours() {
        return this.cours;
    }
    public void setCours(Cours c){this.cours=c;}
    public Poste getPoste() {
        return this.poste;
    }
    public void setPoste(Poste p){this.poste=p;}
    public Groupe getGroupe() {
        return this.groupe;
    }
    public void setGroupe(Groupe g){this.groupe=g;}
    public void setId(Long id) {
        this.id = id;
    }
    public Date getHeureDebut() {
        return heureDebut;
    }
    public void setHeureDebut(Date heureDebut) {
        this.heureDebut = heureDebut;
    }
    public Date getHeureFin() {
        return heureFin;
    }
    public void setHeureFin(Date heureFin) {
        this.heureFin = heureFin;
    }

    /* Comparaison */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Horaire horaire = (Horaire) o;

        return id != null ? id.equals(horaire.id) : horaire.id == null;
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }

    public String toString(){
        DateFormat df_h = new SimpleDateFormat("HH:mm");
        return "G : " + this.getGroupe().getNom() + " P : " + this.getPoste().getNom() + " HDEB : " + this.heureDebut + " HFIN : " + this.heureFin;
    }

    @Override
    public int compareTo(Horaire o) {
        return this.getHeureDebut().compareTo(o.getHeureDebut());
    }
}
