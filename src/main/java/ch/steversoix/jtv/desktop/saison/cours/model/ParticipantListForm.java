package ch.steversoix.jtv.desktop.saison.cours.model;

import ch.steversoix.jtv.desktop.saison.model.Inscription;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;


public class ParticipantListForm {
    private ArrayList<Participant> participantList;

    public ArrayList<Participant> getParticipantList() {
        return participantList;
    }

    public void setParticipantList(ArrayList<Participant> participantList) {
        this.participantList = participantList;
    }
}
