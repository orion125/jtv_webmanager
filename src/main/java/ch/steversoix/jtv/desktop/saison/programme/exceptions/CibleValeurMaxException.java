package ch.steversoix.jtv.desktop.saison.programme.exceptions;

public class CibleValeurMaxException extends IllegalArgumentException {
    public CibleValeurMaxException(String s) {
        super(s);
    }

    public CibleValeurMaxException() {
    }
}
