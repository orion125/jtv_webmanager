package ch.steversoix.jtv.desktop.saison.programme.exceptions;

public class CibleWithNullException extends IllegalArgumentException {
    public CibleWithNullException(String s) {
        super(s);
    }

    public CibleWithNullException() {
    }
}
