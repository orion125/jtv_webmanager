package ch.steversoix.jtv.desktop.saison.programme.services;

public enum TypeCoupEnumeration {
    // Ajouter un nouveau type de coups =
    // CONSTANTE( text pour type de coup, new id > 2, pondération ou combien vaux le coup lors du calcule du résultat )
    COUPS_PAR_COUPS("Coups par coups",0,1),
    COUPS_CACHES("Coups cachés",1,1),
    COUPS_ENTRAINEMENT("Coups d'entrainement",2,0);

    private String type;
    private int code;
    private int ponderation;

    TypeCoupEnumeration(String type, int code, int ponderation) {
        this.type = type;
        this.code = code;
        this.ponderation = ponderation;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public int getPonderation(){ return ponderation; }
    public void setPonderation(int ponderation){ this.ponderation = ponderation; }
}
