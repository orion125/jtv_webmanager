package ch.steversoix.jtv.desktop.saison.cours.service;

import ch.steversoix.jtv.desktop.core.exception.ResourceNotFoundException;
import ch.steversoix.jtv.desktop.poste.model.Poste;
import ch.steversoix.jtv.desktop.saison.cours.exception.AbsenceException;
import ch.steversoix.jtv.desktop.saison.cours.exception.CoursException;
import ch.steversoix.jtv.desktop.saison.cours.exception.HoraireException;
import ch.steversoix.jtv.desktop.saison.cours.exception.ParticipantException;
import ch.steversoix.jtv.desktop.saison.cours.model.*;
import ch.steversoix.jtv.desktop.saison.model.Inscription;
import ch.steversoix.jtv.desktop.saison.model.Saison;
import ch.steversoix.jtv.desktop.saison.programme.model.Programme;
import ch.steversoix.jtv.desktop.tireur.model.JeuneTireur;
import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class CoursService {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private CoursRepository coursRepository;
    private GroupeRepository groupeRepository;
    private ParticipantRepository participantRepository;
    private HoraireService horaireService;
    private CoursProgrammeAnneeRepository coursProgrammeAnneeRepository;
    private AbsenceRepository absenceRepository;

    /* Constructeur */
    public CoursService(CoursRepository coursRepository,
                        GroupeRepository groupeRepository,
                        HoraireService horaireService,
                        ParticipantRepository participantRepository,
                        CoursProgrammeAnneeRepository coursProgrammeAnneeRepository,
                        AbsenceRepository absenceRepository) {
        this.coursRepository = coursRepository;
        this.groupeRepository = groupeRepository;
        this.participantRepository = participantRepository;
        this.horaireService = horaireService;
        this.coursProgrammeAnneeRepository = coursProgrammeAnneeRepository;
        this.absenceRepository = absenceRepository;
    }

    public Optional<Cours> findCoursById(long id) {
        return coursRepository.findById(id);
    }

    public List<Cours> findCoursBySaisonAndByActif(long id, boolean actif) {
        return coursRepository.findCoursBySaisonAndActif(id, actif);
    }

    public List<Cours> findCoursBySaison(Saison saison) {
        return coursRepository.findBySaison(saison);
    }

    public List<Groupe> getGroupesWithParticipants(Cours cours) {
        LOGGER.log(Level.INFO, String.format("Récupération des groupes de participants au cours %s", cours.getNom()));

        List<Groupe> list = new ArrayList<>();
        list.addAll(cours.getGroupes());

        for (Groupe g : list) {
            LOGGER.log(Level.INFO, String.format("Récupération des participants du groupe %s pour le cours %s",
                    g.getNom(),
                    cours.getNom()));
            g.setParticipants(participantRepository.findByCoursAndGroupe(cours, g));
        }
        return list;
    }

    public List<TrancheHoraire> getTranchesHoraire(Cours cours) {
        List<TrancheHoraire> listTH = new ArrayList<>();

        SortedSet<Horaire> horaires = cours.getHoraires();
        // Map
        List<Date> listHDeb = horaires.stream()
                .map(Horaire::getHeureDebut)
                .distinct()
                .collect(Collectors.toList());
        List<Date> listHFin = horaires.stream()
                .map(Horaire::getHeureFin)
                .distinct()
                .collect(Collectors.toList());
        List<Poste> listPostes = horaires.stream()
                .map(Horaire::getPoste)
                .distinct()
                .collect(Collectors.toList());

        for (int i = 0; i < listHDeb.size(); i++) {
            TrancheHoraire th = new TrancheHoraire(listHDeb.get(i), listHFin.get(i));
            // Get all horaire within TrancheHoraire
            ArrayList<Horaire> horaireWithinTH = horaires.stream()
                    .filter(h -> h.getHeureDebut().equals(th.getHeureDebut()))
                    .collect(Collectors.toCollection(ArrayList::new));

            for (Poste p : listPostes) {
                Groupe g = horaireWithinTH.stream()
                        .filter(h -> h.getPoste() == p)
                        .limit(2)
                        .collect(Collectors.toList())
                        .get(0).getGroupe();
                th.getGroupeWithPoste().put(p, g);
            }
            listTH.add(th);
        }
        return listTH;
    }

    public List<Participant> getParticipantsByInscrit(Inscription inscription){
        return participantRepository.findByInscrit(inscription);
    }

    public void removeParticpant(Participant participant){
        participantRepository.removeByInscritAndCours(participant.getInscrit(),participant.getCours());
    }

    public void save(Cours newCours) throws CoursException {
        LOGGER.log(Level.INFO, "Enregistrement cours : " + newCours.getNom());
        CoursException exception = new CoursException();
        List<CoursProgrammeAnnee> lstCoursProgAnn = newCours.getProgrammesAnnee();
        if (lstCoursProgAnn != null && lstCoursProgAnn.size() > 0)
            for (CoursProgrammeAnnee cpa : lstCoursProgAnn) cpa.setCours(newCours);
        exception.addAll(rulesAddUpdate(newCours));
        if (exception.isEmpty()) {
            coursRepository.save(newCours);
        } else {
            throw exception;
        }
    }

    public void create(Cours newCours) {
        LOGGER.log(Level.INFO, "Enregistrement cours : " + newCours.getNom());
        coursRepository.save(newCours);
    }

    public void update(Cours cours) throws CoursException {
        LOGGER.log(Level.INFO, "UPDATE " + cours.getId() + " " + cours.getNom());
        CoursException exception = new CoursException();
        List<CoursProgrammeAnnee> lstCoursProgAnn = cours.getProgrammesAnnee();

        if (lstCoursProgAnn != null && lstCoursProgAnn.size() > 0)
            for (CoursProgrammeAnnee cpa : lstCoursProgAnn) cpa.setCours(cours);
        exception.addAll(rulesAddUpdate(cours));
        exception.addAll(rulesUpdate(cours));
        if (exception.isEmpty()) {
            // Suppress old link between programmes and courses
            coursProgrammeAnneeRepository.removeCoursProgrammeAnneeByCours(cours);
            horaireService.removeHoraireByCours(cours);
            // Save course with news link.
            coursRepository.save(cours);
        } else {
            throw exception;
        }
    }

    public void desactivate(Cours cours) throws CoursException {
        CoursException exception = new CoursException();
        /* RS 01 : on ne peut supprimer un cours déjà produit */
        Date d = new Date();
        if (d.after(cours.getDateCours())) {
            LOGGER.log(Level.SEVERE, CoursEnumeration.COURS_DEJA_PRODUIT.getErreurforLog());
            exception.add(CoursEnumeration.COURS_DEJA_PRODUIT.getErreurException());
            throw exception;
        }

        cours.setActif(false);
        coursRepository.save(cours);
    }

    public void activate(Cours cours) throws CoursException {
        CoursException exception = new CoursException();
        /* RS 01 : on ne peut ré activer un cours déjà produit */
        Date d = new Date();
        if (d.after(cours.getDateCours())) {
            LOGGER.log(Level.SEVERE, CoursEnumeration.COURS_DEJA_PRODUIT.getErreurforLog());
            exception.add(CoursEnumeration.COURS_DEJA_PRODUIT.getErreurException());
            throw exception;
        }

        cours.setActif(true);
        coursRepository.save(cours);
    }

    public void generateHoraire(Cours cours) throws CoursException, HoraireException {
        CoursException exception = new CoursException();

        exception.addAll(rulesHoraire(cours));
        if (exception.isEmpty()) {
            try {
                generateHoraires(cours);
                repartir(cours);
            } catch (HoraireException e) {
                throw e;
            }
            horaireService.saveAll(cours.getHoraires());
            for (Participant p : cours.getParticipants()) {
                participantRepository.save(p);
            }

        } else {
            throw exception;
        }
    }

    public Participant saveParticipant(Participant p) {
        return participantRepository.save(p);
    }


    public Participant setPresence(Cours cours, Inscription ins, int etat) {
        LOGGER.log(Level.INFO, "Enregistrement présence Tireur " + ins.getTireur().getId() + " à cours " + cours.getId());
        Participant p = new Participant(cours, ins);
        p.setEtat(etat);
        participantRepository.save(p);
        return p;
    }

    public void setPresent(Cours cours, Inscription ins) {
        setPresence(cours, ins, Participant.PRESENT);
    }

    public void setAbsent(Cours cours, Inscription ins) {
        setPresence(cours, ins, Participant.ABSENT);
    }

    public void setExcuse(Cours cours, Inscription ins, String excuse) {
        Participant p = setPresence(cours, ins, Participant.EXCUSE);
        p.setExcuse(excuse);
        participantRepository.save(p);
    }

    public void setRetard(Cours cours, Inscription ins) {
        setPresence(cours, ins, Participant.RETARD);
    }

    public void setResultats(Cours cours, Participant p, int score, int nbMunSup) throws ParticipantException {
        LOGGER.log(Level.INFO, String.format("Enregistrement des résultats pour %s.", p.getInscrit().getTireur().getFullName()));

        ParticipantException exception = new ParticipantException();

        Optional<Programme> programme = cours.getProgramme(p.getInscrit().getAnnee());
        if (programme.isPresent()) {

            /* RG 13 - Résultat obtenu */
            ArrayList<String> totalEx = checkResultatObtenu(p, score, programme.get());
            if (totalEx.size() > 0)
                exception.addAll(totalEx);
            else
                p.setTotalObtenu(score);

            /* RG 28 - Nombre de coups supplémentaire */
            ArrayList<String> munEx = checkMunitionsConsommees(p, nbMunSup, programme.get());

            if (munEx.size() > 0)
                exception.addAll(munEx);
            else
                p.setNbMunitionsSupplementaires(nbMunSup);
        } else
            exception.add(CoursEnumeration.RESULTAT_PROGRAMME_INDEFINI.getErreurException());

        if (exception.size() > 0)
            throw exception;
        else
            saveParticipant(p);
    }

    /* RG 13 - Résultat obtenu */
    private ArrayList<String> checkResultatObtenu(Participant p, int resultat, Programme programme) {
        ArrayList<String> erreurs = new ArrayList<>();
        if (resultat < 0) {
            erreurs.add(CoursEnumeration.RESULTAT_TOTAL_NEGATIF.getErreurException());
            LOGGER.log(Level.SEVERE, String.format("Le resultat de %s doit être négatif.", p.getInscrit().getTireur().getFullName()));
        } else if (resultat > programme.calculTotalPoints()) {
            erreurs.add(CoursEnumeration.RESULTAT_TOTAL_OVER_LIMIT.getErreurException());
            LOGGER.log(Level.SEVERE, String.format("Le resultat de %s ne peut pas excéder le maximum de points (%d).",
                    p.getInscrit().getTireur().getFullName(),
                    programme.calculTotalPoints()));
        }
        return erreurs;
    }

    /* RG 28 - Nombre de coups supplémentaire */
    private ArrayList<String> checkMunitionsConsommees(Participant p, int nbMunSup, Programme programme) {
        final int maxMunSup = 20;
        ArrayList<String> erreurs = new ArrayList<>();

        if (nbMunSup < 0) {
            erreurs.add(CoursEnumeration.RESULTAT_MUN_NEGATIF.getErreurException());
            LOGGER.log(Level.SEVERE, String.format("Le nombre de munitions supplémentaires de %s doit être négatif.", p.getInscrit().getTireur().getFullName()));
        } else if (nbMunSup > maxMunSup) {
            erreurs.add(CoursEnumeration.RESULTAT_MUN_OVER_LIMIT.getErreurException());
            LOGGER.log(Level.SEVERE, String.format("Le nombre de munitions supplémentaires de %s ne peut pas excéder le maximum de points (%d).",
                    p.getInscrit().getTireur().getFullName(),
                    maxMunSup));
        }
        return erreurs;
    }


    private ArrayList<String> rulesAddUpdate(Cours cours) {
        ArrayList<String> erreurs = new ArrayList<String>();

        /* Début des tests */

        /* si pas de saison */
        if (cours.getSaison() == null) {
            LOGGER.log(Level.SEVERE, CoursEnumeration.COURS_SAISON.getErreurforLog());
            erreurs.add(CoursEnumeration.COURS_SAISON.getErreurException());
        } else {
            /* RG 14 : Date des cours - le cours se situe dans l'intervalle de la saison */
            if (cours.getDateCours().before(cours.getSaison().getDateDebut())
                    || cours.getDateCours().after(cours.getSaison().getDateFin())) {
                LOGGER.log(Level.SEVERE, CoursEnumeration.HORS_SAISON.getErreurforLog());
                erreurs.add(CoursEnumeration.HORS_SAISON.getErreurException());
            }
            if(cours.getDateCours() != null){
                Calendar c = Calendar.getInstance();
                c.setTime(cours.getDateCours());
                c.set(Calendar.HOUR,23);
                c.set(Calendar.MINUTE,50);
                cours.setDateCours(c.getTime());
            }
        }

        /* RG 02 : le temps de pause est inférieur à 1 heure */
        if (cours.getTempsPause() > 60) {
            LOGGER.log(Level.SEVERE, CoursEnumeration.TEMPSPAUSE_MAX.getErreurforLog());
            erreurs.add(CoursEnumeration.TEMPSPAUSE_MAX.getErreurException());
        }

        /* RG 03 : le temps de nettoyage est inférieur à 1 heure */
        if (cours.getTempsNettoyage() > 60) {
            LOGGER.log(Level.SEVERE, CoursEnumeration.TEMPSNETTOYAGE_MAX.getErreurforLog());
            erreurs.add(CoursEnumeration.TEMPSNETTOYAGE_MAX.getErreurException());
        }

        /* RG 33 : heure deb < heure fin */
        if (cours.getHeureDebut().after(cours.getHeureFin())) {
            LOGGER.log(Level.SEVERE, CoursEnumeration.HEURE_DEB_FIN.getErreurforLog());
            erreurs.add(CoursEnumeration.HEURE_DEB_FIN.getErreurException());
        }

        /* Mettre à jour le nombre de groupe selon le nombre de poste */
        List<Groupe> lstGroupe = groupeRepository.findAll();
        if(lstGroupe.size() < cours.getPostes().size()){
            LOGGER.log(Level.SEVERE, CoursEnumeration.NB_POSTE_MAX.getErreurforLog());
            erreurs.add(CoursEnumeration.NB_POSTE_MAX.getErreurException());
        }else {
            cours.getGroupes().clear();
            for (int i = 0; i < cours.getPostes().size(); i++) {
                cours.getGroupes().add(lstGroupe.get(i));
            }
        }

        /* Aucun programme pour le cours */
        List<CoursProgrammeAnnee> lstCoursProgAnn = cours.getProgrammesAnnee();
        if (!(lstCoursProgAnn == null || lstCoursProgAnn.size() <= 0)) {
            /* Pas de doublons dans les programmes pour années */
            List<CoursProgrammeAnnee> temp = new ArrayList<>();
            temp.addAll(lstCoursProgAnn);

            for (CoursProgrammeAnnee cpaTemp : temp) {
                int num = 0;
                for (CoursProgrammeAnnee cpa : lstCoursProgAnn) {
                    if (cpaTemp.equals(cpa)) num++;
                }
                if (num > 1) {
                    LOGGER.log(Level.SEVERE, CoursEnumeration.PROGRAMME_DUPLICATE.getErreurforLog());
                    erreurs.add(CoursEnumeration.PROGRAMME_DUPLICATE.getErreurException());
                }
            }
        }

        /* Fin des tests */
        return erreurs;
    }

    private void rulesAdd() {

    }

    private ArrayList<String> rulesUpdate(Cours cours) {
        ArrayList<String> erreurs = new ArrayList<String>();
        List<CoursProgrammeAnnee> lstCoursProgAnn = cours.getProgrammesAnnee();
        if (!(lstCoursProgAnn == null || lstCoursProgAnn.size() <= 0)) {
            // Secure for central delete
            List<CoursProgrammeAnnee> temp = new ArrayList<>();
            temp.addAll(lstCoursProgAnn);
            for (CoursProgrammeAnnee cpaTemp : temp) {
                if (cpaTemp.getAnnee() == null) lstCoursProgAnn.remove(cpaTemp);
            }
        }
        return erreurs;
    }

    private ArrayList<String> rulesHoraire(Cours cours) {
        ArrayList<String> erreurs = new ArrayList<String>();
        /* RG 04 – Nombre de postes au minimum */
        if (cours.getPostes() == null || cours.getPostes().size() < Cours.MIN_POSTE) {
            LOGGER.log(Level.SEVERE, CoursEnumeration.COURS_POSTES.getErreurforLog());
            erreurs.add(CoursEnumeration.COURS_POSTES.getErreurException());
        }
        Date d = new Date();
        if (cours.getDateCours() == null || cours.getDateCours().before(d)) {
            LOGGER.log(Level.SEVERE, CoursEnumeration.COURS_DEJA_PRODUIT.getErreurforLog());
            erreurs.add(CoursEnumeration.COURS_DEJA_PRODUIT.getErreurException());
        }
        if (cours.getHeureDebut() == null || cours.getHeureFin() == null || cours.getHeureDebut().after(cours.getHeureFin())) {
            LOGGER.log(Level.SEVERE, CoursEnumeration.HEURE_DEB_FIN.getErreurforLog());
            erreurs.add(CoursEnumeration.HEURE_DEB_FIN.getErreurException());
        }

        if(cours.getParticipants() != null) {
            int cptParticipe = 0;
            for (Participant p : cours.getParticipants()) {
                if (p.getInscrit().getTireur() instanceof JeuneTireur) {
                    if (p.getEtat() == Participant.RETARD || p.getEtat() == Participant.PRESENT) {
                        cptParticipe++;
                    }
                }
            }

            if (cptParticipe < cours.getGroupes().size()) {
                LOGGER.log(Level.SEVERE, CoursEnumeration.NB_PARTICPANT_INSUFFISANT.getErreurforLog());
                erreurs.add(CoursEnumeration.NB_PARTICPANT_INSUFFISANT.getErreurException());
            }
        }
        return erreurs;
    }

    public Optional<Participant> findParticipant(Cours c, Inscription i) {
        return participantRepository.findByCoursAndInscrit(c, i);
    }

    public Optional<Participant> findParticipant(long id) {
        return participantRepository.findById(id);
    }

    public Participant setPresence(Participant p, int etat) {
        LOGGER.log(Level.INFO, "État présence de tireur " + p.getInscrit().getTireur().getId() + " défini à " + etat);
        p.setEtat(etat);
        participantRepository.save(p);
        return p;
    }

    /* Génération de l'horaire */
    public void generateHoraires(Cours cours) throws HoraireException {
        DateFormat df_h = new SimpleDateFormat("HH:mm");
        long timediff = cours.getHeureFin().getTime() - cours.getHeureDebut().getTime();

        if (timediff > 0) {
            List<Poste> lstPostes = cours.getPostes().stream().collect(Collectors.toList());
            List<Groupe> lstGroupes = cours.getGroupes().stream().collect(Collectors.toList());
            // Calcule le temps pour chaque cours
            /* RG 03 - temps de nettoyage se situe à la fin d'un cours */
            timediff -= cours.getTempsNettoyage() * 60000; // on enlève le temps de nettoyage
            /* RG36  : 5 min de pause entre les postes */
            timediff -= ((lstPostes.size() - 2) * 300000); // on enlève le temps de pause entre chaque poste

            timediff -= cours.getTempsPause()*60000; // on enleve le temps de pause du cours
            long horairelength = timediff / cours.getPostes().size();

            // Calcule des tranches horaires
            Date debutHoraire = cours.getHeureDebut();
            Date finHoraire;
            boolean firstAddPause = false;
            for (int iHoraire = 0; iHoraire < lstPostes.size(); iHoraire++) {
                // Il y a autant de tranches horaires que de postes
                finHoraire = new Date(debutHoraire.getTime() + horairelength);
                int offset = 0;
                for (int iPoste = 0; iPoste < lstPostes.size(); iPoste++) {
                    // Pour chaque h
                    offset = iPoste + iHoraire;
                    if (offset >= lstGroupes.size())
                        offset = offset % cours.getGroupes().size();
                    Groupe g = lstGroupes.get(offset);
                    Horaire h = new Horaire(cours, lstPostes.get(iPoste), g, debutHoraire, finHoraire);
                    cours.getHoraires().add(h);
                }
                debutHoraire = new Date(finHoraire.getTime());
                SimpleDateFormat format1 = new SimpleDateFormat("HH:mm");
                Calendar c = Calendar.getInstance();
                c.setTime(debutHoraire);
                c.add(Calendar.MINUTE, 5);
                // RG02 - Temps de pause pour un cours ajouter lorsque la moitié des postes ce sont dérouler.
                int tempo = iHoraire;
                tempo++;
                if (tempo >= lstPostes.size()/2 && !firstAddPause){
                    firstAddPause=true;
                    c.add(Calendar.MINUTE, cours.getTempsPause()-5);
                }
                debutHoraire.setTime(c.getTimeInMillis());
            }
        } else {
            LOGGER.log(Level.SEVERE, CoursEnumeration.HEURE_DEB_FIN.getErreurforLog());
            HoraireException exception = new HoraireException();
            exception.add(CoursEnumeration.HEURE_DEB_FIN.getErreurException());
            throw exception;
        }
    }

    public void repartir(Cours cours) {
        // Filtrer les participants à répartir
        LinkedList<Participant> alArepartir = new LinkedList<>();
        for (Participant p: cours.getParticipants()) {
            p.setGroupe(null);
            participantRepository.save(p);
        }
        cours.getParticipants().stream()
                .filter((p) -> p.getInscrit().getTireur() instanceof JeuneTireur)
                .filter(p -> {
                    if (p.getEtat() == Participant.PRESENT || p.getEtat() == Participant.RETARD) {
                        return true;
                    }
                    return false;
                })
                .sorted((Participant p1, Participant p2) -> p1.getEtat() - p2.getEtat())
                .forEach((p) -> {
                    alArepartir.add(p);
                });
        // Repartir
        ArrayList<Groupe> alGroupes = new ArrayList<>();
        alGroupes.addAll(cours.getGroupes());
        int igroupe = 0;
        while (!alArepartir.isEmpty()) {
            Participant p = alArepartir.removeFirst();
            if (p.getEtat() == Participant.PRESENT || p.getEtat() == Participant.RETARD) {
                p.setGroupe(alGroupes.get(igroupe));
                if (++igroupe >= cours.getGroupes().size()) igroupe = 0;
            }
        }
    }

    public List<Absence> getAbsenceForCours(Cours c) {
        return absenceRepository.findAbsenceByCours(c);
    }

    private ArrayList<String> rulesAbsence(Absence absence) {
        ArrayList<String> erreurs = new ArrayList<String>();
        // RG35 - Ne peut pas prévenir d'une absence qu'avant la veille du cours à midi.
        if (!absence.controlDate()) {
            LOGGER.log(Level.SEVERE, CoursEnumeration.ABSENCE_COURS_TERMINER.getErreurforLog());
            erreurs.add(CoursEnumeration.ABSENCE_COURS_TERMINER.getErreurException());
        }

        if (absenceRepository.findAbsenceByCoursAndTireur(absence.getCours(), absence.getTireur()).isPresent()) {
            LOGGER.log(Level.SEVERE, CoursEnumeration.ABSENCE_DEJA_PREVENU.getErreurforLog());
            erreurs.add(CoursEnumeration.ABSENCE_DEJA_PREVENU.getErreurException());
        }
        if (absence.getMotif() == null || absence.getMotif().equals("")) {
            LOGGER.log(Level.SEVERE, CoursEnumeration.ABSENCE_SANS_RAISON.getErreurforLog());
            erreurs.add(CoursEnumeration.ABSENCE_SANS_RAISON.getErreurException());
        }
        return erreurs;
    }

    public List<Absence> findAbsenceByCours(Cours cours) {
        return absenceRepository.findAbsenceByCours(cours);
    }

    public Optional<Absence> findAbsenceByCoursAndTireur(Cours cours, Tireur tireur) {
        return absenceRepository.findAbsenceByCoursAndTireur(cours, tireur);
    }

    public Absence prevenirAbsence(Absence absence) throws AbsenceException {
        AbsenceException exception = new AbsenceException();
        exception.addAll(rulesAbsence(absence));
        if (exception.isEmpty()) return absenceRepository.save(absence);
        else throw exception;
    }

    public Cours getCoursIfExistOrThrow404(long idCours) {
        Optional<Cours> optCours = coursRepository.findById(idCours);
        if (!optCours.isPresent()) {
            LOGGER.log(Level.SEVERE, CoursEnumeration.COURS_INEXISTANT.getErreurforLog());
            throw new ResourceNotFoundException(CoursEnumeration.COURS_INEXISTANT.getErreurException());
        } else
            return optCours.get();
    }
}
