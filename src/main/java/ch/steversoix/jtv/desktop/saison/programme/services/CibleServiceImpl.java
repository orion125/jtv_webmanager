package ch.steversoix.jtv.desktop.saison.programme.services;
import ch.steversoix.jtv.desktop.outils.FilterTools;
import ch.steversoix.jtv.desktop.saison.programme.exceptions.*;
import ch.steversoix.jtv.desktop.saison.programme.model.Cible;
import ch.steversoix.jtv.desktop.saison.programme.model.CibleRepository;
import ch.steversoix.jtv.desktop.saison.programme.model.Passe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class CibleServiceImpl implements CibleService {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private CibleRepository cibleRepository;
    @Autowired
    private ProgrammeService programmeService;

    CibleServiceImpl(CibleRepository cibleRepository){this.cibleRepository = cibleRepository;}

    @Override
    public List<Cible> findCiblesByActif(boolean actif) {return cibleRepository.findCibleByActif(actif);}

    public Cible findCibleById(long id){
        return cibleRepository.findOne(id);
    }
    @Override
    public Optional<Cible> create(String nom, int valuemax) {
        Optional<String> optNom = Optional.ofNullable(nom).filter(s -> !s.isEmpty());
        Optional<Integer> optValueMax = Optional.ofNullable(valuemax);

        cibleRepository.findCibleByNom(nom).ifPresent(u -> {throw new CibleAlreadyExistsException();});

        Cible cib = new Cible(optNom.orElseThrow(() -> new CibleWithNullException())
                ,optValueMax.orElseThrow(() -> new CibleWithNullException()));
        LOGGER.log(Level.INFO, "Create Cible : " + cib.getNom());
        return Optional.of(cibleRepository.save(cib));
    }

    @Override
    public void update(Cible cible) throws CibleException {
        CibleException exception = new CibleException();
        exception.addAll(rulesAddUpdate(cible));

        if(exception.isEmpty()) {
            LOGGER.log(Level.INFO, "Update Cible N°" + cible.getId() + " " + cible.getNom());
            cible.setActif(true);
            cibleRepository.save(cible);
        }else{
            throw exception;
        }
    }

    @Override
    public void save(Cible newCible) throws CibleException {
        CibleException exception = new CibleException();
        exception.addAll(rulesAddUpdate(newCible));

        if(exception.isEmpty()){
            LOGGER.log(Level.INFO, "Save Cible : " + newCible.getNom());
            newCible.setActif(true);
            cibleRepository.save(newCible);
        }else{
            throw exception;
        }

    }

    @Override
    public void desactivate(Cible cible) {
        cible.setActif(false);
        cibleRepository.save(cible);
    }

    @Override
    public void activate(Cible cible) {
        cible.setActif(true);
        cibleRepository.save(cible);
    }

    private ArrayList<String> rulesAddUpdate(Cible cible) {
        ArrayList<String> erreurs = new ArrayList<String>();
        /* Nom NON vide */
        if(cible.getNom().trim().equals("") || cible.getNom() == null){
            LOGGER.log(Level.SEVERE, CibleEnumeration.NOM_VIDE.getErreurforLog());
            erreurs.add(CibleEnumeration.NOM_VIDE.getErreurException());
        }

        /* RG 12 */
        if(cible.getValeurMax() < Cible.MIN_VALUE || cible.getValeurMax() > Cible.MAX_VALUE){
            LOGGER.log(Level.SEVERE, CibleEnumeration.VALEUR_MIN_MAX.getErreurforLog());
            erreurs.add(CibleEnumeration.VALEUR_MIN_MAX.getErreurException());
        }

        /* RG 34 */
        boolean existe = false;
        List<Cible> lst = cibleRepository.findAll();
        for(Cible c : lst){
            if(FilterTools.replaceAccents(c.getNom().toLowerCase()).equals(FilterTools.replaceAccents(cible.getNom().toLowerCase()))){
                existe = true;
            }
        }
        if(existe){
            LOGGER.log(Level.SEVERE, CibleEnumeration.NOM_UNIQUE.getErreurforLog());
            erreurs.add(CibleEnumeration.NOM_UNIQUE.getErreurException());
        }
        return erreurs;
    }


}
