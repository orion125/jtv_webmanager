package ch.steversoix.jtv.desktop.saison.cours.service;

public enum CoursEnumeration {
    COURS_DEJA_PRODUIT("Le cours s'est déjà déroulé.","dateCours ; dateJ"),
    TEMPSNETTOYAGE_MAX("Le temps de nettoyage doit être inférieur à une heure.","tempsNettoyage < 1 heure"),
    TEMPSPAUSE_MAX("Le temps de pause doit être inférieur à une heure.","tempsPause < 1 heure"),
    HORS_SAISON("Le cours doit se situer dans l'intervalle de la saison.","saionDeb ; saisonFin ; dateCours"),
    COURS_SAISON("Une saison est nécessaire pour ajouter un cours.","saison 1 ------ * cours"),
    COURS_POSTES("Un minimum de 2 postes est necessaire pour générer l'horaire.", "cours.poste.size < 2"),
    COURS_NONMOD_TERMINER("On ne peut modifier le cours si celui-ci est déjà terminé.","dateCours ; dateJ"),
    SAISON_FINI("La saison étant terminée on ne peut pas ajouter de cours.","today > saisonFin"),
    PROGRAMME_DUPLICATE("On ne peut pas mêttre 2 programmes différents pour la même année.", "programme duplicated"),
    ABSENCE_DEJA_PREVENU("Vous avez déjà prévenu votre absence pour ce cours.", "absence duplicated"),
    ABSENCE_SANS_RAISON("Vous ne pouvez pas prévenir d'une absence sans indiquer le motif de celle-ci.", "absence.motif = null"),
    ABSENCE_COURS_TERMINER("Vous ne pouvez pas prévenir votre abence après la veille du cours à 12:00.", "date abs > cours.date(day-1, h=12)"),
    HEURE_DEB_FIN("L'heure de début d'un cours doit être avant l'heure de fin.","heureDeb < heureFin"),
    COURS_INEXISTANT("Le cours auquel vous tentez d'accéder n'existe pas.", "cours NOT FOUND"),
    RESULTAT_PROGRAMME_INDEFINI("Il n'y a aucun programme défini pour ce cours et cette année.", "No programme"),
    RESULTAT_TOTAL_OVER_LIMIT("Le score ne peut pas dépasser le total maximum de points du programme.", "totalObtenu > MAX"),
    RESULTAT_TOTAL_NEGATIF("Le score ne peut pas être inférieur à zéro.", "totalObtenu < 0"),
    RESULTAT_MUN_NEGATIF("Le nombre de munitions supplémentaires ne peut pas être inférieur à 0.", "munSup < 0"),
    RESULTAT_MUN_OVER_LIMIT("Le nombre de munitions supplémentaires ne peut pas dépasser 20.", "munSup > 20"),
    RESULTAT_SUCCESS("Les résultats ont été mis à jours.", "Resultats OK"),
    NB_POSTE_MAX("Il y a trop de postes pour ce cours.", "nbGroupes < nbPostes"),
    NB_PARTICPANT_INSUFFISANT("Il n'y a pas assez de participant pour ce nombre de poste.","nbParticipant < nbPoste");

    private String erreurException;
    private String erreurForLog;

    CoursEnumeration(String erreurException, String erreurForLog) {
        this.erreurException = erreurException;
        this.erreurForLog = erreurForLog;
    }

    public String getErreurException() {
        return erreurException;
    }
    public String getErreurforLog() {
        return erreurForLog;
    }
}
