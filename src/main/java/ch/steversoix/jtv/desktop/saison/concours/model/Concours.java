package ch.steversoix.jtv.desktop.saison.concours.model;

import ch.steversoix.jtv.desktop.saison.model.Saison;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.logging.Logger;

@Entity
@Table(name = "jtv_concours")
public class Concours {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    public final static int PRIX_MIN = 0;
    public final static int PRIX_MAX = 100;

    @Id
    @GeneratedValue
    @Column(name = "con_id")
    private Long id;
    @Size(min=2, max=50, message="La taille du nom du concours doit être entre 2 et 50 caractères.")
    @Column(name = "con_nom")
    private String nom;
    @Size(min=2, max=50, message="La taille du lieu du concours doit être entre 2 et 50 caractères.")
    @Column(name = "con_lieu")
    private String lieu;
    @NotNull(message = "La date doit être indiquée.")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "con_date", nullable = false)
    private Date dateConcours;
    @Range(min=PRIX_MIN, max=PRIX_MAX, message="Le prix doit être compris entre " + PRIX_MIN + " et " + PRIX_MAX + ".")
    @Column(name = "con_prix")
    private double prix;
    @Column(name = "con_licence")
    private boolean licence;
    @Column(name = "con_actif")
    private  boolean actif;
    @ManyToOne
    @JoinColumn(name="con_sai_id")
    private Saison saison;

    public Concours(){ }

    public Concours(String nom, String lieu, Date dateConcours, double prix, boolean licence, boolean actif, Saison saison) {
        this.nom = nom;
        this.lieu = lieu;
        this.dateConcours = dateConcours;
        this.prix = prix;
        this.licence = licence;
        this.actif = actif;
        this.saison = saison;
    }

    /* Getter & Setter */

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }
    public String getNom() { return nom; }
    public void setNom(String nom) { this.nom = nom; }
    public String getLieu() { return lieu; }
    public void setLieu(String lieu) { this.lieu = lieu; }
    public Date getDateConcours() { return dateConcours; }
    public void setDateConcours(Date dateConcours) { this.dateConcours = dateConcours; }
    public double getPrix() { return prix; }
    public void setPrix(double prix) { this.prix = prix; }
    public boolean isLicence() { return licence; }
    public void setLicence(boolean licence) { this.licence = licence; }
    public boolean isActif() { return actif; }
    public void setActif(boolean actif) { this.actif = actif; }
    public Saison getSaison() { return saison; }
    public void setSaison(Saison saison) { this.saison = saison; }

    public boolean isConcoursEditable() {
        return !isConcoursFinished() && isActif();
    }

    public boolean isConcoursFinished() {
        Date today = new java.util.Date();
        return dateConcours.before(today);
    }
}
