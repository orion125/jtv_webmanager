package ch.steversoix.jtv.desktop.saison.cours.model;

import ch.steversoix.jtv.desktop.poste.model.Poste;

import java.util.*;

public class TrancheHoraire {
    Date heureDebut;
    Date heureFin;

    HashMap<Poste, Groupe> groupeWithPoste = new HashMap<>();

    public TrancheHoraire() {
    }

    public TrancheHoraire(Date dateFebut, Date heureFin) {
        this.heureDebut = dateFebut;
        this.heureFin = heureFin;
    }

    public Date getHeureDebut() {
        return heureDebut;
    }

    public void setHeureDebut(Date heureDebut) {
        this.heureDebut = heureDebut;
    }

    public Date getHeureFin() {
        return heureFin;
    }

    public void setHeureFin(Date heureFin) {
        this.heureFin = heureFin;
    }

    public HashMap<Poste, Groupe> getGroupeWithPoste() {
        return groupeWithPoste;
    }

    public void setGroupeWithPoste(HashMap<Poste, Groupe> groupeWithPoste) {
        this.groupeWithPoste = groupeWithPoste;
    }

    @Override
    public String toString() {
        return "TrancheHoraire{" +
                "heureDebut=" + heureDebut +
                ", heureFin=" + heureFin +
                ", groupeWithPoste=" + groupeWithPoste +
                '}';
    }
}
