package ch.steversoix.jtv.desktop.saison.concours.model;

import ch.steversoix.jtv.desktop.saison.model.Saison;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ConcoursRepository extends JpaRepository<Concours, Long> {

    List<Concours> findBySaisonAndActif(Saison saison, boolean actif);
    Optional<Concours> findById(long Id);
}
