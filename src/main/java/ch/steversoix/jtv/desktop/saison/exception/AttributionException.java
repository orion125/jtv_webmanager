package ch.steversoix.jtv.desktop.saison.exception;

public class AttributionException extends Exception {
    public AttributionException(String message) {
        super(message);
    }
}
