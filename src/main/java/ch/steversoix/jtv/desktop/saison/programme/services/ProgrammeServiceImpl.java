package ch.steversoix.jtv.desktop.saison.programme.services;

import ch.steversoix.jtv.desktop.core.exception.ResourceNotFoundException;
import ch.steversoix.jtv.desktop.saison.model.Saison;
import ch.steversoix.jtv.desktop.saison.programme.exceptions.PasseWithCibleNullException;
import ch.steversoix.jtv.desktop.saison.programme.exceptions.PasseWithNullException;
import ch.steversoix.jtv.desktop.saison.programme.exceptions.ProgrammeException;
import ch.steversoix.jtv.desktop.saison.programme.model.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class ProgrammeServiceImpl implements ProgrammeService {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private ProgrammeRepository proRepository;
    private PasseRepository passeRepository;

    ProgrammeServiceImpl(ProgrammeRepository proRepository, PasseRepository passeRepository){
        this.proRepository = proRepository;
        this.passeRepository = passeRepository;
    }

    @Override
    public List<Programme> findProgrammesByActif(boolean actif) {return proRepository.findProgrammeByActif(actif);}

    @Override
    public List<Passe> findPassesByCible(Cible cible){
        return passeRepository.findByCible(cible);
    }

    public List<Programme> findProgrammesBySaisonAndByActif(Saison s, boolean actif){
        return proRepository.findProgrammeBySaisonAndActif(s, actif);
    }

    public List<Programme> findProgrammesBySaison(Saison s){
        return proRepository.findProgrammeBySaison(s);
    }

    public Programme findProgrammeById(long id){
        return proRepository.findOne(id);
    }


    @Override
    public Optional<Programme> create(String nom, String description, Saison sai, Optional<ArrayList<Passe>> passes) {
        Optional<String> optNom = Optional.ofNullable(nom).filter(s -> !s.isEmpty());
        Optional<String> optDescription = Optional.ofNullable(description);
        Optional<Saison> optSaison = Optional.ofNullable(sai);
        Optional<ArrayList<Passe>> optPasses = passes;
        Programme pro = new Programme(
                optNom.orElseThrow(() -> new ProgrammeException(ProgrammeEnumeration.NOM_NULL.getErreurException())),
                optDescription.orElse(""),
                optSaison.orElseThrow(() -> new ProgrammeException(ProgrammeEnumeration.SAISON_NULL.getErreurException())));
        LOGGER.log(Level.INFO, "Create Programme : " + pro.getNom());
        Optional<Programme> p = Optional.of(save(pro));
        passes.ifPresent( o -> {
            pro.setPasses(optPasses.get());
            //insertPasseForProgramme(pro);
        });
        return p;
    }

    public Optional<Programme> create(Programme programme) throws ProgrammeException { // add throw here new exception
        ArrayList<String> erreurs = new ArrayList<>();
        erreurs = rulesAddUpdate(programme);
        if(erreurs.size() != 0){
            ProgrammeException exception = new ProgrammeException();
            exception.addAll(erreurs);
            throw exception;
        }
        for(Passe passe : programme.getPasses()) passe.setProgramme(programme);
        Programme p = save(programme);
        LOGGER.log(Level.INFO, "Create Programme : " + programme.getNom());
        return Optional.of(p);
    }

    private ArrayList<String> rulesAddUpdate(Programme programme){
        ArrayList<String> erreurs = new ArrayList<String>();
        /* Nom NON vide */
        if(programme.getNom()==null || programme.getNom().equals("")){
            LOGGER.log(Level.SEVERE, ProgrammeEnumeration.NOM_NULL.getErreurforLog());
            erreurs.add(ProgrammeEnumeration.NOM_NULL.getErreurException());
        }

        /* Saison non-vide */
        if (programme.getSaison().equals(null)) {
            LOGGER.log(Level.SEVERE, ProgrammeEnumeration.SAISON_NULL.getErreurforLog());
            erreurs.add(ProgrammeEnumeration.SAISON_NULL.getErreurException());
        }
        if (programme.getPasses()==null) {
            LOGGER.log(Level.SEVERE, ProgrammeEnumeration.NOPASS.getErreurforLog());
            erreurs.add(ProgrammeEnumeration.NOPASS.getErreurException());
        }else{
            int size = programme.getPasses().size();
            if (size <1) {
                LOGGER.log(Level.SEVERE, ProgrammeEnumeration.NOPASS.getErreurforLog());
                erreurs.add(ProgrammeEnumeration.NOPASS.getErreurException());
            }
            int numpasse = 0;
            for (Passe p : programme.getPasses()){
                numpasse++;
                /* Ordre cohérent (non null ni supérieur au total de passe) */
                if (p.getOrdre() > size || p.getOrdre() < 1){
                    LOGGER.log(Level.SEVERE, ProgrammeEnumeration.ORDER_OUTOFBOUND.getErreurforLog());
                    erreurs.add("Passe n°"+numpasse+": "+ProgrammeEnumeration.ORDER_OUTOFBOUND.getErreurException());
                }
                /* RG26 Sequence et Nombre de coups entre 1 et 50 pour chaque passes */
                if (p.getSequence() > 50 || p.getSequence()<1){
                    LOGGER.log(Level.SEVERE, ProgrammeEnumeration.PASSE_SERIES_OUTOFBOUND.getErreurforLog());
                    erreurs.add("Passe n°"+numpasse+": "+ProgrammeEnumeration.PASSE_SERIES_OUTOFBOUND.getErreurException());
                }
                if (p.getNbcoups() > 50 || p.getNbcoups()<1){
                    LOGGER.log(Level.SEVERE, ProgrammeEnumeration.PASSE_HITCOUNT_OUTOFBOUND.getErreurforLog());
                    erreurs.add("Passe n°"+numpasse+": "+ProgrammeEnumeration.PASSE_HITCOUNT_OUTOFBOUND.getErreurException());
                }
            }
        }
        return erreurs;
    }

    private ArrayList<String> rulesUpdate(Programme programme){
        ArrayList<String> erreurs = new ArrayList<>();
        /* RM02 */
        if (programme.hasCoursConcerner()){
            LOGGER.log(Level.SEVERE, ProgrammeEnumeration.PROGUPDATE_WITH_COURSUSING.getErreurforLog());
            erreurs.add(ProgrammeEnumeration.PROGUPDATE_WITH_COURSUSING.getErreurException());
        }
        if (programme.getPasses()!=null&&programme.getPasses().size()>0)
            for (Passe passe : programme.getPasses())
                if (passe.getCible()==null) programme.getPasses().remove(programme.getPasses().indexOf(passe));
        return erreurs;
    }

    @Override
    public Programme update(Programme programme) {
        LOGGER.log(Level.INFO, "Update Programme : " + programme.getNom());
        ProgrammeException exception = new ProgrammeException();
        if (programme.getPasses()!= null && programme.getPasses().size()>0){
            for(Passe passe : programme.getPasses()) {
                // passe central supprimer (autrement la cible a forcément une valeur null)
                passe.setProgramme(programme);
            }
        }
        exception.addAll(rulesUpdate(programme));
        exception.addAll(rulesAddUpdate(programme));
        if(exception.isEmpty()) {
            passeRepository.removePasseByProgramme(programme);
            Programme p = save(programme);
            return p;
        }else{
            throw exception;
        }
    }

    @Override
    public Programme save(Programme pro) {
        return proRepository.save(pro);
    }

    @Override
    public void desactivate(Programme pro) {
        pro.setActif(false);
        proRepository.save(pro);
    }

    @Override
    public void activate(Programme pro) {
        pro.setActif(true);
        proRepository.save(pro);
    }

    @Override
    public Optional<Passe> create(Programme p, Cible c, int ordre, int sequence, int nbcoups, String typeCoup) {
        Optional<Cible> optCib = Optional.ofNullable(c);
        Optional<Integer> optOrdre = Optional.ofNullable(ordre);
        Optional<Integer> optSequence = Optional.ofNullable(sequence);
        Optional<Integer> optNbCoups = Optional.ofNullable(nbcoups);
        Optional<String> optTypeCoup = Optional.ofNullable(typeCoup).filter(s -> !s.isEmpty());

        Passe passe = new Passe(
                optOrdre,optSequence,optNbCoups,
                optTypeCoup.orElseThrow(() -> new PasseWithNullException()),
                optCib.orElseThrow(() -> new PasseWithCibleNullException()),
                p);
        LOGGER.log(Level.INFO, "Create Passe n°"+ordre+" for Programme: " + p.toString());
        return Optional.of(passeRepository.save(passe));
    }

    public Programme getProgrammeIfExistOrThrow404(long idProgramme) {
        Optional<Programme> optProgramme = proRepository.findById(idProgramme);
        if (!optProgramme.isPresent()) {
            LOGGER.log(Level.SEVERE, ProgrammeEnumeration.PROGRAMME_INEXISTANT.getErreurforLog());
            throw new ResourceNotFoundException(ProgrammeEnumeration.PROGRAMME_INEXISTANT.getErreurException());
        } else
            return optProgramme.get();
    }
}
