package ch.steversoix.jtv.desktop.saison.cours.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CoursProgrammeAnneeRepository extends JpaRepository<CoursProgrammeAnnee, Long> {

    @Transactional
    List<CoursProgrammeAnnee> removeCoursProgrammeAnneeByCours(Cours cours);

}
