package ch.steversoix.jtv.desktop.saison.programme.exceptions;

import java.util.ArrayList;

public class ProgrammeException extends IllegalArgumentException {

    private ArrayList<String> erreurs = new ArrayList<>();

    public void add(String erreur){
        erreurs.add(erreur);
    }

    public ProgrammeException(String s) {
        super(s);
    }
    public ProgrammeException() {
        super();
    }

    public void addAll(ArrayList<String> erreurs){
        this.erreurs.addAll(erreurs);
    }

    public int size(){
        return erreurs.size();
    }

    public boolean isEmpty(){
        return erreurs.isEmpty();
    }

    public ArrayList<String> getErreurs(){return this.erreurs;}
    public String getMessage(){
        String erreur="";
        for(String s : erreurs){
            erreur+= s;
        }
        return erreur;
    }
}
