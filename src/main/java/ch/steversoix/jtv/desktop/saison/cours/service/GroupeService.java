package ch.steversoix.jtv.desktop.saison.cours.service;

import ch.steversoix.jtv.desktop.saison.cours.model.Groupe;
import ch.steversoix.jtv.desktop.saison.cours.model.GroupeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class GroupeService {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private GroupeRepository groupeRepository;

    /*
    * Constructeur
    * */

    public GroupeService(GroupeRepository groupeRepository) {
        this.groupeRepository = groupeRepository;
    }

    public Groupe findGroupeById(long id){
        return groupeRepository.findOne(id);
    }

    public List<Groupe> findAll(){
        return groupeRepository.findAll();
    }

    public void save(Groupe newGroupe){
        groupeRepository.save(newGroupe);
        LOGGER.log(Level.INFO, "Create Groupe : " + newGroupe.getNom());
    }
}
