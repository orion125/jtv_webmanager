package ch.steversoix.jtv.desktop.saison.annee.exception;

import java.util.ArrayList;

public class AnneeException extends Exception {
    private ArrayList<String> erreurs = new ArrayList<>();

    public void add(String erreur){
        erreurs.add(erreur);
    }

    public void addAll(ArrayList<String> erreurs){
        this.erreurs.addAll(erreurs);
    }

    public int size(){
        return erreurs.size();
    }

    public boolean isEmpty(){
        return erreurs.isEmpty();
    }

    public String getMessage(){
        String erreur="";
        for(String s : erreurs){
            erreur+= s;
        }
        return erreur;
    }
}