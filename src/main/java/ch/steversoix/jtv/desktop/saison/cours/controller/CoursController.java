package ch.steversoix.jtv.desktop.saison.cours.controller;

import ch.steversoix.jtv.desktop.outils.FlashMessage;
import ch.steversoix.jtv.desktop.outils.FlashMessageGenerator;
import ch.steversoix.jtv.desktop.poste.model.Poste;
import ch.steversoix.jtv.desktop.poste.service.PosteService;
import ch.steversoix.jtv.desktop.saison.annee.model.Annee;
import ch.steversoix.jtv.desktop.saison.annee.service.AnneeService;
import ch.steversoix.jtv.desktop.saison.cours.exception.AbsenceException;
import ch.steversoix.jtv.desktop.saison.cours.exception.CoursException;
import ch.steversoix.jtv.desktop.saison.cours.exception.HoraireException;
import ch.steversoix.jtv.desktop.saison.cours.exception.ParticipantException;
import ch.steversoix.jtv.desktop.saison.cours.model.*;
import ch.steversoix.jtv.desktop.saison.cours.service.CoursEnumeration;
import ch.steversoix.jtv.desktop.saison.cours.service.CoursService;
import ch.steversoix.jtv.desktop.saison.cours.service.GroupeService;
import ch.steversoix.jtv.desktop.saison.cours.service.HoraireService;
import ch.steversoix.jtv.desktop.saison.model.Inscription;
import ch.steversoix.jtv.desktop.saison.model.Saison;
import ch.steversoix.jtv.desktop.saison.programme.model.Programme;
import ch.steversoix.jtv.desktop.saison.programme.services.ProgrammeService;
import ch.steversoix.jtv.desktop.saison.service.SaisonService;
import ch.steversoix.jtv.desktop.tireur.model.JeuneTireur;
import ch.steversoix.jtv.desktop.tireur.model.Moniteur;
import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import ch.steversoix.jtv.desktop.tireur.services.SecurityUser;
import ch.steversoix.jtv.desktop.tireur.services.TireurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = "/saisons/{idSaison}/cours")
public class CoursController {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Autowired
    SaisonService saisonService;
    @Autowired
    PosteService posteService;
    @Autowired
    CoursService coursService;
    @Autowired
    HoraireService horaireService;
    @Autowired
    GroupeService groupeService;
    @Autowired
    ProgrammeService programmeService;
    @Autowired
    TireurService tireurService;
    @Autowired
    AnneeService anneeService;

    /*
    * Les requêtes qui suivent, concernent la navigation
    * */
    @ModelAttribute("linkHoraire")
    public String getLinkHoraire(@PathVariable("idSaison") long idSaison, @PathVariable("idCours") Optional<Long> idCours) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        if (idCours.isPresent()) {
            Cours cours = coursService.getCoursIfExistOrThrow404(idCours.get());
            return linkLevel(saison, cours);
        }
        return "";
    }

    @ModelAttribute("user")
    public SecurityUser getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (SecurityUser) auth.getPrincipal();
    }

    /* Affichage du formulaire qui liste les cours */
    @GetMapping("")
    public String getCours(@PathVariable("idSaison") long idSaison, Model model, RedirectAttributes redirectAttributes) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        model.addAttribute("saison", saison);
        List<Cours> lst = coursService.findCoursBySaisonAndByActif(idSaison, true);
        model.addAttribute("cours", lst);
        model.addAttribute("actif", true);
        return "gestionCours";
    }

    /* Affichage du formulaire qui liste les cours */
    @GetMapping("/inactifs")
    public String getCoursInactifs(@PathVariable("idSaison") long idSaison, Model model, RedirectAttributes redirectAttributes) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        model.addAttribute("saison", saison);
        List<Cours> lst = coursService.findCoursBySaisonAndByActif(idSaison, false);
        model.addAttribute("cours", lst);
        model.addAttribute("actif", false);
        return "gestionCours";
    }

    @GetMapping("/{idCours}")
    public String getCours(@PathVariable("idSaison") long idSaison,
                           @PathVariable("idCours") long idCours,
                           Model model) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        Cours cours = coursService.getCoursIfExistOrThrow404(idCours);
        model.addAttribute("saison", saison);
        model.addAttribute("cours", cours);
        return "cours/detailCours";
    }

    @GetMapping("/{idCours}/presences")
    public String getPresences(@PathVariable("idSaison") long idSaison,
                               @PathVariable("idCours") long idCours,
                               Model model) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        Cours cours = coursService.getCoursIfExistOrThrow404(idCours);
        model.addAttribute("saison", saison);
        model.addAttribute("cours", cours);
        for (Inscription inscrit : saison.getInscriptions()) {
            if((inscrit.isActif())||(inscrit.getDateDesinscription().isPresent() && inscrit.getDateDesinscription().get().after(cours.getDateCours()))) {
                Optional<Participant> p = coursService.findParticipant(cours, inscrit);
                if (!p.isPresent()) {
                    coursService.setPresence(cours, inscrit, 4);
                }
            }
        }

        List<Participant> parJT=cours.getParticipants().stream()
                .filter(p -> p.getInscrit().getTireur() instanceof JeuneTireur)
                .collect(Collectors.toList());

        List<Participant> parMON=cours.getParticipants().stream()
                .filter(p -> p.getInscrit().getTireur() instanceof Moniteur)
                .collect(Collectors.toList());

        ParticipantListForm participantsWrapperJT = new ParticipantListForm();
        participantsWrapperJT.setParticipantList(
                new ArrayList<>(parJT)
        );

        ParticipantListForm participantsWrapperMON = new ParticipantListForm();
        participantsWrapperMON.setParticipantList(
                new ArrayList<>(parMON)
        );

        model.addAttribute("participantsMON",participantsWrapperMON);
        model.addAttribute("participantsJT",participantsWrapperJT);
        model.addAttribute("absences", coursService.findAbsenceByCours(cours));
        return "cours/gestionPresences";
    }

    @GetMapping("/{idCours}/resultats")
    public String getResultats(@PathVariable("idSaison") long idSaison,
                               @PathVariable("idCours") long idCours,
                               Model model) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        Cours cours = coursService.getCoursIfExistOrThrow404(idCours);

        model.addAttribute("saison", saison);
        model.addAttribute("cours", cours);
        model.addAttribute("participants", getParticipantListJTOnly(cours));

        return "cours/gestionResultats";
    }

    @PostMapping("/{idCours}/resultats")
    public String getResultats(@PathVariable("idSaison") long idSaison,
                               @PathVariable("idCours") long idCours,
                               @ModelAttribute("participants") ParticipantListForm participants,
                               BindingResult bindingResult,
                               Model model) {

        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        Cours cours = coursService.getCoursIfExistOrThrow404(idCours);

        model.addAttribute("saison", saison);
        model.addAttribute("cours", cours);

        Boolean noErrorFlag = true;
        for (Participant taintedP : participants.getParticipantList()) {
            Optional<Participant> p = coursService.findParticipant(taintedP.getId());
            if (p.isPresent()) {
                try {
                    coursService.setResultats(cours,
                            p.get(),
                            taintedP.getTotalObtenu(),
                            taintedP.getNbMunitionsSupplementaires());

                } catch (ParticipantException ex) {
                    model.addAttribute("messages", FlashMessageGenerator.generateMultipleMessages(
                            FlashMessage.Level.DANGER, ex.getErreurs())
                    );
                    noErrorFlag = false;
                }
            } else {
                LOGGER.log(Level.WARNING, String.format("Aucun participant d'id %d, les résultats seront ignorés."));
            }
        }
        if (noErrorFlag) {
            model.addAttribute("message", FlashMessageGenerator.generateSingleMessage(
                    FlashMessage.Level.SUCCESS, CoursEnumeration.RESULTAT_SUCCESS.getErreurException())
            );
        }
        model.addAttribute("participants", getParticipantListJTOnly(cours));
        return "cours/gestionResultats";
    }

    private ParticipantListForm getParticipantListAll(Cours c) {
        ParticipantListForm participantsWrapper = new ParticipantListForm();
        participantsWrapper.setParticipantList(
                c.getParticipants()
                        .stream()
                        .collect(Collectors.toCollection(ArrayList::new))
        );
        return participantsWrapper;
    }

    private ParticipantListForm getParticipantListJTOnly(Cours c) {
        ParticipantListForm participantsWrapper = new ParticipantListForm();
        participantsWrapper.setParticipantList(
                c.getParticipants()
                        .stream()
                        .filter(p -> p.getInscrit().getTireur() instanceof JeuneTireur)
                        .filter(p -> !p.getInscrit().getDateDesinscription().isPresent() || p.getInscrit().getDateDesinscription().get().after(c.getDateCours()))
                        .collect(Collectors.toCollection(ArrayList::new))
        );
        return participantsWrapper;
    }


    @GetMapping("/{idCours}/presences/etat")
    public String setPresence(@PathVariable("idSaison") long idSaison,
                              @PathVariable("idCours") long idCours,
                              @RequestParam(value = "p", required = true) long present,
                              @RequestParam(value = "state", required = true) int etat,
                              Model model) {


        LOGGER.log(Level.INFO, "Changer participant " + present + " à état " + etat);
        //coursService.setPresence()

        Optional<Participant> p = coursService.findParticipant(present);
        if (p.isPresent()) {
            coursService.setPresence(p.get(), etat);
        }
        return "redirect:/saisons/" + idSaison + "/cours/" + idCours + "/presences";
    }

    /* Affichage du formulaire qui ajoute un cours */
    @GetMapping("/add")
    public String addCours(@PathVariable("idSaison") long idSaison, Model model, RedirectAttributes redirAttrs) {
        /* Vérifie que la saison existe */
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        /* RA 04 : saison terminée - on ne peut plus ajouter de cours */
        Date d = new Date();
        if (saison.getDateFin().before(d)) {
            redirAttrs.addFlashAttribute("messages", FlashMessageGenerator.generateSingleMessage(
                    FlashMessage.Level.DANGER, CoursEnumeration.SAISON_FINI.getErreurException())
            );
            return "redirect:/saisons/" + idSaison + "/cours/";
        }

        List<Poste> postes = posteService.findPostesByActif(true);
        List<Programme> programmes = programmeService.findProgrammesBySaisonAndByActif(saison, true);
        List<Annee> annees = anneeService.findAnneesByActif(true);

        model.addAttribute("annees", annees);
        model.addAttribute("programmes", programmes);
        model.addAttribute("postes", postes);
        model.addAttribute("cours", new Cours());
        model.addAttribute("idSaison", idSaison);
        model.addAttribute("minPoste", Cours.MIN_POSTE);
        return "ajoutCours";
    }

    /* Affichage du formulaire qui modifie un cours */
    @GetMapping("/{idCours}/edit")
    public String editCours(@PathVariable("idSaison") long idSaison, @PathVariable("idCours") long idCours, Model model,
                            RedirectAttributes redirAttrs) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        Cours cours = coursService.getCoursIfExistOrThrow404(idCours);

        /* RM 01 : Date du cours dépassé imposible de le modifier */
        Date d = new Date();
        if (d.after(cours.getDateCours())) {
            redirAttrs.addFlashAttribute("messages", FlashMessageGenerator.generateSingleMessage(
                    FlashMessage.Level.DANGER, CoursEnumeration.COURS_NONMOD_TERMINER.getErreurException())
            );
            return "redirect:/saisons/" + idSaison + "/cours/";
        }

        List<Poste> postes = posteService.findPostesByActif(true);
        List<Programme> programmes = programmeService.findProgrammesBySaisonAndByActif(saison, true);
        List<Annee> annees = anneeService.findAnneesByActif(true);

        model.addAttribute("annees", annees);
        model.addAttribute("programmes", programmes);
        model.addAttribute("postes", postes);
        model.addAttribute("cours", cours);
        model.addAttribute("idSaison", idSaison);
        model.addAttribute("minPoste", Cours.MIN_POSTE);
        return "ajoutCours";
    }

    /* Affichage du formulaire de génération d'horaire pour un cours */
    @GetMapping(value = {"/{idCours}/horaire", "/{idCours}/horaire/details"})
    public String horaireCoursForm(@PathVariable("idSaison") long idSaison, @PathVariable("idCours") long idCours, Model model) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        Cours cours = coursService.getCoursIfExistOrThrow404(idCours);
        model.addAttribute("saison", saison);
        model.addAttribute("cours", cours);
        model.addAttribute("postes", cours.getPostes());
        model.addAttribute("groupes", coursService.getGroupesWithParticipants(cours));
        model.addAttribute("tranchesHoraires", coursService.getTranchesHoraire(cours));
        model.addAttribute("minPoste", Cours.MIN_POSTE);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_MONITEUR")))
            return "cours/coursHoraire";
        else
            return "cours/coursHoraireDetails";
    }

    /* Génération de l'horaire */
    @PostMapping("/{idCours}/horaire")
    public String horaireCours(@Valid @ModelAttribute("cours") Cours c, BindingResult bindingResult, Model model,
                               @PathVariable("idSaison") long idSaison, @PathVariable("idCours") long idCours,
                               RedirectAttributes redirAttrs) {
        /* Mise à jour des infos du cours */
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        Cours cours = coursService.getCoursIfExistOrThrow404(idCours);

        /* RG 19 on ne peut générer un planning déjà passé */
        if (cours.getDateCours().before(new Date())) {
            redirAttrs.addFlashAttribute("messages", FlashMessageGenerator.generateSingleMessage(
                    FlashMessage.Level.DANGER, CoursEnumeration.COURS_DEJA_PRODUIT.getErreurException())
            );
            return "redirect:/saisons/" + idSaison + "/cours/" + idCours + "/horaire";
        }
        cours.setHeureDebut(c.getHeureDebut());
        cours.setHeureFin(c.getHeureFin());
        cours.setTempsNettoyage(c.getTempsNettoyage());
        cours.setTempsPause(c.getTempsPause());
        List<Groupe> lstGro = groupeService.findAll();
        for (int i = 0; i < lstGro.size() & i < cours.getPostes().size(); i++) {
            cours.getGroupes().add(lstGro.get(i));
        }

        try {
            coursService.update(cours);
            coursService.generateHoraire(cours);
        } catch (CoursException ex) {
            LOGGER.log(Level.SEVERE, "Exception lors de la mise à jour d'un cours (horaire) : " + ex.getMessage());
            redirAttrs.addFlashAttribute("messages", FlashMessageGenerator.generateMultipleMessages(
                    FlashMessage.Level.DANGER, ex.getErreurs())
            );
            return "redirect:/saisons/" + idSaison + "/cours/" + idCours + "/horaire";
        } catch (HoraireException ex) {
            redirAttrs.addFlashAttribute("messages", FlashMessageGenerator.generateMultipleMessages(
                    FlashMessage.Level.DANGER, ex.getErreurs())
            );
            return "redirect:/saisons/" + idSaison + "/cours/" + idCours + "/horaire";
        }
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Horaire généré avec succès.")
        );
        return "redirect:/saisons/" + idSaison + "/cours/" + idCours + "/horaire";
    }

    /*
    * Les requêtes qui suivent, intéragissent avec la BDD
    * */

    /* Ajout du cours dans la BDD */
    @PostMapping("")
    public String addCoursDB(@Valid @ModelAttribute("cours") Cours newCours, BindingResult bindingResult, Model model,
                             @PathVariable("idSaison") long idSaison, RedirectAttributes redirAttrs) {
        if (bindingResult.hasErrors()) {
            reloadAddUpdatePageAfterBindingResultError(model, newCours, idSaison);
            return "ajoutCours";
        }

        newCours.setSaison(saisonService.findSaisonById(idSaison));
        try {
            coursService.save(newCours);
        } catch (CoursException ex) {
            LOGGER.log(Level.SEVERE, "Exception lors de l'ajour d'un cours " + ex.getMessage());
            reloadAddUpdatePageAfterException(ex, model, newCours, idSaison);
        }
        model.addAttribute("idSaison", idSaison);

        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS, "Cours ajouté."));

        return "redirect:/saisons/" + idSaison + "/cours";
    }

    /* Modifie un cours dans la BDD */
    @PostMapping("/{idCours}/update")
    public String updateCoursDB(@Valid @ModelAttribute("cours") Cours cours, BindingResult bindingResult, Model model,
                                @PathVariable("idSaison") long idSaison, @PathVariable("idCours") long idCours,
                                RedirectAttributes redirAttrs) {
        if (bindingResult.hasErrors()) {
            reloadAddUpdatePageAfterBindingResultError(model, cours, idSaison);
            return "ajoutCours";
        }

        cours.setId(idCours);
        cours.setSaison(saisonService.findSaisonById(idSaison));
        try {
            coursService.update(cours);
        } catch (CoursException ex) {
            LOGGER.log(Level.SEVERE, "Exception lors de la mise à jour d'un cours : " + ex.getMessage());
            return reloadAddUpdatePageAfterException(ex, model, cours, idSaison);
        }
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(
                FlashMessage.Level.SUCCESS, "Le cours " + cours.getNom() + " à été modifié")
        );
        return "redirect:/saisons/" + idSaison + "/cours/" + idCours;
    }

    /* Désactive un cours dans la BDD */
    @GetMapping("/{idCours}/desactivate")
    public String desactivateCoursDB(@PathVariable("idCours") long idCours, @PathVariable("idSaison") long idSaison,
                                     Model model, RedirectAttributes redirAttrs) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        model.addAttribute("saison", saison);
        Cours cours = coursService.getCoursIfExistOrThrow404(idCours);
        model.addAttribute("cours", cours);
        try {
            coursService.desactivate(cours);
        } catch (CoursException ex) {
            LOGGER.log(Level.SEVERE, "Exception lors de la désacivation d'un cours : " + ex.getMessage());
            redirAttrs.addFlashAttribute("messages", FlashMessageGenerator.generateMultipleMessages(
                    FlashMessage.Level.DANGER, ex.getErreurs())
            );
            return "redirect:/saisons/" + idSaison + "/cours";
        }
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(
                FlashMessage.Level.SUCCESS, "Cours " + idCours + " à été désactiver")
        );
        return "redirect:/saisons/" + idSaison + "/cours";
    }

    /* Active un cours dans la BDD */
    @GetMapping("/{idCours}/activate")
    public String activateCoursDB(@PathVariable("idCours") long idCours, @PathVariable("idSaison") long idSaison,
                                  Model model, RedirectAttributes redirAttrs) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        model.addAttribute("saison", saison);
        Cours cours = coursService.getCoursIfExistOrThrow404(idCours);
        model.addAttribute("cours", cours);
        try {
            coursService.activate(cours);
        } catch (CoursException ex) {
            LOGGER.log(Level.SEVERE, "Exception lors de l'activation d'un cours : " + ex.getMessage());
            redirAttrs.addFlashAttribute("messages", FlashMessageGenerator.generateMultipleMessages(
                    FlashMessage.Level.DANGER, ex.getErreurs())
            );
            return "redirect:/saisons/" + idSaison + "/cours/inactifs";
        }
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(
                FlashMessage.Level.SUCCESS, "Cours " + idCours + " à été activé")
        );
        return "redirect:/saisons/" + idSaison + "/cours/inactifs";
    }

    // prévenir d'une absence - Appel de la fenêtre (template)
    @GetMapping("/{idCours}/prevenirabsence")
    public String getPrevenirAbsence(@PathVariable("idSaison") long idSaison, @PathVariable("idCours") long idCours,
                                     Model model, RedirectAttributes redirAttrs) {
        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        model.addAttribute("saison", saison);
        Cours cours = coursService.getCoursIfExistOrThrow404(idCours);
        model.addAttribute("cours", cours);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        SecurityUser su = (SecurityUser) auth.getPrincipal();
        Optional<Tireur> tireur = tireurService.findTireurByUsername(su.getUsername());
        // flag servant à savoir si la personne à déjà prévenu de son absence pour l'affichage ou non du bouton
        if (coursService.findAbsenceByCoursAndTireur(cours, tireur.get()).isPresent()) {
            model.addAttribute("erreurs", CoursEnumeration.ABSENCE_DEJA_PREVENU.getErreurException());
            redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(
                    FlashMessage.Level.DANGER, CoursEnumeration.ABSENCE_DEJA_PREVENU.getErreurException()));
            return "redirect:/saisons/" + idSaison + "/cours/";
        }
        model.addAttribute("absence", new Absence());
        return "cours/prevenirAbsence";
    }

    // prévenir d'une absence - DB
    @PostMapping("/{idCours}/prevenirabsence")
    public String prevenirAbsence(@Valid @ModelAttribute("absence") Absence newAbsence, BindingResult bindingResult, Model model, @PathVariable("idSaison") long idSaison,
                                  @PathVariable("idCours") long idCours, RedirectAttributes redirAttrs) {
        if (bindingResult.hasErrors()) {
            return "cours/prevenirAbsence";
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        SecurityUser su = (SecurityUser) auth.getPrincipal();
        Optional<Tireur> tireur = tireurService.findTireurByUsername(su.getUsername());
        if (tireur.isPresent())
            newAbsence.setTireur(tireur.get());
        newAbsence.setCours(coursService.findCoursById(idCours).get());
        Saison saisonSelectionner = saisonService.getSaisonIfExistOrThrow404(idSaison);
        Cours coursSelectionner = coursService.getCoursIfExistOrThrow404(idCours);
        try {
            coursService.prevenirAbsence(newAbsence);
        } catch (AbsenceException ex) {
            LOGGER.log(Level.SEVERE, "Exception lors de l'annonce d'une absence : " + ex.getMessage());
            model.addAttribute("cours", coursSelectionner);
            model.addAttribute("absence", newAbsence);
            model.addAttribute("saison", saisonSelectionner);
            model.addAttribute("messages", FlashMessageGenerator.generateMultipleMessages(
                    FlashMessage.Level.DANGER, ex.getErreurs())
            );
            return "cours/prevenirAbsence";
        }
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");

        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.
                generateSingleMessage(FlashMessage.Level.SUCCESS,
                        "Vous avez prévenu de votre absence pour le cours du " +
                                df.format(coursSelectionner.getDateCours()) + "."));
        return "redirect:/saisons/" + idSaison + "/cours";
    }

    /*
    * Méthodes internes
    * */

    private String reloadAddUpdatePageAfterException(CoursException ex, Model model, Cours cours, long idSaison) {
        Saison s = saisonService.findSaisonById(idSaison);
        List<Poste> postes = posteService.findPostesByActif(true);
        List<Programme> programmes = programmeService.findProgrammesBySaisonAndByActif(s, true);
        List<Annee> annees = anneeService.findAnneesByActif(true);

        model.addAttribute("annees", annees);
        model.addAttribute("programmes", programmes);
        model.addAttribute("postes", postes);
        model.addAttribute("cours", cours);
        model.addAttribute("idSaison", idSaison);
        model.addAttribute("minPoste", Cours.MIN_POSTE);
        model.addAttribute("messages", FlashMessageGenerator.generateMultipleMessages(
                FlashMessage.Level.DANGER, ex.getErreurs())
        );
        return "ajoutCours";
    }

    private String reloadAddUpdatePageAfterBindingResultError(Model model, Cours cours, long idSaison) {
        Saison s = saisonService.findSaisonById(idSaison);
        List<Poste> postes = posteService.findPostesByActif(true);
        List<Programme> programmes = programmeService.findProgrammesBySaisonAndByActif(s, true);
        List<Annee> annees = anneeService.findAnneesByActif(true);

        model.addAttribute("annees", annees);
        model.addAttribute("programmes", programmes);
        model.addAttribute("postes", postes);
        model.addAttribute("cours", cours);
        model.addAttribute("minPoste", Cours.MIN_POSTE);
        model.addAttribute("idSaison", idSaison);
        return "ajoutCours";
    }

    /*
    * si l'utilisateur est jt il aura accès au formulaire coursHoraireDetail
    * sinon coursHoraire
    * */

    private String linkLevel(Saison saison, Cours cours) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        SecurityUser su = (SecurityUser) auth.getPrincipal();
        if (su.getLevel() == Tireur.LEVEL_USER) {
            return "/saisons/" + saison.getId() + "/cours/" + cours.getId() + "/horaire/details";
        } else {
            return "/saisons/" + saison.getId() + "/cours/" + cours.getId() + "/horaire";
        }
    }
}
