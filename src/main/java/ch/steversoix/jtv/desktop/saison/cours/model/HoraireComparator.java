package ch.steversoix.jtv.desktop.saison.cours.model;

import java.util.Comparator;

public class HoraireComparator implements Comparator<Horaire> {
    @Override
    public int compare(Horaire h1, Horaire h2) {
        int code = h1.getGroupe().compareTo(h2.getGroupe());
        if(code==0){
            return h1.getHeureDebut().compareTo(h2.getHeureDebut());
        }
        return code;
    }
}
