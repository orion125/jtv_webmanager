package ch.steversoix.jtv.desktop;

import java.util.logging.Logger;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.annotation.Order;


@SpringBootApplication
@Order(0)
public class JTVMobileMain implements ApplicationRunner {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public static void main(String[] args) {
        SpringApplication.run(JTVMobileMain.class, args);
        new FrontController().Do();
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {

    }
}