package ch.steversoix.jtv.desktop.tireur.services;

import ch.steversoix.jtv.desktop.core.exception.ResourceNotFoundException;
import ch.steversoix.jtv.desktop.outils.FilterTools;
import ch.steversoix.jtv.desktop.tireur.controller.NewPassword;
import ch.steversoix.jtv.desktop.tireur.exceptions.ImportException;
import ch.steversoix.jtv.desktop.tireur.exceptions.JeuneTireurException;
import ch.steversoix.jtv.desktop.tireur.exceptions.PasswordNotTheSameException;
import ch.steversoix.jtv.desktop.tireur.model.ImportJTWrapper;
import ch.steversoix.jtv.desktop.tireur.model.JeuneTireur;
import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import ch.steversoix.jtv.desktop.tireur.model.TireurRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class JeuneTireurService {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private TireurRepository tireurRepository;
    private TireurServiceImpl tireurService;
    private PasswordEncoder passwordEncoder;

    /* Constructeur*/
    public JeuneTireurService(TireurRepository tireurRepository, TireurServiceImpl tireurService, PasswordEncoder passwordEncoder) {
        this.tireurRepository = tireurRepository;
        this.tireurService = tireurService;
        this.passwordEncoder = passwordEncoder;
    }

    public List<JeuneTireur> findTireursByActif(boolean actif) {
        return tireurRepository.findJeunesTireursByActif(actif);
    }

    public JeuneTireur findJeuneTireurById(long id) {
        return tireurRepository.findJeuneTireurById(id).get();
    }

    public void save(JeuneTireur newJT) throws JeuneTireurException {
        ArrayList<String> erreurs = new ArrayList<>();

        erreurs = rulesAddUpdate(newJT);
        if (erreurs.size() != 0) {
            JeuneTireurException exception = new JeuneTireurException();
            exception.addAll(erreurs);
            throw exception;
        }

        newJT.setLevel(Tireur.LEVEL_USER);
        newJT.setPassword(passwordEncoder.encode(newJT.getPassword()));
        newJT.setActif(true);
        tireurRepository.save(newJT);
    }

    public void desactivate(JeuneTireur jt) {
        jt.setActif(false);
        tireurRepository.save(jt);
    }

    public void activate(JeuneTireur jt) {
        jt.setActif(true);
        tireurRepository.save(jt);
    }

    public void update(JeuneTireur jt) throws JeuneTireurException {
        ArrayList<String> erreurs = new ArrayList<>();

        erreurs = rulesAddUpdate(jt);
        if (erreurs.size() != 0) {
            JeuneTireurException exception = new JeuneTireurException();
            exception.addAll(erreurs);
            throw exception;
        }

        jt.setLevel(Tireur.LEVEL_USER);
        jt.setActif(true);
        tireurRepository.save(jt);
    }

    public void updateDroit(JeuneTireur jt, NewPassword newPassword) throws PasswordNotTheSameException {
        if (newPassword.checkPassword()) {
            jt.setPassword(passwordEncoder.encode(newPassword.getNewPassword()));
            tireurRepository.save(jt);
            return;
        }
        throw new PasswordNotTheSameException();
    }


    private ArrayList<String> rulesAddUpdate(JeuneTireur jt) {
        ArrayList<String> erreurs = new ArrayList<String>();

        erreurs = tireurService.rulesAddUpdate(jt);

        /* Nom prénom NON vide */
        if (!FilterTools.isOnlyChar(jt.getPrenomParent()) || !FilterTools.isOnlyChar(jt.getNomParent())) {
            LOGGER.log(Level.SEVERE, TireurEnumeration.NOM_PRENOM_VIDE.getErreurforLog());
            erreurs.add(TireurEnumeration.NOM_PRENOM_VIDE.getErreurException());
        }
        // regle model JeuneTireur
        if(jt.getPrenomParent().trim().length() < 2 || jt.getNomParent().trim().length() < 2 || jt.getPrenomParent().trim().length() > 50 || jt.getNomParent().trim().length() > 50){
            LOGGER.log(Level.SEVERE, TireurEnumeration.MODEL_NOM_PRENOM_PARENT_INVALIDE.getErreurforLog());
            erreurs.add(TireurEnumeration.MODEL_NOM_PRENOM_PARENT_INVALIDE.getErreurException());
        }

        /* Adresse NON vide */
        if (jt.getAdresseParent().trim().equals("") || jt.getNpaParent().trim().equals("") || jt.getLocaliteParent().trim().equals("")) {
            LOGGER.log(Level.SEVERE, TireurEnumeration.ADRESSE_VIDE.getErreurforLog());
            erreurs.add(TireurEnumeration.ADRESSE_VIDE.getErreurException());
        }
        // regle model JeuneTireur
        if(jt.getAdresseParent().trim().length() < 2 || jt.getLocaliteParent().trim().length() < 2 || jt.getAdresseParent().trim().length() > 50 || jt.getLocaliteParent().trim().length() > 50){
            LOGGER.log(Level.SEVERE, TireurEnumeration.MODEL_ADRESSE_LOCALITE_PARENT_INVALIDE.getErreurforLog());
            erreurs.add(TireurEnumeration.MODEL_ADRESSE_LOCALITE_PARENT_INVALIDE.getErreurException());
        }
        // regle model JeuneTireur
        if(jt.getNpaParent().trim().length() < 4 || jt.getNpaParent().trim().length() > 8){
            LOGGER.log(Level.SEVERE, TireurEnumeration.MODEL_NPA_PARENT_INVALIDE.getErreurforLog());
            erreurs.add(TireurEnumeration.MODEL_NPA_PARENT_INVALIDE.getErreurException());
        }

        /* Contact NON vide */
        if (!FilterTools.isTelephoneNumber(jt.getNumNatelParent())) {
            LOGGER.log(Level.SEVERE, TireurEnumeration.CONTACT_VIDE.getErreurforLog());
            erreurs.add(TireurEnumeration.CONTACT_VIDE.getErreurException());
        }
        return erreurs;
    }


    public JeuneTireur getTypeJeuneTireurIfExistOrThrow404(long idJeuneTireur) {
        Optional<JeuneTireur> optJeuneTireur = tireurRepository.findJeuneTireurById(idJeuneTireur);
        if (!optJeuneTireur.isPresent()) {
            LOGGER.log(Level.SEVERE, TireurEnumeration.JT_NOT_FOUND.getErreurforLog());
            throw new ResourceNotFoundException(TireurEnumeration.JT_NOT_FOUND.getErreurException());
        } else
            return optJeuneTireur.get();
    }

    public String generatePasswordForImport(JeuneTireur tireur) {
        return "JTV" + tireur.getPrenomParent() + tireur.getNpa();
        //return "Hello123";
    }

    public JeuneTireur importJT(ImportJTWrapper ijtw) throws JeuneTireurException {
        JeuneTireur jt = ijtw.getJT();
        if (jt.getUsername() == null || jt.getUsername().isEmpty()) {
            //TODO vérifier que l'username n'existe pas
            jt.setUsername(String.format("%s.%s", jt.getPrenom(), jt.getNom()));
        }

        if(jt.getPassword() == null || jt.getPassword().isEmpty()){
            jt.setPassword(generatePasswordForImport(jt));
        }

        save(jt);
        return jt;
    }

    public JeuneTireur convertCSVtoJT(String[] fields) throws ImportException {
        ImportException importEx = new ImportException();

        if (fields.length == 18) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
            JeuneTireur jtToImport = new JeuneTireur();

            jtToImport.setTitre(fields[0].trim());
            jtToImport.setNom(fields[1].trim());
            jtToImport.setPrenom(fields[2].trim());
            try {
                jtToImport.setDateNaissance(formatter.parse(fields[3].trim()));
            } catch (ParseException e) {
                LOGGER.log(Level.SEVERE, "Champ date de naissance invalide");
                importEx.add("Le champ date de naissance est invalide.");
            }
            jtToImport.setNumTelephone(fields[4].trim());
            jtToImport.setNumNatel(fields[5].trim());
            jtToImport.setNpa(fields[6].trim());
            jtToImport.setLocalite(fields[7].trim());
            jtToImport.setAdresse(fields[8].trim());
            jtToImport.setEmail(fields[9].trim());
            jtToImport.setNomParent(fields[10].trim());
            jtToImport.setPrenomParent(fields[11].trim());
            jtToImport.setNumNatelParent(fields[12].trim());
            jtToImport.setNpaParent(fields[13].trim());
            jtToImport.setLocaliteParent(fields[14].trim());
            jtToImport.setAdresseParent(fields[15].trim());
            jtToImport.setUsername(fields[16].trim());
            jtToImport.setPassword(fields[17].trim());

            importEx.addAll(rulesAddUpdate(jtToImport));

            if(importEx.size() > 0)
                throw importEx;
            else
                return jtToImport;
        } else {
            LOGGER.log(Level.SEVERE, TireurEnumeration.IMPORT_FILE_INVALID.getErreurforLog());
            importEx.add(TireurEnumeration.IMPORT_FILE_INVALID.getErreurException());
            throw importEx;
        }
    }
}
