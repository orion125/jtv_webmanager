package ch.steversoix.jtv.desktop.tireur.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "jtv_jeuneTireur")
public class JeuneTireur extends Tireur implements Serializable {
    @Size(min=2, max=50)
    @Column(name = "jeu_nomParent")
    private String nomParent;
    @Size(min=2, max=50)
    @Column(name = "jeu_prenomParent")
    private String prenomParent;
    @Size(min=2, max=50)
    @Column(name = "jeu_adresseParent")
    private String adresseParent;
    @Size(min=4, max=8)
    @Column(name = "jeu_npa")
    private String npaParent;
    @Size(min=2, max=50)
    @Column(name = "jeu_localiteParent")
    private String localiteParent;
    @NotNull(message = "Veuillez renseignez un N° de contact.")
    @Column(name = "jeu_numNatelParent")
    private String numNatelParent;

    public JeuneTireur() {
        super();
    }

    public JeuneTireur(String nom, String prenom, String titre, Date dateNaissance, String adresse, String npa, String localite, String email, String numTelephone, String numNatel, String username, String password, int level, String nomParent, String prenomParent, String adresseParent, String npaParent, String localiteParent, String numNatelParent) {
        super(nom, prenom, titre, dateNaissance, adresse, npa, localite, email, numTelephone, numNatel, username, password, level);
        this.nomParent = nomParent;
        this.prenomParent = prenomParent;
        this.adresseParent = adresseParent;
        this.npaParent = npaParent;
        this.localiteParent = localiteParent;
        this.numNatelParent = numNatelParent;
    }

    /* Getter & Setter */
    public String getNomParent() {
        return nomParent;
    }

    public void setNomParent(String nomParent) {
        this.nomParent = nomParent;
    }

    public String getPrenomParent() {
        return prenomParent;
    }

    public void setPrenomParent(String prenomParent) {
        this.prenomParent = prenomParent;
    }

    public String getAdresseParent() {
        return adresseParent;
    }

    public void setAdresseParent(String adresseParent) {
        this.adresseParent = adresseParent;
    }

    public String getNpaParent() {
        return npaParent;
    }

    public void setNpaParent(String npaParent) {
        this.npaParent = npaParent;
    }

    public String getLocaliteParent() {
        return localiteParent;
    }

    public void setLocaliteParent(String localiteParent) {
        this.localiteParent = localiteParent;
    }

    public String getNumNatelParent() {
        return numNatelParent;
    }

    public void setNumNatelParent(String numNatelParent) {
        this.numNatelParent = numNatelParent;
    }
}
