package ch.steversoix.jtv.desktop.tireur.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

public class ImportJTWrapper {
    SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    /* Non-JT method */
    private boolean toAdd;
    private ArrayList<String> erreurs;
    private boolean empty;

    /* JT Method */

    private String nom;
    private String prenom;
    private String titre;
    private String dateNaissance;
    private String adresse;
    private String npa;
    private String localite;
    private String email;
    private String numTelephone;
    private String numNatel;
    private String username;
    private String password;
    private String nomParent;
    private String prenomParent;
    private String adresseParent;
    private String npaParent;
    private String localiteParent;
    private String numNatelParent;


    public ImportJTWrapper() {
    }

    public ImportJTWrapper(JeuneTireur JT) {
        this.toAdd = true;
        this.empty = false;
        this.nom = JT.getNom();
        this.prenom = JT.getPrenom();
        this.titre = JT.getTitre();
        this.dateNaissance = formatter.format(JT.getDateNaissance());
        this.adresse = JT.getAdresse();
        this.npa = JT.getNpa();
        this.localite = JT.getLocalite();
        this.email = JT.getEmail();
        this.numTelephone = JT.getNumTelephone();
        this.numNatel = JT.getNumNatel();
        this.prenomParent = JT.getPrenomParent();
        this.nomParent = JT.getNomParent();
        this.adresseParent = JT.getAdresseParent();
        this.npaParent = JT.getNpaParent();
        this.localiteParent = JT.getLocaliteParent();
        this.numNatelParent = JT.getNumNatelParent();
        this.username = JT.getUsername();
        this.password = JT.getPassword();
    }

    public ImportJTWrapper(ArrayList<String> erreurs) {
        this.erreurs = erreurs;
        this.toAdd = false;
        this.empty = true;
    }

    public boolean isToAdd() {
        return toAdd;
    }

    public void setToAdd(boolean toAdd) {
        this.toAdd = toAdd;
    }

    public ArrayList<String> getErreurs() {
        return erreurs;
    }

    public void setErreurs(ArrayList<String> erreurs) {
        this.erreurs = erreurs;
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    public JeuneTireur getJT() {

        JeuneTireur jt = new JeuneTireur();
        jt.setNom(this.nom);
        jt.setPrenom(this.prenom);
        jt.setTitre(this.titre);
        try {
            jt.setDateNaissance(formatter.parse(this.dateNaissance));
        } catch (ParseException e) {
            jt.setDateNaissance(null);
        }
        jt.setAdresse(this.adresse);
        jt.setNpa(this.npa);
        jt.setLocalite(this.localite);
        jt.setEmail(this.email);
        jt.setNumTelephone(this.numTelephone);
        jt.setNumNatel(this.numNatel);
        jt.setUsername(this.username);
        jt.setPassword(this.password);
        jt.setPrenomParent(this.prenomParent);
        jt.setNomParent(this.nomParent);
        jt.setAdresseParent(this.adresseParent);
        jt.setNpaParent(this.npaParent);
        jt.setLocaliteParent(this.localiteParent);
        jt.setNumNatelParent(this.numNatelParent);
        return jt;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNpa() {
        return npa;
    }

    public void setNpa(String npa) {
        this.npa = npa;
    }

    public String getLocalite() {
        return localite;
    }

    public void setLocalite(String localite) {
        this.localite = localite;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumTelephone() {
        return numTelephone;
    }

    public void setNumTelephone(String numTelephone) {
        this.numTelephone = numTelephone;
    }

    public String getNumNatel() {
        return numNatel;
    }

    public void setNumNatel(String numNatel) {
        this.numNatel = numNatel;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNomParent() {
        return nomParent;
    }

    public void setNomParent(String nomParent) {
        this.nomParent = nomParent;
    }

    public String getPrenomParent() {
        return prenomParent;
    }

    public void setPrenomParent(String prenomParent) {
        this.prenomParent = prenomParent;
    }

    public String getAdresseParent() {
        return adresseParent;
    }

    public void setAdresseParent(String adresseParent) {
        this.adresseParent = adresseParent;
    }

    public String getNpaParent() {
        return npaParent;
    }

    public void setNpaParent(String npaParent) {
        this.npaParent = npaParent;
    }

    public String getLocaliteParent() {
        return localiteParent;
    }

    public void setLocaliteParent(String localiteParent) {
        this.localiteParent = localiteParent;
    }

    public String getNumNatelParent() {
        return numNatelParent;
    }

    public void setNumNatelParent(String numNatelParent) {
        this.numNatelParent = numNatelParent;
    }
}
