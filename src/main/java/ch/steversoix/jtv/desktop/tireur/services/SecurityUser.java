package ch.steversoix.jtv.desktop.tireur.services;

import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

public class SecurityUser extends Tireur implements UserDetails {

    private static final long serialVersionUID = 1L;

    public SecurityUser(Tireur t) {
        this.setId(t.getId());
        this.setUsername(t.getUsername());
        this.setEmail((t.getEmail()));
        this.setPassword(t.getPassword());
        this.setLevel(t.getLevel());
        this.setActif(t.isActif());
        this.setLastEditedSaison(t.getLastEditedSaison());
        this.setNom(t.getNom());
        this.setPrenom(t.getPrenom());
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        switch (getLevel()) {
            case LEVEL_USER:
                authorities.add(new SimpleGrantedAuthority("ROLE_TIREUR"));
                break;
            case LEVEL_MANAGER:
                authorities.add(new SimpleGrantedAuthority("ROLE_TIREUR"));
                authorities.add(new SimpleGrantedAuthority("ROLE_MONITEUR"));
                break;
            case LEVEL_ADMIN:
                authorities.add(new SimpleGrantedAuthority("ROLE_TIREUR"));
                authorities.add(new SimpleGrantedAuthority("ROLE_MONITEUR"));
                authorities.add(new SimpleGrantedAuthority("ROLE_DIRECTEUR"));
                break;
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public String getUsername() {
        return super.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isActif();
    }
}
