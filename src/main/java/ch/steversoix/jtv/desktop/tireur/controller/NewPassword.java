package ch.steversoix.jtv.desktop.tireur.controller;

public class NewPassword {
    private String password;
    private String newPassword;
    private String reNewPassword;

    /*
    * Constructeur
    * */
    public NewPassword(String password, String newPassword, String reNewPassword) {
        this.password = password;
        this.newPassword = newPassword;
        this.reNewPassword = reNewPassword;
    }

    public NewPassword() {
    }



/*
    * Getter & Setter
    * */

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getReNewPassword() {
        return reNewPassword;
    }

    public void setReNewPassword(String reNewPassword) {
        this.reNewPassword = reNewPassword;
    }

    public boolean checkPassword() {
        /* GD 02 */
        String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}";
        if (!newPassword.matches(pattern)) {
            return false;
        }
        if (newPassword.equals(reNewPassword)) {
            return true;
        } else {
            return false;
        }
    }
}
