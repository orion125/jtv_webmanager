package ch.steversoix.jtv.desktop.tireur.services;

import ch.steversoix.jtv.desktop.core.exception.ResourceNotFoundException;
import ch.steversoix.jtv.desktop.tireur.controller.NewPassword;
import ch.steversoix.jtv.desktop.tireur.exceptions.MoniteurException;
import ch.steversoix.jtv.desktop.tireur.exceptions.NewPasswordException;
import ch.steversoix.jtv.desktop.tireur.exceptions.PasswordNotTheSameException;
import ch.steversoix.jtv.desktop.tireur.model.Moniteur;
import ch.steversoix.jtv.desktop.tireur.model.TireurRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class MoniteurService {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private TireurRepository tireurRepository;
    private TireurServiceImpl tireurService;
    private PasswordEncoder passwordEncoder;

    /* Constructeur*/
    public MoniteurService(TireurRepository tireurRepository,TireurServiceImpl tireurService, PasswordEncoder passwordEncoder) {
        this.tireurRepository = tireurRepository;
        this.tireurService = tireurService;
        this.passwordEncoder = passwordEncoder;
    }

    public List<Moniteur> findMoniteursByActif(boolean actif) {
        return tireurRepository.findMoniteursByActif(actif);
    }
    public Moniteur findMoniteurById(long id) {return tireurRepository.findMoniteurById(id).get();}

    public void save(Moniteur newMoniteur) throws MoniteurException {
        ArrayList<String> erreurs = rulesAddUpdate(newMoniteur);

        if(erreurs.size() != 0){
            MoniteurException exception = new MoniteurException();
            exception.addAll(erreurs);
            throw exception;
        }

        newMoniteur.setPassword(passwordEncoder.encode(newMoniteur.getPassword()));
        newMoniteur.setActif(true);
        tireurRepository.save(newMoniteur);
    }

    public void desactivate(Moniteur moniteur){
        moniteur.setActif(false);
        tireurRepository.save(moniteur);
    }

    public void activate(Moniteur moniteur){
        moniteur.setActif(true);
        tireurRepository.save(moniteur);
    }

    public void update(Moniteur moniteur) throws MoniteurException {
        ArrayList<String> erreurs = rulesAddUpdate(moniteur);

        if(erreurs.size() != 0){
            MoniteurException exception = new MoniteurException();
            exception.addAll(erreurs);
            throw exception;
        }

        moniteur.setActif(true);
        tireurRepository.save(moniteur);
    }

    public void updateDroit(Moniteur moniteur, NewPassword newPassword) throws PasswordNotTheSameException {
        if(newPassword.checkPassword()){
            moniteur.setPassword(passwordEncoder.encode(newPassword.getNewPassword()));
            tireurRepository.save(moniteur);
            return;
        }
        throw new PasswordNotTheSameException();
    }

    private ArrayList<String> rulesAddUpdate(Moniteur moniteur){
        ArrayList<String> erreurs = new ArrayList<>();

        // Controle pour le tireur
        erreurs.addAll(tireurService.rulesAddUpdate(moniteur));

        return erreurs;
    }


    public Moniteur getTypeMoniteurIfExistOrThrow404(long idMoniteur) {
        Optional<Moniteur> optMon = tireurRepository.findMoniteurById(idMoniteur);
        if (!optMon.isPresent()) {
            LOGGER.log(Level.SEVERE, TireurEnumeration.MON_NOT_FOUND.getErreurforLog());
            throw new ResourceNotFoundException(TireurEnumeration.MON_NOT_FOUND.getErreurException());
        } else
            return optMon.get();
    }
}
