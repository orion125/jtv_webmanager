package ch.steversoix.jtv.desktop.tireur.controller;

public enum TireurTitreEnumeration {
    /* RG 24 - Titre  d'un tireur */
    Monsieur("Monsieur",0),
    Madame("Madame",1);

    private String titre;
    private int code;

    TireurTitreEnumeration(String titre, int code) {
        this.titre = titre;
        this.code = code;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String erreurException) {
        this.titre = erreurException;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
