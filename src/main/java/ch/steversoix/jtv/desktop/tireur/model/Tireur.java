/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.steversoix.jtv.desktop.tireur.model;

import ch.steversoix.jtv.desktop.saison.model.Saison;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Jonathan Blum
 */
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@Table(name = "jtv_tireur")
public abstract class Tireur implements Comparable<Tireur>, Serializable {
    public final static int LEVEL_USER = 0;
    public final static int LEVEL_MANAGER = 1;
    public final static int LEVEL_ADMIN = 2;
    public final static int MIN_AGE = 10;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "tir_id")
    private Long id;
    @NotNull
    @Size(min=2, max=50)
    @Column(name = "tir_nom")
    private String nom;
    @NotNull
    @Size(min=2, max=50)
    @Column(name = "tir_prenom")
    private String prenom;
    @NotNull
    @Size(min=2, max=50)
    @Column(name = "tir_titre")
    private String titre;
    @NotNull(message="La date de naissance ne peut pas être vide.")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "tir_dateNaissance")
    private Date dateNaissance;
    @NotNull(message = "Veuillez renseignez les informations relatives au domicile du tireur.")
    @Size(min=2, max=50)
    @Column(name = "tir_adresse")
    private String adresse;
    @NotNull(message = "Veuillez renseignez les informations relatives au domicile du tireur")
    @Size(min=4, max=8)
    @Column(name = "tir_npa")
    private String npa;
    @NotNull(message = "Veuillez renseignez les informations relatives au domicile du tireur")
    @Size(min=2, max=50)
    @Column(name = "tir_localite")
    private String localite;
    @NotNull(message = "Veuillez renseignez un email de contact.")
    @Size(min=2, max=50)
    @Column(name = "tir_email")
    private String email;
    @NotNull(message = "Veuillez renseignez un N° de contact.")
    @Column(name = "tir_numtelephone")
    private String numTelephone;
    @NotNull(message = "Veuillez renseignez un N° de contact.")
    @Column(name = "tir_numNatel")
    private String numNatel;
    @Column(name = "tir_actif")
    private boolean actif;
    @Size(min=2, max=50)
    @Column(name = "tir_username")
    @NotEmpty(message = "*Please provide an username")
    private String username;
    @NotNull(message = "Le mot de passe doit avoir 8 caractères minimum, un chiffre, une majuscule et une minuscule au minimum.")
    @Column(name = "tir_password")
    private String password;
    @Column(name = "tir_level")
    private int level;
    @ManyToOne
    @JoinColumn(name = "tir_sai_last_edit_id", referencedColumnName = "sai_id")
    private Saison lastEditedSaison;


    public Tireur(Long id, String username, String password,Saison lastSaison, int level, boolean isactif) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.lastEditedSaison = lastSaison;
        this.level = level;
        this.actif = isactif;
    }

    public Tireur(String username, int level){
        this((long) -1, username,null ,null, level,true);
    }

    public Tireur(String username) {
        this(username, 0);
    }

    public Tireur() { }

    public Tireur(String nom, String prenom, String titre, Date dateNaissance, String adresse, String npa, String localite, String email,
                  String numTelephone, String numNatel, String username, String password, int level) {
        this.nom = nom;
        this.prenom = prenom;
        this.titre = titre;
        this.dateNaissance = dateNaissance;
        this.adresse = adresse;
        this.npa = npa;
        this.localite = localite;
        this.email = email;
        this.numTelephone = numTelephone;
        this.numNatel = numNatel;
        this.actif = true;
        this.username = username;
        this.password = password;
        this.level = level;
        this.lastEditedSaison = null;
    }

    public Tireur(String nom, String prenom, String titre, Date dateNaissance, String adresse, String npa, String localite,
                  String email, String numTelephone, String numNatel, String username, String password, int level, Saison lastEditedSaison) {
        this.nom = nom;
        this.prenom = prenom;
        this.titre = titre;
        this.dateNaissance = dateNaissance;
        this.adresse = adresse;
        this.npa = npa;
        this.localite = localite;
        this.email = email;
        this.numTelephone = numTelephone;
        this.numNatel = numNatel;
        this.actif = true;
        this.username = username;
        this.password = password;
        this.level = level;
        this.lastEditedSaison = lastEditedSaison;
    }

    public Long getId() { return id;}
    public void setId(Long id) { this.id = id; }
    public String getUsername() {return username;}
    public void setUsername(String username) {this.username = username;}
    public Saison getLastEditedSaison() { return lastEditedSaison; }
    public void setLastEditedSaison(Saison lastSaison) {this.lastEditedSaison = lastSaison;}
    public int getLevel() {return level;}
    public void setLevel(int level) {this.level = level;}
    public boolean isActif() {return actif;}
    public void setActif(boolean actif) {this.actif = actif;}
    public String getPassword() { return String.valueOf(password); }
    public void setPassword(String str) { this.password = str; }
    public String getNom() { return nom; }
    public void setNom(String nom) { this.nom = nom; }
    public String getPrenom() { return prenom; }
    public void setPrenom(String prenom) { this.prenom = prenom; }
    public Date getDateNaissance() { return dateNaissance; }
    public void setDateNaissance(Date dateNaissance) { this.dateNaissance = dateNaissance; }
    public String getAdresse() { return adresse; }
    public void setAdresse(String adresse) { this.adresse = adresse; }
    public String getNpa() { return npa; }
    public void setNpa(String npa) { this.npa = npa; }
    public String getLocalite() { return localite; }
    public void setLocalite(String localite) { this.localite = localite; }
    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }
    public String getNumTelephone() { return numTelephone; }
    public void setNumTelephone(String numTelephone) { this.numTelephone = numTelephone; }
    public String getNumNatel() { return numNatel; }
    public void setNumNatel(String numNatel) { this.numNatel = numNatel; }
    public boolean comparePassword(String str) { return password.equals(str); }
    public String getTitre() { return titre; }
    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getFullName() { return this.nom + " " + this.prenom; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tireur tireur = (Tireur) o;

        if (id != tireur.id) return false;
        return username != null ? username.equals(tireur.username) : tireur.username == null;
    }

    @Override
    public String toString() {
        return "Tireur{" +
                "username='" + username + '\'' +
                '}';
    }

    @Override
    public int compareTo(Tireur that) {
        int compare = this.getNom().compareTo(that.getNom());
        if (compare == 0) {
            compare = this.getPrenom().compareTo(that.getPrenom());
        }
        return compare;
    }
}
