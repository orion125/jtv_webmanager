package ch.steversoix.jtv.desktop.tireur.controller;

import ch.steversoix.jtv.desktop.outils.FlashMessage;
import ch.steversoix.jtv.desktop.outils.FlashMessageGenerator;
import ch.steversoix.jtv.desktop.saison.model.Inscription;
import ch.steversoix.jtv.desktop.saison.service.SaisonService;
import ch.steversoix.jtv.desktop.tireur.exceptions.MoniteurException;
import ch.steversoix.jtv.desktop.tireur.exceptions.NewPasswordException;
import ch.steversoix.jtv.desktop.tireur.exceptions.PasswordNotTheSameException;
import ch.steversoix.jtv.desktop.tireur.model.Moniteur;
import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import ch.steversoix.jtv.desktop.tireur.services.MoniteurService;
import ch.steversoix.jtv.desktop.tireur.services.SecurityUser;
import ch.steversoix.jtv.desktop.tireur.services.TireurEnumeration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
@RequestMapping(path="/moniteurs")
public class MoniteurController {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    @Autowired
    private MoniteurService moniteurService;
    @Autowired
    private SaisonService saisonService;

    @ModelAttribute("user")
    public SecurityUser getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (SecurityUser) auth.getPrincipal();
    }

    /* Affichage du formulaire qui liste les moniteurs */
    @GetMapping("")
    public String getMoniteurs(Model model){
        List<Moniteur> lst = moniteurService.findMoniteursByActif(true);
        model.addAttribute("tireurs",lst);
        model.addAttribute("actif",true);
        model.addAttribute("mdp", new NewPassword());

        model.addAttribute("enumDroit", MoniteurDroitEnumeration.values());
        return "gestionMoniteurs";
    }

    /* Affichage du formulaire qui liste les moniteurs */
    @GetMapping("/inactifs")
    public String getMoniteursInactifs(Model model){
        List<Moniteur> lst = moniteurService.findMoniteursByActif(false);
        model.addAttribute("tireurs",lst);
        model.addAttribute("actif",false);
        model.addAttribute("mdp", new NewPassword());
        model.addAttribute("enumDroit", MoniteurDroitEnumeration.values());
        return "gestionMoniteurs";
    }

    /* Affichage du formulaire qui ajoute un moniteur */
    @GetMapping("/add")
    public String showAddMoniteurForm(Model model){
        model.addAttribute("moniteur",new Moniteur());
        model.addAttribute("enumTitre",TireurTitreEnumeration.values());
        model.addAttribute("enumDroit",MoniteurDroitEnumeration.values());
        model.addAttribute("ageMin", Tireur.MIN_AGE);
        return "ajoutMoniteur";
    }

    @GetMapping("/{id}/edit")
    public String showUpdateMoniteurForm(@PathVariable("id") long id, Model model){
        Moniteur m = moniteurService.getTypeMoniteurIfExistOrThrow404(id);
        model.addAttribute("moniteur",m);
        model.addAttribute("enumTitre",TireurTitreEnumeration.values());
        model.addAttribute("enumDroit",MoniteurDroitEnumeration.values());
        model.addAttribute("ageMin", Tireur.MIN_AGE);
        return "ajoutMoniteur";
    }

    @GetMapping("/{id}/details")
    public String showDetailMoniteurForm(@PathVariable("id") long id, Model model){
        Moniteur m = moniteurService.getTypeMoniteurIfExistOrThrow404(id);
        List<Inscription> lstInsc = saisonService.findInscriptionsByTireur(m);
        model.addAttribute("moniteur",m);
        model.addAttribute("titre",m.getTitre());
        model.addAttribute("inscriptions",lstInsc);
        return "detailsMoniteur";
    }

    @PostMapping("/{id}/updateNiveauUser")
    public String showUpdateDroitForm(@PathVariable("id") long id,Model model, HttpServletRequest request, RedirectAttributes redirAttrs){
        if(request.getParameter("droit"+id)==null){
            redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                    "Une erreur est parvenue."));
            return "redirect:/moniteurs";
        }else {
            Moniteur m = moniteurService.getTypeMoniteurIfExistOrThrow404(id);
            m.setLevel(Integer.parseInt(request.getParameter("droit" + id)));

            try {
                moniteurService.update(m);
            } catch (MoniteurException e) {
                LOGGER.log(Level.FINE,"Exception à la mise à jour du moniteur "+m.getFullName()+" : "+e.getMessage());
                model.addAttribute("message", FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER,
                        e.getErreurs()));
            }

            redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                    "Les droits du moniteur " + m.getFullName() + " ont été changé."));
            return "redirect:/moniteurs";
        }
    }

    @PostMapping("/add")
    public String addMoniteur(@ModelAttribute Moniteur newMoniteur, Model model, RedirectAttributes redirAttrs){
        try {
            if(getUser().getLevel()==Tireur.LEVEL_MANAGER){
                newMoniteur.setLevel(Tireur.LEVEL_MANAGER);
            }
            moniteurService.save(newMoniteur);
        } catch (MoniteurException e) {
            LOGGER.log(Level.SEVERE,"Exception à l'ajout du moniteur "+newMoniteur.getFullName()+" : "+e.getMessage());
            model.addAttribute("moniteur", newMoniteur);
            model.addAttribute("enumTitre",TireurTitreEnumeration.values());
            model.addAttribute("enumDroit",MoniteurDroitEnumeration.values());
            model.addAttribute("ageMin", Tireur.MIN_AGE);
            model.addAttribute("messages", FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER, e.getErreurs()));
            return "ajoutMoniteur";
        }
        LOGGER.log(Level.FINE,"Le moniteur "+newMoniteur.getFullName()+" a été ajouté.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le moniteur "+newMoniteur.getFullName()+" a été ajouté."));
        return "redirect:/moniteurs";
    }

    @PostMapping("/{id}/update")
    public String updateMoniteur(@ModelAttribute Moniteur moniteur, Model model,@PathVariable("id") long id
            , RedirectAttributes redirAttrs){

        /* Récupérer toutes les infos conservées pour ce qui est des droits */
        Moniteur tempo = moniteurService.getTypeMoniteurIfExistOrThrow404(id);
        moniteur.setUsername(tempo.getUsername());
        moniteur.setPassword(tempo.getPassword());
        moniteur.setLevel(tempo.getLevel());

        try {
            moniteurService.update(moniteur);
        } catch (MoniteurException e) {
            LOGGER.log(Level.SEVERE,"Exception à la mise à jour du moniteur "+moniteur.getFullName()+" : "+e.getMessage());
            model.addAttribute("message", FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER,
                    e.getErreurs()));
            model.addAttribute("enumTitre",TireurTitreEnumeration.values());
            model.addAttribute("enumDroit",MoniteurDroitEnumeration.values());
            model.addAttribute("moniteur",moniteur);
            model.addAttribute("ageMin", Tireur.MIN_AGE);
            return "ajoutMoniteur";
        }
        LOGGER.log(Level.FINE,"Le moniteur "+moniteur.getFullName()+" a été modifié.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le moniteur "+moniteur.getFullName()+" a été modifié."));
        return "redirect:/moniteurs";
    }

    @PostMapping("/{id}/updateDroit")
    public String updateDroit(@PathVariable("id") long id, @ModelAttribute("nPass") NewPassword newPassword,
            RedirectAttributes redirAttrs){
        Moniteur m = moniteurService.findMoniteurById(id);

        /* Le moniteur peut modifier uniquement son propre mot de passe */
        SecurityUser su = getUser();
        if(su.getLevel() == Tireur.LEVEL_MANAGER){
            if(id != m.getId()){
                LOGGER.log(Level.SEVERE, "UpdateDroit - MoniteurException : "+TireurEnumeration.MDP_AUTRE_TIREUR.getErreurforLog());
                redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.DANGER,
                        TireurEnumeration.MDP_AUTRE_TIREUR.getErreurException()));
                return "redirect:/moniteurs";
            }
        }
        try {
            moniteurService.updateDroit(m, newPassword);
        } catch (PasswordNotTheSameException e) {
            LOGGER.log(Level.SEVERE, "UpdateDroit - MoniteurException : " + e.getMessage());
            redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.DANGER,
                    e.getMessage()));
            return "redirect:/moniteurs";
        }
        LOGGER.log(Level.FINE,"Le mot de passe du moniteur "+m.getFullName()+" a été modifié.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le mot de passe du moniteur "+m.getFullName()+" a été modifié."));
        return "redirect:/moniteurs";
    }


    @GetMapping("/{id}/desactivate")
    public String desactivateMoniteurDB(@PathVariable("id") long id, Model model, RedirectAttributes redirAttrs){
        Moniteur moniteur = moniteurService.getTypeMoniteurIfExistOrThrow404(id);
        moniteurService.desactivate(moniteur);
        LOGGER.log(Level.FINE,"Le moniteur "+moniteur.getFullName()+" a été desactivé.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le moniteur "+moniteur.getFullName()+" a été desactivé."));
        return "redirect:/moniteurs";
    }

    @GetMapping("/{id}/activate")
    public String activateMoniteurDB(@PathVariable("id") long id, Model model, RedirectAttributes redirAttrs){
        Moniteur moniteur = moniteurService.getTypeMoniteurIfExistOrThrow404(id);
        moniteurService.activate(moniteur);
        LOGGER.log(Level.FINE,"Le moniteur "+moniteur.getFullName()+" a été activé.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le moniteur "+moniteur.getFullName()+" a été activé."));
        return "redirect:/moniteurs/inactifs";
    }
}