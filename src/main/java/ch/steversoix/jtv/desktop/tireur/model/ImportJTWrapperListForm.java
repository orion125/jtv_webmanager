package ch.steversoix.jtv.desktop.tireur.model;

import java.util.ArrayList;


public class ImportJTWrapperListForm {
    private ArrayList<ImportJTWrapper> importJTList;

    public ArrayList<ImportJTWrapper> getImportJTList() {
        return importJTList;
    }

    public void setImportJTList(ArrayList<ImportJTWrapper> importJTList) {
        this.importJTList = importJTList;
    }

    public ImportJTWrapperListForm() {
    }
    public ImportJTWrapperListForm(ArrayList<ImportJTWrapper> importJTList) {
        this.importJTList = importJTList;
    }
}
