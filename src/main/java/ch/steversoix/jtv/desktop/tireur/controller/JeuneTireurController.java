package ch.steversoix.jtv.desktop.tireur.controller;

import ch.steversoix.jtv.desktop.outils.FlashMessage;
import ch.steversoix.jtv.desktop.outils.FlashMessageGenerator;
import ch.steversoix.jtv.desktop.saison.model.Inscription;
import ch.steversoix.jtv.desktop.saison.service.SaisonService;
import ch.steversoix.jtv.desktop.tireur.exceptions.ImportException;
import ch.steversoix.jtv.desktop.tireur.exceptions.JeuneTireurException;
import ch.steversoix.jtv.desktop.tireur.exceptions.PasswordNotTheSameException;
import ch.steversoix.jtv.desktop.tireur.model.ImportJTWrapper;
import ch.steversoix.jtv.desktop.tireur.model.ImportJTWrapperListForm;
import ch.steversoix.jtv.desktop.tireur.model.JeuneTireur;
import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import ch.steversoix.jtv.desktop.tireur.services.JeuneTireurService;
import ch.steversoix.jtv.desktop.tireur.services.SecurityUser;
import ch.steversoix.jtv.desktop.tireur.services.TireurEnumeration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
@RequestMapping(path = "/jeunestireurs")
public class JeuneTireurController {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    @Autowired
    private JeuneTireurService jeuneTireurService;
    @Autowired
    private SaisonService saisonService;

    private String fileLocation;

    @ModelAttribute("user")
    public SecurityUser getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (SecurityUser) auth.getPrincipal();
    }

    /* Affichage du formulaire qui liste les jt */
    @GetMapping("")
    public String getJT(Model model) {
        List<JeuneTireur> lst = jeuneTireurService.findTireursByActif(true);
        model.addAttribute("tireurs", lst);
        model.addAttribute("actif", true);
        model.addAttribute("mdp", new NewPassword());
        return "gestionJeunesTireurs";
    }

    /* Affichage du formulaire qui liste les jt inactifs */
    @GetMapping("/inactifs")
    public String getJTInactifs(Model model) {
        List<JeuneTireur> lst = jeuneTireurService.findTireursByActif(false);
        model.addAttribute("tireurs", lst);
        model.addAttribute("actif", false);
        model.addAttribute("mdp", new NewPassword());
        return "gestionJeunesTireurs";
    }

    /* Affichage du formulaire qui ajoute un jt */
    @GetMapping("/add")
    public String showAddJTForm(Model model) {
        model.addAttribute("jeunetireur", new JeuneTireur());
        model.addAttribute("enumTitre", TireurTitreEnumeration.values());
        model.addAttribute("ageMin", Tireur.MIN_AGE);
        return "ajoutJeuneTireur";
    }

    @GetMapping("/{id}/edit")
    public String showUpdateJtForm(@PathVariable("id") long id, Model model) {
        JeuneTireur jt = jeuneTireurService.getTypeJeuneTireurIfExistOrThrow404(id);
        model.addAttribute("jeunetireur", jt);
        model.addAttribute("enumTitre", TireurTitreEnumeration.values());
        model.addAttribute("ageMin", Tireur.MIN_AGE);
        return "ajoutJeuneTireur";
    }

    @GetMapping("/{id}/details")
    public String showDetailJtForm(@PathVariable("id") long id, Model model) {
        JeuneTireur jt = jeuneTireurService.getTypeJeuneTireurIfExistOrThrow404(id);
        List<Inscription> lstInsc = saisonService.findInscriptionsByTireur(jt);
        model.addAttribute("jeunetireur", jt);
        model.addAttribute("titre", jt.getTitre());
        model.addAttribute("inscriptions", lstInsc);
        return "detailsJT";
    }

    @GetMapping("/{id}/updateDroit")
    public String showUpdateDroitForm(@PathVariable("id") long id, Model model) {
        JeuneTireur jt = jeuneTireurService.getTypeJeuneTireurIfExistOrThrow404(id);
        model.addAttribute("tireur", jt);
        model.addAttribute("lien", "/jeunestireurs/" + jt.getId() + "/updateDroit");
        model.addAttribute("nPass", new NewPassword());
        return "changerMotPasse";
    }

    @PostMapping("/add")
    public String addJeuneTireur( @ModelAttribute JeuneTireur newJT, Model model, RedirectAttributes redirAttrs) {
        try {
            jeuneTireurService.save(newJT);
        } catch (JeuneTireurException e) {
            LOGGER.log(Level.SEVERE, "Exception à l'ajout du jeune " + newJT.getFullName() + " : " + e.getMessage());
            model.addAttribute("messages", FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER,
                    e.getErreurs()));
            model.addAttribute("enumTitre", TireurTitreEnumeration.values());
            model.addAttribute("jeunetireur", newJT);
            model.addAttribute("ageMin", Tireur.MIN_AGE);
            return "ajoutJeuneTireur";
        }
        LOGGER.log(Level.FINE, "Le jeune tireur " + newJT.getFullName() + " a été ajouté.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le jeune tireur " + newJT.getFullName() + " a été ajouté."));
        return "redirect:/jeunestireurs";
    }

    @PostMapping("/{id}/update")
    public String updateJeuneTireur(@ModelAttribute JeuneTireur jt, Model model, @PathVariable("id") long id,
                                    RedirectAttributes redirAttrs) {

        JeuneTireur tempo = jeuneTireurService.getTypeJeuneTireurIfExistOrThrow404(id);
        jt.setUsername(tempo.getUsername());
        jt.setPassword(tempo.getPassword());
        jt.setLevel(tempo.getLevel());

        try {
            jeuneTireurService.update(jt);
        } catch (JeuneTireurException e) {
            LOGGER.log(Level.SEVERE, "Exception à la mise à jour du jeune " + jt.getFullName() + " : " + e.getMessage());
            model.addAttribute("messages", FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER,
                    e.getErreurs()));
            model.addAttribute("jeunetireur", jt);
            model.addAttribute("enumTitre", TireurTitreEnumeration.values());
            model.addAttribute("ageMin", Tireur.MIN_AGE);
            return "ajoutJeuneTireur";
        }
        LOGGER.log(Level.FINE, "Le jeune tireur " + jt.getFullName() + " a été modifié.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le jeune tireur " + jt.getFullName() + " a été modifié."));
        return "redirect:/jeunestireurs";
    }

    @PostMapping("/{id}/updateDroit")
    public String updateDroit(@PathVariable("id") long id, @ModelAttribute() NewPassword newPassword, RedirectAttributes redirAttrs) {
        JeuneTireur jt = jeuneTireurService.findJeuneTireurById(id);

        try {
            jeuneTireurService.updateDroit(jt, newPassword);
        } catch (PasswordNotTheSameException e) {
            LOGGER.log(Level.SEVERE, "UpdateDroit - JeuneTireurException : " + e.getMessage());
            redirAttrs.addFlashAttribute("messages", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.DANGER,
                    e.getMessage()));
            return "redirect:/jeunestireurs";
        }
        LOGGER.log(Level.FINE, "Le mot de passe du jeune tireur " + jt.getFullName() + " a été modifié.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le mot de passe du  jeune tireur " + jt.getFullName() + " a été modifié."));
        return "redirect:/jeunestireurs";
    }

    @GetMapping("/{id}/desactivate")
    public String desactivateJeuneTireurDB(@PathVariable("id") long id, Model model, RedirectAttributes redirAttrs) {
        JeuneTireur jt = jeuneTireurService.getTypeJeuneTireurIfExistOrThrow404(id);
        jeuneTireurService.desactivate(jt);
        LOGGER.log(Level.FINE, "Le jeune tireur " + jt.getFullName() + " a été désactivé.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le jeune tireur " + jt.getFullName() + " a été désactivé."));
        return "redirect:/jeunestireurs";
    }

    @GetMapping("/{id}/activate")
    public String activateJeuneTireurDB(@PathVariable("id") long id, Model model, RedirectAttributes redirAttrs) {
        JeuneTireur jt = jeuneTireurService.getTypeJeuneTireurIfExistOrThrow404(id);
        jeuneTireurService.activate(jt);
        LOGGER.log(Level.FINE, "Le jeune tireur " + jt.getFullName() + " a été activé.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le jeune tireur " + jt.getFullName() + " a été activé."));
        return "redirect:/jeunestireurs/inactifs";
    }

    @GetMapping("/import")
    public String importJT(Model model) {

        return "tireurs/importJT";
    }

    @PostMapping("/import")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
                                   Model model,
                                   RedirectAttributes redirAttrs) {
        ArrayList<String> erreurs = new ArrayList<String>();
        ArrayList<ImportJTWrapper> listJT = new ArrayList<>();

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
            String ligne;
            int i = 0;

            while ((ligne = br.readLine()) != null) {
                if (i > 0) {
                    String[] data = ligne.split(";");
                    if (data.length == 18) {
                        try {
                            JeuneTireur lineJT = jeuneTireurService.convertCSVtoJT(data);
                            listJT.add(new ImportJTWrapper(lineJT));
                        } catch (ImportException ex) {
                            listJT.add(new ImportJTWrapper(ex.getErreurs()));
                        }
                    } else {
                        LOGGER.log(Level.SEVERE, TireurEnumeration.IMPORT_FILE_INVALID.getErreurforLog());
                        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.DANGER,
                                "La ligne " + i + " n'est pas conforme."));
                        return "redirect:/jeunestireurs/import";
                    }
                }
                i++;
            }
            model.addAttribute("jtToImport", new ImportJTWrapperListForm(listJT));
            return "tireurs/importJT";
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, TireurEnumeration.IMPORT_FILE_INVALID.getErreurforLog());
            redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.DANGER,
                    TireurEnumeration.IMPORT_FILE_INVALID.getErreurException()));
            return "redirect:/jeunestireurs/import";
        }
    }

    @PostMapping("/import/validate")
    public String validationImport(@ModelAttribute("jtToImport") ImportJTWrapperListForm importJTWrapperList,
                                   BindingResult bindingResult,
                                   Model model,
                                   RedirectAttributes redirAttrs) {

        Boolean flagAdded = false;
        for (ImportJTWrapper ijtw : importJTWrapperList.getImportJTList()) {
            if (ijtw.isToAdd()) {
                try {
                    jeuneTireurService.importJT(ijtw);
                    flagAdded = true;
                } catch (JeuneTireurException e) {
                    redirAttrs.addFlashAttribute("messages", FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER,
                            e.getErreurs()));
                    return "redirect:/jeunestireurs/import";
                }
            }
        }
        if (flagAdded) {
            redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                    "Tireurs importés avec succès."));
            return "redirect:/jeunestireurs";
        } else {
            redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.WARNING,
                    "Rien à importer."));
            return "redirect:/jeunestireurs/import";
        }
    }

/*    @PostMapping("/{idCours}/resultats")
    public String getResultats(@PathVariable("idSaison") long idSaison,
                               @PathVariable("idCours") long idCours,
                               @ModelAttribute("participants") ParticipantListForm participants,
                               BindingResult bindingResult,
                               Model model) {

        Saison saison = saisonService.getSaisonIfExistOrThrow404(idSaison);
        Cours cours = coursService.getCoursIfExistOrThrow404(idCours);

        model.addAttribute("saison", saison);
        model.addAttribute("cours", cours);

        Boolean noErrorFlag = true;
        for (Participant taintedP : participants.getParticipantList()) {
            Optional<Participant> p = coursService.findParticipant(taintedP.getId());
            if (p.isPresent()) {
                try {
                    coursService.setResultats(cours,
                            p.get(),
                            taintedP.getTotalObtenu(),
                            taintedP.getNbMunitionsSupplementaires());

                } catch (ParticipantException ex) {
                    model.addAttribute("messages", FlashMessageGenerator.generateMultipleMessages(
                            FlashMessage.Level.DANGER, ex.getErreurs())
                    );
                    noErrorFlag = false;
                }
            } else {
                LOGGER.log(Level.WARNING, String.format("Aucun participant d'id %d, les résultats seront ignorés."));
            }
        }
        if (noErrorFlag) {
            model.addAttribute("message", FlashMessageGenerator.generateSingleMessage(
                    FlashMessage.Level.SUCCESS, CoursEnumeration.RESULTAT_SUCCESS.getErreurException())
            );
        }
        model.addAttribute("participants", getParticipantListJTOnly(cours));
        return "cours/gestionResultats";
    }*/

}