package ch.steversoix.jtv.desktop.tireur.controller;

import ch.steversoix.jtv.desktop.core.CoreController;
import ch.steversoix.jtv.desktop.outils.FlashMessage;
import ch.steversoix.jtv.desktop.outils.FlashMessageGenerator;
import ch.steversoix.jtv.desktop.tireur.exceptions.MoniteurException;
import ch.steversoix.jtv.desktop.tireur.exceptions.PasswordNotTheSameException;
import ch.steversoix.jtv.desktop.tireur.model.JeuneTireur;
import ch.steversoix.jtv.desktop.tireur.model.Moniteur;
import ch.steversoix.jtv.desktop.tireur.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import ch.steversoix.jtv.desktop.tireur.model.TireurRepository;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.swing.text.html.Option;
import javax.validation.Valid;

/**
 * @author Jonathan Blum
 */
@Controller
@RequestMapping(path = "/tireurs")
@Order(2)
public class TireursController implements ApplicationRunner {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Value("${jtv.testdata}")
    private Boolean testdata;

    @Value("${jtv.admin.password}")
    private String adminPwd;

    @Autowired
    private TireurService tireurService;
    @Autowired
    private JeuneTireurService jeuneTireurService;
    @Autowired
    private MoniteurService moniteurService;

    @ModelAttribute("user")
    public SecurityUser getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (SecurityUser) auth.getPrincipal();
    }

    @GetMapping("/{id}/updateDroit")
    public String changerMotPasseForm(@PathVariable("id") long id, Model model) {
        model.addAttribute("nPass", new NewPassword());
        return "changerMotPasse";
    }

    @PostMapping("/{id}/updateDroit")
    public String changerMotPasse(@PathVariable("id") long id, @ModelAttribute("nPass") NewPassword newPassword, Model model, RedirectAttributes redirAttrs) {
        SecurityUser su = getUser();
        // si une personne essaie de modifier le mot de passe de quelqu'un d'autre
        if (!su.getId().equals(id)) {
            LOGGER.log(Level.SEVERE, "UpdateDroit - TireurException : " + TireurEnumeration.MDP_AUTRE_TIREUR);
            redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.DANGER,
                    TireurEnumeration.MDP_AUTRE_TIREUR.getErreurException()));
            return "redirect:/";
        }
        //si nouveau mot de passe est correcte
        if (!newPassword.checkPassword()) {
            LOGGER.log(Level.SEVERE, "UpdateDroit - TireurException : " + TireurEnumeration.MDP_BASE);
            redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.DANGER,
                    TireurEnumeration.MDP_BASE.getErreurException()));
            return "redirect:/tireurs/" + id + "/updateDroit";
        }
        if (su.getLevel() == Tireur.LEVEL_USER) {
            JeuneTireur jt = jeuneTireurService.findJeuneTireurById(id);
            try {
                jeuneTireurService.updateDroit(jt, newPassword);
            } catch (PasswordNotTheSameException e) {
                return reloadUpdateDroitForException(e, redirAttrs, id);
            }
        } else {
            Moniteur mo = moniteurService.findMoniteurById(id);
            try {
                moniteurService.updateDroit(mo, newPassword);
            } catch (PasswordNotTheSameException e) {
                return reloadUpdateDroitForException(e, redirAttrs, id);
            }
        }
        return "redirect:/";
    }

    public String reloadUpdateDroitForException(PasswordNotTheSameException ex, RedirectAttributes redirAttrs, long id) {
        LOGGER.log(Level.SEVERE, "UpdateDroit - TireurException : " + ex.getMessage());
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.DANGER,
                ex.getMessage()));
        return "redirect:/tireurs/" + id + "/updateDroit";
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        initAdminAccount();
        if (applicationArguments.containsOption("jtv.testdata") || testdata) {

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date dateNaiss1 = dateFormat.parse("15/01/1990");
            Date dateNaiss2 = dateFormat.parse("27/01/1991");
            Date dateNaiss3 = dateFormat.parse("27/01/1992");
            tireurService.save(new Moniteur("Granger", "Hermione", "Madame", dateNaiss1, "poudlard 578", "1708", "Poudlard", "hermione.granger@gmail.com", "0291241285", "0713744891", "hg", "Hello123", Tireur.LEVEL_ADMIN));
            tireurService.save(new Moniteur("Pablo", "Escobar", "Monsieur", dateNaiss2, "chemin del castro", "1214", "Mexico", "pablo@escobar.com", "0221457896", "0784569874", "directeur", "Hello123", Tireur.LEVEL_ADMIN));
            tireurService.save(new Moniteur("Stark", "Eddard", "Monsieur", dateNaiss3, "route du chateau 1", "1215", "Winterfell", "ned@stark.com", "0221234578", "0751236998", "moniteur", "Hello123", Tireur.LEVEL_MANAGER));
            tireurService.save(new Moniteur("Torvald", "Linus", "Monsieur", dateNaiss1, "chemin de l'open source 8", "2005", "Free", "linus@torvald.com", "0221234752", "0751166998", "linus", "Hello123", Tireur.LEVEL_MANAGER));

            tireurService.save(new JeuneTireur("Potter", "Harry", "Monsieur", dateNaiss2, "poudlard 50", "1708", "Poudlard", "harry.potter@gmail.com", "0291247485", "0713254891", "hp", "Hello123", Tireur.LEVEL_USER, "Potter", "Lily", "rue du chauderon 20", "7841", "London", "0712478495"));
            tireurService.save(new JeuneTireur("Weasley", "Ron", "Monsieur", dateNaiss3, "poudlard 50", "1708", "Poudlard", "ron.weasley@gmail.com", "0291247485", "0711254891", "rw", "Hello123", Tireur.LEVEL_USER, "Weasley", "Madame", "rue du chat 15", "7845", "Cambridge", "0719878495"));
            tireurService.save(new JeuneTireur("Weasley", "Jorge", "Monsieur", dateNaiss1, "poudlard 50", "1708", "Poudlard", "jorge.weasley@gmail.com", "0291247485", "0711254891", "gw", "Hello123", Tireur.LEVEL_USER, "Weasley", "Madame", "rue du chat 15", "7845", "Cambridge", "0719878495"));
            tireurService.save(new JeuneTireur("Weasley", "Ginny", "Monsieur", dateNaiss2, "poudlard 50", "1708", "Poudlard", "ginny.weasley@gmail.com", "0291247485", "0711254891", "giw", "Hello123", Tireur.LEVEL_USER, "Weasley", "Madame", "rue du chat 15", "7845", "Cambridge", "0719878495"));
            tireurService.save(new JeuneTireur("Weasley", "Fred", "Monsieur", dateNaiss3, "poudlard 50", "1708", "Poudlard", "fred.weasley@gmail.com", "0291247485", "0711254891", "fw", "Hello123", Tireur.LEVEL_USER, "Weasley", "Madame", "rue du chat 15", "7845", "Cambridge", "0719878495"));
        }
    }

    private void initAdminAccount() {
        Optional<Tireur> admin = tireurService.findTireurById(new Long(1));

        try {
            if (admin.isPresent()) {
                Moniteur adminMon = (Moniteur) admin.get();
                adminMon.setUsername("admin");
                adminMon.setPassword(adminPwd);
                adminMon.setActif(true);
                adminMon.setLevel(Tireur.LEVEL_ADMIN);
                moniteurService.update(adminMon);
                moniteurService.updateDroit(adminMon, new NewPassword(adminPwd, adminPwd, adminPwd));
            } else {
                LOGGER.log(Level.INFO, "Admin non-trouvé, création de l'admin.");
                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date dateNaissance = dateFormat.parse("01/01/1900");
                Moniteur newAdmin = new Moniteur("Admin", "Super", "Indefini", dateNaissance, "In the cloud", "1234", "heaven", "admin@example.com", "0221234567", "0791234567", "admin", adminPwd, Tireur.LEVEL_ADMIN);
                newAdmin.setActif(true);
                moniteurService.save(newAdmin);
            }
        } catch (ParseException | MoniteurException | PasswordNotTheSameException ex) {
            LOGGER.log(Level.SEVERE, "Impossible de créer le compte admin.");
        }
    }
}
