package ch.steversoix.jtv.desktop.tireur.services;

import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component(value="userDetailService")
public class CustomUserDetailsService implements UserDetailsService {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Autowired
    private TireurService tireurService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LOGGER.log(Level.INFO,"Authenticating "+ username);
        Optional<Tireur> tireur = tireurService.findTireurByUsername(username);
        if (!tireur.isPresent()) {
            LOGGER.log(Level.WARNING,"Username not found");
            throw new UsernameNotFoundException("UserName " + username + " not found");
        }

        return new SecurityUser(tireur.get());
    }

}
