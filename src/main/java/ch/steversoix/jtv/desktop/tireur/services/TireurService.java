package ch.steversoix.jtv.desktop.tireur.services;

import ch.steversoix.jtv.desktop.saison.model.Saison;
import ch.steversoix.jtv.desktop.tireur.model.Tireur;

import java.util.List;
import java.util.Optional;

public interface TireurService {
    List<Tireur> findAll();

    Tireur update(Tireur tireur);
    int selectSaison(Saison saison, Tireur tireur);

    Tireur save(Tireur t);

    void desactivate(Tireur t);

    Optional<Tireur> findTireurByUsername(String username);

    List<Tireur> findTireursByActif(boolean actif);

    Optional<Tireur> findTireurById(long id);
}
