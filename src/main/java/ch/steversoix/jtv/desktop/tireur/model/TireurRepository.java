package ch.steversoix.jtv.desktop.tireur.model;

import ch.steversoix.jtv.desktop.saison.model.Saison;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface TireurRepository extends JpaRepository<Tireur, Long> {

    @Query(value="select t from Tireur t WHERE t.actif = ?1")
    List<Tireur> findTireursByActif(boolean actif);

    @Query("select t from Tireur t where t.username =?1 and t.password = ?2")
    Tireur login(String username, String password);

    @Query("select t from Tireur t where t.id =?1")
    Tireur findTireursById(long id);

    @Transactional
    @Modifying
    @Query("update Tireur tir set tir.lastEditedSaison = ?1 where tir.id = ?2")
    int updateTireurSaisonSelect(Saison s, long id);

    @Query("select m from Moniteur m where m.id =?1")
    Optional<Moniteur>  findMoniteurById(long id);

    @Query("select jt from JeuneTireur jt where jt.id =?1")
    Optional<JeuneTireur> findJeuneTireurById(long id);


    Optional<Tireur> findByUsername(String username);

    @Query(value="select jt from JeuneTireur jt WHERE jt.actif = ?1")
    List<JeuneTireur> findJeunesTireursByActif(boolean actif);

    @Query(value="select mo from Moniteur mo WHERE mo.actif = ?1")
    List<Moniteur> findMoniteursByActif(boolean actif);
}