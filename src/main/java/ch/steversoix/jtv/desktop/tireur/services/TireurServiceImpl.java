package ch.steversoix.jtv.desktop.tireur.services;


import ch.steversoix.jtv.desktop.outils.FilterTools;
import ch.steversoix.jtv.desktop.saison.model.Saison;
import ch.steversoix.jtv.desktop.tireur.controller.NewPassword;
import ch.steversoix.jtv.desktop.tireur.exceptions.InvalidUsernameException;
import ch.steversoix.jtv.desktop.tireur.exceptions.NewPasswordException;
import ch.steversoix.jtv.desktop.tireur.exceptions.PasswordNotTheSameException;
import ch.steversoix.jtv.desktop.tireur.exceptions.UsernameAlreadyExistsException;
import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import ch.steversoix.jtv.desktop.tireur.model.TireurRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.logging.Filter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class TireurServiceImpl implements TireurService {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private TireurRepository tireurRepository;
    private PasswordEncoder passwordEncoder;

    public TireurServiceImpl(TireurRepository repository, PasswordEncoder passwordEncoder) {
        this.tireurRepository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<Tireur> findAll() {
        return tireurRepository.findAll();
    }

    @Override
    public Optional<Tireur> findTireurById(long id) {
        return Optional.ofNullable(tireurRepository.findTireursById(id));
    }

    @Override
    public Tireur update(Tireur tireur) {
        // TODO : Contrôles
        return save(tireur);
    }

    @Override
    public int selectSaison(Saison saison, Tireur tireur) {
        if (tireur.getId() != -1)
            return tireurRepository.updateTireurSaisonSelect(saison, tireur.getId());
        else return -1;
    }

    @Override
    public Optional<Tireur> findTireurByUsername(String username) {
        return tireurRepository.findByUsername(username);
    }

    @Override
    public Tireur save(Tireur t) {
        LOGGER.log(Level.INFO, "Save Tireur : " + t.getUsername());
        t.setPassword(passwordEncoder.encode(t.getPassword()));
        return tireurRepository.save(t);
    }

    public void desactivate(Tireur t) {
        t.setActif(false);
        tireurRepository.save(t);
    }

    @Override
    public List<Tireur> findTireursByActif(boolean actif) {
        return tireurRepository.findTireursByActif(actif);
    }

    public ArrayList<String> rulesAddUpdate(Tireur t) {
        ArrayList<String> erreurs = new ArrayList<String>();

        // regle model Tireur
        if(t.getTitre()==null || t.getTitre().trim().length() < 2 || t.getTitre().trim().length() > 50){
            LOGGER.log(Level.SEVERE, TireurEnumeration.MODEL_TITRE_INVALIDE.getErreurforLog());
            erreurs.add(TireurEnumeration.MODEL_TITRE_INVALIDE.getErreurException());
        }

        /* Nom prénom NON vide */
        try {
            if (!FilterTools.isOnlyChar(t.getPrenom()) || !FilterTools.isOnlyChar(t.getNom())) {
                LOGGER.log(Level.SEVERE, TireurEnumeration.NOM_PRENOM_VIDE.getErreurforLog());
                erreurs.add(TireurEnumeration.NOM_PRENOM_VIDE.getErreurException());
            }
            // regle model Tireur
            if(t.getPrenom().trim().length() < 2 || t.getNom().trim().length() < 2 || t.getPrenom().trim().length() > 50 || t.getNom().trim().length() > 50){
                LOGGER.log(Level.SEVERE, TireurEnumeration.MODEL_NOM_PRENOM_INVALIDE.getErreurforLog());
                erreurs.add(TireurEnumeration.MODEL_NOM_PRENOM_INVALIDE.getErreurException());
            }
        } catch (NullPointerException ex) {
            LOGGER.log(Level.SEVERE, TireurEnumeration.NOM_PRENOM_VIDE.getErreurforLog());
            erreurs.add(TireurEnumeration.NOM_PRENOM_VIDE.getErreurException());
        }

        /* Adresse NON vide */
        try {
            if (t.getAdresse().trim().equals("") || t.getNpa().trim().equals("") || t.getLocalite().trim().equals("")) {
                LOGGER.log(Level.SEVERE, TireurEnumeration.ADRESSE_VIDE.getErreurforLog());
                erreurs.add(TireurEnumeration.ADRESSE_VIDE.getErreurException());
            }
            // regle model Tireur
            if(t.getAdresse().trim().length() < 2 || t.getLocalite().trim().length() < 2 || t.getAdresse().trim().length() > 50 || t.getLocalite().trim().length() > 50){
                LOGGER.log(Level.SEVERE, TireurEnumeration.MODEL_ADRESSE_LOCALITE_INVALIDE.getErreurforLog());
                erreurs.add(TireurEnumeration.MODEL_ADRESSE_LOCALITE_INVALIDE.getErreurException());
            }
            // regle model Tireur
            if(t.getNpa().trim().length() < 4 || t.getNpa().trim().length() > 8){
                LOGGER.log(Level.SEVERE, TireurEnumeration.MODEL_NPA_INVALIDE.getErreurforLog());
                erreurs.add(TireurEnumeration.MODEL_NPA_INVALIDE.getErreurException());
            }
        } catch (NullPointerException ex) {
            LOGGER.log(Level.SEVERE, TireurEnumeration.ADRESSE_VIDE.getErreurforLog());
            erreurs.add(TireurEnumeration.ADRESSE_VIDE.getErreurException());
        }

        /* Contact NON vide */
        try {
            if (!FilterTools.isTelephoneNumber(t.getNumNatel()) || !FilterTools.isTelephoneNumber(t.getNumTelephone())) {
                LOGGER.log(Level.SEVERE, TireurEnumeration.CONTACT_VIDE.getErreurforLog());
                erreurs.add(TireurEnumeration.CONTACT_VIDE.getErreurException());
            }
        } catch (NullPointerException ex) {
            LOGGER.log(Level.SEVERE, TireurEnumeration.CONTACT_VIDE.getErreurforLog());
            erreurs.add(TireurEnumeration.CONTACT_VIDE.getErreurException());
        }

        /* controle de l'email */
        try {
            if (!FilterTools.isEmailAdress(t.getEmail().trim())) {
                LOGGER.log(Level.SEVERE, TireurEnumeration.EMAIL_INVALIDE.getErreurforLog());
                erreurs.add(TireurEnumeration.EMAIL_INVALIDE.getErreurException());
            }
            // regle model Tireur
            if(t.getEmail().trim().length() < 2 || t.getEmail().trim().length() > 50){
                LOGGER.log(Level.SEVERE, TireurEnumeration.MODEL_EMAIL_INVALIDE.getErreurforLog());
                erreurs.add(TireurEnumeration.MODEL_EMAIL_INVALIDE.getErreurException());
            }
        } catch (NullPointerException ex) {
            LOGGER.log(Level.SEVERE, TireurEnumeration.EMAIL_INVALIDE.getErreurforLog());
            erreurs.add(TireurEnumeration.EMAIL_INVALIDE.getErreurException());
        }

        /* Date naissance NON null */
        if (t.getDateNaissance() == null) {
            LOGGER.log(Level.SEVERE, TireurEnumeration.DATE_NAISSANCE_NULL.getErreurforLog());
            erreurs.add(TireurEnumeration.DATE_NAISSANCE_NULL.getErreurException());
        } else {
            /* RG 16 - Age tireur */
            Calendar dateJour = Calendar.getInstance();
            dateJour.setTime(new Date());
            Calendar dateNaiss = Calendar.getInstance();
            dateNaiss.setTime(t.getDateNaissance());
            int age = dateJour.get(Calendar.YEAR) - dateNaiss.get(Calendar.YEAR);
            if (age < Tireur.MIN_AGE) {
                LOGGER.log(Level.SEVERE, TireurEnumeration.AGE_MIN.getErreurforLog());
                erreurs.add(TireurEnumeration.AGE_MIN.getErreurException());
            }
        }
        try {
            if (t.getUsername().trim().equals("") || t.getPassword().trim().equals("")) {
                LOGGER.log(Level.SEVERE, TireurEnumeration.DROIT_NULL.getErreurforLog());
                erreurs.add(TireurEnumeration.DROIT_NULL.getErreurException());
            }
            // regle model Tireur
            if(t.getUsername().trim().length() < 2 || t.getUsername().trim().length() > 50){
                LOGGER.log(Level.SEVERE, TireurEnumeration.MODEL_USERNAME_INVALIDE.getErreurforLog());
                erreurs.add(TireurEnumeration.MODEL_USERNAME_INVALIDE.getErreurException());
            }

        } catch (NullPointerException ex) {
            LOGGER.log(Level.SEVERE, TireurEnumeration.DROIT_NULL.getErreurforLog());
            erreurs.add(TireurEnumeration.DROIT_NULL.getErreurException());
        }

        /* GD 03 */
        Optional<Tireur> opTir = findTireurByUsername(t.getUsername());
        if (opTir != null && opTir.isPresent() && opTir.get().getId() != t.getId()) {
            LOGGER.log(Level.SEVERE, TireurEnumeration.USER_EXIST.getErreurforLog());
            erreurs.add(TireurEnumeration.USER_EXIST.getErreurException());
        }

        /* GD 02 */
        String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}";
        if (t.getPassword()==null || !t.getPassword().matches(pattern)) {
            LOGGER.log(Level.SEVERE, TireurEnumeration.MDP_BASE.getErreurforLog());
            erreurs.add(TireurEnumeration.MDP_BASE.getErreurException());
        }

        return erreurs;
    }
}
