package ch.steversoix.jtv.desktop.tireur.services;

import ch.steversoix.jtv.desktop.tireur.model.Tireur;

public enum TireurEnumeration {
    NOM_PRENOM_VIDE("Le nom et le prenom ne peuvent pas être vides.","nom || prenom = ''"),
    CONTACT_VIDE("Veuillez renseignez un N° de contact.","tel || natel || email = ''"),
    ADRESSE_VIDE("Veuillez renseignez les informations relatives au domicile du tireur.","adresse || npa || localite = ''"),
    DATE_NAISSANCE_NULL("Veuillez renseignez la date de naissance du tireur.","dateNaissance == null"),
    EMAIL_INVALIDE("L'adresse email que vous avez entré n'est pas une adresse email valide.","email contient pas @ .<text>"),
    DROIT_NULL("Le nom d'utilisateur et le mot de passe ne peuvent pas être vides.","username || password == null"),
    AGE_MIN("L'âge du tireur ne peut pas être inférieur à "+Tireur.MIN_AGE+" ans.","age < "+ Tireur.MIN_AGE),
    MDP_BASE("Le mot de passe doit avoir 8 caractères minimum, un chiffre, une majuscule et une minuscule au minimum.",
            "Regle de complexité MDP non respectée."),
    MDP_AUTRE_TIREUR("Vous ne pouvez pas modifier le mot de passe d'un autre tireur.","userCo != userChangerMotPasse"),
    TIREUR_NOT_FOUND("Vous n'avez pas pu changer de saison car le système n'a pas réussi à récupérer le tireur vous concernant.","Tireur Not Found"),
    USER_EXIST("Ce nom d'utilisateur existe déjà.","username existe déjà"),
    JT_NOT_FOUND("Le jeune tireur auquel vous tentez d'accéder n'existe pas.", "JT Not Found"),
    MON_NOT_FOUND("Le moniteur auquel vous tentez d'accéder n'existe pas.", "Moniteur Not Found"),
    IMPORT_FIELD_INVALID("Un des champs est invalide.", "champ invalide"),
    IMPORT_FILE_INVALID("Le fichier téléchargé est invalide.", "Fichier importé invalide"),
    MODEL_TITRE_INVALIDE("Le titre doit contenir au moins 2 caractères au maximum 50.","titre.length < 2 || titre.length > 50"),
    MODEL_NOM_PRENOM_INVALIDE("Le nom et le prénom doivent contenir au moins 2 caractères au maximum 50.","nom|Prenom.length < 2 || nom|Prenom.length > 50"),
    MODEL_ADRESSE_LOCALITE_INVALIDE("L'adresse et la localité doivent contenir au moins 2 caractères au maximum 50.","adresse|localite.length < 2 || adresse|localite.length > 50"),
    MODEL_NPA_INVALIDE("Le NPA doit contenir au moins 4 caractères au maximum 8.","npa.length < 4 || npa.length > 8"),
    MODEL_EMAIL_INVALIDE("L'email doit contenir au moins 2 caractères au maximum 50.","email.length < 2 || email.length > 50"),
    MODEL_USERNAME_INVALIDE("Le nom d'utilsiateur doit contenir au moins 2 caractères au maximum 50.","username.length < 2 || username.length > 50"),
    MODEL_NOM_PRENOM_PARENT_INVALIDE("Le nom et le prénom du parent doivent contenir au moins 2 caractères au maximum 50.","nomParent|PrenomParent.length < 2 || nomParent|PrenomParent.length > 50"),
    MODEL_ADRESSE_LOCALITE_PARENT_INVALIDE("L'adresse et la localité du parent doivent contenir au moins 2 caractères au maximum 50.","adresseParent|localiteParent.length < 2 || adresseParent|localiteParent.length > 50"),
    MODEL_NPA_PARENT_INVALIDE("Le NPA du parent doit contenir au moins 4 caractères au maximum 8.","npaParent.length < 4 || npaParent.length > 8"),
    ;

    private String erreurException;
    private String erreurForLog;

    TireurEnumeration(String erreurException, String erreurForLog) {
        this.erreurException = erreurException;
        this.erreurForLog = erreurForLog;
    }

    public String getErreurException() {
        return erreurException;
    }
    public String getErreurforLog() {
        return erreurForLog;
    }
}