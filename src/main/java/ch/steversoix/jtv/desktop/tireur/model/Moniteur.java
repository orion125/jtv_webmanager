package ch.steversoix.jtv.desktop.tireur.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "jtv_moniteur")
public class Moniteur extends Tireur{

    /* Constructor */
    public Moniteur() {
        super();
    }

    public Moniteur(String nom, String prenom, String titre, Date dateNaissance, String adresse, String npa, String localite, String email, String numTelephone, String numNatel, String username, String password, int level) {
        super(nom, prenom, titre, dateNaissance, adresse, npa, localite, email, numTelephone, numNatel, username, password, level);
    }
}
