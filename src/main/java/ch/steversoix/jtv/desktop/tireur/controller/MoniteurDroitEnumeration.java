package ch.steversoix.jtv.desktop.tireur.controller;

import ch.steversoix.jtv.desktop.tireur.model.Tireur;

public enum MoniteurDroitEnumeration {
    Moniteur("Moniteur", Tireur.LEVEL_MANAGER),
    Directeur("Directeur",Tireur.LEVEL_ADMIN);

    private String droit;
    private int code;

    MoniteurDroitEnumeration(String droit, int code) {
        this.droit = droit;
        this.code = code;
    }

    public String getDroit() {
        return droit;
    }

    public void setDroit(String droit) {
        this.droit = droit;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
