/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.steversoix.jtv.desktop;


import ch.steversoix.jtv.desktop.core.*;
import ch.steversoix.jtv.desktop.saison.model.Saison;
import ch.steversoix.jtv.desktop.saison.service.SaisonService;
import ch.steversoix.jtv.desktop.tireur.services.SecurityUser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
@Order(1)
public class FrontController implements ApplicationRunner {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Value("${jtv.testdata}")
    private Boolean testdata;

    private String profile;

    private String loglevel;

    @ModelAttribute("user")
    public SecurityUser getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (SecurityUser) auth.getPrincipal();
    }

    @GetMapping("/")
    public String index(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        SecurityUser su = (SecurityUser) auth.getPrincipal();
        if(Optional.ofNullable(su.getLastEditedSaison()).isPresent())
            return "redirect:/saisons/"+su.getLastEditedSaison().getId();
        else
            return "redirect:/saisons";
    }

    public void Do() {
        try {
            CustomLogger.setup();
            LOGGER.setLevel(Level.FINEST);
        } catch (IOException ex) {
            throw new RuntimeException("Problems with creating the log files");
        }
        new ConfigurationController().Do();

        //StringsManager.getInstance().loadFromYaml("/strings/fr.yml");

        ConfigurationManager cm = ConfigurationManager.getInstance();
        String appname = cm.getProperty("application.name");
        String version = cm.getProperty("application.version");
        profile = cm.getProperty("build.profile.id");
        loglevel = cm.getProperty("jtv.log.level");
        System.out.println(appname);
        System.out.println(String.format("Version : %s", version));
        System.out.println(String.format("Current mode: %s", profile));
        LOGGER.log(Level.INFO, String.format("%s - Version %s - mode %s", appname, version, profile));
        LOGGER.log(Level.INFO, String.format("LogLevel %s", loglevel));
        LOGGER.setLevel(Level.parse(loglevel));
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if(args.containsOption("jtv.testdata") || testdata)
            LOGGER.log(Level.INFO, String.format("Données de test activées"));
    }
}
