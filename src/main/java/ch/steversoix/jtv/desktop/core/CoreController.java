/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.steversoix.jtv.desktop.core;

/**
 *
 * @author Jonathan Blum
 * @version 0.2
 */
public abstract class CoreController {
    
    /**
     * Execute Controller's actions
     */
    abstract public void Do();

}