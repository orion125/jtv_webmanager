package ch.steversoix.jtv.desktop.core;

import java.io.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by jonathan on 06.05.17.
 */
public class ConfigurationManager {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private Properties properties = new Properties();

    private static class SingletonHolder     {
        private final static ConfigurationManager INSTANCE = new ConfigurationManager();
    }

    public static ConfigurationManager getInstance() {
        return ConfigurationManager.SingletonHolder.INSTANCE;
    }

    private ConfigurationManager() { }


    Properties loadConfigurationFile(InputStream file) {
        Properties p = new Properties();

        try {
            p.load(file);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Cannot load configuration file");
        }

        return p;
    }
    Properties loadConfigurationFile(String path) {
        Properties p = new Properties();
        InputStream input = null;
        File f = new File(path);
        if(f.isFile()) {
            try {
                p = loadConfigurationFile(input);
                input = new FileInputStream(f);
            } catch (FileNotFoundException e) {
                LOGGER.log(Level.WARNING, String.format("Cannot read configuration file from %s", f.getAbsolutePath()));
            }
        }
        return p;
    }

    void appendProperties(Properties prop) {
        this.properties.putAll(prop);
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    public void setProperty(String key, String value) {
        properties.setProperty(key, value);
    }
}
