package ch.steversoix.jtv.desktop.core;


import ch.steversoix.jtv.desktop.tireur.services.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.annotation.Resource;
import java.util.logging.Logger;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Resource(name = "userDetailService")
    private CustomUserDetailsService userDetailService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").access("hasRole('TIREUR')")
                .antMatchers("/cibles").access("hasRole('TIREUR')")
                .antMatchers("/annees").access("hasRole('MONITEUR')")
                .antMatchers("/postes").access("hasRole('MONITEUR')")
                .antMatchers("/postes/inactifs").access("hasRole('MONITEUR')")
                .antMatchers("/postes/add").access("hasRole('MONITEUR')")
                .antMatchers("/postes/{idPoste}/edit").access("hasRole('MONITEUR')")
                .antMatchers("/postes/{idPoste}/update").access("hasRole('MONITEUR')")
                .antMatchers("/postes/{idPoste}/activate").access("hasRole('MONITEUR')")
                .antMatchers("/postes/{idPoste}/desactivate").access("hasRole('MONITEUR')")
                .antMatchers("/tireurs/{idMoniteur}/updateDroit").access("hasRole('TIREUR')")
                .antMatchers("/moniteurs").access("hasRole('MONITEUR')")
                .antMatchers("/moniteurs/add").access("hasRole('MONITEUR')")
                .antMatchers("/moniteurs/{idMoniteur}/edit").access("hasRole('MONITEUR')")
                .antMatchers("/moniteurs/{idMoniteur}/details").access("hasRole('MONITEUR')")
                .antMatchers("/moniteurs/{idMoniteur}/update").access("hasRole('MONITEUR')")
                .antMatchers("/moniteurs/{idMoniteur}/updateDroit").access("hasRole('MONITEUR')")
                .antMatchers("/jeunestireurs").access("hasRole('MONITEUR')")
                .antMatchers("/jeunestireurs/add").access("hasRole('MONITEUR')")
                .antMatchers("/jeunestireurs/{idMoniteur}/edit").access("hasRole('MONITEUR')")
                .antMatchers("/jeunestireurs/{idMoniteur}/details").access("hasRole('MONITEUR')")
                .antMatchers("/jeunestireurs/{idMoniteur}/update").access("hasRole('MONITEUR')")
                .antMatchers("/jeunestireurs/{idMoniteur}/updateDroit").access("hasRole('MONITEUR')")
                .antMatchers("/armes").access("hasRole('MONITEUR')")
                .antMatchers("/armes/add").access("hasRole('MONITEUR')")
                .antMatchers("/armes/{idArme}/edit").access("hasRole('MONITEUR')")
                .antMatchers("/armes/{idArme}/update").access("hasRole('MONITEUR')")
                .antMatchers("/typesarmes").access("hasRole('MONITEUR')")
                .antMatchers("/typesarmes/add").access("hasRole('MONITEUR')")
                .antMatchers("/typesarmes/{idTypeArme}/edit").access("hasRole('MONITEUR')")
                .antMatchers("/typesarmes/{idTypeArme}/update").access("hasRole('MONITEUR')")
                .antMatchers("/saisons").access("hasRole('TIREUR')")
                .antMatchers("/saisons/{id}").access("hasRole('TIREUR')")
                .antMatchers("/saisons/{id}/change").access("hasRole('TIREUR')")
                .antMatchers("/saisons/{id}/attributionarme").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/statistique").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/attributionarme/attribue").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/attributionarme/enlever/{IdInscrit}").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/cours").access("hasRole('TIREUR')")
                .antMatchers("/saisons/{id}/cours/{idCours}").access("hasRole('TIREUR')")
                .antMatchers("/saisons/{id}/cours/{idCours}/prevenirabsence").access("hasRole('TIREUR')")
                .antMatchers("/saisons/{id}/cours/{idCours}/edit").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/cours/{idCours}/update").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/cours/{idCours}/presences").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/cours/{idCours}/resultats").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/cours/{idCours}/horaire").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/cours/{idCours}/horaire/details").access("hasRole('TIREUR')")
                .antMatchers("/saisons/{id}/programmes").access("hasRole('TIREUR')")
                .antMatchers("/saisons/{id}/programmes/{idProgramme}/detail").access("hasRole('TIREUR')")
                .antMatchers("/saisons/{id}/programmes/add").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/programmes/{idProgramme}/edit").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/programmes/{idProgramme}/update").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/inscrits").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/inscrits/add").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/inscrits/delete").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/concours").access("hasRole('TIREUR')")
                .antMatchers("/saisons/{id}/concours/inactifs").access("hasRole('TIREUR')")
                .antMatchers("/saisons/{id}/concours/add").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/concours/{idConcours}").access("hasRole('TIREUR')")
                .antMatchers("/saisons/{id}/concours/{idCours}/edit").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/concours/{idCours}/update").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/concours/{idCours}/desactivate").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/concours/{idCours}/activate").access("hasRole('MONITEUR')")
                .antMatchers("/saisons/{id}/concours/{idCours}/edit").access("hasRole('MONITEUR')")

                .anyRequest().access("hasRole('DIRECTEUR')")
                .and()
            .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
            //.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"));
            .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
            .logoutSuccessUrl("/login");

    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailService).passwordEncoder(passwordEncoder());
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**", "/assets/**");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }
}
