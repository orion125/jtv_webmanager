package ch.steversoix.jtv.desktop.core;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by jonathan on 30.05.17.
 */
public class StringsManager {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    Map<String, String> strings = new HashMap<String,String>();

    private static class SingletonHolder     {
        private final static StringsManager INSTANCE = new StringsManager();
    }

    public static StringsManager getInstance() {
        return StringsManager.SingletonHolder.INSTANCE;
    }

    private StringsManager() { }

/*    public void loadFromYaml(String file) {
        try {
            Yaml yaml = new Yaml();
            InputStream is = getClass().getResourceAsStream(file);
            strings = (Map) yaml.load(IOUtils.toString(is, "UTF-8"));
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Could not load Strings file");
        }
    }*/

    public String getString(String key) {
        if(strings.containsKey(key))
            return strings.get(key);
        else
            return "";
    }

    public String getFormatedString(String key, Object... args) {
        return String.format(getString(key), args);
    }
}
