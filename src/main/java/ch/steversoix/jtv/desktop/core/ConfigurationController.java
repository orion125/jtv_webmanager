package ch.steversoix.jtv.desktop.core;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.Properties;

/**
 * Created by jonathan on 06.05.17.
 */
@Configuration
public class ConfigurationController extends WebMvcConfigurerAdapter {


    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
        //registry.addViewController("/greeting").setViewName("greeting");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/saison").setViewName("ajoutSaison");
        registry.addViewController("/typesarmes").setViewName("ajoutTypeArme");
        registry.addViewController("/armes").setViewName("ajoutArme");
        registry.addViewController("/saisons/{id}/cours").setViewName("ajoutCours");
        registry.addViewController("/saisons/{id}/programmes").setViewName("ajoutProgramme");
        registry.addViewController("/cibles").setViewName("ajoutCible");
        registry.addViewController("/saisons/{id}/concours").setViewName("ajoutConcours");
    }

    public void Do() {
        ConfigurationManager cm = ConfigurationManager.getInstance();

        Properties p1,p2,p3;

        p1 = cm.loadConfigurationFile(
               getClass().getClassLoader().getResourceAsStream("application.properties")
        );
        cm.appendProperties(p1);

        p2 = cm.loadConfigurationFile(
                String.format("%s/%s",System.getProperty("user.dir"),"config.properties")
        );
        cm.appendProperties(p2);

        p3 = cm.loadConfigurationFile(
               String.format("%s/%s",System.getProperty("user.home"),".jtv_desktop/config.properties")
        );
        cm.appendProperties(p3);

    }
}
