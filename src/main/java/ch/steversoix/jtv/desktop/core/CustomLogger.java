/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.steversoix.jtv.desktop.core;


import java.io.File;
import java.io.IOException;
import java.util.logging.*;

/**
 *
 * @author Jonathan Blum
 * @version 1.0.0
 */
public class CustomLogger {
    static private String STR_DIR_LOGS = "logs/";
    static private FileHandler fileTxt;
    static private SimpleFormatter formatterTxt;
    
    static private FileHandler fileHTML;
    static private Formatter formatterHTML;
    
    static public void setup() throws IOException {
        Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
        
        // suppress the logging output to the console
        /*
        Logger rootLogger = Logger.getLogger("");
        Handler[] handlers = rootLogger.getHandlers();
        if (handlers[0] instanceof ConsoleHandler) {
          rootLogger.removeHandler(handlers[0]);
        }*/
        
        boolean dirs = new File(STR_DIR_LOGS).mkdirs();
        
        logger.setLevel(Level.INFO);
        fileTxt = new FileHandler(STR_DIR_LOGS+"Logging.txt");
        fileHTML = new FileHandler(STR_DIR_LOGS+"Logging.html");
        
        // create a TXT formatter
        formatterTxt = new SimpleFormatter();
        fileTxt.setFormatter(formatterTxt);
        logger.addHandler(fileTxt);

        // create an HTML formatter
        formatterHTML = new HtmlLogsFormatter();
        fileHTML.setFormatter(formatterHTML);
        logger.addHandler(fileHTML);
    }
}
