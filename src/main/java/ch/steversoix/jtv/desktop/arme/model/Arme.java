package ch.steversoix.jtv.desktop.arme.model;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "jtv_arme")
public class Arme {
    public final static int MIN_NUMERO = 1000;
    public final static int MAX_NUMERO = 99999999;
    @Id
    @NotNull(message = "Le numéro ne peut pas être vide.")
    @Range(min=MIN_NUMERO,max=MAX_NUMERO, message = "Le numéro de l arme doit être entre "+MIN_NUMERO+" et "+MAX_NUMERO)
    @Column(name = "arm_id")
    private Long id;
    @ManyToOne
    @JoinColumn(name="arm_typ_id")
    private TypeArme type;
    @Column(name="arm_description")
    private String description;
    @Column(name = "arm_actif")
    private boolean actif;

    /* Constructeur */
    public Arme(){
        this.actif = true;
    }

    public Arme(Long id, TypeArme typeArme, String description, boolean actif){
        this.id = id;
        this.type = typeArme;
        this.description = description;
        this.actif = actif;
    }

    /* Getter & Setter */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeArme getType() {
        return type;
    }

    public void setType(TypeArme type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description.trim();
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    /* Comparaison */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Arme other = (Arme) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
}
