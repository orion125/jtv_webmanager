package ch.steversoix.jtv.desktop.arme.service;

public enum TypeArmeEnumeration {
    NOM_NULL("Le nom d'un type d'arme ne peut pas être vide.","nom != null"),
    EXISTE_DEJA("Ce type d'arme existe déjà.","type_arme existe déjà"),
    NO_TYPE_ARMES("Veuillez créer un type d'arme préalablement.","TypesArme.size()<= 0"),
    TYPE_ARME_INEXISTANT("Le type d'arme auquel vous tentez d'accéder n'existe pas.", "typeArme NOT FOUND");

    private String erreurException;
    private String erreurForLog;

    TypeArmeEnumeration(String erreurException, String erreurForLog) {
        this.erreurException = erreurException;
        this.erreurForLog = erreurForLog;
    }

    public String getErreurException() {
        return erreurException;
    }
    public String getErreurforLog() {
        return erreurForLog;
    }
}
