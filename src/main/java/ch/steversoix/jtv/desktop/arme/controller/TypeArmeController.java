package ch.steversoix.jtv.desktop.arme.controller;

import ch.steversoix.jtv.desktop.arme.exception.TypeArmeException;
import ch.steversoix.jtv.desktop.arme.model.TypeArme;
import ch.steversoix.jtv.desktop.arme.model.TypeArmeRepository;

import ch.steversoix.jtv.desktop.arme.service.TypeArmeService;
import ch.steversoix.jtv.desktop.outils.FlashMessage;
import ch.steversoix.jtv.desktop.outils.FlashMessageGenerator;
import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import ch.steversoix.jtv.desktop.tireur.services.SecurityUser;
import ch.steversoix.jtv.desktop.tireur.services.TireurService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
@RequestMapping(path="/typesarmes")
public class TypeArmeController extends WebMvcConfigurerAdapter {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Autowired
    private TypeArmeService typeArmeService;

    @ModelAttribute("user")
    public SecurityUser getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (SecurityUser) auth.getPrincipal();
    }


    /*
    * Les requêtes qui suivent concernent la navigation
    * */

    /* Affichage du formulaire qui liste les types armes */
    @GetMapping("")
    public String getTypesArmes(Model model){
        List<TypeArme> lst = typeArmeService.findTypesArmesByActif(true);
        model.addAttribute("typesArmes",lst);
        model.addAttribute("actif",true);
        return "gestionTypesArmes";
    }

    @GetMapping("/inactifs")
    public String getTypesArmesInactifs(Model model){
        List<TypeArme> lst = typeArmeService.findTypesArmesByActif(false);
        model.addAttribute("typesArmes",lst);
        model.addAttribute("actif",false);
        return "gestionTypesArmes";
    }

    /* Affichage du formulaire qui ajoute un type arme */
    @GetMapping("/add")
    public String addTypeArme(Model model){
        model.addAttribute("typearme",new TypeArme());
        return "ajoutTypeArme";
    }

    /* Affichage du formulaire qui modifie un type arme */
    @GetMapping("/{id}/edit")
    public String editTypeArme(@PathVariable("id") long id, Model model){
        TypeArme ta = typeArmeService.getTypeArmeIfExistOrThrow404(id);
        model.addAttribute("typearme",ta);
        /* RM04 - Type Arme */
        model.addAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.WARNING,
                "Attention ! Les modifications apportées à ce type d'arme vont se répercuter sur toutes les saisons qui l'utilisent."));

        return "ajoutTypeArme";
    }

    /*
    * Les requêtes qui suivent intéragissent avec la BDD
    * */

    /* Ajout un type arme dans la BDD */
    @PostMapping("")
    public String addTypeArmeDB(@Valid @ModelAttribute("typearme") TypeArme newTypeArme, BindingResult bindingResult, Model model, RedirectAttributes redirAttrs){
        if (bindingResult.hasErrors()) {
            return "ajoutTypeArme";
        }

        try {
            typeArmeService.save(newTypeArme);
        } catch (TypeArmeException e) {
            LOGGER.log(Level.SEVERE,e.getMessage());
            model.addAttribute("messages",FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER,e.getErreurs()));
            return "ajoutTypeArme";
        }

        LOGGER.log(Level.FINE,"Le type d'arme "+newTypeArme.getNom()+" a été ajouté.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le type d'arme "+newTypeArme.getNom()+" a été ajouté."));
        return "redirect:/typesarmes";
    }

    /* Modifie un type arme dans la BDD */
    @PostMapping("/{id}/update")
    public String updateTypeArmeDB(@Valid @ModelAttribute("typearme") TypeArme typeArme, BindingResult bindingResult, Model model, RedirectAttributes redirAttrs){
        if (bindingResult.hasErrors()) {
            return "ajoutTypeArme";
        }

        try {
            typeArmeService.update(typeArme);
        } catch (TypeArmeException e) {
            LOGGER.log(Level.SEVERE,e.getMessage());
            model.addAttribute("messages",FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER,e.getErreurs()));
            return "ajoutTypeArme";
        }

        LOGGER.log(Level.FINE,"Le type d'arme "+typeArme.getNom()+" a été modifié.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le type d'arme "+typeArme.getNom()+" a été modifié."));
        return "redirect:/typesarmes";
    }

    @GetMapping("/{id}/desactivate")
    public String desactivateTypeArmeDB(@PathVariable("id") long id, RedirectAttributes redirAttrs){
        TypeArme ta = typeArmeService.getTypeArmeIfExistOrThrow404(id);
        typeArmeService.desactivate(ta);
        LOGGER.log(Level.FINE,"Le type d'arme "+ta.getNom()+" a été désactivé.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le type d'arme "+ta.getNom()+" a été désactivé."));
        return "redirect:/typesarmes";
    }

    @GetMapping("/{id}/activate")
    public String activateTypeArmeDB(@PathVariable("id") long id, RedirectAttributes redirAttrs){
        TypeArme ta = typeArmeService.getTypeArmeIfExistOrThrow404(id);
        typeArmeService.activate(ta);
        LOGGER.log(Level.FINE,"Le type d'arme "+ta.getNom()+" a été activé.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le type d'arme "+ta.getNom()+" a été activé."));
        return "redirect:/typesarmes/inactifs";
    }

    /*
    * Méthodes internes
    * */

}
