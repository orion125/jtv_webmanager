package ch.steversoix.jtv.desktop.arme.model;

import ch.steversoix.jtv.desktop.saison.model.Saison;

import javax.persistence.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Set;

@Entity
@Table(name = "jtv_typeArme")
public class TypeArme {
    @Id
    @GeneratedValue
    @Column(name = "typ_id")
    private Long id;
    @NotNull
    @Size(min=2, max=50)
    @Column(name = "typ_nom")
    private String nom;
    @Column(name = "typ_actif")
    private boolean actif;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "type")
    private Set<Saison> saisons;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "type")
    private Set<Arme> armes;

    /* Constructeur */
    public TypeArme(){
        this(null);
    };
    public TypeArme(String nom) {
        this.nom = nom;
        this.actif = true;
    }

    /* Getter & Setter */
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) { this.nom = nom; }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TypeArme other = (TypeArme) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public boolean equals(int id) {
        return this.id == id;
    }

    @Override
    public String toString() {
        return nom;
    }

}
