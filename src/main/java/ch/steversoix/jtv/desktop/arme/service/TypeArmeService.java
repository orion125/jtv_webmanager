package ch.steversoix.jtv.desktop.arme.service;

import ch.steversoix.jtv.desktop.arme.exception.TypeArmeException;
import ch.steversoix.jtv.desktop.arme.model.TypeArme;
import ch.steversoix.jtv.desktop.arme.model.TypeArmeRepository;
import ch.steversoix.jtv.desktop.core.exception.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class TypeArmeService {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private TypeArmeRepository typeArmeRepository;

    /*
    * Constructeur
    * */

    public TypeArmeService(TypeArmeRepository typeArmeRepository){
        this.typeArmeRepository = typeArmeRepository;
    }

    public TypeArme findTypeArmeById(long id){
        return typeArmeRepository.findOne(id);
    }

    public List<TypeArme> findTypesArmesByActif(boolean actif){
        return typeArmeRepository.findTypesArmesByActif(actif);
    }

    /* Méthodes appelées par les controlleurs */

    public void save(TypeArme newTypeArme) throws TypeArmeException {
        TypeArmeException exception = new TypeArmeException();

        /* Traitement des erreurs AJOUT et MODIFICATION */
        exception.addAll(rulesAddUpdate(newTypeArme));

        /* Si aucune erreur alors on ajoute */
        if(exception.isEmpty()){
            typeArmeRepository.save(newTypeArme);
            LOGGER.log(Level.INFO, "Create TypeArme : " + newTypeArme.getNom());
        }else{
            throw exception;
        }
    }

    public void update(TypeArme typeArme) throws TypeArmeException {
        TypeArmeException exception = new TypeArmeException();

        /* Traitement des erreurs AJOUT et MODIFICATION */
        exception.addAll(rulesAddUpdate(typeArme));

        /* Si aucune erreur alors on modifie */
        if(exception.isEmpty()){
            typeArmeRepository.save(typeArme);
            LOGGER.log(Level.INFO, "Update TypeArme : " + typeArme.getId()+ " "+typeArme.getNom());
        }else{
            throw exception;
        }
    }

    public void desactivate(TypeArme typeArme){
        typeArme.setActif(false);
        typeArmeRepository.save(typeArme);
        LOGGER.log(Level.INFO, "Desactivate TypeArme : " + typeArme.getId()+ " "+typeArme.getNom());
    }

    public void activate(TypeArme typeArme){
        typeArme.setActif(true);
        typeArmeRepository.save(typeArme);
        LOGGER.log(Level.INFO, "Activate TypeArme : " + typeArme.getId()+ " "+typeArme.getNom());
    }



    /* Méthodes internes */

    /* Controles qui doivent être éffectué en cas d'ajout ET de modification !
    *  Si un tableau est renvoyé, c est qu il y a une erreur */
    private ArrayList<String> rulesAddUpdate(TypeArme typeArme){
        ArrayList<String> erreurs = new ArrayList<String>();

        /* Début des controles */

        if(typeArme.getNom().trim().equals("")){
            erreurs.add(TypeArmeEnumeration.NOM_NULL.getErreurException());
        }

        /* RG 31 : Le type d'arme (nom )doit être unique */
        boolean existe = false;
        List<TypeArme> al = typeArmeRepository.findAll(); // Il faut aussi vérifier les saisons supprimées
        for(TypeArme t: al){
            if(t.getNom().toLowerCase().equals(typeArme.getNom().toLowerCase())){
                existe = true;
            }
        }

        if(existe){
            erreurs.add(TypeArmeEnumeration.EXISTE_DEJA.getErreurException());
        }

        /* Fin des controles */
        return erreurs;
    }

    public List<TypeArme> findAll() {
        return typeArmeRepository.findAll();
    }

    public TypeArme getTypeArmeIfExistOrThrow404(long idTypeArme) {
        Optional<TypeArme> optTypeArme = typeArmeRepository.findById(idTypeArme);
        if (!optTypeArme.isPresent()) {
            LOGGER.log(Level.SEVERE, TypeArmeEnumeration.TYPE_ARME_INEXISTANT.getErreurforLog());
            throw new ResourceNotFoundException(TypeArmeEnumeration.TYPE_ARME_INEXISTANT.getErreurException());
        } else
            return optTypeArme.get();
    }
}
