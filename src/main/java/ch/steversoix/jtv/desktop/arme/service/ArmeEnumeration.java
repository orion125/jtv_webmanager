package ch.steversoix.jtv.desktop.arme.service;

public enum ArmeEnumeration {
    VALEUR_MIN_MAX_ID("Le numéro d'une arme doit être entre 1000 et 99999999.","1000 >= id <= 99999999"),
    ID_EXISTANT("Une arme ayant ce numéro existe déjà.","id existe déjà"),
    ARME_INEXISTANTE("L'arme à laquelle vous tentez d'accéder n'existe pas.", "arme NOT FOUND");

    private String erreurException;
    private String erreurForLog;

    ArmeEnumeration(String erreurException, String erreurForLog) {
        this.erreurException = erreurException;
        this.erreurForLog = erreurForLog;
    }

    public String getErreurException() {
        return erreurException;
    }
    public String getErreurforLog() {
        return erreurForLog;
    }
}
