package ch.steversoix.jtv.desktop.arme.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface TypeArmeRepository extends JpaRepository<TypeArme, Long> {

    @Query(value="select ta from TypeArme ta where ta.actif= ?1")
    List<TypeArme> findTypesArmesByActif(boolean actif);
    Optional<TypeArme> findById(long id);
}
