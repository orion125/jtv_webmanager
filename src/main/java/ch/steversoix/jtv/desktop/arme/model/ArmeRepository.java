package ch.steversoix.jtv.desktop.arme.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ArmeRepository extends JpaRepository<Arme, Long> {

    @Query(value="select ar from Arme ar where ar.actif= ?1")
    List<Arme> findArmesByActif(boolean actif);

    List<Arme> findByTypeAndActif(TypeArme ta, boolean actif);

    List<Arme> findByType(TypeArme ta);
    Optional<Arme> findById(long id);
}
