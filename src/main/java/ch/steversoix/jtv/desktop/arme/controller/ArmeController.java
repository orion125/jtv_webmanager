package ch.steversoix.jtv.desktop.arme.controller;

import ch.steversoix.jtv.desktop.arme.exception.ArmeException;
import ch.steversoix.jtv.desktop.arme.model.Arme;
import ch.steversoix.jtv.desktop.arme.model.TypeArme;
import ch.steversoix.jtv.desktop.arme.service.ArmeService;
import ch.steversoix.jtv.desktop.arme.service.TypeArmeEnumeration;
import ch.steversoix.jtv.desktop.arme.service.TypeArmeService;
import ch.steversoix.jtv.desktop.outils.FlashMessage;
import ch.steversoix.jtv.desktop.outils.FlashMessageGenerator;
import ch.steversoix.jtv.desktop.saison.model.Inscription;
import ch.steversoix.jtv.desktop.saison.service.SaisonService;
import ch.steversoix.jtv.desktop.tireur.services.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
@RequestMapping(path="/armes")
public class ArmeController {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Autowired
    private TypeArmeService typeArmeService;
    @Autowired
    private ArmeService armeService;
    @Autowired
    private SaisonService saisonService;

    @ModelAttribute("user")
    public SecurityUser getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (SecurityUser) auth.getPrincipal();
    }

    /*
    * Les requêtes qui suivent concernent la navigation
    * */

    /* Affichage du formulaire qui liste les armes */
    @GetMapping("")
    public String getArmes(Model model){
        List<Arme> lst = armeService.findArmesByActif(true);
        model.addAttribute("armes",lst);
        model.addAttribute("actif",true);
        return "gestionArmes";
    }

    /* Affichage du formulaire qui liste les armes */
    @GetMapping("/inactifs")
    public String getArmesInactif(Model model){
        List<Arme> lst = armeService.findArmesByActif(false);
        model.addAttribute("armes",lst);
        model.addAttribute("actif",false);
        return "gestionArmes";
    }

    /* Affichage du formulaire qui ajoute une arme */
    @GetMapping("/add")
    public String addArme(Model model, RedirectAttributes redirAttrs){
        Optional<List<TypeArme>> opLst = checkTypeArme();
        if(!opLst.isPresent()){
            LOGGER.log(Level.SEVERE,TypeArmeEnumeration.NO_TYPE_ARMES.getErreurforLog());
            redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.DANGER,
                    TypeArmeEnumeration.NO_TYPE_ARMES.getErreurException()));
            return "redirect:/armes";
        }

        model.addAttribute("edit_type_arme",true);
        model.addAttribute("arme",new Arme());
        model.addAttribute("typesArmes",opLst.get());
        return "ajoutArme";
    }

    /* Affichage du formulaire qui modifie une arme */
    @GetMapping("/{id}/edit")
    public String editArme(@PathVariable("id") long id, Model model){
        List<TypeArme> lst = typeArmeService.findTypesArmesByActif(true);

        Arme ar = armeService.getArmeIfExistOrThrow404(id);
        if(!lst.contains(ar.getType())){
            lst.add(typeArmeService.findTypeArmeById(ar.getType().getId()));
        }

        Optional<Inscription> insc = saisonService.findFirstInscriptionByArme(ar);
        boolean edit_type_arme = true;
        if(insc.isPresent()){
            edit_type_arme = false;
        }
        model.addAttribute("edit_type_arme",edit_type_arme);
        model.addAttribute("arme",ar);
        model.addAttribute("typesArmes",lst);
        return "ajoutArme";
    }

    /*
    * Les requêtes qui suivent intéragissent avec la BDD
    * */

    /* Ajout de l'arme dans la BDD */
    @PostMapping("")
    public String addArmeDB(@ModelAttribute("arme") Arme newArme, Model model, RedirectAttributes redirAttrs){
        try {
            armeService.save(newArme);
        } catch (ArmeException e) {
            boolean edit_type_arme = true;
            long id = newArme.getId()==null?new Long(0):newArme.getId();
                Optional<Arme> optArme = armeService.findArmeById(id);
//                System.out.println("id "+ar.getId());
                if (optArme.isPresent()) {
                    model.addAttribute("arme", optArme.get());
                    Optional<Inscription> insc = saisonService.findFirstInscriptionByArme(optArme.get());
                    if(insc.isPresent()){
                        edit_type_arme = false;
                    }
                } else {
//                    System.out.println("ar == null");
                    model.addAttribute("arme", new Arme());
                    model.addAttribute("messages", FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER,
                            e.getErreurs()));
                }

            List<TypeArme> lst = typeArmeService.findTypesArmesByActif(true);
            model.addAttribute("edit_type_arme",edit_type_arme);
            model.addAttribute("typesArmes",lst);
            return "ajoutArme";
        }
        LOGGER.log(Level.FINE,"L'arme n°"+newArme.getId()+" a été ajoutée.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "L'arme n°"+newArme.getId()+" a été ajoutée."));
        return "redirect:/armes";
    }

    /* Modifie l'arme dans la BDD */
    @PostMapping("/{id}/update")
    public String updateArmeDB(@ModelAttribute("arme") Arme arme, Model model, RedirectAttributes redirAttrs){
        armeService.update(arme);
        LOGGER.log(Level.FINE,"L'arme n°"+arme.getId()+" a été modifiée.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "L'arme n°"+arme.getId()+" a été modifiée."));
        return "redirect:/armes";
    }

    /* Désactive une arme de la BDD */
    @GetMapping("/{id}/desactivate")
    public String desactivateArmeDB(@PathVariable("id") long id, RedirectAttributes redirAttrs){
        Arme arme = armeService.getArmeIfExistOrThrow404(id);
        armeService.desactivate(arme);
        LOGGER.log(Level.FINE,"L'arme n°"+arme.getId()+" a été désactivée.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "L'arme n°"+arme.getId()+" a été désactivée."));
        return "redirect:/armes";
    }

    /* Désactive une arme de la BDD */
    @GetMapping("/{id}/activate")
    public String activateArmeDB(@PathVariable("id") long id, RedirectAttributes redirAttrs){
        Arme arme = armeService.getArmeIfExistOrThrow404(id);
        armeService.activate(arme);
        LOGGER.log(Level.FINE,"L'arme n°"+arme.getId()+" a été activée.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "L'arme n°"+arme.getId()+" a été activée."));
        return "redirect:/armes/inactifs";
    }

    private Optional<List<TypeArme>> checkTypeArme(){
        List<TypeArme> lst = typeArmeService.findTypesArmesByActif(true);
        if(lst.isEmpty()){
            return Optional.empty();
        }
        return Optional.of(lst);
    }


}
