package ch.steversoix.jtv.desktop.arme.service;

import ch.steversoix.jtv.desktop.arme.exception.ArmeException;
import ch.steversoix.jtv.desktop.arme.model.Arme;
import ch.steversoix.jtv.desktop.arme.model.ArmeRepository;
import ch.steversoix.jtv.desktop.arme.model.TypeArme;
import ch.steversoix.jtv.desktop.core.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class ArmeService {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    ArmeRepository armeRepository;

    /*
    * Constructeur
    * */

    public ArmeService(ArmeRepository armeRepository){
        this.armeRepository = armeRepository;
    }


    public Optional<Arme> findArmeById(long id){ return armeRepository.findById(id); }

    public List<Arme> findArmesByActif(boolean actif){
        return armeRepository.findArmesByActif(actif);
    }

    public List<Arme> findArmeByTypeAndByActif(TypeArme ta, boolean actif){return armeRepository.findByTypeAndActif(ta, actif);}

    public List<Arme> findArmeByType(TypeArme ta){return armeRepository.findByType(ta);}
    /*
    * Méthodes appelées par le controller
    * */

    public void save(Arme newArme) throws ArmeException {
        ArmeException exception = new ArmeException();

        exception.addAll(rulesAdd(newArme));

        /* Si aucune erreur alors on ajoute */
        if(exception.isEmpty()){
            armeRepository.save(newArme);
            LOGGER.log(Level.INFO, "Enregistrement de arme "+newArme.getId());
        }else{
            throw exception;
        }
    }

    public void update(Arme arme){
        armeRepository.save(arme);
    }

    public void desactivate(Arme arme){
        arme.setActif(false);
        armeRepository.save(arme);
    }

    public void activate(Arme arme){
        arme.setActif(true);
        armeRepository.save(arme);
    }

    /* Méthodes internes */
    private ArrayList<String> rulesAdd(Arme newArme){
        ArrayList<String> erreurs = new ArrayList<String>();
        if(newArme.getId() != null) {
            if (newArme.getId().longValue() < Arme.MIN_NUMERO || newArme.getId().longValue() > Arme.MAX_NUMERO) {
                LOGGER.log(Level.SEVERE,ArmeEnumeration.VALEUR_MIN_MAX_ID.getErreurforLog());
                erreurs.add(ArmeEnumeration.VALEUR_MIN_MAX_ID.getErreurException());
            }

            List<Arme> al = armeRepository.findAll(); // Il faut aussi vérifier les armes supprimées
            /* RG 09 : Numéro d'arme unique */
            for(Arme a:al){
                if(a.getId().longValue()==newArme.getId().longValue()){
                    LOGGER.log(Level.SEVERE,ArmeEnumeration.ID_EXISTANT.getErreurforLog());
                    erreurs.add(ArmeEnumeration.ID_EXISTANT.getErreurException());
                }
            }
        }else{
            LOGGER.log(Level.SEVERE,ArmeEnumeration.VALEUR_MIN_MAX_ID.getErreurforLog());
            erreurs.add(ArmeEnumeration.VALEUR_MIN_MAX_ID.getErreurException());
        }

        return erreurs;
    }

    public Arme getArmeIfExistOrThrow404(long idArme) {
        Optional<Arme> optArme = armeRepository.findById(idArme);
        if (!optArme.isPresent()) {
            LOGGER.log(Level.SEVERE, ArmeEnumeration.ARME_INEXISTANTE.getErreurforLog());
            throw new ResourceNotFoundException(ArmeEnumeration.ARME_INEXISTANTE.getErreurException());
        } else
            return optArme.get();
    }
}
