package ch.steversoix.jtv.desktop.poste.controller;

import ch.steversoix.jtv.desktop.outils.FlashMessage;
import ch.steversoix.jtv.desktop.outils.FlashMessageGenerator;
import ch.steversoix.jtv.desktop.poste.exception.PosteException;
import ch.steversoix.jtv.desktop.poste.model.Poste;
import ch.steversoix.jtv.desktop.poste.model.PosteRepository;
import ch.steversoix.jtv.desktop.poste.service.PosteService;
import ch.steversoix.jtv.desktop.tireur.services.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
@RequestMapping(path="/postes")
public class PosteController {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Autowired
    private PosteService posteService;

    @ModelAttribute("user")
    public SecurityUser getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (SecurityUser) auth.getPrincipal();
    }

    /*
    * Les requêtes qui suivent concernent la navigation
    * */

    /* Affichage du formulaire qui liste les postes */
    @GetMapping("")
    public String getPostes(Model model){
        List<Poste> lst = posteService.findPostesByActif(true);
        model.addAttribute("postes",lst);
        model.addAttribute("actif",true);
        return "gestionPostes";
    }

    @GetMapping("/inactifs")
    public String getPostesInactifs(Model model){
        List<Poste> lst = posteService.findPostesByActif(false);
        model.addAttribute("postes",lst);
        model.addAttribute("actif",false);
        return "gestionPostes";
    }


    /* Affichage du formulaire qui ajoute un poste */
    @GetMapping("/add")
    public String addPoste(Model model){
        model.addAttribute("poste",new Poste());
        return "ajoutPoste";
    }

    /* Affichage du formulaire qui modifie un poste */
    @GetMapping("/{id}/edit")
    public String editPoste(@PathVariable("id") long id, Model model){
        Poste po = posteService.getTypePosteIfExistOrThrow404(id);
        model.addAttribute("poste",po);
        /* RM07 - Poste */
        model.addAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.WARNING,
                "Attention ! Les modifications apportées à ce poste vont se répercuter sur toutes les saisons qui l'utilisent."));

        return "ajoutPoste";
    }

    /*
    * Les requêtes qui suivent intéragissent avec la BDD
    * */

    /* Ajout un poste dans la BDD */
    @PostMapping("")
    public String addPosteDB(@Valid @ModelAttribute("poste") Poste newPoste, BindingResult bindingResult, Model model, RedirectAttributes redirAttrs){
        if (bindingResult.hasErrors()) {
            return "ajoutPoste";
        }

        try {
            posteService.save(newPoste);
        } catch (PosteException e) {
            model.addAttribute("messages", FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER,
                    e.getErreurs()));
            return "ajoutPoste";
        }
        LOGGER.log(Level.FINE,"Le poste "+newPoste.getNom()+" a été ajouté.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le poste "+newPoste.getNom()+" a été ajouté."));
        return "redirect:/postes";
    }

    /* Modifie un type arme dans la BDD */
    @PostMapping("/{id}/update")
    public String updatePosteDB(@Valid @ModelAttribute("poste") Poste poste, BindingResult bindingResult, Model model, RedirectAttributes redirAttrs){
        if (bindingResult.hasErrors()) {
            return "ajoutPoste";
        }

        try {
            posteService.update(poste);
        } catch (PosteException e) {
            model.addAttribute("messages", FlashMessageGenerator.generateMultipleMessages(FlashMessage.Level.DANGER,
                    e.getErreurs()));
            return "ajoutPoste";
        }
        LOGGER.log(Level.FINE,"Le poste "+poste.getNom()+" a été modifié.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le poste "+poste.getNom()+" a été modifié."));
        return "redirect:/postes";
    }

    @GetMapping("/{id}/desactivate")
    public String desactivatePosteDB(@PathVariable("id") long id, RedirectAttributes redirAttrs){
        Poste poste = posteService.getTypePosteIfExistOrThrow404(id);
        posteService.desactivate(poste);
        LOGGER.log(Level.FINE,"Le poste "+poste.getNom()+" a été désactivé.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le poste "+poste.getNom()+" a été desactivé."));
        return "redirect:/postes";
    }

    @GetMapping("/{id}/activate")
    public String activatePosteDB(@PathVariable("id") long id, RedirectAttributes redirAttrs){
        Poste poste = posteService.getTypePosteIfExistOrThrow404(id);
        posteService.activate(poste);
        LOGGER.log(Level.FINE,"Le poste "+poste.getNom()+" a été activé.");
        redirAttrs.addFlashAttribute("message", FlashMessageGenerator.generateSingleMessage(FlashMessage.Level.SUCCESS,
                "Le poste "+poste.getNom()+" a été activé."));
        return "redirect:/postes/inactifs";
    }

    /*
    * Méthodes internes
    * */

}
