package ch.steversoix.jtv.desktop.poste.model;

import ch.steversoix.jtv.desktop.saison.cours.model.Cours;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "jtv_poste")
public class Poste {
    @Id
    @GeneratedValue
    @Column(name = "pos_id")
    private Long id;
    @NotNull
    @Size(min=2, max=50)
    @Column(name = "pos_nom")
    private String nom;
    @Column(name = "pos_actif")
    private boolean actif;
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "postes")
    private Set<Cours> cours = new HashSet<Cours>(0);

    /*
    * Constructeur
    * */
    public Poste(){
        this("");
    }
    public Poste(String nom) {
        this.nom = nom;
        this.actif = true;
    }
    public Poste(Long id, String nom) {this(id,nom,true);}
    public Poste(Long id, String nom, boolean actif) {
        this.id = id;
        this.nom = nom;
        this.actif = true;
    }

    /*
    * Getter & Setter
    * */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) { this.nom = nom; }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    /* Comparaison */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Poste poste = (Poste) o;

        return id != null ? id.equals(poste.id) : poste.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
