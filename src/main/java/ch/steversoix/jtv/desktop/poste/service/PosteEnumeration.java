package ch.steversoix.jtv.desktop.poste.service;

public enum PosteEnumeration {
    NOM_NULL("Le nom d'un poste ne peut pas être vide.","nom != null"),
    EXISTE_DEJA("Ce poste éxiste déjà.","poste existe déjà"),
    POSTE_INEXISTANT("Le poste auquel vous tentez d'accéder n'existe pas.", "post NOT FOUND");

    private String erreurException;
    private String erreurForLog;

    PosteEnumeration(String erreurException, String erreurForLog) {
        this.erreurException = erreurException;
        this.erreurForLog = erreurForLog;
    }

    public String getErreurException() {
        return erreurException;
    }
    public String getErreurforLog() {
        return erreurForLog;
    }
}
