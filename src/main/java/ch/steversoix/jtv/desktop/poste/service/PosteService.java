package ch.steversoix.jtv.desktop.poste.service;

import ch.steversoix.jtv.desktop.core.exception.ResourceNotFoundException;
import ch.steversoix.jtv.desktop.outils.FilterTools;
import ch.steversoix.jtv.desktop.poste.exception.PosteException;
import ch.steversoix.jtv.desktop.poste.model.Poste;
import ch.steversoix.jtv.desktop.poste.model.PosteRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class PosteService {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    PosteRepository posteRepository;

    /*
    * Constructeur
    * */

    public PosteService(PosteRepository posteRepository){
        this.posteRepository = posteRepository;
    }

    public Poste findPosteById(long id){
        return posteRepository.findOne(id);
    }

    public List<Poste> findPostesByActif(boolean actif){
        return posteRepository.findPosteByActif(actif);
    }
    /*
    * Méthodes appelées par les controlleurs
    * */

    public void save(Poste newPoste) throws PosteException {
        PosteException exception = new PosteException();

        /* Traitement des erreurs AJOUT et MODIFICATION */
        exception.addAll(rulesAddUpdate(newPoste));

        /* Si aucune erreur alors on ajoute */
        if(exception.isEmpty()){
            posteRepository.save(newPoste);
        }else{
            throw exception;
        }
    }

    public void update(Poste poste) throws PosteException {
        PosteException exception = new PosteException();

        /* Traitement des erreurs AJOUT et MODIFICATION */
        exception.addAll(rulesAddUpdate(poste));

        /* Si aucune erreur alors on modifie */
        if(exception.isEmpty()) {
            posteRepository.save(poste);
        }else{
            throw exception;
        }

    }

    public void desactivate(Poste poste){
        poste.setActif(false);
        posteRepository.save(poste);
    }

    public void activate(Poste poste){
        poste.setActif(true);
        posteRepository.save(poste);
    }

    private ArrayList<String> rulesAddUpdate(Poste poste){
        ArrayList<String> erreurs = new ArrayList<String>();
        /* Début des controles */

        /* nom != "" */
        if(poste.getNom().trim().equals("")){
            LOGGER.log(Level.SEVERE,PosteEnumeration.NOM_NULL.getErreurforLog());
            erreurs.add(PosteEnumeration.NOM_NULL.getErreurException());
        }

        /* RG 30 : Le poste (nom) doit être unique */
        boolean existe = false;
        List<Poste> al = posteRepository.findAll(); // Il faut aussi vérifier les saisons supprimées
        for(Poste p: al){
            if(FilterTools.replaceAccents(p.getNom().toLowerCase()).equals(FilterTools.replaceAccents(poste.getNom().toLowerCase()))){
                existe = true;
            }
        }

        if(existe){
            LOGGER.log(Level.SEVERE,PosteEnumeration.EXISTE_DEJA.getErreurforLog());
            erreurs.add(PosteEnumeration.EXISTE_DEJA.getErreurException());
        }

        /* Fin des controles */
        return erreurs;
    }


    public Poste getTypePosteIfExistOrThrow404(long idPoste) {
        Optional<Poste> optPoste = posteRepository.findById(idPoste);
        if (!optPoste.isPresent()) {
            LOGGER.log(Level.SEVERE, PosteEnumeration.POSTE_INEXISTANT.getErreurforLog());
            throw new ResourceNotFoundException(PosteEnumeration.POSTE_INEXISTANT.getErreurException());
        } else
            return optPoste.get();
    }
}
