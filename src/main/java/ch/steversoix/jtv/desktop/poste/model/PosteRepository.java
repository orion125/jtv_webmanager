package ch.steversoix.jtv.desktop.poste.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface PosteRepository extends JpaRepository<Poste, Long> {

    @Query(value="select pos from Poste pos WHERE pos.actif = ?1")
    List<Poste> findPosteByActif(boolean actif);


    Optional<Poste> findById(long id);
}
