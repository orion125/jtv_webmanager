/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.steversoix.jtv.desktop.outils;

import java.util.Random;

/**
 *
 * @author jonathan.capitao
 */
public class PasswordGenerator {	   
 	   
    private static final Random rand = new Random();
    private static final String Xsi ="abcdefghijklmnopqrstuvwxyz";
    private static final String Gamm ="ABCDEFGHIJKLMNOPQRSTUVWXYZ";  
    private static final String Iot = "1234567890";
    private static final String Zeta="-_)(/?,;:.";
    private static String pass =""; 
    private static double i =0;
    public static String getNewPass() {
        pass="";
        int size = 0;
        while (size < 7) size = (int)(Math.random()*10);
        while ((pass.length() != size)){
            int rPick=rand.nextInt(4);
            if (rPick ==0) {
                int erzat=rand.nextInt(Xsi.length()-1);
                pass+=Xsi.charAt(erzat);
            } else if (rPick == 1) {
                int erzat=rand.nextInt(Iot.length()-1);
                pass+=Iot.charAt(erzat);
            } else if (rPick==2) {
                int erzat=rand.nextInt(Gamm.length()-1);
                pass+=Gamm.charAt(erzat);
            }else if (rPick==3) {
                int erzat=rand.nextInt(Zeta.length()-1);
                pass+=Zeta.charAt(erzat);
            }
        }
        return pass;
    }
}

