/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.steversoix.jtv.desktop.outils;

/**
 *
 * @author Jonathan Blum
 */
public class NotEmailException extends RuntimeException {

    public NotEmailException(String msg) {
        super(msg);
    }
    
}
