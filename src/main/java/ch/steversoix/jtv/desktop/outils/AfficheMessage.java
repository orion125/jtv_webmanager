/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.steversoix.jtv.desktop.outils;

import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Diana Pedro Pinto
 * @version 0.1
 */
public class AfficheMessage {
    private JLabel l;
    private JPanel p;
    public static final int MESS_INFO = 1;
    public static final int MESS_ERREUR = 2;
    public static final int MESS_VALIDE = 3;
    
    public AfficheMessage (int type, String message, JLabel l, JPanel p) {
        this.l = l;
        this.p = p;
        if (type == MESS_INFO) {
            // Info
            p.setBackground(new Color (152, 211, 246));
            l.setForeground(new Color (4, 119, 195));
            l.setIcon(new ImageIcon(getClass().getResource("/img/infoB16.png")));
        } else if (type == MESS_ERREUR) {
            // Erreur
            p.setBackground(new Color(246, 135, 129));
            l.setForeground(new Color(251, 13, 0));
            l.setIcon(new ImageIcon(getClass().getResource("/img/erreurR16.png")));
        } else if (type == MESS_VALIDE) {
            // Valide
            p.setBackground(new Color(174, 234, 178));
            l.setForeground(new Color(0, 144, 10));
            l.setIcon(new ImageIcon(getClass().getResource("/img/validerV16.png")));
        }
        l.setText(message);
    }
}
