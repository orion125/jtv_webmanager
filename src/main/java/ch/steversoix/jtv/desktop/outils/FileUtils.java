package ch.steversoix.jtv.desktop.outils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
//import org.yaml.snakeyaml.*;

/**
 * @author jonathan.capitao
*/

public class FileUtils {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private static final int EOF = -1;
  
    // Lit un fichier texte et retourne son contenu.
    public static String read (String fileName) {
        try {
            FileInputStream fileToRead = new FileInputStream(fileName);
            StringBuilder reader = new StringBuilder(fileToRead.available());
            int charNum = fileToRead.read();
            while (charNum != EOF) {
              reader.append((char)charNum);
              charNum = fileToRead.read();
            }
            fileToRead.close();
            return reader.toString();
        }
        catch (FileNotFoundException e0) {
            LOGGER.log(Level.SEVERE, "Unable to find file {0}", fileName);
            return "";
        }
        catch (IOException e1) {
            LOGGER.log(Level.SEVERE, "Unable to read file {0}", fileName);
            return "";
        }
    } // read
 
    // Ecrit dans un fichier texte
    public static void write (String fileName, String[] str) {
        try {
            OutputStreamWriter writer = new OutputStreamWriter(
                    new FileOutputStream(fileName), 
                    java.nio.charset.Charset.forName("ISO-8859-1")
            );
            for (int lignActu = 0; lignActu < str.length; lignActu++) {
              writer.write(str[lignActu], 0, str[lignActu].length());
              writer.write("\r\n", 0, 2);
            }
            writer.close();
        }
        catch (FileNotFoundException e0) {
            LOGGER.log(Level.SEVERE, "Unable to find file {0}", fileName);
        }
        catch (IOException e1) {
            LOGGER.log(Level.SEVERE, "Unable to write to file {0}", fileName);
        }
    } // write

    public static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s;
    }
} // FileToStr
