package ch.steversoix.jtv.desktop.outils;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.text.Normalizer;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 *
 * @author jonathan.capitao
 * @version 0.2
 */
public class FilterTools {
    public static boolean isEmailAdress(String email){
        if(email.trim().equals("")){
            return false;
        }
        Pattern p = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$");
        Matcher m = p.matcher(email.toUpperCase());
        return m.matches();
    }

    public static boolean isTelephoneNumber(String telephone){
        // Suppression de tous les espaces
        String numTel = telephone.trim();
        numTel = numTel.replace(" ", "");

        if(numTel.equals("")){
            return false;
        }

        // Que des nombres et le +
        Pattern p = Pattern.compile("[0-9+]+");
        Matcher m = p.matcher(numTel);

        if(m.matches()) {
            if (numTel.charAt(0) == '+' && numTel.length() == 12) {
                return true;
            } else if (numTel.charAt(0) == '0' && numTel.length() == 10) {
                return true;
            }
        }
        return false;
    }

    public static  boolean isOnlyChar(String chaine){
        // Suppression de tous les espaces
        String s = chaine.trim();
        s = s.replace(" ", "");

        if(s.equals("")){
            return false;
        }

        Pattern p = Pattern.compile("[A-Za-z0-9éèàäöüïçâêûôÁÂÀÄÆÇÈÉÊËÎÏÖÔÚÙÜÛß-]+");
        Matcher m = p.matcher(s);

        return m.matches();
    }

    public static String replaceAccents(String text) {
        return Normalizer.normalize(text, Normalizer.Form.NFD)
                .replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }
}
