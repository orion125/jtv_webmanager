package ch.steversoix.jtv.desktop.outils;

public class FlashMessage {
    private String message;
    private Level level;

    public FlashMessage(Level level, String message) {
        this.level = level;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public Level getLevel() {
        return level;
    }

    public enum Level {
        PRIMARY,
        SECONDARY,
        SUCCESS,
        DANGER,
        WARNING,
        INFO,
        LIGHT,
        DARK,
        WHITE;
    }
}
