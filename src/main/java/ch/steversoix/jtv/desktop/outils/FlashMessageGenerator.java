package ch.steversoix.jtv.desktop.outils;

import java.util.ArrayList;

public class FlashMessageGenerator {

    public static ArrayList<FlashMessage> generateMultipleMessages(FlashMessage.Level level, ArrayList<String> messages){
        ArrayList<FlashMessage> flashmessages = new ArrayList<>();
        for (String s : messages)
            flashmessages.add(generateSingleMessage(level, s));
        return flashmessages;
    }

    public static FlashMessage generateSingleMessage(FlashMessage.Level level, String message){
        return new FlashMessage(level, message);
    }
}
