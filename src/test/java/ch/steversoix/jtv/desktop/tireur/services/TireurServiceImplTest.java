package ch.steversoix.jtv.desktop.tireur.services;

import ch.steversoix.jtv.desktop.tireur.exceptions.JeuneTireurException;
import ch.steversoix.jtv.desktop.tireur.exceptions.MoniteurException;
import ch.steversoix.jtv.desktop.tireur.model.JeuneTireur;
import ch.steversoix.jtv.desktop.tireur.model.Moniteur;
import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import ch.steversoix.jtv.desktop.tireur.model.TireurRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;

public class TireurServiceImplTest {

    private TireurServiceImpl tireurService;
    private MoniteurService moniteurService;
    private JeuneTireurService jeuneTireurService;

    private TireurRepository tireurRepositoryMock;
    private PasswordEncoder passwordEncoderMock;

    private Moniteur moniteurClean;
    private JeuneTireur jeuneTireurClean;

    private Moniteur moniteurMotPasse;
    private Moniteur moniteurAgeMin;
    private Moniteur moniteurUsernameDoublon;
    private JeuneTireur jtVide;

    private Date dateNaissClean;
    private DateFormat dateFormat;


    @BeforeMethod
    public void setUp() throws Exception {
        passwordEncoderMock = mock(PasswordEncoder.class);
        tireurRepositoryMock = mock(TireurRepository.class);

        tireurService = new TireurServiceImpl(tireurRepositoryMock, passwordEncoderMock);
        jeuneTireurService =new JeuneTireurService(tireurRepositoryMock,tireurService,passwordEncoderMock);
        moniteurService= new MoniteurService(tireurRepositoryMock,tireurService,passwordEncoderMock);

        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateNaissClean = dateFormat.parse("22/02/1990");

        moniteurClean = new Moniteur("Granger","Hermione","Madame",dateNaissClean,"poudlard 578", "1708", "Poudlard", "hermione.granger@gmail.com","0291241285","0713744891","hg","Hello123",Tireur.LEVEL_ADMIN);
        moniteurClean.setId(Long.valueOf("1"));
        jeuneTireurClean = new JeuneTireur("Potter","Harry","Monsieur",dateNaissClean,"poudlard 50", "1708", "Poudlard", "harry.potter@gmail.com","0291247485","0713254891","hp","Hello123",Tireur.LEVEL_USER,"Potter","Lily","rue du chauderon 20","7841","London","0712478495");

        moniteurMotPasse = new Moniteur("Dumbledore","Albus","Monsieur",dateNaissClean,"poudlard 578", "1708", "Poudlard", "hermione.granger@gmail.com","0291241285","0713744891","ab","hello123",Tireur.LEVEL_ADMIN);
        moniteurMotPasse.setId(Long.valueOf("2"));
        moniteurAgeMin = new Moniteur("Torvald","Linus","Monsieur",new Date(),"poudlard 578", "1708", "Poudlard", "hermione.granger@gmail.com","0291241285","0713744891","linus","Hello123",Tireur.LEVEL_ADMIN);
        moniteurUsernameDoublon = new Moniteur("Granger","Hermione","Madame",dateNaissClean,"poudlard 578", "1708", "Poudlard", "hermione.granger@gmail.com","0291241285","0713744891","hg","Hello123",Tireur.LEVEL_ADMIN);
        jtVide = new JeuneTireur("","","",dateNaissClean,"", "", "", "","","","","",Tireur.LEVEL_USER,"","","","","","");

    }

    @AfterMethod
    public void tearDown() throws Exception {
    }

    @Test
    public void testSaveNoIssue() throws Exception {
        doAnswer(returnsFirstArg()).when(tireurRepositoryMock).save(any(Tireur.class));

        jeuneTireurService.save(jeuneTireurClean);
        moniteurService.save(moniteurClean);
    }

    /* RG 16 - Age tireur */
    @Test(expectedExceptions = MoniteurException.class)
    public void moniteurAgeMin() throws Exception {
        doAnswer(returnsFirstArg()).when(tireurRepositoryMock).save(any(Tireur.class));
        moniteurService.save(moniteurAgeMin);
    }

    /* GD 02 */
    @Test(expectedExceptions = MoniteurException.class)
    public void checkMotPasse() throws Exception {
        doAnswer(returnsFirstArg()).when(tireurRepositoryMock).save(any(Tireur.class));
        moniteurService.save(moniteurMotPasse);
    }

    @Test(expectedExceptions = MoniteurException.class)
    public void checkUserNameDoublon()throws Exception {
        List<Moniteur> lstMoniteur = new ArrayList<>();
        doAnswer(returnsFirstArg()).when(tireurRepositoryMock).save(any(Tireur.class));
        moniteurService.save(moniteurClean);
        lstMoniteur.add(moniteurClean);
        when(tireurRepositoryMock.findByUsername(moniteurUsernameDoublon.getUsername())).thenReturn(Optional.of(moniteurClean));
        moniteurService.save(moniteurUsernameDoublon);
    }

    @Test(expectedExceptions = JeuneTireurException.class)
    public void checkChampsVide()throws Exception{
        doAnswer(returnsFirstArg()).when(tireurRepositoryMock).save(any(Tireur.class));

        assertTrue(jtVide.getNom().isEmpty());
        assertTrue(jtVide.getPrenom().isEmpty());
        assertTrue(jtVide.getNpa().isEmpty());
        assertTrue(jtVide.getAdresse().isEmpty());
        assertTrue(jtVide.getLocalite().isEmpty());
        assertTrue(jtVide.getNumNatel().isEmpty());
        assertTrue(jtVide.getNumTelephone().isEmpty());
        assertTrue(jtVide.getEmail().isEmpty());
        assertTrue(jtVide.getUsername().isEmpty());
        assertTrue(jtVide.getPassword().isEmpty());

        assertTrue(jtVide.getNomParent().isEmpty());
        assertTrue(jtVide.getPrenomParent().isEmpty());
        assertTrue(jtVide.getAdresseParent().isEmpty());
        assertTrue(jtVide.getNpaParent().isEmpty());
        assertTrue(jtVide.getLocaliteParent().isEmpty());
        assertTrue(jtVide.getNumNatelParent().isEmpty());

        jeuneTireurService.save(jtVide);
    }
}