package ch.steversoix.jtv.desktop.saison.cours.model;

import ch.steversoix.jtv.desktop.arme.model.TypeArme;
import ch.steversoix.jtv.desktop.saison.model.Saison;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.testng.Assert.*;

public class CoursTest {

    private Saison saisonCleanMock;
    private TypeArme typeArmeMock;
    private Date dateFinClean;
    private Date dateDebClean;
    private DateFormat dateFormat;


    private Cours coursTestClean;
    private Cours coursFinished;
    private Cours coursToday;
    private Cours coursDesactived;

    @BeforeMethod
    public void setUp() throws Exception {
        // MOCK DATA FOR TEST COURSE
        // Mock Saison
        typeArmeMock = new TypeArme("Pisolet");
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateDebClean = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dateDebClean);
        c.add(Calendar.DATE, 1);
        dateDebClean = c.getTime();
        dateFinClean = dateDebClean;
        c.setTime(dateFinClean);
        c.add(Calendar.MONTH,2);
        dateFinClean = c.getTime();
        Date dateCoursClean = dateDebClean;
        c.setTime(dateCoursClean);
        c.add(Calendar.DATE,4);
        dateCoursClean = c.getTime();

        Date datec2 = dateFormat.parse("02/01/2017");
        Date today = Calendar.getInstance().getTime();
        DateFormat heureFormat = new SimpleDateFormat("HH:mm");
        Date heureDebut = heureFormat.parse("08:00");
        Date heureFin = heureFormat.parse("12:00");
        saisonCleanMock = new Saison(typeArmeMock, dateDebClean, dateFinClean);
        // MOCK Cours
        coursTestClean = new Cours("Cours 1", "STE Versoix", dateCoursClean, heureDebut, heureFin,
                15, 15, saisonCleanMock);
        coursDesactived = new Cours("Cours 1", "STE Versoix", dateCoursClean, heureDebut, heureFin,
                15, 15, saisonCleanMock);
        coursDesactived.setActif(false);
        coursFinished = new Cours("Cours 1", "STE Versoix", datec2, heureDebut, heureFin,
                15, 15, saisonCleanMock);
        coursToday = new Cours("Cours 1", "STE Versoix", today, heureDebut, heureFin,
                15, 15, saisonCleanMock);
    }

    @Test
    public void testIsCoursFinishedCoursFinished() throws Exception {
        assertTrue(coursFinished.isCoursFinished());
    }

    @Test
    public void testIsCoursFinishedTodayNotFinished() throws Exception {
        assertFalse(coursToday.isCoursFinished());
    }

    @Test
    public void testIsCoursFinishedCoursActivedAndDesactivedOk() throws Exception {
        assertFalse(coursTestClean.isCoursFinished());
        assertFalse(coursDesactived.isCoursFinished());
    }

    @Test
    public void testIsCoursEditableCoursClean() throws Exception {
        assertTrue(coursTestClean.isCoursEditable());
    }
    @Test
    public void testIsCoursEditableCoursDesactived() throws Exception {
        assertFalse(coursDesactived.isCoursEditable());
    }
    @Test
    public void testIsCoursEditableCoursFinished() throws Exception {
        assertFalse(coursFinished.isCoursEditable());
    }
    @Test
    public void testIsCoursEditableTodayOK() throws Exception {
        assertTrue(coursToday.isCoursEditable());
    }

}