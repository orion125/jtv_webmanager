package ch.steversoix.jtv.desktop.saison.service;

import ch.steversoix.jtv.desktop.arme.model.TypeArme;
import ch.steversoix.jtv.desktop.saison.cours.model.GroupeRepository;
import ch.steversoix.jtv.desktop.saison.cours.model.*;
import ch.steversoix.jtv.desktop.saison.cours.service.CoursService;
import ch.steversoix.jtv.desktop.saison.cours.service.HoraireService;
import ch.steversoix.jtv.desktop.saison.exception.SaisonException;
import ch.steversoix.jtv.desktop.saison.model.InscritRepository;
import ch.steversoix.jtv.desktop.saison.model.Saison;
import ch.steversoix.jtv.desktop.saison.model.SaisonRepository;
import org.mockito.Mockito;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;


public class SaisonServiceTest {

    private SaisonService saisonService;
    private SaisonRepository saisonRepositoryMock;
    private InscritRepository inscritRepositoryMock;

    private CoursRepository coursRepositoryMock ;//
    private GroupeRepository groupeRepositoryMock;//
    private ParticipantRepository participantRepositoryMock;//
    private HoraireRepository horaireRepositoryMock;//
    private HoraireService horaireServiceMock;
    private CoursProgrammeAnneeRepository coursProgrammeAnneeRepositoryMock;
    private AbsenceRepository absenceRepositoryMock;
    private CoursService coursServiceMock;

    private Saison saisonCleanMock;
    private Saison saisonPastMock;
    private Saison saisonNullMock;
    private Saison saisonOver1AnsMock;
    private Saison saisonFinAnterieurMock;
    private TypeArme typeArmeMock;
    private Date dateFinClean;
    private Date dateDebClean;
    private Date datedebBefToday;
    private Date datefinLate2019;
    private Date datedebLate2018;

    private DateFormat dateFormat;

    @BeforeMethod
    public void setUp() throws Exception {
        saisonRepositoryMock = Mockito.mock(SaisonRepository.class);
        inscritRepositoryMock = Mockito.mock(InscritRepository.class);
        coursRepositoryMock = Mockito.mock(CoursRepository.class);
        groupeRepositoryMock= Mockito.mock(GroupeRepository.class);
        participantRepositoryMock= Mockito.mock(ParticipantRepository.class);
        horaireRepositoryMock= Mockito.mock(HoraireRepository.class);
        coursProgrammeAnneeRepositoryMock= Mockito.mock(CoursProgrammeAnneeRepository.class);
        absenceRepositoryMock= Mockito.mock(AbsenceRepository.class);
        horaireServiceMock = new HoraireService(horaireRepositoryMock);
        coursServiceMock = new CoursService(coursRepositoryMock, groupeRepositoryMock, horaireServiceMock,participantRepositoryMock, coursProgrammeAnneeRepositoryMock,absenceRepositoryMock);
        saisonService = new SaisonService(saisonRepositoryMock, inscritRepositoryMock,coursServiceMock);
        typeArmeMock = new TypeArme("Pisolet");
        dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        datedebLate2018 = dateFormat.parse("12/22/2018");
        datedebBefToday = dateFormat.parse("02/22/2017");
        datefinLate2019 = dateFormat.parse("11/22/2019");

        dateDebClean = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dateDebClean);
        c.add(Calendar.DATE, 1);
        dateDebClean = c.getTime();
        dateFinClean = dateDebClean;
        c.setTime(dateFinClean);
        c.add(Calendar.MONTH,2);
        dateFinClean = c.getTime();

        saisonCleanMock = new Saison(typeArmeMock,dateDebClean,dateFinClean);
        saisonPastMock = new Saison(typeArmeMock,datedebBefToday,dateFinClean);
        saisonOver1AnsMock = new Saison(typeArmeMock,dateDebClean,datefinLate2019);
        saisonFinAnterieurMock = new Saison(typeArmeMock,datedebLate2018, dateFinClean);
    }

    @AfterMethod
    public void tearDown() throws Exception {}

    @Test
    public void testSaveNoIssue() throws Exception {
        doAnswer(returnsFirstArg()).when(saisonRepositoryMock).save(any(Saison.class));
        saisonService.save(saisonCleanMock);
    }

    /* RA 06 : Saison - date du jour > date début */
    @Test(expectedExceptions = SaisonException.class)
    public void testSaveBeforeToday() throws Exception {
        doAnswer(returnsFirstArg()).when(saisonRepositoryMock).save(any(Saison.class));
        saisonService.save(saisonPastMock);
    }


    @Test(expectedExceptions = SaisonException.class)
    public void testSaveDateNull() throws Exception {
        doAnswer(returnsFirstArg()).when(saisonRepositoryMock).save(any(Saison.class));
        saisonNullMock = new Saison(typeArmeMock,null,dateFinClean);
        saisonService.save(saisonNullMock);

        saisonNullMock = new Saison(typeArmeMock,null,null);
        saisonService.save(saisonNullMock);

        saisonNullMock = new Saison(typeArmeMock,dateDebClean,null);
        saisonService.save(saisonNullMock);
    }

    /* RG 18.B : Création d une saison (date début / fin) -  durée max saison = 1 an */
    @Test(expectedExceptions = SaisonException.class)
    public void testSaveDureeOver1Ans() throws Exception {
        doAnswer(returnsFirstArg()).when(saisonRepositoryMock).save(any(Saison.class));
        saisonService.save(saisonOver1AnsMock);
    }

    /* RG 18.A : Création d une saison (date début / fin) - date début < date fin */
    @Test(expectedExceptions = SaisonException.class)
    public void testSaveDateFinAnterieur() throws Exception {
        doAnswer(returnsFirstArg()).when(saisonRepositoryMock).save(any(Saison.class));
        saisonService.save(saisonFinAnterieurMock);
    }
//
//    /* RG 08 : Intervalles de saisons du même type */
//    @Test(expectedExceptions = SaisonException.class)
//    public void testSaveSaisonExistante() throws Exception {
//        List<Saison> saisons = new ArrayList<>();
//        saisons.add(saisonCleanMock);
//        when(saisonRepositoryMock.findAll(true)).thenReturn(saisons);
//        saisonService.save(saisonCleanMock);
//    }


    @Test
    public void testUpdate() throws Exception {
    }

    @Test
    public void testUpdateDureeOver1Ans() throws Exception {

    }

    @Test
    public void testUpdateDateFinAnterieur() throws Exception {

    }

    @Test
    public void testUpdateSaisonExistante() throws Exception {
        // a tester
    }


    @Test
    public void testDesactivate() throws Exception {
    }

}