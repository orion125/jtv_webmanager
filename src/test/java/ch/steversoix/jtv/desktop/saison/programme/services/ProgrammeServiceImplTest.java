package ch.steversoix.jtv.desktop.saison.programme.services;

import ch.steversoix.jtv.desktop.arme.model.TypeArme;
import ch.steversoix.jtv.desktop.saison.exception.SaisonException;
import ch.steversoix.jtv.desktop.saison.model.InscritRepository;
import ch.steversoix.jtv.desktop.saison.model.Saison;
import ch.steversoix.jtv.desktop.saison.model.SaisonRepository;
import ch.steversoix.jtv.desktop.saison.programme.exceptions.ProgrammeException;
import ch.steversoix.jtv.desktop.saison.programme.model.*;
import ch.steversoix.jtv.desktop.saison.service.SaisonService;
import org.aspectj.asm.internal.ProgramElement;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.testng.Assert.*;

public class ProgrammeServiceImplTest {

    private ProgrammeServiceImpl progService;
    private Saison saisonCleanMock;
    private TypeArme typeArmeMock;
    private Date datefinClean2018;
    private Date datedebClean2018;
    private ProgrammeRepository proRepository;
    private PasseRepository passeRepository;
    private Programme programmeTestClean;
    private Programme programmeNoPasses;
    private Programme programmeNoNames;
    private Programme programmePassesBadOrder;
    private Programme programmePassesBadSequence;
    private Programme programmePassesHitNbs;
    private Programme programmeCleanPassesChanged;
    private DateFormat dateFormat;
    private List<Cible> alCible;
    ArrayList<Passe> passesChangeAddClean;
    ArrayList<Passe> passesChangeModClean;
    ArrayList<Passe> passesSupprClean;
    ArrayList<Passe> passesSupprCenterBefore;
    ArrayList<Passe> passesSupprCenterAfter;

    @BeforeMethod
    public void setUp() throws Exception {
        proRepository = Mockito.mock(ProgrammeRepository.class);
        passeRepository = Mockito.mock(PasseRepository.class);
        progService = new ProgrammeServiceImpl(proRepository, passeRepository);

        // MOCK DATA FOR TEST PROGRAMME
        typeArmeMock = new TypeArme("Pisolet");
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        datedebClean2018 = dateFormat.parse("22/02/2018");
        datefinClean2018 = dateFormat.parse("22/12/2018");
        saisonCleanMock = new Saison(typeArmeMock,datedebClean2018,datefinClean2018);
        alCible = new ArrayList<>();
        alCible.add(new Cible("A5",5));
        alCible.add(new Cible("A10",10));
        alCible.add(new Cible("A100",100));
        alCible.add(new Cible("B4",4));

        // MOCK PROGRAMMES DATA
        ArrayList<Passe> passesClean = new ArrayList<>();
        String coupParCoup = TypeCoupEnumeration.COUPS_PAR_COUPS.getType();
        passesClean.add(new Passe(-1, 1, 2, 5, coupParCoup , alCible.get(0), programmeTestClean));
        passesClean.add(new Passe(-1, 2, 1, 5, coupParCoup , alCible.get(2), programmeTestClean));
        passesClean.add(new Passe(-1, 3, 1, 5, coupParCoup , alCible.get(1), programmeTestClean));
        ArrayList<Passe> passesForNoName = new ArrayList<>();
        passesForNoName.add(new Passe(-1, 1, 2, 5, coupParCoup , alCible.get(0), programmeNoNames));
        passesForNoName.add(new Passe(-1, 2, 3, 5, coupParCoup , alCible.get(1), programmeNoNames));
        ArrayList<Passe> passesBadOrder = new ArrayList<>();
        passesBadOrder.add(new Passe(-1, 3, 2, 5, coupParCoup , alCible.get(0), programmePassesBadOrder));
        passesBadOrder.add(new Passe(-1, -1, 1, 5, coupParCoup , alCible.get(2), programmePassesBadOrder));
        ArrayList<Passe> passesOutOfBoundSequence = new ArrayList<>();
        passesOutOfBoundSequence.add(new Passe(-1, 1, 999, 5, coupParCoup , alCible.get(0), programmePassesBadSequence));
        passesOutOfBoundSequence.add(new Passe(-1, 2, -50, 5, coupParCoup , alCible.get(2), programmePassesBadSequence));
        ArrayList<Passe> passesOutOfBoundHits = new ArrayList<>();
        passesOutOfBoundHits.add(new Passe(-1, 1, 2, 999, coupParCoup , alCible.get(0), programmePassesHitNbs));
        passesOutOfBoundHits.add(new Passe(-1, 2, 1, -50, coupParCoup , alCible.get(2), programmePassesHitNbs));

        // MOCK PROGRAMMES
        programmeTestClean = new Programme("Jour 1", "", saisonCleanMock, passesClean);
        programmeNoPasses = new Programme("Jour 2", "", saisonCleanMock);
        programmeNoNames = new Programme(null, "", saisonCleanMock, passesForNoName);
        programmePassesBadOrder = new Programme("Jour 3", "", saisonCleanMock, passesBadOrder);
        programmePassesBadSequence = new Programme("Jour 4", "", saisonCleanMock, passesOutOfBoundSequence);
        programmePassesHitNbs = new Programme("Jour 5", "", saisonCleanMock, passesOutOfBoundHits);

        // Useful once update is implemented
        programmeCleanPassesChanged = new Programme("Jour 1", "", saisonCleanMock, passesClean);



        passesSupprClean = new ArrayList<>();
        passesSupprClean.add(new Passe(-1, 1, 2, 5, coupParCoup , alCible.get(0), programmeTestClean));
        passesSupprClean.add(new Passe(-1, 2, 1, 5, coupParCoup , alCible.get(2), programmeTestClean));
        passesSupprClean.add(new Passe(-1, 3, 1, 5, coupParCoup , alCible.get(2), programmeTestClean));

        passesSupprCenterBefore = new ArrayList<>();
        passesSupprCenterBefore.add(new Passe(-1, 1, 2, 5, coupParCoup , alCible.get(0), programmeTestClean));
        passesSupprCenterBefore.add(new Passe(-1, 2, 1, 5, coupParCoup , alCible.get(2), programmeTestClean));
        passesSupprCenterBefore.add(new Passe(-1, 3, 1, 5, coupParCoup , alCible.get(2), programmeTestClean));
        passesSupprCenterAfter = new ArrayList<>();
        passesSupprCenterAfter.add(new Passe(-1, 1, 2, 5, coupParCoup , alCible.get(0), programmeTestClean));
        passesSupprCenterAfter.add(new Passe(-1, 2, 1, 5, coupParCoup , alCible.get(2), programmeTestClean));
    }


    @Test
    public void testCreate() throws ProgrammeException {
        doAnswer(returnsFirstArg()).when(proRepository).save(any(Programme.class));
        progService.create(programmeTestClean);
    }

    @Test(expectedExceptions = ProgrammeException.class)
    public void testCreateWithoutPasses() throws ProgrammeException {
        doAnswer(returnsFirstArg()).when(proRepository).save(any(Programme.class));
        progService.create(programmeNoPasses);
    }

    @Test(expectedExceptions = ProgrammeException.class)
    public void testCreateWithoutName() throws ProgrammeException {
        doAnswer(returnsFirstArg()).when(proRepository).save(any(Programme.class));
        progService.create(programmeNoNames);
    }

    @Test(expectedExceptions = ProgrammeException.class)
    public void testCreateBadOrder() throws ProgrammeException {
        doAnswer(returnsFirstArg()).when(proRepository).save(any(Programme.class));
        progService.create(programmePassesBadOrder);
    }

    /* RG26 Sequence et Nombre de coups entre 1 et 50 pour chaque passes */
    @Test(expectedExceptions = ProgrammeException.class)
    public void testCreateOutOfBoundSequence() throws ProgrammeException {
        doAnswer(returnsFirstArg()).when(proRepository).save(any(Programme.class));
        progService.create(programmePassesBadSequence);
    }

    /* RG26 Sequence et Nombre de coups entre 1 et 50 pour chaque passes */
    @Test(expectedExceptions = ProgrammeException.class)
    public void testCreateOutOfBoundHitAmount() throws ProgrammeException {
        doAnswer(returnsFirstArg()).when(proRepository).save(any(Programme.class));
        progService.create(programmePassesHitNbs);
    }


    @Test
    public void testUpdateAddPasse() throws ProgrammeException {
        doAnswer(returnsFirstArg()).when(proRepository).save(any(Programme.class));
        programmeCleanPassesChanged.getPasses().add(
                new Passe(-1, 4, 4, 5, TypeCoupEnumeration.COUPS_PAR_COUPS.getType()
                        ,alCible.get(2), programmeCleanPassesChanged));
        Programme pro = progService.update(programmeCleanPassesChanged);
        assertTrue(pro.getPasses().size()>3);
    }

    @Test
    public void testUpdateModPasse() throws ProgrammeException {
        doAnswer(returnsFirstArg()).when(proRepository).save(any(Programme.class));
        programmeCleanPassesChanged.getPasses().get(2).setNbcoups(9);
        Programme pro = progService.update(programmeCleanPassesChanged);
        assertTrue(pro.getPasses().get(2).getNbcoups()==9);

    }

    @Test
    public void testUpdateSupprFirstPasse() throws ProgrammeException {
        doAnswer(returnsFirstArg()).when(proRepository).save(any(Programme.class));
        programmeCleanPassesChanged.getPasses().remove(0);
        for (Passe p : programmeCleanPassesChanged.getPasses()){
            int ordreTemp = p.getOrdre();
            p.setOrdre(ordreTemp-1);
        }
        Programme pro = progService.update(programmeCleanPassesChanged);
        assertTrue(pro.getPasses().size()<=3);
    }

    @Test
    public void testUpdateSupprLastPasse() throws ProgrammeException {
        doAnswer(returnsFirstArg()).when(proRepository).save(any(Programme.class));
        programmeCleanPassesChanged.getPasses().remove(programmeCleanPassesChanged.getPasses().size()-1);
        Programme pro = progService.update(programmeCleanPassesChanged);
        assertTrue(pro.getPasses().size()<=3);
    }

    @Test
    public void testUpdateSupprMidPasse() throws ProgrammeException {
        doAnswer(returnsFirstArg()).when(proRepository).save(any(Programme.class));
        programmeCleanPassesChanged.getPasses().remove(1);
        for (Passe p : programmeCleanPassesChanged.getPasses()){
            int ordreTemp = p.getOrdre();
            if (ordreTemp >1) p.setOrdre(ordreTemp-1);
        }
        Programme pro = progService.update(programmeCleanPassesChanged);
        assertTrue(pro.getPasses().size()<=3);
    }

    @Test
    public void testDesactivate() throws ProgrammeException {
        doAnswer(returnsFirstArg()).when(proRepository).save(any(Programme.class));
        programmeTestClean.setActif(true);
        progService.desactivate(programmeTestClean);
        assertFalse(programmeTestClean.isActif());
    }

    @Test
    public void testActivate() throws ProgrammeException {
        doAnswer(returnsFirstArg()).when(proRepository).save(any(Programme.class));
        programmeTestClean.setActif(false);
        progService.activate(programmeTestClean);
        assertTrue(programmeTestClean.isActif());
    }

}