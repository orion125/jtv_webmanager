package ch.steversoix.jtv.desktop.saison.concours.service;

import ch.steversoix.jtv.desktop.arme.model.TypeArme;
import ch.steversoix.jtv.desktop.saison.concours.exception.ConcoursException;
import ch.steversoix.jtv.desktop.saison.concours.model.Concours;
import ch.steversoix.jtv.desktop.saison.concours.model.ConcoursRepository;
import ch.steversoix.jtv.desktop.saison.model.Saison;
import org.mockito.Mockito;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.mockito.Matchers.any;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.doAnswer;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;


public class ConcoursServiceTest {
    private ConcoursService concoursService;
    private ConcoursRepository concoursRepository;

    private Date datefinClean2018;
    private Date datedebClean2018;
    private Date dateConcoursClean;
    private Date dateConcoursHorsSaison;
    private DateFormat dateFormat;
    private Saison saisonMock;
    private TypeArme typeArmeMock;

    private Concours concoursClean;
    private Concours concoursVide;
    private Concours concoursHorsSaison;
    private Concours concoursPrixNegatif;
    private Concours concoursPrixTropCher;



    @BeforeMethod
    public void setUp() throws Exception {
        concoursRepository = Mockito.mock(ConcoursRepository.class);
        concoursService = new ConcoursService(concoursRepository);

        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        datedebClean2018 = dateFormat.parse("22/02/2018");
        datefinClean2018 = dateFormat.parse("22/12/2018");
        dateConcoursClean = dateFormat.parse("01/05/2018");
        dateConcoursHorsSaison = dateFormat.parse("01/05/2019");

        typeArmeMock = new TypeArme("fusil");
        saisonMock = new Saison(typeArmeMock,datedebClean2018,datefinClean2018);

        concoursClean = new Concours("Tir du diable", "Vaud", dateConcoursClean,15,true,true,saisonMock);
        concoursVide = new Concours("", "", dateConcoursClean,15,true,true,saisonMock);
        concoursHorsSaison = new Concours("Tir du diable", "Vaud", dateConcoursHorsSaison,15,true,true,saisonMock);
        concoursPrixNegatif = new Concours("Tir du diable", "Vaud", dateConcoursClean,-15,true,true,saisonMock);
        concoursPrixTropCher = new Concours("Tir du diable", "Vaud", dateConcoursClean,150,true,true,saisonMock);
    }

    @AfterMethod
    public void tearDown() throws Exception {}

    @Test
    public void testSaveNoIssue() throws Exception {
        doAnswer(returnsFirstArg()).when(concoursRepository).save(any(Concours.class));
        concoursService.save(concoursClean);
    }


    @Test(expectedExceptions = ConcoursException.class)
    public void testVide() throws Exception {
        doAnswer(returnsFirstArg()).when(concoursRepository).save(any(Concours.class));
        assertTrue(concoursVide.getNom().isEmpty());
        assertTrue(concoursVide.getLieu().isEmpty());
        concoursService.save(concoursVide);
    }

    /* RG 25 - Prix */
    @Test(expectedExceptions = ConcoursException.class)
    public void testPrixNegatif() throws Exception {
        doAnswer(returnsFirstArg()).when(concoursRepository).save(any(Concours.class));
        concoursService.save(concoursPrixNegatif);
    }

    /* RG 25 - Prix */
    @Test(expectedExceptions = ConcoursException.class)
    public void testPrixTropCher() throws Exception {
        doAnswer(returnsFirstArg()).when(concoursRepository).save(any(Concours.class));
        concoursService.save(concoursPrixTropCher);
    }

    /* RG 29 - Date concours */
    @Test(expectedExceptions = ConcoursException.class)
    public void testHorsSaison() throws Exception {
        doAnswer(returnsFirstArg()).when(concoursRepository).save(any(Concours.class));
        concoursService.save(concoursHorsSaison);
    }

}
