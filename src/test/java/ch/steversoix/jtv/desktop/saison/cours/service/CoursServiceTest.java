package ch.steversoix.jtv.desktop.saison.cours.service;

import ch.steversoix.jtv.desktop.arme.model.TypeArme;
import ch.steversoix.jtv.desktop.saison.cours.model.Groupe;
import ch.steversoix.jtv.desktop.saison.cours.model.GroupeRepository;
import ch.steversoix.jtv.desktop.poste.model.Poste;
import ch.steversoix.jtv.desktop.saison.annee.model.Annee;
import ch.steversoix.jtv.desktop.saison.cours.exception.AbsenceException;
import ch.steversoix.jtv.desktop.saison.cours.exception.CoursException;
import ch.steversoix.jtv.desktop.saison.cours.exception.HoraireException;
import ch.steversoix.jtv.desktop.saison.cours.exception.ParticipantException;
import ch.steversoix.jtv.desktop.saison.cours.model.*;
import ch.steversoix.jtv.desktop.saison.model.Inscription;
import ch.steversoix.jtv.desktop.saison.model.Saison;
import ch.steversoix.jtv.desktop.saison.programme.model.Cible;
import ch.steversoix.jtv.desktop.saison.programme.model.Passe;
import ch.steversoix.jtv.desktop.saison.programme.model.Programme;
import ch.steversoix.jtv.desktop.saison.programme.services.TypeCoupEnumeration;
import ch.steversoix.jtv.desktop.tireur.model.JeuneTireur;
import ch.steversoix.jtv.desktop.tireur.model.Tireur;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class CoursServiceTest {

    private CoursService coursService;
    private Saison saisonCleanMock;
    private TypeArme typeArmeMock;
    private Date dateFinClean;
    private Date dateDebClean;
    private CoursRepository coursRepository;
    private GroupeRepository groupeRepository;
    private HoraireService horaireService;
    private ParticipantRepository participantRepository;
    private CoursProgrammeAnneeRepository coursProgrammeAnneeRepository;
    private AbsenceRepository absenceRepository;
    private Programme programme1;
    private Programme programme2;
    private Programme programme3;
    private DateFormat dateFormat;
    private List<Cible> alCible;
    ArrayList<CoursProgrammeAnnee> progAnneeClean;


    private Cours coursTestClean;
    private Cours coursNoProgAnnee;
    private Cours coursProgAnneeDuplicate;
    private Cours coursNoSaison;
    private Cours coursNoWithCleanToLong;
    private Cours coursNoWithPauseToLong;
    private Cours coursHorsSaison;
    private Cours coursHeureIncoherente;
    private Cours coursWithoutPostes;
    private Cours coursTestForUpdate;
    private Cours coursWithPosteHoursInverse;

    Cours mockCours = Mockito.mock(Cours.class);
    JeuneTireur mockJT = Mockito.mock(JeuneTireur.class);
    Inscription mockInscrit = Mockito.mock(Inscription.class);
    Participant mockParticipant = Mockito.mock(Participant.class);

    private JeuneTireur jeuneTireurClean;
    private Absence absenceOk;
    private Absence absenceJourCours;

    @BeforeMethod
    public void setUp() throws Exception {
        coursRepository = Mockito.mock(CoursRepository.class);
        groupeRepository = Mockito.mock(GroupeRepository.class);
        horaireService = Mockito.mock(HoraireService.class);
        participantRepository = Mockito.mock(ParticipantRepository.class);
        coursProgrammeAnneeRepository = Mockito.mock(CoursProgrammeAnneeRepository.class);
        absenceRepository = Mockito.mock(AbsenceRepository.class);
        coursService = new CoursService(coursRepository, groupeRepository,
                horaireService, participantRepository, coursProgrammeAnneeRepository, absenceRepository);

        // MOCK DATA FOR TEST COURSE
        // Mock Saison
        typeArmeMock = new TypeArme("Pisolet");
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        dateDebClean = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dateDebClean);
        c.add(Calendar.DATE, 1);
        dateDebClean = c.getTime();
        dateFinClean = dateDebClean;
        c.setTime(dateFinClean);
        c.add(Calendar.MONTH,2);
        dateFinClean = c.getTime();
        Date dateCoursClean = dateDebClean;
        c.setTime(dateCoursClean);
        c.add(Calendar.DATE,4);
        dateCoursClean = c.getTime();

        Date datec2 = dateFormat.parse("27/01/2017");

        DateFormat heureFormat = new SimpleDateFormat("HH:mm");
        Date heureDebut = heureFormat.parse("08:00");
        Date heureFin = heureFormat.parse("12:00");
        saisonCleanMock = new Saison(typeArmeMock, dateDebClean, dateFinClean);
        // MOCK Postes
        Set<Poste> postesForProgClean = new HashSet<>();
        postesForProgClean.add(new Poste(new Long(1), "tir"));
        postesForProgClean.add(new Poste(new Long(2), "secrétaire"));
        // MOCK Groupe
        Set<Groupe> groupeClean = new HashSet<>();
        groupeClean.add(new Groupe(1, "test"));
        groupeClean.add(new Groupe(2, "test2"));
        // MOCK Programmes
        alCible = new ArrayList<>();
        alCible.add(new Cible("A5", 5));
        alCible.add(new Cible("A10", 10));
        alCible.add(new Cible("A100", 100));
        alCible.add(new Cible("B4", 4));
        ArrayList<Passe> passesClean = new ArrayList<>();
        String coupParCoup = TypeCoupEnumeration.COUPS_PAR_COUPS.getType();
        String coupCache = TypeCoupEnumeration.COUPS_CACHES.getType();
        programme1 = new Programme("Jour 1", "", saisonCleanMock);
        passesClean.add(new Passe(-1, 1, 2, 5, coupParCoup, alCible.get(0), programme1));
        passesClean.add(new Passe(-1, 2, 1, 5, coupParCoup, alCible.get(2), programme1));
        programme1.setPasses(passesClean);
        programme2 = new Programme("Jour 2", "", saisonCleanMock);
        passesClean.add(new Passe(-1, 1, 2, 5, coupParCoup, alCible.get(2), programme2));
        passesClean.add(new Passe(-1, 2, 1, 10, coupCache, alCible.get(1), programme2));
        programme2.setPasses(passesClean);
        programme3 = new Programme("Jour 3", "", saisonCleanMock);
        passesClean.add(new Passe(-1, 1, 1, 20, coupParCoup, alCible.get(0), programme3));
        passesClean.add(new Passe(-1, 2, 3, 5, coupParCoup, alCible.get(1), programme3));
        programme3.setPasses(passesClean);
        // MOCK Annees
        ArrayList<Annee> annees = new ArrayList<>();
        annees.add(new Annee(new Long(1), "JT 1", true));
        annees.add(new Annee(new Long(2), "JT 2", true));
        annees.add(new Annee(new Long(3), "JT 3", true));
        annees.add(new Annee(new Long(4), "JT 4", true));
        annees.add(new Annee(new Long(5), "JT 5", true));
        annees.add(new Annee(new Long(6), "JT 6", true));
        annees.add(new Annee(new Long(7), "JT 7", true));
        annees.add(new Annee(new Long(8), "JT 8", true));

        // MOCK COURSES DATA
        progAnneeClean = new ArrayList<>();
        progAnneeClean.add(new CoursProgrammeAnnee(null, programme1, annees.get(0)));
        progAnneeClean.add(new CoursProgrammeAnnee(null, programme2, annees.get(1)));
        progAnneeClean.add(new CoursProgrammeAnnee(null, programme3, annees.get(2)));
        progAnneeClean.add(new CoursProgrammeAnnee(null, programme2, annees.get(3)));
        ArrayList<CoursProgrammeAnnee> progAnneeWithDuplicate = new ArrayList<>();
        progAnneeWithDuplicate.add(new CoursProgrammeAnnee(null, programme1, annees.get(0)));
        progAnneeWithDuplicate.add(new CoursProgrammeAnnee(null, programme2, annees.get(1)));
        progAnneeWithDuplicate.add(new CoursProgrammeAnnee(null, programme3, annees.get(1)));
        progAnneeWithDuplicate.add(new CoursProgrammeAnnee(null, programme2, annees.get(3)));

        coursTestClean = new Cours("Cours 1", "STE Versoix", dateCoursClean, heureDebut, heureFin,
                15, 15, saisonCleanMock);
        coursNoProgAnnee = new Cours("Cours 1", "STE Versoix", dateCoursClean, heureDebut, heureFin,
                15, 15, saisonCleanMock);
        coursProgAnneeDuplicate = new Cours("Cours 1", "STE Versoix", dateCoursClean, heureDebut, heureFin,
                15, 15, saisonCleanMock);
        coursNoSaison = new Cours("Cours 1", "STE Versoix", dateCoursClean, heureDebut, heureFin,
                15, 15, null);
        coursNoWithCleanToLong = new Cours("Cours 1", "STE Versoix", dateCoursClean, heureDebut, heureFin,
                15, 120, saisonCleanMock);
        coursNoWithPauseToLong = new Cours("Cours 1", "STE Versoix", dateCoursClean, heureDebut, heureFin,
                120, 15, saisonCleanMock);
        coursHorsSaison = new Cours("Cours 1", "STE Versoix", datec2, heureDebut, heureFin,
                15, 15, saisonCleanMock);
        coursHeureIncoherente = new Cours("Cours 1", "STE Versoix", dateCoursClean, heureFin, heureDebut,
                15, 15, saisonCleanMock);
        coursWithoutPostes = new Cours("Cours 1", "STE Versoix", dateCoursClean, heureDebut, heureFin,
                15, 15, saisonCleanMock);
        coursTestForUpdate = new Cours("Cours 1", "STE Versoix", dateCoursClean, heureDebut, heureFin,
                15, 15, saisonCleanMock);
        coursWithPosteHoursInverse = new Cours("Cours 1", "STE Versoix", dateCoursClean, heureFin, heureDebut, 15, 15, saisonCleanMock);

        coursTestClean.setProgrammesAnnee(progAnneeClean);
        coursProgAnneeDuplicate.setProgrammesAnnee(progAnneeWithDuplicate);
        coursNoWithCleanToLong.setProgrammesAnnee(progAnneeClean);
        coursNoSaison.setProgrammesAnnee(progAnneeClean);
        coursHorsSaison.setProgrammesAnnee(progAnneeClean);
        coursHeureIncoherente.setProgrammesAnnee(progAnneeClean);
        coursWithoutPostes.setProgrammesAnnee(progAnneeClean);
        coursTestForUpdate.setProgrammesAnnee(progAnneeClean);
        coursWithPosteHoursInverse.setPostes(postesForProgClean);

        doReturn(mockInscrit).when(mockParticipant).getInscrit();
        doReturn(mockJT).when(mockInscrit).getTireur();

        Date dateNaissClean = dateFormat.parse("22/02/1990");
        jeuneTireurClean = new JeuneTireur("Potter", "Harry", "Monsieur", dateNaissClean, "poudlard 50", "1708", "Poudlard", "harry.potter@gmail.com", "0291247485", "0713254891", "hp", "Hello123", Tireur.LEVEL_USER, "Potter", "Lily", "rue du chauderon 20", "7841", "London", "0712478495");

        absenceOk = new Absence(coursTestClean, jeuneTireurClean, "Maladie");
        absenceJourCours = new Absence(new Long(2), coursTestClean, jeuneTireurClean, "Maladie", LocalDateTime.ofInstant(dateCoursClean.toInstant(), ZoneId.systemDefault()));
    }

    @Test
    public void testSave() throws CoursException {
        // test also in alternative with programme 0, 1 and multiples programmes or postes
        // and with others data incoherence.
        doAnswer(returnsFirstArg()).when(coursRepository).save(any(Cours.class));
        coursService.save(coursTestClean);
        assertTrue(coursTestClean.getProgrammesAnnee().size() == 4);
    }

    @Test
    public void testSaveNoProgAnnee() throws CoursException {
        doAnswer(returnsFirstArg()).when(coursRepository).save(any(Cours.class));
        coursService.save(coursNoProgAnnee);
    }

    // RG 21 – Gestion des programmes pour un cours
    @Test(expectedExceptions = CoursException.class)
    public void testSavoProgAnneeDuplicate() throws CoursException {
        doAnswer(returnsFirstArg()).when(coursRepository).save(any(Cours.class));
        coursService.save(coursProgAnneeDuplicate);
    }

    @Test(expectedExceptions = CoursException.class)
    public void testSaveNoSaison() throws CoursException {
        doAnswer(returnsFirstArg()).when(coursRepository).save(any(Cours.class));
        coursService.save(coursNoSaison);
    }

    // RG 33 – Heure de début, heure de fin
    @Test(expectedExceptions = CoursException.class)
    public void testSaveBadTime() throws CoursException {
        doAnswer(returnsFirstArg()).when(coursRepository).save(any(Cours.class));
        coursService.save(coursHeureIncoherente);
    }

    // RG 03 – Temps de nettoyage
    @Test(expectedExceptions = CoursException.class)
    public void testSaveCleanTooLong() throws CoursException {
        doAnswer(returnsFirstArg()).when(coursRepository).save(any(Cours.class));
        coursService.save(coursNoWithCleanToLong);
    }

    // RG 02 – Temps de pause
    @Test(expectedExceptions = CoursException.class)
    public void testSavePauseTooLong() throws CoursException {
        doAnswer(returnsFirstArg()).when(coursRepository).save(any(Cours.class));
        coursService.save(coursNoWithPauseToLong);
    }

    // RG 14 – Date des cours
    @Test(expectedExceptions = CoursException.class)
    public void testSaveOutOfSaison() throws CoursException {
        doAnswer(returnsFirstArg()).when(coursRepository).save(any(Cours.class));
        coursService.save(coursHorsSaison);
    }

    @Test
    public void testUpdateClean() throws CoursException {
        // take a course with programmes launch the methode.
        // nominal case, test also in alternative with programme amount change, or with programme.
        doAnswer(returnsFirstArg()).when(coursRepository).save(any(Cours.class));
        coursTestForUpdate.getProgrammesAnnee().remove(3);
        coursService.update(coursTestForUpdate);
        assertTrue(coursTestForUpdate.getProgrammesAnnee().size() == 3);
    }

    @Test
    public void testUpdateNoProgrammeLeft() throws CoursException {
        // take a course with programmes launch the methode.
        // nominal case, test also in alternative with programme amount change, or with programme.
        doAnswer(returnsFirstArg()).when(coursRepository).save(any(Cours.class));
        coursTestForUpdate.setProgrammesAnnee(null);
        coursService.update(coursTestForUpdate);
    }

    @Test
    public void testUpdateMiddleProgrammeLeft() throws CoursException {
        // take a course with programmes launch the methode.
        // nominal case, test also in alternative with programme amount change, or with programme.
        doAnswer(returnsFirstArg()).when(coursRepository).save(any(Cours.class));
        coursTestForUpdate.getProgrammesAnnee().remove(1);
        coursService.update(coursTestForUpdate);
        assertTrue(coursTestForUpdate.getProgrammesAnnee().size() == 3);
    }

    @Test
    public void testDesactivate() throws CoursException {
        // create course normal and check if after test it's really desactivated
        doAnswer(returnsFirstArg()).when(coursRepository).save(any(Cours.class));
        coursTestClean.setActif(true);
        coursService.desactivate(coursTestClean);
        assertFalse(coursTestClean.isActif());
    }

    @Test
    public void testActivate() throws CoursException {
        // create course normal, set the activate parameter to false and try reactivating with method
        // then check if the activated became true.
        doAnswer(returnsFirstArg()).when(coursRepository).save(any(Cours.class));
        coursTestClean.setActif(false);
        coursService.activate(coursTestClean);
        assertTrue(coursTestClean.isActif());
    }


    @Test
    public void testGenerateHoraire() {
        // take a course with programmes and postes, launch the methode and check if the horaires are properly generated.
        // nominal case, test also in alternative with course without poste.
        doAnswer(returnsFirstArg()).when(coursRepository).save(any(Cours.class));
    }

    @Test(expectedExceptions = CoursException.class)
    public void testNoPosteForHoraire() throws HoraireException, CoursException {
        doAnswer(returnsFirstArg()).when(coursRepository).save(any(Cours.class));
        coursService.generateHoraire(coursWithoutPostes);
    }

    @Test(expectedExceptions = CoursException.class)
    public void testPosteHourInverseForHoraire() throws HoraireException, CoursException {
        doAnswer(returnsFirstArg()).when(coursRepository).save(any(Cours.class));
        coursService.generateHoraire(coursWithPosteHoursInverse);
    }

    /* RG 13 - Résultat obtenu */
    @Test(expectedExceptions = ParticipantException.class)
    public void testSetResultatsTotalNegative() throws Exception {
        doReturn(Optional.of(programme1)).when(mockCours).getProgramme(any());
        coursService.setResultats(mockCours, mockParticipant, -1, 0);
    }

    /* RG 13 - Résultat obtenu */
    @Test(expectedExceptions = ParticipantException.class)
    public void testSetResultatsTotalOverMax() throws Exception {
        doReturn(Optional.of(programme1)).when(mockCours).getProgramme(any());
        coursService.setResultats(mockCours, mockParticipant, programme1.calculTotalPoints() + 1, 0);
    }

    /* RG 13 - Résultat obtenu */
    @Test
    public void testSetResultatsTotalNominal() throws Exception {
        doReturn(Optional.of(programme1)).when(mockCours).getProgramme(any());
        coursService.setResultats(mockCours, mockParticipant, programme1.calculTotalPoints() - 1, 0);
    }

    /* RG 28 - Nombre de coups supplémentaire */
    @Test(expectedExceptions = ParticipantException.class)
    public void testSetResultatsMunNegative() throws Exception {
        doReturn(Optional.of(programme1)).when(mockCours).getProgramme(any());
        coursService.setResultats(mockCours, mockParticipant, 0, -1);
    }

    /* RG 28 - Nombre de coups supplémentaire */
    @Test(expectedExceptions = ParticipantException.class)
    public void testSetResultatsMunOverMax() throws Exception {
        doReturn(Optional.of(programme1)).when(mockCours).getProgramme(any());
        coursService.setResultats(mockCours, mockParticipant, 0, 21);
    }

    /* RG 28 - Nombre de coups supplémentaire */
    @Test
    public void testSetResultatsNominalMun() throws Exception {
        doReturn(Optional.of(programme1)).when(mockCours).getProgramme(any());
        coursService.setResultats(mockCours, mockParticipant, 0, 19);
    }

    @Test(expectedExceptions = ParticipantException.class)
    public void testSetResultatsNoProgramme() throws Exception {
        doReturn(Optional.empty()).when(mockCours).getProgramme(any());
        coursService.setResultats(mockCours, mockParticipant, 0, 21);
    }

    @Test
    public void testPrevenirAbsence() throws AbsenceException {
        doAnswer(returnsFirstArg()).when(absenceRepository).save(any(Absence.class));
        when(absenceRepository.findAbsenceByCoursAndTireur(coursTestClean, jeuneTireurClean)).thenReturn(Optional.empty());
        coursService.prevenirAbsence(absenceOk);
    }

    @Test(expectedExceptions = AbsenceException.class)
    public void testPrevenirAbsenceDejaPrevenu() throws AbsenceException {
        doAnswer(returnsFirstArg()).when(absenceRepository).save(any(Absence.class));
        when(absenceRepository.findAbsenceByCoursAndTireur(coursTestClean, jeuneTireurClean)).thenReturn(Optional.of(absenceOk));
        doThrow(AbsenceException.class).when(absenceRepository).save(absenceOk);
        coursService.prevenirAbsence(absenceOk);
    }

    // RG35 - Ne peut pas prévenir d'une absence qu'avant la veille du cours à midi.
    @Test(expectedExceptions = AbsenceException.class)
    public void testPrevenirAbsenceJourMeme() throws AbsenceException {
        doAnswer(returnsFirstArg()).when(absenceRepository).save(any(Absence.class));
        when(absenceRepository.findAbsenceByCoursAndTireur(coursTestClean, jeuneTireurClean)).thenReturn(Optional.empty());
        coursService.prevenirAbsence(absenceJourCours);
    }
}