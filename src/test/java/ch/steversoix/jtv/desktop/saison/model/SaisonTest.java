package ch.steversoix.jtv.desktop.saison.model;

import ch.steversoix.jtv.desktop.arme.model.TypeArme;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;

import static org.testng.Assert.*;

public class SaisonTest {

    private Saison saisonCleanMock;
    private Saison saisonEndBeforeToday;
    private Saison saisonEndToday;
    private Saison saisonEndTomorrow;
    private Saison saisonStartTomorrow;
    private Saison saisonDesactivated;
    private TypeArme typeArmeMock;
    private Date datefinClean2018;
    private Date datedebClean2018;
    private DateFormat dateFormat;


    @BeforeMethod
    public void setUp() throws Exception {
        // MOCK DATA FOR TEST COURSE
        // Mock Saison
        typeArmeMock = new TypeArme("Pisolet");
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        datedebClean2018 = dateFormat.parse("01/01/2018");
        datefinClean2018 = dateFormat.parse("22/06/2018");
        Date datePastDeb = dateFormat.parse("02/01/2017");
        Date datePastFin = dateFormat.parse("02/06/2017");
        Date today = Calendar.getInstance().getTime();
        Date tomorrow = Date.from(Calendar.getInstance().getTime().toInstant().atZone(ZoneId.systemDefault()).
                toLocalDateTime().plusDays(1).atZone(ZoneId.systemDefault()).toInstant());
        saisonCleanMock = new Saison(typeArmeMock, datedebClean2018, datefinClean2018);
        saisonDesactivated = new Saison(typeArmeMock, datedebClean2018, datefinClean2018);
        saisonDesactivated.setActif(false);
        saisonEndBeforeToday = new Saison(typeArmeMock, datePastDeb,datePastFin);
        saisonEndToday = new Saison(typeArmeMock, datedebClean2018, today);
        saisonEndTomorrow = new Saison(typeArmeMock, datedebClean2018, tomorrow);
        saisonStartTomorrow = new Saison(typeArmeMock, tomorrow, datefinClean2018);

    }

    @Test
    public void testIsSaisonFinishedSaisonCleanMock() throws Exception {
        assertFalse(saisonCleanMock.isSaisonFinished());
    }
    @Test
    public void testIsSaisonFinishedSaisonDesactivated() throws Exception {
        assertFalse(saisonDesactivated.isSaisonFinished());
    }
    @Test
    public void testIsSaisonFinishedSaisonEndBeforeToday() throws Exception {
        assertTrue(saisonEndBeforeToday.isSaisonFinished());
    }
    @Test
    public void testIsSaisonFinishedSaisonEndToday() throws Exception {
        assertFalse(saisonEndToday.isSaisonFinished());
    }
    @Test
    public void testIsSaisonFinishedSaisonEndTomorrow() throws Exception {
        assertFalse(saisonEndTomorrow.isSaisonFinished());
    }
    @Test
    public void testIsSaisonFinishedSaisonStartTomorrow() throws Exception {
        assertFalse(saisonStartTomorrow.isSaisonFinished());
    }


    @Test
    public void testIsSaisonEditableSaisonCleanMock() throws Exception {
        assertTrue(saisonCleanMock.isSaisonEditable());
    }
    @Test
    public void testIsSaisonEditableSaisonDesactivated() throws Exception {
        assertFalse(saisonDesactivated.isSaisonEditable());
    }
    @Test
    public void testIsSaisonEditableSaisonEndBeforeToday() throws Exception {
        assertFalse(saisonEndBeforeToday.isSaisonEditable());
    }
    @Test
    public void testIsSaisonEditableSaisonEndToday() throws Exception {
        assertTrue(saisonEndToday.isSaisonEditable());
    }
    @Test
    public void testIsSaisonEditableSaisonEndTomorrow() throws Exception {
        assertTrue(saisonEndTomorrow.isSaisonEditable());
    }
    @Test
    public void testIsSaisonEditableSaisonStartTomorrow() throws Exception {
        assertTrue(saisonStartTomorrow.isSaisonEditable());
    }

    @Test
    public void testIsSaisonOpenSaisonCleanMock() throws Exception {
        assertTrue(saisonCleanMock.isSaisonOpen());
    }
    @Test
    public void testIsSaisonOpenSaisonDesactivated() throws Exception {
        assertTrue(saisonDesactivated.isSaisonOpen());
    }
    @Test
    public void testIsSaisonOpenSaisonEndBeforeToday() throws Exception {
        assertFalse(saisonEndBeforeToday.isSaisonOpen());
    }
    @Test
    public void testIsSaisonOpenSaisonEndToday() throws Exception {
        assertTrue(saisonEndToday.isSaisonOpen());
    }
    @Test
    public void testIsSaisonOpenSaisonEndTomorrow() throws Exception {
        assertTrue(saisonEndTomorrow.isSaisonOpen());
    }
    @Test
    public void testIsSaisonOpenSaisonStartTomorrow() throws Exception {
        assertFalse(saisonStartTomorrow.isSaisonOpen());
    }

}