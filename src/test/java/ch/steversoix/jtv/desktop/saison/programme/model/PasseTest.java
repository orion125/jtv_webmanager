package ch.steversoix.jtv.desktop.saison.programme.model;

import ch.steversoix.jtv.desktop.arme.model.TypeArme;
import ch.steversoix.jtv.desktop.saison.model.Saison;
import ch.steversoix.jtv.desktop.saison.programme.services.ProgrammeServiceImpl;
import ch.steversoix.jtv.desktop.saison.programme.services.TypeCoupEnumeration;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.testng.Assert.*;

public class PasseTest {
    Passe passewithPond;
    Passe passewithPondCoupCache;
    Passe passeEntrainement;
    private Saison saisonCleanMock;
    private TypeArme typeArmeMock;
    private Date datefinClean2018;
    private Date datedebClean2018;
    private Programme programmeTest;
    private DateFormat dateFormat;
    private List<Cible> alCible;

    @BeforeMethod
    public void setUp() throws Exception {

        // MOCK DATA FOR TEST PROGRAMME
        typeArmeMock = new TypeArme("Pisolet");
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        datedebClean2018 = dateFormat.parse("22/02/2018");
        datefinClean2018 = dateFormat.parse("22/12/2018");
        saisonCleanMock = new Saison(typeArmeMock,datedebClean2018,datefinClean2018);
        alCible = new ArrayList<>();
        programmeTest = Mockito.mock(Programme.class);
        alCible.add(new Cible("A5",5));
        alCible.add(new Cible("A10",10));
        alCible.add(new Cible("A100",100));
        alCible.add(new Cible("B4",4));
        passewithPond = new Passe(-1,1,2,5,
                TypeCoupEnumeration.COUPS_PAR_COUPS.getType(),alCible.get(0),programmeTest);
        passewithPondCoupCache = new Passe(-1,1,5,5,
                TypeCoupEnumeration.COUPS_CACHES.getType(),alCible.get(2),programmeTest);
        passeEntrainement = new Passe(-1,1,3,5,
                TypeCoupEnumeration.COUPS_ENTRAINEMENT.getType(),alCible.get(1),programmeTest);
    }

    @Test
    public void testCalculeTotalPointsPasseCoupParCoups() throws Exception {
        int expectedvalue = passewithPond.getNbcoups()*passewithPond.getSequence()*
                TypeCoupEnumeration.COUPS_CACHES.getPonderation()*passewithPond.getCible().getValeurMax();
        assertEquals(expectedvalue,passewithPond.calculeTotalPointsPasse());
    }


    @Test
    public void testCalculeTotalPointsPasseCoupCache() throws Exception {
        int expectedvalue = passewithPondCoupCache.getNbcoups()*passewithPondCoupCache.getSequence()*
                TypeCoupEnumeration.COUPS_CACHES.getPonderation()*passewithPondCoupCache.getCible().getValeurMax();
        assertEquals(expectedvalue,passewithPondCoupCache.calculeTotalPointsPasse());
    }

    @Test
    public void testCalculeTotalPointsPasseEntrainement() throws Exception {
        assertEquals(0,passeEntrainement.calculeTotalPointsPasse());
    }


    @Test
    public void testCalculeNombreTirsPasse() throws Exception {
        assertEquals(passewithPond.getNbcoups()*passewithPond.getSequence(),passewithPond.calculeNombreTirsPasse());
    }

    @Test
    public void testCalculeNombreTirsPasseCoupCache() throws Exception {
        assertEquals(passewithPondCoupCache.getNbcoups()*passewithPondCoupCache.getSequence(),
                passewithPondCoupCache.calculeNombreTirsPasse());
    }

    @Test
    public void testCalculeNombreTirsPasseEntrainement() throws Exception {
        assertEquals(passeEntrainement.getNbcoups()*passeEntrainement.getSequence(),passeEntrainement.calculeNombreTirsPasse());
    }
}