package ch.steversoix.jtv.desktop.saison.programme.services;

import ch.steversoix.jtv.desktop.saison.programme.exceptions.CibleException;
import ch.steversoix.jtv.desktop.saison.programme.model.Cible;
import ch.steversoix.jtv.desktop.saison.programme.model.CibleRepository;
import org.mockito.Mockito;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

public class CibleServiceImplTest {
    private CibleServiceImpl cibleService;
    private CibleRepository cibleRepositoryMock;

    private Cible cibleClean;
    private Cible cibleCleanA;
    private Cible cibleVide;
    private Cible cibleValMin;
    private Cible cibleValMax;
    private Cible cibleDoublon;

    @BeforeMethod
    public void setUp() throws Exception {
        cibleRepositoryMock = Mockito.mock(CibleRepository.class);

        cibleClean = new Cible("A5",5);
        cibleCleanA = new Cible("A10",10);
        cibleVide = new Cible("",0);

        cibleValMin = new Cible("A1",-1);
        cibleValMax = new Cible("A1001",1001);
        cibleDoublon = new Cible("A5",5);

        cibleService = new CibleServiceImpl(cibleRepositoryMock);
    }

    @AfterMethod
    public void tearDown() throws Exception {}

    @Test
    public void testSaveNoIssue() throws Exception {
        doAnswer(returnsFirstArg()).when(cibleRepositoryMock).save(any(Cible.class));
        cibleService.save(cibleClean);
        cibleService.save(cibleCleanA);
    }

    /* RG 12 */
    @Test(expectedExceptions = CibleException.class)
    public void testValMin() throws Exception {
        doAnswer(returnsFirstArg()).when(cibleRepositoryMock).save(any(Cible.class));

        cibleService.save(cibleValMin);
    }

    /* RG 12 */
    @Test(expectedExceptions = CibleException.class)
    public void testValMax() throws Exception {
        doAnswer(returnsFirstArg()).when(cibleRepositoryMock).save(any(Cible.class));

        cibleService.save(cibleValMax);
    }

    @Test(expectedExceptions = CibleException.class)
    public void testEmpty() throws Exception {
        doAnswer(returnsFirstArg()).when(cibleRepositoryMock).save(any(Cible.class));

        cibleService.save(cibleVide);
    }

    /* RG 34 */
    @Test(expectedExceptions = CibleException.class)
    public void testCibleExiste() throws Exception {
        List<Cible> lstCibles = new ArrayList<>();
        doAnswer(returnsFirstArg()).when(cibleRepositoryMock).save(any(Cible.class));

        cibleService.save(cibleClean);
        cibleService.save(cibleCleanA);
        lstCibles.add(cibleClean);
        lstCibles.add(cibleCleanA);
        when(cibleRepositoryMock.findAll()).thenReturn(lstCibles);
        cibleService.save(cibleDoublon);
    }
}
