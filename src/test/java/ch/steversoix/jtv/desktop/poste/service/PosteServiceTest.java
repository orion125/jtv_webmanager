package ch.steversoix.jtv.desktop.poste.service;

import ch.steversoix.jtv.desktop.poste.exception.PosteException;
import ch.steversoix.jtv.desktop.poste.model.Poste;
import ch.steversoix.jtv.desktop.poste.model.PosteRepository;
import org.mockito.Mockito;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

public class PosteServiceTest {
    private PosteService posteService;
    private Poste posteClean;
    private Poste posteCleanA;
    private Poste posteDoublon;
    private Poste posteNomVide;

    private PosteRepository posteRepositoryMock;
    @BeforeMethod
    public void setUp() throws Exception {
        posteRepositoryMock = Mockito.mock(PosteRepository.class);

        posteService = new PosteService(posteRepositoryMock);

        posteClean = new Poste("Tir");
        posteCleanA = new Poste("Secrétaire");

        posteDoublon = new Poste("secrétaire");
        posteNomVide = new Poste("");
    }

    @Test
    public void testSaveNoIssue() throws Exception {
        doAnswer(returnsFirstArg()).when(posteRepositoryMock).save(any(Poste.class));
        posteService.save(posteClean);
        posteService.save(posteCleanA);
    }

    /* RG 30 : Le poste (nom) doit être unique */
    @Test(expectedExceptions = PosteException.class)
    public void testPosteExiste() throws Exception {
        List<Poste> lstPostes = new ArrayList<>();
        doAnswer(returnsFirstArg()).when(posteRepositoryMock).save(any(Poste.class));

        posteService.save(posteClean);
        posteService.save(posteCleanA);
        lstPostes.add(posteClean);
        lstPostes.add(posteCleanA);
        when(posteRepositoryMock.findAll()).thenReturn(lstPostes);
        posteService.save(posteDoublon);
    }

    @Test(expectedExceptions = PosteException.class)
    public void testPosteVide() throws Exception {
        doAnswer(returnsFirstArg()).when(posteRepositoryMock).save(any(Poste.class));

        posteService.save(posteClean);
        posteService.save(posteCleanA);
        posteService.save(posteNomVide);
    }
}
