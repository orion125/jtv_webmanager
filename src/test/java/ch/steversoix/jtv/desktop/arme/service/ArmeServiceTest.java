package ch.steversoix.jtv.desktop.arme.service;

import ch.steversoix.jtv.desktop.arme.exception.ArmeException;
import ch.steversoix.jtv.desktop.arme.model.Arme;
import ch.steversoix.jtv.desktop.arme.model.ArmeRepository;
import ch.steversoix.jtv.desktop.arme.model.TypeArme;
import ch.steversoix.jtv.desktop.arme.model.TypeArmeRepository;
import org.mockito.Mockito;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

public class ArmeServiceTest {
    private ArmeService armeService;
    private TypeArme typeArmeMock;
    private Arme armeClean;
    private Arme armeCleanA;
    private Arme armeDoubleId;
    private Arme armeMinId;
    private Arme armeMaxId;

    private ArmeRepository armeRepositoryMock;
    private TypeArmeRepository typeArmeRepositoryMock;

    @BeforeMethod
    public void setUp() throws Exception {
        armeRepositoryMock = Mockito.mock(ArmeRepository.class);
        typeArmeRepositoryMock = Mockito.mock(TypeArmeRepository.class);

        armeService = new ArmeService(armeRepositoryMock);

        typeArmeMock = new TypeArme("fusil");

        armeClean = new Arme(Long.valueOf("1010"),typeArmeMock,"",true);
        armeCleanA = new Arme(Long.valueOf("1011"),typeArmeMock,"",true);
        armeDoubleId = new Arme(Long.valueOf("1010"),typeArmeMock,"",true);
        armeMinId = new Arme(Long.valueOf("1"),typeArmeMock,"",true);
        armeMaxId = new Arme(Long.valueOf("100000000"),typeArmeMock,"",true);
    }

    @AfterMethod
    public void tearDown() throws Exception {}

    @Test
    public void testSaveNoIssue() throws Exception {
        doAnswer(returnsFirstArg()).when(armeRepositoryMock).save(any(Arme.class));
        armeService.save(armeClean);
        armeService.save(armeCleanA);
    }

    /* RG 09 : Numéro d'arme unique */
    @Test(expectedExceptions = ArmeException.class)
    public void testDoubleId() throws Exception {
        List<Arme> lstArmes = new ArrayList<>();
        doAnswer(returnsFirstArg()).when(armeRepositoryMock).save(any(Arme.class));

        armeService.save(armeClean);
        lstArmes.add(armeClean);
        when(armeRepositoryMock.findAll()).thenReturn(lstArmes);
        armeService.save(armeDoubleId);
    }

    @Test(expectedExceptions = ArmeException.class)
    public void texMinId() throws Exception {
        doAnswer(returnsFirstArg()).when(armeRepositoryMock).save(any(Arme.class));

        armeService.save(armeMinId);
    }

    @Test(expectedExceptions = ArmeException.class)
    public void texMaxId() throws Exception {
        doAnswer(returnsFirstArg()).when(armeRepositoryMock).save(any(Arme.class));

        armeService.save(armeMaxId);
    }

}
