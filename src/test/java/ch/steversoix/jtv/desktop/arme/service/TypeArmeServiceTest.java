package ch.steversoix.jtv.desktop.arme.service;

import ch.steversoix.jtv.desktop.arme.exception.TypeArmeException;
import ch.steversoix.jtv.desktop.arme.model.TypeArme;
import ch.steversoix.jtv.desktop.arme.model.TypeArmeRepository;
import org.mockito.Mockito;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

public class TypeArmeServiceTest {
    private TypeArmeRepository typeArmeRepositoryMock;
    private TypeArmeService typeArmeService;

    private TypeArme typeArmeClean;
    private TypeArme typeArmeCleanA;

    private TypeArme typeArmeDoublon;
    private TypeArme typeArmeVide;

    @BeforeMethod
    public void setUp() throws Exception {
        typeArmeRepositoryMock = Mockito.mock(TypeArmeRepository.class);

        typeArmeClean = new TypeArme("fusil");
        typeArmeCleanA = new TypeArme("pistolet");

        typeArmeDoublon = new TypeArme("Pistolet");
        typeArmeVide = new TypeArme("");

        typeArmeService = new TypeArmeService(typeArmeRepositoryMock);
    }

    @AfterMethod
    public void tearDown() throws Exception {}

    @Test
    public void testSaveNoIssue() throws Exception {
        doAnswer(returnsFirstArg()).when(typeArmeRepositoryMock).save(any(TypeArme.class));
        typeArmeService.save(typeArmeClean);
        typeArmeService.save(typeArmeCleanA);
    }

    /* RG 31 : Le type d'arme (nom )doit être unique */
    @Test(expectedExceptions = TypeArmeException.class)
    public void testPosteExiste() throws Exception {
        List<TypeArme> lstTypesArmes = new ArrayList<>();
        doAnswer(returnsFirstArg()).when(typeArmeRepositoryMock).save(any(TypeArme.class));

        typeArmeService.save(typeArmeClean);
        typeArmeService.save(typeArmeCleanA);
        lstTypesArmes.add(typeArmeClean);
        lstTypesArmes.add(typeArmeCleanA);
        when(typeArmeRepositoryMock.findAll()).thenReturn(lstTypesArmes);
        typeArmeService.save(typeArmeDoublon);
    }

    @Test(expectedExceptions = TypeArmeException.class)
    public void testVide() throws Exception {
        doAnswer(returnsFirstArg()).when(typeArmeRepositoryMock).save(any(TypeArme.class));

        typeArmeService.save(typeArmeVide);
    }
}
