# JTV - WebManager

This is an application for management of a shooting gallery for the Société des Jeunes-Tireurs de Versoix.

Il s'agit d'une application de gestion d'un stand de tir à destination de la Société des Jeunes-Tireurs de Versoix.

## Getting Started / Pour commencer

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

Ces instructions vous permettrons d'obtenir une version du projet fonctionnelle sur votre machine à des buts de développement et tests. Referez-vous aux notes de déploiement pour savoir comment déployer l'application sur un système.

### Prerequisites / Prérequis

 - JDK 8.0
 - MySQL Server

### Installing / Installation

```
git clone https://gitlab.com/grep_jtv/jtv_webmanager.git
```

For starting project, first edit *application.properties* at the root of the project's directory to fill MySQL database credentials. **DO NOT COMMIT THIS FILE.**  
Pour lancer le projet, commencez par éditer le fichier *application.properties* à la racine du projet afin de renseigner vos identifiants MySQL. **NE COMMITEZ PAS CE FICHIER.**

Then start the App / Puis lancez-l'application
```
mvnw spring-boot:run
```
or build and package it then use the jar / ou compilez-là et utilisez le jar

```
mvnw clean package
java -jar target/jtv_web-3.0-SNAPSHOT.jar
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

